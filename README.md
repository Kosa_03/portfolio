[PL] Portfolio
--------------
**1. O projekcie**

Projekt ma na celu przedstawienie moich umiejętności front i backendowych. 
Do zbudowania mojego portfolio wykorzystałem Symfony 3.4 wraz z innymi 
technologiami jak Twig, Bootstrap, CSS3, HTML5, itd.

Portfolio będzie dwujęzyczną stroną internetową. Posiadać będzie
systemem użytkowników oraz system zarządzania treścią. Między innymi 
administracja newsami.

**2. Instalacja**

* Pobierz repozytorium.
* Uruchom polecenie `composer update` lub dla środowiska produkcyjnego 
  `composer update --no-dev`.
* Nadaj odpowienie uprawnienia katalogom: `var/cache`, `var/logs`, `var/sessions`.
 [Wiecej informacji](https://symfony.com/doc/3.4/setup/file_permissions.html).


[EN] Portfolio
--------------
**1. About project**

Project presents my front and backend skills. To build this website 
I used Symfony 3.4 with other technologies like Twig, Bootstrap, CSS3, HTML5, etc.

Portfolio will be multilingual website. It will be had users system and content 
management system like news administration and others.

**2. Installation**

* Download repository.
* Run command `composer update` or for production environment run
  `composer update --no-dev`.
* Setup file permission for: `var/cache`, `var/logs`, `var/sessions`.
  [More information](https://symfony.com/doc/3.4/setup/file_permissions.html).