<?php

namespace AppBundle\Validator\Constraints;

use AppBundle\Entity\PageContent\Template\FieldTemplate;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;


/**
 * Class FieldTemplateExistValidator
 */
class FieldTemplateExistValidator extends ConstraintValidator
{
    private $entityManager;

    /**
     * FieldTemplateExistValidator constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!($constraint instanceof FieldTemplateExist)) {
            throw new UnexpectedTypeException($constraint, FieldTemplateExist::class);
        }

        if ($value === null || $value === '') {
            return;
        }

        $fieldTemplate = $this->entityManager->getRepository(FieldTemplate::class)
            ->find($value);

        if (is_null($fieldTemplate)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ id }}', $value)
                ->addViolation();
        }
    }
}
