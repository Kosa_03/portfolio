<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;


/**
 * @Annotation
 * Class FieldTemplateExist
 */
class FieldTemplateExist extends Constraint
{
    public $message;
}
