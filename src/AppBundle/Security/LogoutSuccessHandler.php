<?php

namespace AppBundle\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Security\Http\HttpUtils;
use Symfony\Component\Security\Http\Logout\DefaultLogoutSuccessHandler;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;


class LogoutSuccessHandler extends DefaultLogoutSuccessHandler implements LogoutSuccessHandlerInterface
{
    private $flashBag;

    /**
     * LogoutSuccessHandler constructor.
     *
     * @param HttpUtils $httpUtils
     * @param string $targetUrl
     * @param FlashBagInterface $flashBag
     */
    public function __construct(HttpUtils $httpUtils, $targetUrl = '/', FlashBagInterface $flashBag)
    {
        parent::__construct($httpUtils, $targetUrl);

        $this->flashBag = $flashBag;
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function onLogoutSuccess(Request $request)
    {
        $this->flashBag->add('success', 'You are successfully logged out.');

        return parent::onLogoutSuccess($request);
    }
}
