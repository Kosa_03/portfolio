<?php

namespace AppBundle\Security;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationSuccessHandler;
use Symfony\Component\Security\Http\HttpUtils;


class AuthenticationSuccessHandler extends DefaultAuthenticationSuccessHandler implements AuthenticationSuccessHandlerInterface
{
    private $flashBag;
    private $entityManager;

    /**
     * AuthenticationSuccessHandler constructor.
     *
     * @param HttpUtils $httpUtils
     * @param array $options
     * @param FlashBagInterface $flashBag
     * @param EntityManager $entityManager
     */
    public function __construct(HttpUtils $httpUtils, array $options = array(), FlashBagInterface $flashBag, EntityManager $entityManager)
    {
        parent::__construct($httpUtils, $options);

        $this->flashBag = $flashBag;
        $this->entityManager = $entityManager;
    }

    /**
     * @param Request $request
     * @param TokenInterface $token
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $user = $token->getUser();

        $lastLoginTime = $user->getLoginTime();

        $user->setLoginTime(new \DateTime());
        $user->setLastLoginTime($lastLoginTime);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $this->flashBag->add('success', 'You are successfully logged in!');

        return parent::onAuthenticationSuccess($request, $token);
    }
}
