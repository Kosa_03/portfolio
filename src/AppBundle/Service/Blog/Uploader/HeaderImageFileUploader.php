<?php

namespace AppBundle\Service\Blog\Uploader;

use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;


/**
 * Class HeaderImageFileUploader
 */
class HeaderImageFileUploader implements FileUploaderInterface
{
    private $fileSystem;
    private $targetDirectory = 'images/blog_article/';
    private $imageFileName;

    /**
     * HeaderImageFileUploader constructor.
     * @param Filesystem $fileSystem
     */
    public function __construct(Filesystem $fileSystem)
    {
        $this->fileSystem = $fileSystem;
    }

    /**
     * @param UploadedFile $file
     * @return bool
     */
    public function upload(UploadedFile $file) : bool
    {
        $originImageFileName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeImageFileName = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9\-_] remove; Lower()', $originImageFileName);
        $imageFileName = $safeImageFileName . '-' . uniqid() . '.' . $file->guessExtension();

        try {
            $file->move($this->targetDirectory, $imageFileName);
            $this->imageFileName = $imageFileName;
            return true;

        } catch (FileException $e) {
            return false;
        }
    }

    /**
     * @param string $imageFileName
     * @return bool
     */
    public function remove(string $imageFileName) : bool
    {
        if ($imageFileName == 'default-bg-landscape.jpg') {
            return false;
        }

        $oldImageFileName = $this->targetDirectory . $imageFileName;

        if (file_exists($oldImageFileName)) {
            try {
                $this->fileSystem->remove($oldImageFileName);
                return true;

            } catch (IOException $e) {
                return false;
            }
        }

        return false;
    }

    /**
     * @return null|string
     */
    public function getFileName() : ?string
    {
        return $this->imageFileName;
    }

    /**
     * @return null|string
     */
    public function getFileDirectory() : ?string
    {
        return $this->targetDirectory;
    }
}
