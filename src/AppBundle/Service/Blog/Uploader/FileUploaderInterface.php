<?php

namespace AppBundle\Service\Blog\Uploader;

use Symfony\Component\HttpFoundation\File\UploadedFile;


/**
 * Interface FileUploaderInterface
 */
interface FileUploaderInterface
{
    public function upload(UploadedFile $file) : bool;
    public function remove(string $fileName) : bool;
    public function getFileName() : ?string;
    public function getFileDirectory() : ?string;
}
