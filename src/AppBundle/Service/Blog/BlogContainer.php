<?php

namespace AppBundle\Service\Blog;

use Doctrine\Common\Collections\ArrayCollection;


/**
 * Class BlogContainer
 */
class BlogContainer
{
    private $collections = [];
    private $commandResults = [];

    /**
     * @param string $collectionName
     * @return bool
     */
    public function addCollection(string $collectionName) : bool
    {
        if (!key_exists($collectionName, $this->collections)) {
            $this->collections[$collectionName] = new ArrayCollection();

            return true;
        }

        return false;
    }

    /**
     * @param string $collectionName
     * @return bool
     */
    public function removeCollection(string $collectionName) : bool
    {
        unset($this->collections[$collectionName]);

        return true;
    }

    /**
     * @param string $collectionName
     * @return bool
     */
    public function existsCollection(string $collectionName) : bool
    {
        if (key_exists($collectionName, $this->collections)) {
            return true;
        }

        return false;
    }

    /**
     * @param string $collectionName
     * @return ArrayCollection
     */
    public function getCollection(string $collectionName) : ArrayCollection
    {
        if (key_exists($collectionName, $this->collections)) {
            return $this->collections[$collectionName];
        }

        return new ArrayCollection();
    }

    /**
     * @param string $resultName
     * @param $result
     */
    public function addCommandResult(string $resultName, $result) : void
    {
        $this->commandResults[$resultName] = $result;
    }

    /**
     * @param string $resultName
     * @return mixed|null
     */
    public function getCommandResult(string $resultName)
    {
        if (array_key_exists($resultName, $this->commandResults)) {
            return $this->commandResults[$resultName];
        }

        return null;
    }
}
