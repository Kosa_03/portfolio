<?php

namespace AppBundle\Service\Blog;

use AppBundle\Service\Blog\Command\CommandInterface;
use AppBundle\Service\Blog\DataMapper\DataMapperInterface;
use AppBundle\Service\Blog\Factory\BlogFactoryManager;
use AppBundle\Service\Blog\Options\BlogFindOptions;
use AppBundle\Service\Blog\Uploader\FileUploaderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;


/**
 * Class BlogManager
 */
class BlogManager
{
    /** @var CommandInterface */
    private $command;

    private $blogFindOptions;

    private $blogFactoryManager;
    private $blogContainer;

    private $serviceContainer;

    /**
     * BlogManager constructor.
     * @param BlogFindOptions $blogFindOptions
     * @param BlogFactoryManager $blogFactoryManager
     * @param BlogContainer $blogContainer
     * @param ContainerInterface $serviceContainer
     */
    public function __construct
    (
        BlogFindOptions $blogFindOptions,
        BlogFactoryManager $blogFactoryManager,
        BlogContainer $blogContainer,
        ContainerInterface $serviceContainer
    )
    {
        $this->blogFindOptions = $blogFindOptions;
        $this->blogFactoryManager = $blogFactoryManager;
        $this->blogContainer = $blogContainer;

        $this->serviceContainer = $serviceContainer;
    }

    /**
     * @param CommandInterface $command
     */
    public function setCommand(CommandInterface $command) : void
    {
        $this->command = $command;
    }

    /**
     * @param string $resultName
     * @return mixed|null
     */
    public function getCmdResult(string $resultName)
    {
        return $this->blogContainer->getCommandResult($resultName);
    }

    public function run() : void
    {
        if ($this->command instanceof CommandInterface) {
            $this->command->execute();

        } else {
            throw new \RuntimeException('Incorrect command in blog service for run.');
        }
    }

    /**
     * @return BlogFindOptions
     */
    public function getFindOptions() : BlogFindOptions
    {
        return $this->blogFindOptions;
    }

    /**
     * @return BlogFactoryManager
     */
    public function getFactory() : BlogFactoryManager
    {
        return $this->blogFactoryManager;
    }

    /**
     * @param string $dataMapper
     * @return DataMapperInterface|null
     */
    public function getDataMapper(string $dataMapper) : ?DataMapperInterface
    {
        try {
            $dataMapper = $this->serviceContainer->get($dataMapper);

            if ($dataMapper instanceof DataMapperInterface) {
                return $dataMapper;
            }

            return null;

        } catch (ServiceNotFoundException $e) {
            return null;
        }
    }

    /**
     * @param string $fileUploader
     * @return FileUploaderInterface|null
     */
    public function getFileUploader(string $fileUploader) : ?FileUploaderInterface
    {
        try {
            $fileUploader = $this->serviceContainer->get($fileUploader);

            if ($fileUploader instanceof FileUploaderInterface) {
                return $fileUploader;
            }

            return null;

        } catch (ServiceNotFoundException $e) {
            return null;
        }
    }

    /**
     * @return BlogContainer
     */
    public function getContainer() : BlogContainer
    {
        return $this->blogContainer;
    }

    /**
     * @param string $collectionName
     * @return array
     */
    public function getArrayCollection(string $collectionName) : array
    {
        return $this->blogContainer->getCollection($collectionName)->getValues();
    }
}
