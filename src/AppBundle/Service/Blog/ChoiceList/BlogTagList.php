<?php

namespace AppBundle\Service\Blog\ChoiceList;

use AppBundle\Entity\Blog\Tag\BlogTag;
use AppBundle\Service\Blog\Command\CreateBlogTagCmd;
use Doctrine\ORM\EntityManagerInterface;


/**
 * Class BlogTagList
 */
class BlogTagList
{
    private $entityManager;
    private $createBlogTagCmd;

    /**
     * BlogTagList constructor.
     * @param EntityManagerInterface $entityManager
     * @param CreateBlogTagCmd $createBlogTagCmd
     */
    public function __construct(EntityManagerInterface $entityManager, CreateBlogTagCmd $createBlogTagCmd)
    {
        $this->entityManager = $entityManager;
        $this->createBlogTagCmd = $createBlogTagCmd;
    }

    /**
     * @return array
     */
    public function getList() : array
    {
        return $this->findTagList();
    }

    /**
     * @return array
     */
    private function findTagList() : array
    {
        $blogTagList = $this->entityManager->getRepository(BlogTag::class)
            ->findBy([], ['weight' => 'ASC']);

        foreach ($blogTagList as $blogTag) {
            $this->createBlogTagCmd->setBlogTag($blogTag);
            $this->createBlogTagCmd->execute();
        }

        return $blogTagList;
    }
}
