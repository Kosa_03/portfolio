<?php

namespace AppBundle\Service\Blog\DataMapper;

use AppBundle\Entity\Blog\Tag\BlogTag;
use AppBundle\FormDTO\Blog\BlogTagListDTO;
use AppBundle\FormDTO\Blog\CollectionType\BlogTagDTO;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Class BlogTagListMapper
 */
class BlogTagListMapper implements DataMapperInterface
{
    /**
     * @param ArrayCollection $blogTagList
     * @param BlogTagListDTO $blogTagListDTO
     */
    public function mapBlogTagListToDTO(ArrayCollection $blogTagList, BlogTagListDTO $blogTagListDTO) : void
    {
        foreach ($blogTagList as $blogTag) {
            $blogTagDTO = new BlogTagDTO();

            $blogTagDTO->setTagId($blogTag->getTagId());
            $blogTagDTO->setName($blogTag->getName());
            $blogTagDTO->setWeight($blogTag->getWeight());
            $blogTagDTO->setTranslations($blogTag->getTranslations());

            $blogTagListDTO->getTags()->add($blogTagDTO);
        }
    }

    /**
     * @param ArrayCollection $blogTagList
     * @param BlogTagListDTO $blogTagListDTO
     */
    public function mapDTOToBlogTagList(ArrayCollection $blogTagList, BlogTagListDTO $blogTagListDTO) : void
    {
        $blogTagList->first();

        foreach ($blogTagListDTO->getTags() as $blogTagDTO) {
            if ($blogTagList->current() == false) {
                $blogTagList->add(new BlogTag());
            }

            $blogTagList->current()->setWeight($blogTagDTO->getWeight());
            $blogTagList->current()->setTranslations($blogTagDTO->getTranslations());

            $blogTagList->next();
        }
    }
}
