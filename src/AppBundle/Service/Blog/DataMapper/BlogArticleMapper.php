<?php

namespace AppBundle\Service\Blog\DataMapper;

use AppBundle\Entity\Blog\BlogArticle;
use AppBundle\FormDTO\Blog\BlogArticleDTO;
use AppBundle\Service\Blog\Factory\BlogArticleDTOFactory;
use AppBundle\Service\Blog\Factory\BlogArticleFactory;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Class BlogArticleMapper
 */
class BlogArticleMapper implements DataMapperInterface
{
    private $blogArticleFactory;
    private $blogArticleDTOFactory;

    /**
     * BlogArticleMapper constructor.
     * @param BlogArticleFactory $blogArticleFactory
     * @param BlogArticleDTOFactory $blogArticleDTOFactory
     */
    public function __construct(BlogArticleFactory $blogArticleFactory, BlogArticleDTOFactory $blogArticleDTOFactory)
    {
        $this->blogArticleFactory = $blogArticleFactory;
        $this->blogArticleDTOFactory = $blogArticleDTOFactory;
    }

    /**
     * @param BlogArticle $blogArticle
     * @param BlogArticleDTO $blogArticleDTO
     */
    public function mapDTOToBlogArticle(BlogArticle $blogArticle, BlogArticleDTO $blogArticleDTO) : void
    {
        if ($blogArticleDTO->getStatus() == 'published') {
            $blogArticle->setPublished();
        } else {
            $blogArticle->setNotPublished();
        }

        $blogTagsCollection = new ArrayCollection();
        foreach ($blogArticleDTO->getTags() as $blogTag) {
            $blogTagsCollection->add($blogTag);
        }

        $blogArticle->setTags($blogTagsCollection);
        $blogArticle->setHeaderImageFile($blogArticleDTO->getHeaderImageFile());
    }

    /**
     * @param BlogArticle $blogArticle
     * @param BlogArticleDTO $blogArticleDTO
     */
    public function mapBlogArticleToDTO(BlogArticle $blogArticle, BlogArticleDTO $blogArticleDTO) : void
    {
        if ($blogArticle->getStatus() == 'published') {
            $blogArticleDTO->setStatus('published');
        } else {
            $blogArticleDTO->setStatus('not_published');
        }

        $blogTagsArray = [];
        foreach ($blogArticle->getTags() as $blogTag) {
            $blogTagsArray[] = $blogTag;
        }

        $blogArticleDTO->setTags($blogTagsArray);
        $blogArticleDTO->setHeaderImageFile($blogArticle->getHeaderImageFile());
    }

    /**
     * @param BlogArticle $blogArticle
     * @param BlogArticleDTO $blogArticleDTO
     */
    public function mapDTOToBlogTranslations(BlogArticle $blogArticle, BlogArticleDTO $blogArticleDTO) : void
    {
        $blogTranslations = $blogArticle->getTranslations();
        $blogTranslationsDTO = $blogArticleDTO->getTranslations();

        if ($blogTranslationsDTO->isEmpty()) {
            return;
        }

        $blogTranslations->first();

        foreach ($blogTranslationsDTO as $blogTranslationDTO) {
            if ($blogTranslations->current() == false) {
                $this->blogArticleFactory->addEmptyBlogTranslation($blogArticle);
            }

            $blogTranslations->current()->setLanguage($blogTranslationDTO->getLanguage());
            $blogTranslations->current()->setSubject($blogTranslationDTO->getSubject());
            $blogTranslations->current()->setContent($blogTranslationDTO->getContent());

            $blogTranslations->next();
        }
    }

    /**
     * @param BlogArticle $blogArticle
     * @param BlogArticleDTO $blogArticleDTO
     */
    public function mapBlogTranslationsToDTO(BlogArticle $blogArticle, BlogArticleDTO $blogArticleDTO) : void
    {
        $blogTranslations = $blogArticle->getTranslations();
        $blogTranslationsDTO = $blogArticleDTO->getTranslations();

        if ($blogTranslations->isEmpty()) {
            return;
        }

        $blogTranslationsDTO->first();

        foreach ($blogTranslations as $blogTranslation) {
            if ($blogTranslationsDTO->current() == false) {
                $this->blogArticleDTOFactory->addEmptyBlogTranslation($blogArticleDTO);
            }

            $blogTranslationsDTO->current()->setTranslationId($blogTranslation->getId());
            $blogTranslationsDTO->current()->setLanguage($blogTranslation->getLanguage());
            $blogTranslationsDTO->current()->setSubject($blogTranslation->getSubject());
            $blogTranslationsDTO->current()->setContent($blogTranslation->getContent());

            $blogTranslationsDTO->next();
        }
    }
}
