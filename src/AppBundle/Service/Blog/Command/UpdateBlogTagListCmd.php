<?php

namespace AppBundle\Service\Blog\Command;

use AppBundle\Service\Blog\BlogManager;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;


/**
 * Class UpdateBlogTagListCmd
 */
class UpdateBlogTagListCmd implements CommandInterface
{
    private $entityManager;
    private $blogManager;
    private $blogContainer;

    private $blogTagList;

    /**
     * UpdateBlogTagListCmd constructor.
     * @param EntityManagerInterface $entityManager
     * @param BlogManager $blogManager
     */
    public function __construct(EntityManagerInterface $entityManager, BlogManager $blogManager)
    {
        $this->entityManager = $entityManager;
        $this->blogManager = $blogManager;
        $this->blogContainer = $blogManager->getContainer();
    }

    /**
     * @param ArrayCollection $blogTagList
     */
    public function setBlogTagList(ArrayCollection $blogTagList) : void
    {
        $this->blogTagList = $blogTagList;
    }

    public function execute() : void
    {
        $this->updateBlogTags();
    }

    private function updateBlogTags() : void
    {
        try {
            $this->entityManager->getConnection()->beginTransaction();

            foreach ($this->blogTagList as $blogTag) {
                if (!$this->entityManager->contains($blogTag)) {
                    $this->entityManager->persist($blogTag);
                }
            }

            $this->entityManager->flush();
            $this->entityManager->getConnection()->commit();
            $this->blogContainer->addCommandResult('updateError', false);

        } catch (ORMException $e) {
            $this->entityManager->getConnection()->rollBack();
            $this->blogContainer->addCommandResult('updateError', true);
        }
    }
}
