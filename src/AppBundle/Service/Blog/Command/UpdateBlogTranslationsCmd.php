<?php

namespace AppBundle\Service\Blog\Command;

use AppBundle\Entity\Blog\BlogArticle;
use AppBundle\Service\Blog\BlogContainer;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;


/**
 * Class UpdateBlogTranslationsCmd
 */
class UpdateBlogTranslationsCmd implements CommandInterface
{
    private $entityManager;
    private $blogContainer;

    /** @var BlogArticle */
    private $blogArticle;

    /**
     * UpdateBlogTranslationsCmd constructor.
     * @param EntityManagerInterface $entityManager
     * @param BlogContainer $blogContainer
     */
    public function __construct(EntityManagerInterface $entityManager, BlogContainer $blogContainer)
    {
        $this->entityManager = $entityManager;
        $this->blogContainer = $blogContainer;
    }

    /**
     * @param BlogArticle $blogArticle
     */
    public function setBlogArticle(BlogArticle $blogArticle) : void
    {
        $this->blogArticle = $blogArticle;
    }

    public function  execute() : void
    {
        $this->update();
    }

    private function update() : void
    {
        try {
            $this->entityManager->getConnection()->beginTransaction();

            foreach ($this->blogArticle->getTranslations() as $blogTranslation) {
                if (!$this->entityManager->contains($blogTranslation)) {
                    $blogTranslation->setArticleId($this->blogArticle->getId());
                    $this->entityManager->persist($blogTranslation);
                }
            }

            $this->entityManager->flush();
            $this->entityManager->getConnection()->commit();
            $this->blogContainer->addCommandResult('updateTranslationsError', false);

        } catch (ORMException $e) {
            $this->entityManager->getConnection()->rollBack();
            $this->blogContainer->addCommandResult('updateTranslationsError', true);
        }
    }
}
