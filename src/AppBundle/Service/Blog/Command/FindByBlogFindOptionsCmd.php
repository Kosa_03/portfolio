<?php

namespace AppBundle\Service\Blog\Command;

use AppBundle\Entity\Blog\BlogArticle;
use AppBundle\Service\Blog\BlogContainer;
use AppBundle\Service\Blog\Options\BlogFindOptions;
use Doctrine\ORM\EntityManagerInterface;


/**
 * Class FindByBlogFindOptionsCmd
 */
class FindByBlogFindOptionsCmd implements CommandInterface
{
    private $entityManager;

    private $blogFindOptions;
    private $blogContainer;

    private $foundedBlogArticles;

    /**
     * FindByBlogFindOptionsCmd constructor.
     * @param EntityManagerInterface $entityManager
     * @param BlogFindOptions $findOptions
     * @param BlogContainer $container
     */
    public function __construct(EntityManagerInterface $entityManager, BlogFindOptions $findOptions, BlogContainer $container)
    {
        $this->entityManager = $entityManager;

        $this->blogFindOptions = $findOptions;
        $this->blogContainer = $container;
    }

    public function execute() : void
    {
        $this->findBlogArticles();

        $this->blogContainer->addCommandResult('foundedBlogArticles', $this->foundedBlogArticles);
    }

    private function findBlogArticles() : void
    {
        $this->foundedBlogArticles = $this->entityManager->getRepository(BlogArticle::class)
            ->findByBlogFindOptions($this->blogFindOptions);
    }
}
