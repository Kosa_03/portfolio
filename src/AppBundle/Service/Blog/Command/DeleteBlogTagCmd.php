<?php

namespace AppBundle\Service\Blog\Command;

use AppBundle\Entity\Blog\Tag\BlogTag;
use AppBundle\Entity\Blog\CrossReference\BlogArticleTagsXRef;
use AppBundle\Service\Blog\BlogManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


/**
 * Class DeleteBlogTagCmd
 */
class DeleteBlogTagCmd implements CommandInterface
{
    private $session;
    private $entityManager;
    private $blogManager;
    private $blogContainer;

    /** @var BlogTag */
    private $blogTag;

    /**
     * DeleteBlogTagCmd constructor.
     * @param SessionInterface $session
     * @param EntityManagerInterface $entityManager
     * @param BlogManager $blogManager
     */
    public function __construct
    (
        SessionInterface $session,
        EntityManagerInterface $entityManager,
        BlogManager $blogManager
    )
    {
        $this->session = $session;
        $this->entityManager = $entityManager;
        $this->blogManager = $blogManager;
        $this->blogContainer = $blogManager->getContainer();
    }

    /**
     * @param BlogTag $blogTag
     */
    public function setBlogTag(BlogTag $blogTag) : void
    {
        $this->blogTag = $blogTag;
    }

    public function execute(): void
    {
        $this->deleteBlogTagFromSession();
        $this->deleteBlogTag();
    }

    private function deleteBlogTag() : void
    {
        try {
            $this->entityManager->getConnection()->beginTransaction();

            $arrayBlogTagData = $this->entityManager->getRepository(BlogArticleTagsXRef::class)
                ->findBy(['tagId' => $this->blogTag->getTagId()]);

            foreach ($arrayBlogTagData as $blogTagData) {
                $this->entityManager->remove($blogTagData);
                $this->entityManager->flush();
            }

            $this->entityManager->remove($this->blogTag);
            $this->entityManager->flush();

            $this->entityManager->getConnection()->commit();
            $this->blogContainer->addCommandResult('deleteError', false);

        } catch (ORMException $e) {
            $this->entityManager->getConnection()->rollBack();
            $this->blogContainer->addCommandResult('deleteError', true);
        }
    }

    private function deleteBlogTagFromSession() : void
    {
        $blogTagListDTOFactory = $this->blogManager->getFactory()->getBlogTagListDTOFactory();
        $blogTagListDTO = $blogTagListDTOFactory->createBlogTagList();

        $serializedBlogTagListDTO = $this->session->get('admin_blog_tag_list/blogTagListDTO');
        if (!is_null($serializedBlogTagListDTO)) {
            $blogTagListDTO->unserialize($serializedBlogTagListDTO);

            $blogTagListDTO->getTags()->first();
            foreach ($blogTagListDTO->getTags() as $blogTagDTO) {

                if (is_null($blogTagDTO->getTagId())) {
                    continue;
                }

                if ($blogTagDTO->getTagId() == $this->blogTag->getTagId()) {
                    $key = $blogTagListDTO->getTags()->key();
                    $blogTagListDTOFactory->deleteBlogTag($blogTagListDTO, $key);
                    break;
                }

                $blogTagListDTO->getTags()->next();
            }

            $this->session->set('admin_blog_tag_list/blogTagListDTO', $blogTagListDTO->serialize());
        }
    }
}
