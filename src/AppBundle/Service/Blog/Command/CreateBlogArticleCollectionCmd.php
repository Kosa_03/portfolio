<?php

namespace AppBundle\Service\Blog\Command;

use AppBundle\Service\Blog\BlogContainer;
use AppBundle\Service\Blog\Options\BlogFindOptions;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Class CreateBlogArticleCollectionCmd
 */
class CreateBlogArticleCollectionCmd implements CommandInterface
{
    private $blogFindOptions;
    private $blogContainer;
    private $collectionName;
    private $buildBlogArticle;
    private $blogCollection;

    private $findByBlogFindOptionsCmd;
    private $createBlogArticleCmd;

    /**
     * CreateBlogArticleCollectionCmd constructor.
     * @param BlogFindOptions $findOptions
     * @param BlogContainer $container
     * @param FindByBlogFindOptionsCmd $findByBlogFindOptionsCmd
     * @param CreateBlogArticleCmd $createBlogArticleCmd
     */
    public function __construct
    (
        BlogFindOptions $findOptions,
        BlogContainer $container,
        FindByBlogFindOptionsCmd $findByBlogFindOptionsCmd,
        CreateBlogArticleCmd $createBlogArticleCmd
    )
    {
        $this->blogFindOptions = $findOptions;
        $this->blogContainer = $container;

        $this->findByBlogFindOptionsCmd = $findByBlogFindOptionsCmd;
        $this->createBlogArticleCmd = $createBlogArticleCmd;

        $this->buildBlogArticle = true;
        $this->blogCollection = new ArrayCollection();
    }

    /**
     * @param string $collectionName
     */
    public function setCollectionName(string $collectionName = 'default') : void
    {
        $this->collectionName = $collectionName;
    }

    /**
     * @param bool $buildBlogArticle
     */
    public function buildBlogArticle(bool $buildBlogArticle = true) : void
    {
        $this->buildBlogArticle = $buildBlogArticle;
    }

    public function execute() : void
    {
        if (!$this->blogContainer->addCollection($this->collectionName)) {
            throw new \InvalidArgumentException(get_class() . ': Collection ' . $this->collectionName . ' exists');
        }

        $this->blogCollection = $this->blogContainer->getCollection($this->collectionName);

        $this->createCollection();
    }

    private function createCollection() : void
    {
        $this->findByBlogFindOptionsCmd->execute();

        $blogArticles = $this->blogContainer->getCommandResult('foundedBlogArticles');

        foreach ($blogArticles as $blogArticle) {
            if ($this->buildBlogArticle) {
                $this->createBlogArticleCmd->setBlogArticle($blogArticle);
                $this->createBlogArticleCmd->execute();
            }

            $this->blogCollection->add($blogArticle);
        }
    }
}
