<?php

namespace AppBundle\Service\Blog\Command;

use AppBundle\Entity\Blog\BlogArticle;
use AppBundle\Service\Blog\BlogContainer;
use AppBundle\Service\Blog\Options\BlogFindOptions;
use Doctrine\ORM\EntityManagerInterface;


/**
 * Class CountByBlogFindOptionsCmd
 */
class CountByBlogFindOptionsCmd implements CommandInterface
{
    private $entityManager;

    private $blogFindOptions;
    private $blogContainer;

    /**
     * CountByBlogFindOptionsCmd constructor.
     * @param EntityManagerInterface $entityManager
     * @param BlogFindOptions $findOptions
     * @param BlogContainer $container
     */
    public function __construct(EntityManagerInterface $entityManager, BlogFindOptions $findOptions, BlogContainer $container)
    {
        $this->entityManager = $entityManager;
        $this->blogFindOptions = $findOptions;
        $this->blogContainer = $container;
    }

    public function execute() : void
    {
        $amountOfBlogArticles = $this->countBlogArticles();

        $this->blogContainer->addCommandResult('amountOfBlogArticles', $amountOfBlogArticles);
    }

    /**
     * @return int
     */
    private function countBlogArticles() : int
    {
        return $this->entityManager->getRepository(BlogArticle::class)
            ->countByBlogFindOptions($this->blogFindOptions);
    }
}
