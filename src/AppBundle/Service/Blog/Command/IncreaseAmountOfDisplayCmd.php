<?php

namespace AppBundle\Service\Blog\Command;

use AppBundle\Entity\Blog\BlogArticle;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;


/**
 * Class IncreaseAmountOfDisplayCmd
 */
class IncreaseAmountOfDisplayCmd implements CommandInterface
{
    private $entityManager;

    /** @var BlogArticle */
    private $blogArticle;

    /**
     * IncreaseAmountOfDisplayCmd constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param BlogArticle $blogArticle
     */
    public function setBlogArticle(BlogArticle $blogArticle) : void
    {
        $this->blogArticle = $blogArticle;
    }

    public function execute() : void
    {
        $this->updateAmountOfDisplay();
    }

    private function updateAmountOfDisplay() : void
    {
        try {
            $amountOfDisplay = $this->blogArticle->getAmountOfDisplay() + 1;
            $this->blogArticle->setAmountOfDisplay($amountOfDisplay);

            $this->entityManager->flush();

        } catch (ORMException $exception) {
            // TODO: Implements PSR Logger Notice information ??
        }
    }
}
