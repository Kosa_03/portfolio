<?php

namespace AppBundle\Service\Blog\Command;


/**
 * Interface CommandInterface
 */
interface CommandInterface
{
    public function execute() : void;
}
