<?php

namespace AppBundle\Service\Blog\Command;

use AppBundle\Entity\Blog\BlogArticle;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;


/**
 * Class BlogArticleVoteNeutralCmd
 */
class BlogArticleVoteNeutralCmd implements CommandInterface
{
    private $entityManager;

    /** @var BlogArticle */
    private $blogArticle;

    /**
     * BlogArticleVoteNeutralCmd constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param BlogArticle $blogArticle
     */
    public function setBlogArticle(BlogArticle $blogArticle) : void
    {
        $this->blogArticle = $blogArticle;
    }

    public function execute() : void
    {
        $this->updateAmountOfNeutral();
    }

    private function updateAmountOfNeutral() : void
    {
        try {
            $amountOfNeutral = $this->blogArticle->getAmountOfNeutral() + 1;
            $this->blogArticle->setAmountOfNeutral($amountOfNeutral);

            $this->entityManager->flush();

        } catch (ORMException $exception) {
            // TODO: Implements PSR Logger Notice information ??
        }
    }
}
