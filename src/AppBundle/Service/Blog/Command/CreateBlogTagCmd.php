<?php

namespace AppBundle\Service\Blog\Command;

use AppBundle\Entity\Blog\Tag\BlogTag;
use Symfony\Component\HttpFoundation\RequestStack;


/**
 * Class CreateBlogTagCmd
 */
class CreateBlogTagCmd implements CommandInterface
{
    /** @var BlogTag */
    private $blogTag;

    private $locale;
    private $defaultLocale;

    /**
     * CreateTagCmd constructor.
     * @param RequestStack $request
     */
    public function __construct(RequestStack $request)
    {
        $this->locale = $request->getCurrentRequest()->getLocale();
        $this->defaultLocale = 'und'; //$request->getCurrentRequest()->getDefaultLocale();
    }

    /**
     * @param BlogTag $blogTag
     */
    public function setBlogTag(BlogTag $blogTag) : void
    {
        $this->blogTag = $blogTag;
    }

    public function execute() : void
    {
        $this->selectBlogTagTranslation();
    }

    private function selectBlogTagTranslation() : void
    {
        $blogTagTranslations = $this->blogTag->getTranslations();

        if (key_exists($this->locale, $blogTagTranslations)) {
            $this->blogTag->setName($blogTagTranslations[$this->locale]);
            return;
        }

        if (key_exists($this->defaultLocale, $blogTagTranslations)) {
            $this->blogTag->setName($blogTagTranslations[$this->defaultLocale]);
            return;
        }

        $this->blogTag->setName('BlogTagName');
    }
}
