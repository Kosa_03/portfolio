<?php

namespace AppBundle\Service\Blog\Command;

use AppBundle\Entity\Blog\BlogArticle;
use AppBundle\Service\Blog\BlogManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


/**
 * Class DeleteBlogTranslationCmd
 */
class DeleteBlogTranslationCmd implements CommandInterface
{
    private $session;
    private $entityManager;
    private $blogManager;
    private $blogContainer;

    /** @var  BlogArticle */
    private $blogArticle;
    private $translationId;

    /**
     * DeleteBlogTranslationCmd constructor.
     * @param SessionInterface $session
     * @param EntityManagerInterface $entityManager
     * @param BlogManager $blogManager
     */
    public function __construct(SessionInterface $session, EntityManagerInterface $entityManager, BlogManager $blogManager)
    {
        $this->session = $session;
        $this->entityManager = $entityManager;
        $this->blogManager = $blogManager;
        $this->blogContainer = $blogManager->getContainer();
    }

    /**
     * @param BlogArticle $blogArticle
     * @param int $translationId
     */
    public function setBlogArticle(BlogArticle $blogArticle, int $translationId) : void
    {
        $this->blogArticle = $blogArticle;
        $this->translationId = $translationId;
    }

    public function execute() : void
    {
        $this->delete();
    }

    private function delete() : void
    {
        try {
            $this->entityManager->getConnection()->beginTransaction();

            foreach ($this->blogArticle->getTranslations() as $blogTranslation) {
                if ($blogTranslation->getId() == $this->translationId) {
                    $this->entityManager->remove($blogTranslation);
                    $this->entityManager->flush();
                }
            }

            $this->entityManager->getConnection()->commit();

            $this->deleteFromSession($this->blogArticle->getId());

            $this->blogContainer->addCommandResult('deleteError', false);

        } catch (ORMException $e) {
            $this->entityManager->getConnection()->rollBack();
            $this->blogContainer->addCommandResult('deleteError', true);
        }
    }

    /**
     * @param int $articleId
     */
    private function deleteFromSession(int $articleId) : void
    {
        $blogArticleDTOFactory = $this->blogManager->getFactory()->getBlogArticleDTOFactory();
        $blogArticleDTO = $blogArticleDTOFactory->createBlogArticle();

        $serializedBlogArticleDTO = $this->session->get('admin_blog_article_edit/' . $articleId . '/blogArticleDTO');
        if (!is_null($serializedBlogArticleDTO)) {
            $blogArticleDTO->unserialize($serializedBlogArticleDTO);
        }

        foreach ($blogArticleDTO->getTranslations() as $blogTranslationDTO) {
            if ($blogTranslationDTO->getTranslationId() == $this->translationId) {
                $blogArticleDTO->getTranslations()->removeElement($blogTranslationDTO);
            }
        }

        $this->session->set('admin_blog_article_edit/' . $articleId . '/blogArticleDTO', $blogArticleDTO->serialize());
    }
}
