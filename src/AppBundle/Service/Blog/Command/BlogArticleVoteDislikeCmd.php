<?php

namespace AppBundle\Service\Blog\Command;

use AppBundle\Entity\Blog\BlogArticle;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;


/**
 * Class BlogArticleVoteDislikeCmd
 */
class BlogArticleVoteDislikeCmd implements CommandInterface
{
    private $entityManager;

    /** @var BlogArticle */
    private $blogArticle;

    /**
     * BlogArticleVoteDislikeCmd constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param BlogArticle $blogArticle
     */
    public function setBlogArticle(BlogArticle $blogArticle) : void
    {
        $this->blogArticle = $blogArticle;
    }

    public function execute() : void
    {
        $this->updateAmountOfDislike();
    }

    private function updateAmountOfDislike() : void
    {
        try {
            $amountOfDislike = $this->blogArticle->getAmountOfDislike() + 1;
            $this->blogArticle->setAmountOfDislike($amountOfDislike);

            $this->entityManager->flush();

        } catch (ORMException $exception) {
            // TODO: Implements PSR Logger Notice information ??
        }
    }
}
