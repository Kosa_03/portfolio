<?php

namespace AppBundle\Service\Blog\Command;

use AppBundle\Entity\Blog\BlogArticle;
use AppBundle\Entity\Blog\CrossReference\BlogArticleTagsXRef;
use AppBundle\Service\Blog\BlogContainer;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;


/**
 * Class InsertBlogArticleCmd
 */
class InsertBlogArticleCmd implements CommandInterface
{
    private $entityManager;
    private $blogContainer;

    /** @var BlogArticle */
    private $blogArticle;

    /**
     * InsertBlogCmd constructor.
     * @param EntityManagerInterface $entityManager
     * @param BlogContainer $blogContainer
     */
    public function __construct(EntityManagerInterface $entityManager, BlogContainer $blogContainer)
    {
        $this->entityManager = $entityManager;
        $this->blogContainer = $blogContainer;
    }

    /**
     * @param BlogArticle $blogArticle
     */
    public function setBlogArticle(BlogArticle $blogArticle) : void
    {
        $this->blogArticle = $blogArticle;
    }

    public function execute() : void
    {
        $this->insert();
    }

    private function insert() : void
    {
        $this->entityManager->getConnection()->beginTransaction();

        try {
            $this->entityManager->persist($this->blogArticle);
            $this->entityManager->flush();

            $blogArticleId = $this->blogArticle->getId();
            $blogTranslations = $this->blogArticle->getTranslations();

            foreach ($blogTranslations as $blogTranslation) {
                $blogTranslation->setArticleId($blogArticleId);

                $this->entityManager->persist($blogTranslation);
                $this->entityManager->flush();
            }

            $blogTags = $this->blogArticle->getTags();

            foreach ($blogTags as $blogTag) {
                $blogTagData = new BlogArticleTagsXRef();
                $blogTagData->setArticleId($blogArticleId);
                $blogTagData->setTagId($blogTag->getTagId());

                $this->entityManager->persist($blogTagData);
                $this->entityManager->flush();
            }

            $this->entityManager->getConnection()->commit();

            $this->blogContainer->addCommandResult('insertError', false);

        } catch (ORMException $e) {
            $this->entityManager->getConnection()->rollBack();

            $this->blogContainer->addCommandResult('insertError', true);
        }
    }
}
