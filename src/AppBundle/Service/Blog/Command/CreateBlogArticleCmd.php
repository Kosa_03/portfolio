<?php

namespace AppBundle\Service\Blog\Command;

use AppBundle\Entity\Blog\BlogArticle;
use AppBundle\Entity\Blog\BlogArticleInterface;
use AppBundle\Entity\Blog\Tag\BlogTag;
use AppBundle\Entity\Blog\CrossReference\BlogArticleTagsXRef;
use AppBundle\Entity\Blog\Translation\BlogTranslation;
use AppBundle\Entity\Blog\Translation\NullBlogTranslation;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;


/**
 * Class CreateBlogArticleCmd
 */
class CreateBlogArticleCmd implements CommandInterface
{
    private $entityManager;
    private $createBlogTagCmd;

    private $locale;
    private $defaultLocale;

    /** @var BlogArticle */
    private $blogArticle;

    /**
     * CreateBlogArticleCmd constructor.
     * @param RequestStack $request
     * @param EntityManagerInterface $entityManager
     * @param CreateBlogTagCmd $createBlogTagCmd
     */
    public function __construct(RequestStack $request, EntityManagerInterface $entityManager, CreateBlogTagCmd $createBlogTagCmd)
    {
        $this->entityManager = $entityManager;

        $this->createBlogTagCmd = $createBlogTagCmd;

        $this->locale = $request->getCurrentRequest()->getLocale();
        $this->defaultLocale = 'und'; //$request->getCurrentRequest()->getDefaultLocale();
    }

    /**
     * @param BlogArticleInterface $blogArticle
     */
    public function setBlogArticle(BlogArticleInterface $blogArticle) : void
    {
        $this->blogArticle = $blogArticle;
    }

    public function execute() : void
    {
        $this->findBlogTranslations();
        $this->selectCurrentBlogTranslation();
        $this->findBlogTags();
    }

    private function findBlogTranslations() : void
    {
        $blogArticleId = $this->blogArticle->getId();

        $blogTranslations = $this->entityManager->getRepository(BlogTranslation::class)
            ->findTranslationsByArticleId($blogArticleId);

        foreach ($blogTranslations as $blogTranslation) {
            if ($blogTranslation->getArticleId() == $blogArticleId) {
                $this->blogArticle->getTranslations()->add($blogTranslation);
            }
        }
    }

    private function selectCurrentBlogTranslation() : void
    {
        $blogTranslations = $this->blogArticle->getTranslations();

        $blogTranslationLocale = null;
        $blogTranslationDefaultLocale = null;

        foreach ($blogTranslations as $blogTranslation) {
            if ($blogTranslation->getLanguage() == $this->locale) {
                $blogTranslationLocale = $blogTranslation;
            }

            if ($blogTranslation->getLanguage() == $this->defaultLocale) {
                $blogTranslationDefaultLocale = $blogTranslation;
            }
        }

        if (!is_null($blogTranslationLocale)) {
            $this->blogArticle->setTranslation($blogTranslationLocale);

        } elseif (!is_null($blogTranslationDefaultLocale)) {
            $this->blogArticle->setTranslation($blogTranslationDefaultLocale);

        } else {
            $this->blogArticle->setTranslation(new NullBlogTranslation());
        }
    }

    private function findBlogTags() : void
    {
        $blogArticleId = $this->blogArticle->getId();

        $arrayBlogTagData = $this->entityManager->getRepository(BlogArticleTagsXRef::class)
            ->findBy(['articleId' => $blogArticleId]);

        foreach ($arrayBlogTagData as $blogTagData) {
            $blogTag = $this->entityManager->getRepository(BlogTag::class)
                ->findOneBy(['tagId' => $blogTagData->getTagId()]);

            if (!is_null($blogTag)) {
                $this->createBlogTagCmd->setBlogTag($blogTag);
                $this->createBlogTagCmd->execute();

                $this->blogArticle->getTags()->add($blogTag);
            }
        }
    }
}
