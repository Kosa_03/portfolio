<?php

namespace AppBundle\Service\Blog\Command;

use AppBundle\Entity\Blog\BlogArticle;
use AppBundle\Entity\Blog\Tag\BlogTag;
use AppBundle\Service\Blog\BlogContainer;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;


/**
 * Class CreateBlogTagCollectionCmd
 */
class CreateBlogTagCollectionCmd implements CommandInterface
{
    private $entityManager;
    private $blogContainer;
    private $createBlogTagCmd;

    private $blogTagCollection;
    private $collectionName;
    private $buildBlogTag;

    /** @var BlogArticle */
    private $blogArticle;

    /**
     * CreateBlogTagCollectionCmd constructor.
     * @param EntityManagerInterface $entityManager
     * @param BlogContainer $blogContainer
     * @param CreateBlogTagCmd $createBlogTagCmd
     */
    public function __construct
    (
        EntityManagerInterface $entityManager,
        BlogContainer $blogContainer,
        CreateBlogTagCmd $createBlogTagCmd
    )
    {
        $this->entityManager = $entityManager;
        $this->blogContainer = $blogContainer;

        $this->createBlogTagCmd = $createBlogTagCmd;

        $this->collectionName = 'blog_tags';
        $this->buildBlogTag = true;
        $this->blogTagCollection = new ArrayCollection();
    }

    /**
     * @param string $collectionName
     */
    public function setCollectionName(string $collectionName = 'blog_tags') : void
    {
        $this->collectionName = $collectionName;
    }

    /**
     * @param bool $buildBlogTag
     */
    public function buildBlogTag(bool $buildBlogTag = true) : void
    {
        $this->buildBlogTag = $buildBlogTag;
    }

    /**
     * @param BlogArticle $blogArticle
     */
    public function setBlogArticle(BlogArticle $blogArticle) : void
    {
        $this->blogArticle = $blogArticle;
    }

    public function execute() : void
    {
        if (!$this->blogContainer->addCollection($this->collectionName)) {
            throw new \InvalidArgumentException(get_class() . ': Collection ' . $this->collectionName . ' exists');
        }

        $this->blogTagCollection = $this->blogContainer->getCollection($this->collectionName);

        $this->createBlogTagCollection();
    }

    private function createBlogTagCollection() : void
    {
        $foundedBlogTags = $this->entityManager->getRepository(BlogTag::class)
            ->findBy([], ['weight' => 'ASC']);

        foreach ($foundedBlogTags as $blogTag) {
            if ($this->buildBlogTag) {
                $this->createBlogTagCmd->setBlogTag($blogTag);
                $this->createBlogTagCmd->execute();
            }

            $this->blogTagCollection->add($blogTag);
        }
    }
}
