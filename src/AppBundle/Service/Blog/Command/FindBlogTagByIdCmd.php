<?php

namespace AppBundle\Service\Blog\Command;

use AppBundle\Entity\Blog\Tag\BlogTag;
use AppBundle\Service\Blog\BlogContainer;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;


/**
 * Class FindBlogTagByIdCmd
 */
class FindBlogTagByIdCmd implements CommandInterface
{
    private $entityManager;
    private $blogContainer;

    private $tagId;

    /**
     * FindBlogTagByIdCmd constructor.
     * @param EntityManagerInterface $entityManager
     * @param BlogContainer $blogContainer
     */
    public function __construct(EntityManagerInterface $entityManager, BlogContainer $blogContainer)
    {
        $this->entityManager = $entityManager;
        $this->blogContainer = $blogContainer;
    }

    /**
     * @param int $tagId
     */
    public function setTagId(int $tagId) : void
    {
        $this->tagId = $tagId;
    }

    public function execute() : void
    {
        $this->findBlogTagById();
    }

    private function findBlogTagById() : void
    {
        try {
            $blogTag = $this->entityManager->getRepository(BlogTag::class)
                ->findOneBy(['tagId' => $this->tagId]);

            $this->blogContainer->addCommandResult('foundedBlogTag', $blogTag);

        } catch (ORMException $e) {
            $this->blogContainer->addCommandResult('foundedBlogTag', null);
        }
    }
}
