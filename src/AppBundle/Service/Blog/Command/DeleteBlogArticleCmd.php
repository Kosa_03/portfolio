<?php

namespace AppBundle\Service\Blog\Command;

use AppBundle\Entity\Blog\BlogArticle;
use AppBundle\Entity\Blog\CrossReference\BlogArticleTagsXRef;
use AppBundle\Service\Blog\BlogManager;
use AppBundle\Service\Blog\Uploader\HeaderImageFileUploader;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


/**
 * Class DeleteBlogArticleCmd
 */
class DeleteBlogArticleCmd implements CommandInterface
{
    private $session;
    private $entityManager;
    private $blogContainer;
    private $headerImageFileUploader;

    /** @var BlogArticle */
    private $blogArticle;

    /**
     * DeleteBlogArticleCmd constructor.
     * @param SessionInterface $session
     * @param EntityManagerInterface $entityManager
     * @param BlogManager $blogManager
     */
    public function __construct(SessionInterface $session, EntityManagerInterface $entityManager, BlogManager $blogManager)
    {
        $this->session = $session;
        $this->entityManager = $entityManager;
        $this->blogContainer = $blogManager->getContainer();
        /** @var HeaderImageFileUploader headerImageFileUploader */
        $this->headerImageFileUploader = $blogManager->getFileUploader(HeaderImageFileUploader::class);
    }

    /**
     * @param BlogArticle $blogArticle
     */
    public function setBlogArticle(BlogArticle $blogArticle) : void
    {
        $this->blogArticle = $blogArticle;
    }

    public function execute() : void
    {
        $this->delete();
    }

    private function delete() : void
    {
        try {
            $this->entityManager->getConnection()->beginTransaction();

            $arrayBlogTagData = $this->entityManager->getRepository(BlogArticleTagsXRef::class)
                ->findBy(['articleId' => $this->blogArticle->getId()]);

            foreach ($arrayBlogTagData as $blogTagData) {
                $this->entityManager->remove($blogTagData);
                $this->entityManager->flush();
            }

            foreach ($this->blogArticle->getTranslations() as $blogTranslation) {
                $this->entityManager->remove($blogTranslation);
                $this->entityManager->flush();
            }

            $articleId = $this->blogArticle->getId();
            $headerImageFileName = $this->blogArticle->getHeaderImageFile();

            $this->entityManager->remove($this->blogArticle);
            $this->entityManager->flush();

            $this->entityManager->getConnection()->commit();

            $this->deleteFromSession($articleId);
            $this->deleteHeaderImageFile($headerImageFileName);

            $this->blogContainer->addCommandResult('deleteError', false);

        } catch (ORMException $e) {
            $this->entityManager->getConnection()->rollBack();
            $this->blogContainer->addCommandResult('deleteError', true);
        }
    }

    /**
     * @param int $articleId
     */
    private function deleteFromSession(int $articleId) : void
    {
        $this->session->remove('admin_blog_article_edit/' . $articleId . '/blogArticleDTO');
    }

    /**
     * @param string $fileName
     */
    private function deleteHeaderImageFile(string $fileName) : void
    {
        $this->headerImageFileUploader->remove($fileName);
    }
}
