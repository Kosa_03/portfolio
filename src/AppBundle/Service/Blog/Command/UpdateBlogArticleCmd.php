<?php

namespace AppBundle\Service\Blog\Command;

use AppBundle\Entity\Blog\BlogArticle;
use AppBundle\Entity\Blog\CrossReference\BlogArticleTagsXRef;
use AppBundle\Service\Blog\BlogContainer;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;


/**
 * Class UpdateBlogArticleCmd
 */
class UpdateBlogArticleCmd implements CommandInterface
{
    private $entityManager;
    private $blogContainer;

    /** @var BlogArticle */
    private $blogArticle;

    /**
     * UpdateBlogArticleCmd constructor.
     * @param EntityManagerInterface $entityManager
     * @param BlogContainer $blogContainer
     */
    public function __construct(EntityManagerInterface $entityManager, BlogContainer $blogContainer)
    {
        $this->entityManager = $entityManager;
        $this->blogContainer = $blogContainer;
    }

    /**
     * @param BlogArticle $blogArticle
     */
    public function setBlogArticle(BlogArticle $blogArticle) : void
    {
        $this->blogArticle = $blogArticle;
    }

    public function execute(): void
    {
        $this->update();
    }

    private function update() : void
    {
        $this->entityManager->getConnection()->beginTransaction();

        try {
            $blogArticle = $this->blogArticle;

            foreach ($blogArticle->getTranslations() as $blogTranslation) {
                $this->entityManager->refresh($blogTranslation);
            }

            $this->createCheckedBlogTagsData($blogArticle);
            $this->removeUncheckedBlogTagsData($blogArticle);

            $this->entityManager->flush();
            $this->entityManager->getConnection()->commit();
            $this->blogContainer->addCommandResult('updateOptionsError', false);

        } catch (ORMException $e) {
            $this->entityManager->getConnection()->rollBack();
            $this->blogContainer->addCommandResult('updateOptionsError', true);
        }
    }

    /**
     * @param BlogArticle $blogArticle
     */
    private function createCheckedBlogTagsData(BlogArticle $blogArticle) : void
    {
        $blogTagsData = $this->findBlogTagsData();

        $oldBlogTagsIdList = [];
        foreach ($blogTagsData as $blogTagData) {
            $oldBlogTagsIdList[] = $blogTagData->getTagId();
        }

        foreach ($blogArticle->getTags() as $blogTag) {
            if (!in_array($blogTag->getTagId(), $oldBlogTagsIdList)) {
                $newBlogTagData = new BlogArticleTagsXRef();

                $newBlogTagData->setArticleId($blogArticle->getId());
                $newBlogTagData->setTagId($blogTag->getTagId());

                $this->entityManager->persist($newBlogTagData);
            }
        }
    }

    /**
     * @param BlogArticle $blogArticle
     */
    private function removeUncheckedBlogTagsData(BlogArticle $blogArticle) : void
    {
        $blogTagsData = $this->findBlogTagsData();

        $oldBlogTagsIdList = [];
        foreach ($blogArticle->getTags() as $blogTag) {
            $oldBlogTagsIdList[] = $blogTag->getTagId();
        }

        foreach ($blogTagsData as $blogTagData) {
            if (!in_array($blogTagData->getTagId(), $oldBlogTagsIdList)) {
                $this->entityManager->remove($blogTagData);
            }
        }
    }

    /**
     * @return array
     */
    private function findBlogTagsData() : array
    {
        return $this->entityManager->getRepository(BlogArticleTagsXRef::class)
            ->findBy(['articleId' => $this->blogArticle->getId()]);
    }
}
