<?php

namespace AppBundle\Service\Blog\Command;

use AppBundle\Entity\Blog\BlogArticle;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;


/**
 * Class BlogArticleVoteLikeCmd
 */
class BlogArticleVoteLikeCmd implements CommandInterface
{
    private $entityManager;

    /** @var BlogArticle */
    private $blogArticle;

    /**
     * BlogArticleVoteLikeCmd constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param BlogArticle $blogArticle
     */
    public function setBlogArticle(BlogArticle $blogArticle) : void
    {
        $this->blogArticle = $blogArticle;
    }

    public function execute() : void
    {
        $this->updateAmountOfLike();
    }

    private function updateAmountOfLike() : void
    {
        try {
            $amountOfLike = $this->blogArticle->getAmountOfLike() + 1;
            $this->blogArticle->setAmountOfLike($amountOfLike);

            $this->entityManager->flush();

        } catch (ORMException $exception) {
            // TODO: Implements PSR Logger Notice information ??
        }
    }
}
