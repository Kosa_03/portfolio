<?php

namespace AppBundle\Service\Blog\EntityLoader;

use AppBundle\Service\Blog\BlogManager;
use AppBundle\Service\Blog\Command\CreateBlogArticleCollectionCmd;
use AppBundle\Service\Field\EntityLoader\EntityLoaderInterface;
use AppBundle\Service\Field\FieldObject;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;


/**
 * Class BlogArticleListLoader
 * @package AppBundle\Service\Blog\EntityLoader
 */
class BlogArticleListLoader implements EntityLoaderInterface
{
    private $entityManager;
    private $blogManager;

    private $createBlogArticleCollectionCmd;

    /**
     * LastBlogArticleLoader constructor.
     * @param EntityManagerInterface $entityManager
     * @param BlogManager $blogManager
     * @param CreateBlogArticleCollectionCmd $createBlogArticleCollectionCmd
     */
    public function __construct
    (
        EntityManagerInterface $entityManager,
        BlogManager $blogManager,
        CreateBlogArticleCollectionCmd $createBlogArticleCollectionCmd
    )
    {
        $this->entityManager = $entityManager;
        $this->blogManager = $blogManager;
        $this->createBlogArticleCollectionCmd = $createBlogArticleCollectionCmd;
    }

    /**
     * @param FieldObject $fieldObject
     */
    public function loadExternalEntity(FieldObject $fieldObject) : void
    {
        $blogArticlesCollection = $this->findBlogArticles();

        $fieldObject->setExternalEntity($blogArticlesCollection);
    }

    /**
     * @return ArrayCollection
     */
    private function findBlogArticles() : ArrayCollection
    {
        $this->createBlogArticleCollectionCmd->setCollectionName('blog_article_list');
        $this->blogManager->setCommand($this->createBlogArticleCollectionCmd);
        $this->blogManager->run();

        return $this->blogManager->getContainer()->getCollection('blog_article_list');
    }
}
