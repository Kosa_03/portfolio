<?php

namespace AppBundle\Service\Blog\EntityLoader;

use AppBundle\Entity\Blog\BlogArticle;
use AppBundle\Entity\Blog\BlogArticleInterface;
use AppBundle\Entity\Blog\NullBlogArticle;
use AppBundle\Service\Blog\BlogManager;
use AppBundle\Service\Blog\Command\CreateBlogArticleCmd;
use AppBundle\Service\Blog\Command\FindByBlogFindOptionsCmd;
use AppBundle\Service\Field\EntityLoader\EntityLoaderInterface;
use AppBundle\Service\Field\FieldObject;
use Doctrine\ORM\EntityManagerInterface;


/**
 * Class BlogArticleLoader
 */
class BlogArticleLoader implements EntityLoaderInterface
{
    private $entityManager;
    private $blogManager;

    private $findByBlogFindOptions;
    private $createBlogArticleCmd;

    /**
     * BlogArticleLoader constructor.
     * @param EntityManagerInterface $entityManager
     * @param BlogManager $blogManager
     * @param FindByBlogFindOptionsCmd $findByBlogFindOptionsCmd
     * @param CreateBlogArticleCmd $createBlogArticleCmd
     */
    public function __construct
    (
        EntityManagerInterface $entityManager,
        BlogManager $blogManager,
        FindByBlogFindOptionsCmd $findByBlogFindOptionsCmd,
        CreateBlogArticleCmd $createBlogArticleCmd
    )
    {
        $this->entityManager = $entityManager;
        $this->blogManager = $blogManager;

        $this->findByBlogFindOptions = $findByBlogFindOptionsCmd;
        $this->createBlogArticleCmd = $createBlogArticleCmd;
    }

    /**
     * @param FieldObject $fieldObject
     */
    public function loadExternalEntity(FieldObject $fieldObject) : void
    {
        $blogArticle = $this->findBlogArticleById();

        $fieldObject->setExternalEntity($blogArticle);
    }

    /**
     * @return BlogArticleInterface
     */
    private function findBlogArticleById() : BlogArticleInterface
    {
        $this->blogManager->setCommand($this->findByBlogFindOptions);
        $this->blogManager->run();

        $foundedBlogArticle = $this->blogManager->getCmdResult('foundedBlogArticles');

        if (key_exists(0, $foundedBlogArticle) ) {
            $blogArticle = $foundedBlogArticle[0];

            if ($blogArticle instanceof BlogArticle) {
                $this->createBlogArticleCmd->setBlogArticle($blogArticle);

                $this->blogManager->setCommand($this->createBlogArticleCmd);
                $this->blogManager->run();

            } else {
                $blogArticle = new NullBlogArticle();
            }

        } else {
            $blogArticle = new NullBlogArticle();
        }

        return $blogArticle;
    }
}
