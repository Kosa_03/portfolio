<?php

namespace AppBundle\Service\Blog\Factory;

use AppBundle\FormDTO\Blog\CollectionType\BlogTranslationDTO;
use AppBundle\FormDTO\Blog\BlogArticleDTO;


/**
 * Class BlogArticleDTOFactory
 */
class BlogArticleDTOFactory
{
    /**
     * @return BlogArticleDTO
     */
    public function createBlogArticle() : BlogArticleDTO
    {
        $blogArticleDTO =  new BlogArticleDTO();
        $blogArticleDTO->setStatus('not_published');

        return $blogArticleDTO;
    }

    /**
     * @param BlogArticleDTO $blogArticleDTO
     */
    public function addEmptyBlogTranslation(BlogArticleDTO $blogArticleDTO) : void
    {
        $blogTranslationDTO = new BlogTranslationDTO();

        $blogArticleDTO->getTranslations()->add($blogTranslationDTO);
    }

    /**
     * @param BlogArticleDTO $blogArticleDTO
     */
    public function createBlogTranslations(BlogArticleDTO $blogArticleDTO) : void
    {
        $blogArticleDTO->getTranslations()->clear();

        $amountTranslations = $blogArticleDTO->getAmountTranslations();

        for ($i = 0; $i < $amountTranslations; $i++) {
            $this->addEmptyBlogTranslation($blogArticleDTO);
        }
    }

    /**
     * @param BlogArticleDTO $blogArticleDTO
     * @param $key
     */
    public function deleteBlogTranslation(BlogArticleDTO $blogArticleDTO, $key) : void
    {
        $blogArticleDTO->getTranslations()->remove($key);
    }

    /**
     * @param BlogArticleDTO $blogArticleDTO
     */
    public function refreshBlogTranslations(BlogArticleDTO $blogArticleDTO) : void
    {
        $blogTranslations = $blogArticleDTO->getTranslations();

        $amountTranslations = $blogArticleDTO->getAmountTranslations();
        $amountTranslationsInBlogArticleDTO = $blogTranslations->count();

        if ($amountTranslationsInBlogArticleDTO > $amountTranslations) {
            $lastBlogTranslation = $blogTranslations->last();
            $blogTranslations->removeElement($lastBlogTranslation);

            $this->refreshBlogTranslations($blogArticleDTO);
        }

        if ($amountTranslationsInBlogArticleDTO < $amountTranslations) {
            $this->addEmptyBlogTranslation($blogArticleDTO);

            $this->refreshBlogTranslations($blogArticleDTO);
        }
    }
}
