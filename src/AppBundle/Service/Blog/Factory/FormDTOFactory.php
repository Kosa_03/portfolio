<?php

namespace AppBundle\Service\Blog\Factory;

use AppBundle\FormDTO\Blog\SearchBlogDTO;


/**
 * Class FormDTOFactory
 */
class FormDTOFactory
{
    /**
     * @return SearchBlogDTO
     */
    public function createSearchBlog() : SearchBlogDTO
    {
        return new SearchBlogDTO();
    }
}
