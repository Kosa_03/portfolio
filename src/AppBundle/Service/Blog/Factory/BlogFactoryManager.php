<?php

namespace AppBundle\Service\Blog\Factory;


/**
 * Class BlogFactoryManager
 */
class BlogFactoryManager
{
    private $blogArticleFactory;
    private $blogArticleDTOFactory;
    private $blogTagListDTOFactory;
    private $formDTOFactory;

    /**
     * BlogFactoryManager constructor.
     * @param BlogArticleFactory $blogArticleFactory
     * @param BlogArticleDTOFactory $blogArticleDTOFactory
     * @param BlogTagListDTOFactory $blogTagListDTOFactory
     * @param FormDTOFactory $formDTOFactory
     */
    public function __construct
    (
        BlogArticleFactory $blogArticleFactory,
        BlogArticleDTOFactory $blogArticleDTOFactory,
        BlogTagListDTOFactory $blogTagListDTOFactory,
        FormDTOFactory $formDTOFactory
    )
    {
        $this->blogArticleFactory = $blogArticleFactory;
        $this->blogArticleDTOFactory = $blogArticleDTOFactory;
        $this->blogTagListDTOFactory = $blogTagListDTOFactory;
        $this->formDTOFactory = $formDTOFactory;
    }

    /**
     * @return BlogArticleFactory
     */
    public function getBlogArticleFactory() : BlogArticleFactory
    {
        return $this->blogArticleFactory;
    }

    /**
     * @return BlogArticleDTOFactory
     */
    public function getBlogArticleDTOFactory() : BlogArticleDTOFactory
    {
        return $this->blogArticleDTOFactory;
    }

    /**
     * @return BlogTagListDTOFactory
     */
    public function getBlogTagListDTOFactory() : BlogTagListDTOFactory
    {
        return $this->blogTagListDTOFactory;
    }

    /**
     * @return FormDTOFactory
     */
    public function getFormDTOFactory() : FormDTOFactory
    {
        return $this->formDTOFactory;
    }
}
