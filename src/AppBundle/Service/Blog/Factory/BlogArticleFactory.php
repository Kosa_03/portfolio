<?php

namespace AppBundle\Service\Blog\Factory;

use AppBundle\Entity\Blog\BlogArticle;
use AppBundle\Entity\Blog\Translation\BlogTranslation;
use Symfony\Component\Security\Core\Security;


/**
 * Class BlogArticleFactory
 */
class BlogArticleFactory
{
    private $user;

    /**
     * BlogFactory constructor.
     * @param Security $security
     */
    public function __construct(Security $security)
    {
        $this->user = $security->getUser();
    }

    /**
     * @return BlogArticle
     */
    public function createBlogArticle() : BlogArticle
    {
        $blogArticle = new BlogArticle();

        $blogArticle->setAuthorId($this->user->getId());
        $blogArticle->setEditorId($this->user->getId());
        $blogArticle->setCreated(new \DateTime());
        $blogArticle->setModified(new \DateTime());

        return $blogArticle;
    }

    /**
     * @param BlogArticle $blogArticle
     */
    public function addEmptyBlogTranslation(BlogArticle $blogArticle) : void
    {
        $blogTranslation = new BlogTranslation();
        $blogTranslation->setCreated(new \DateTime());
        $blogTranslation->setModified(new \DateTime());

        $blogArticle->getTranslations()->add($blogTranslation);
    }
}
