<?php

namespace AppBundle\Service\Blog\Factory;

use AppBundle\Service\Blog\EntityLoader\BlogArticleListLoader;
use AppBundle\Service\Blog\EntityLoader\BlogArticleLoader;
use AppBundle\Service\Field\EntityLoader\EntityLoaderInterface;


/**
 * Class BlogEntityLoaderFactory
 */
class BlogEntityLoaderFactory
{
    private $entityLoaders = [];

    /**
     * BlogEntityLoaderFactory constructor.
     * @param BlogArticleLoader $lastBlogArticleLoader
     * @param BlogArticleListLoader $newestThreeBlogArticlesLoader
     */
    public function __construct
    (
        BlogArticleLoader $lastBlogArticleLoader,
        BlogArticleListLoader $newestThreeBlogArticlesLoader
    )
    {
        $this->entityLoaders[BlogArticleLoader::class] = $lastBlogArticleLoader;
        $this->entityLoaders[BlogArticleListLoader::class] = $newestThreeBlogArticlesLoader;
    }

    /**
     * @param string $entityLoaderClassName
     * @return EntityLoaderInterface
     */
    public function getEntityLoader(string $entityLoaderClassName) : EntityLoaderInterface
    {
        if (key_exists($entityLoaderClassName, $this->entityLoaders)) {
            return $this->entityLoaders[$entityLoaderClassName];
        }

        throw new \InvalidArgumentException(
            'BlogEntityLoaderFactory does not contains loader class name "' . $entityLoaderClassName . '".'
        );
    }
}
