<?php

namespace AppBundle\Service\Blog\Factory;

use AppBundle\FormDTO\Blog\BlogTagListDTO;
use AppBundle\FormDTO\Blog\CollectionType\BlogTagDTO;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Class BlogTagListDTOFactory
 */
class BlogTagListDTOFactory
{
    /**
     * @return BlogTagListDTO
     */
    public function createBlogTagList() : BlogTagListDTO
    {
        return new BlogTagListDTO();
    }

    /**
     * @param BlogTagListDTO $blogTagListDTO
     */
    public function addEmptyBlogTag(BlogTagListDTO $blogTagListDTO) : void
    {
        $blogTagDTO = new BlogTagDTO();

        $blogTagListDTO->getTags()->add($blogTagDTO);
    }

    /**
     * @param BlogTagListDTO $blogTagListDTO
     * @param $key
     */
    public function deleteBlogTag(BlogTagListDTO $blogTagListDTO, $key) : void
    {
        $blogTagListDTO->getTags()->remove($key);

        $this->fixWrongKeysInArrayCollection($blogTagListDTO);
    }

    /**
     * @param BlogTagListDTO $blogTagListDTO
     */
    private function fixWrongKeysInArrayCollection(BlogTagListDTO $blogTagListDTO) : void
    {
        $newCollection = new ArrayCollection($blogTagListDTO->getTags()->getValues());
        $blogTagListDTO->setTags($newCollection);
    }
}
