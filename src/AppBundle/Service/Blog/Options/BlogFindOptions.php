<?php

namespace AppBundle\Service\Blog\Options;

use AppBundle\Entity\Blog\CrossReference\BlogArticleTagsXRef;


/**
 * Class BlogFindOptions
 */
class BlogFindOptions
{
    const SORT_METHOD_ASC = 'ASC';
    const SORT_METHOD_DESC = 'DESC';

    private $joinParams = [];
    private $joinConditions = [];
    private $whereConditions = [];
    private $whereParams = [];

    private $limit;
    private $offset;

    private $sortBy = 'created';
    private $sortMethod = self::SORT_METHOD_DESC;

    public function clear() : void
    {
        $this->joinParams = [];
        $this->joinConditions = [];
        $this->whereConditions = [];
        $this->whereParams = [];

        $this->limit = null;
        $this->offset = null;

        $this->sortBy = 'created';
        $this->sortMethod = self::SORT_METHOD_DESC;
    }

    /**
     * @param string $tableAlias
     */
    public function joinBlogTagDataTable(string $tableAlias) : void
    {
        $this->joinTable(BlogArticleTagsXRef::class, $tableAlias, 'articleId', 'join_tag_table');
        $this->addJoinCondition('join_tag_table', 'id');
    }

    /**
     * @return array
     */
    public function getJoinParams() : array
    {
        return $this->joinParams;
    }

    /**
     * @param string $tableName
     * @param string $tableAlias
     * @param string $tableColumn
     * @param string $conditionKey
     * @param string $joinType
     */
    public function joinTable(string $tableName, string $tableAlias, string $tableColumn, string $conditionKey, string $joinType = 'left') : void
    {
        $this->joinParams[$conditionKey] = [
            'tableName' => $tableName,
            'tableAlias' => $tableAlias,
            'tableColumn' => $tableColumn,
            'joinType' => $joinType
        ];
    }

    /**
     * @return array
     */
    public function getJoinConditions() : array
    {
        return $this->joinConditions;
    }

    /**
     * @param string $conditionKey
     * @param string $columnName
     * @param string $tableAlias
     * @param string $operator
     */
    public function addJoinCondition(string $conditionKey, string $columnName, string $tableAlias = 'blog_article', string $operator = '=') : void
    {
        $this->joinConditions[$conditionKey] = [
            'columnName' => $columnName,
            'tableAlias' => $tableAlias,
            'operator' => $operator
        ];
    }

    /**
     * @return array
     */
    public function getWhereConditions() : array
    {
        return $this->whereConditions;
    }

    /**
     * @param string $conditionKey
     * @param string $columnName
     * @param string $tableAlias
     * @param string $operator
     * @param string $conditionType
     */
    public function addWhereCondition(string $conditionKey, string $columnName, string $tableAlias = 'blog_article', string $operator = '=', string $conditionType = 'AND') : void
    {
        $this->whereConditions[$conditionKey] = [
            'columnName' => $columnName,
            'tableAlias' => $tableAlias,
            'operator' => $operator,
            'conditionType' => $conditionType
        ];
    }

    /**
     * @return array
     */
    public function getWhereParams() : array
    {
        return $this->whereParams;
    }

    /**
     * @param string $conditionKey
     * @param string $paramName
     * @param $value
     */
    public function addWhereParam(string $conditionKey, string $paramName, $value) : void
    {
        $this->whereParams[$conditionKey] = [
            'paramName' => $paramName,
            'value' => $value
        ];
    }

    /**
     * @return int|null
     */
    public function getLimit() : ?int
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit(int $limit) : void
    {
        $this->limit = $limit;
    }

    /**
     * @return int|null
     */
    public function getOffset() : ?int
    {
        return $this->offset;
    }

    /**
     * @param int $offset
     */
    public function setOffset(int $offset) : void
    {
        $this->offset = $offset;
    }

    /**
     * @return string
     */
    public function getSortBy() : string
    {
        return $this->sortBy;
    }

    /**
     * @param string $columnName
     */
    public function setSortBy(string $columnName) : void
    {
        $this->sortBy = $columnName;
    }

    /**
     * @return string
     */
    public function getSortMethod() : string
    {
        return $this->sortMethod;
    }

    public function sortASC() : void
    {
        $this->sortMethod = self::SORT_METHOD_ASC;
    }

    public function sortDESC() : void
    {
        $this->sortMethod = self::SORT_METHOD_DESC;
    }
}
