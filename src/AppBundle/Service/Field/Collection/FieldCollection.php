<?php

namespace AppBundle\Service\Field\Collection;

use AppBundle\Entity\Field\FieldType\Form\FormFieldInterface;
use AppBundle\Entity\Field\Group\GroupTemplateInterface;
use AppBundle\Service\Field\FieldObject;


/**
 * Class FieldCollection
 */
class FieldCollection
{
    private $fields = [];
    private $groupTemplate;

    public function clear() : void
    {
        $this->fields = [];
    }

    /**
     * @return array
     */
    public function getCollection() : array
    {
        return $this->fields;
    }
    
    /**
     * @param string $key
     * @return FieldObject|null
     */
    public function getField(string $key) : ?FieldObject
    {
        if ($this->checkKeyExists($key)) {
            return $this->fields[$key];
        }

        return null;
    }

    /**
     * @param string $key
     * @return FormFieldInterface|null
     */
    public function getFormField(string $key) : ?FormFieldInterface
    {
        if ($this->checkKeyExists($key)) {
            if ($this->fields[$key]->getField() instanceof FormFieldInterface) {
                return $this->fields[$key]->getField();
            }
        }

        return null;
    }

    /**
     * @param FieldObject $fieldObject
     * @param string|null $key
     */
    public function addField(FieldObject $fieldObject, string $key = null) : void
    {
        if (is_null($key)) {
            $key = $fieldObject->getFieldName();
        }

        if (!$this->checkKeyExists($key)) {
            $this->fields[$key] = $fieldObject;
        }
    }

    /**
     * @param string $key
     * @return FieldCollection|null
     */
    public function getGroup(string $key) : ?FieldCollection
    {
        if ($this->checkKeyExists($key)) {
            return $this->fields[$key];
        }

        return null;
    }

    /**
     * @param string $key
     * @return FieldCollection
     */
    public function addGroup(string $key) : FieldCollection
    {
        if (!$this->checkKeyExists($key)) {
            $this->fields[$key] = new FieldCollection();
        }

        return $this->fields[$key];
    }

    /**
     * @return GroupTemplateInterface
     */
    public function getTemplate() : GroupTemplateInterface
    {
        return $this->groupTemplate;
    }

    /**
     * @param GroupTemplateInterface $groupTemplate
     */
    public function setTemplate(GroupTemplateInterface $groupTemplate) : void
    {
        $this->groupTemplate = $groupTemplate;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function checkKeyExists(string $key) : bool
    {
        if (array_key_exists($key, $this->fields)) {
            return true;
        }

        return false;
    }
}
