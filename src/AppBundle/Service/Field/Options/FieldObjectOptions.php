<?php

namespace AppBundle\Service\Field\Options;


/**
 * Class FieldObjectOptions
 */
class FieldObjectOptions
{
    private $patterns = array();

    /**
     * @return array
     */
    public function getPatterns() : array
    {
        return $this->patterns;
    }

    /**
     * @param array $patterns
     */
    public function setPatterns(array $patterns) : void
    {
        $this->patterns = $patterns;
    }
}
