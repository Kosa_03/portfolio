<?php

namespace AppBundle\Service\Field\Options;


/**
 * Class FieldListOptions
 */
class FieldListOptions
{
    private $configId;
    private $controllerName;
    private $methodName;
    private $fieldClass;
    private $fieldName;

    private $conditions = [];

    private $limit;
    private $offset;

    private $sortBy = 'weight';
    private $sortMethod = 'ASC';

    public function clear() : void
    {
        $this->configId = null;
        $this->controllerName = null;
        $this->methodName = null;
        $this->fieldClass = null;
        $this->fieldName = null;

        $this->conditions = [];

        $this->limit = null;
        $this->offset = null;

        $this->sortBy = 'weight';
        $this->sortMethod = 'ASC';
    }

    /**
     * @return null|int
     */
    public function getConfigId() : ?int
    {
        return $this->configId;
    }

    /**
     * @param int $configId
     */
    public function setConfigId(int $configId) : void
    {
        $this->configId = $configId;
    }

    /**
     * @return null|string
     */
    public function getControllerName() : ?string
    {
        return $this->controllerName;
    }

    /**
     * @param string $controllerName
     */
    public function setControllerName(string $controllerName) : void
    {
        $this->controllerName = $controllerName;
    }

    /**
     * @return null|string
     */
    public function getMethodName() : ?string
    {
        return $this->methodName;
    }

    /**
     * @param string $methodName
     */
    public function setMethodName(string $methodName) : void
    {
        $this->methodName = $methodName;
    }

    /**
     * @return null|string
     */
    public function getFieldClass() : ?string
    {
        return $this->fieldClass;
    }

    /**
     * @param string $fieldClass
     */
    public function setFieldClass(string $fieldClass) : void
    {
        $this->fieldClass = $fieldClass;
    }

    /**
     * @return null|string
     */
    public function getFieldName() : ?string
    {
        return $this->fieldName;
    }

    /**
     * @param string $fieldName
     */
    public function setFieldName(string $fieldName) : void
    {
        $this->fieldName = $fieldName;
    }

    /**
     * @return array
     */
    public function getConditions() : array
    {
        return $this->conditions;
    }

    /**
     * @param string $column
     * @param string $param
     * @param mixed $value
     * @param string $operator
     * @param string $method
     */
    public function addCondition(string $column, string $param, $value, string $operator = '=', string $method = 'and') : void
    {
        $this->conditions[] = [
            'columnName' => $column,
            'paramName' => $param,
            'value' => $value,
            'operator' => $operator,
            'method' => $method
        ];
    }

    /**
     * @return int|null
     */
    public function getLimit() : ?int
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit(int $limit) : void
    {
        $this->limit = $limit;
    }

    /**
     * @return int|null
     */
    public function getOffset() : ?int
    {
        return $this->offset;
    }

    /**
     * @param int $offset
     */
    public function setOffset(int $offset) : void
    {
        $this->offset = $offset;
    }

    /**
     * @return string
     */
    public function getSortBy() : string
    {
        return $this->sortBy;
    }

    /**
     * @param string $columnName
     */
    public function setSortBy(string $columnName) : void
    {
        $this->sortBy = $columnName;
    }

    /**
     * @return string
     */
    public function getSortMethod() : string
    {
        return $this->sortMethod;
    }

    public function sortASC() : void
    {
        $this->sortMethod = 'ASC';
    }

    public function sortDESC() : void
    {
        $this->sortMethod = 'DESC';
    }
}
