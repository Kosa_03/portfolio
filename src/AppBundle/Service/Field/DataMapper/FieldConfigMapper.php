<?php

namespace AppBundle\Service\Field\DataMapper;

use AppBundle\FormDTO\Field\FieldConfigDTO;
use AppBundle\Service\Field\FieldObject;


/**
 * Class FieldConfigMapper
 */
class FieldConfigMapper
{
    /**
     * @param FieldConfigDTO $fieldConfigDTO
     * @param FieldObject $fieldObject
     */
    public function mapToFieldConfig(FieldConfigDTO $fieldConfigDTO, FieldObject $fieldObject) : void
    {
        $fieldConfig = $fieldObject->getConfig();

        $fieldConfig->setFieldClass($fieldConfigDTO->getFieldClass());
        $fieldConfig->setFieldName($fieldConfigDTO->getFieldName());
        $fieldConfig->setGroupId($fieldConfigDTO->getGroupId());
        $fieldConfig->setController($fieldConfigDTO->getController());
        $fieldConfig->setMethod($fieldConfigDTO->getMethod());
        $fieldConfig->setWeight($fieldConfigDTO->getWeight());
        $fieldConfig->setRoles($fieldConfigDTO->getRoles());
    }

    /**
     * @param FieldObject $fieldObject
     * @param FieldConfigDTO $fieldConfigDTO
     */
    public function mapToDTO(FieldObject $fieldObject, FieldConfigDTO $fieldConfigDTO) : void
    {
        $fieldConfig = $fieldObject->getConfig();

        $fieldConfigDTO->setFieldClass($fieldConfig->getFieldClass());
        $fieldConfigDTO->setFieldName($fieldConfig->getFieldName());
        $fieldConfigDTO->setGroupId($fieldConfig->getGroupId());
        $fieldConfigDTO->setController($fieldConfig->getController());
        $fieldConfigDTO->setMethod($fieldConfig->getMethod());
        $fieldConfigDTO->setWeight($fieldConfig->getWeight());
        $fieldConfigDTO->setRoles($fieldConfig->getRoles());
    }
}
