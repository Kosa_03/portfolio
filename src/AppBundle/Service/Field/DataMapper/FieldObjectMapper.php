<?php

namespace AppBundle\Service\Field\DataMapper;

use AppBundle\FormDTO\Field\FieldConfigDTO;
use AppBundle\FormDTO\Field\FieldEntityDTO;
use AppBundle\FormDTO\Field\FieldTemplateDTO;
use AppBundle\Service\Field\FieldObject;


/**
 * Class FieldObjectMapper
 */
class FieldObjectMapper
{
    private $fieldConfigMapper;
    private $fieldTemplateMapper;
    private $fieldEntityMapper;

    /**
     * FieldObjectMapper constructor.
     * @param FieldConfigMapper $configMapper
     * @param FieldTemplateMapper $templateMapper
     * @param FieldEntityMapper $entityMapper
     */
    public function __construct(FieldConfigMapper $configMapper, FieldTemplateMapper $templateMapper, FieldEntityMapper $entityMapper)
    {
        $this->fieldConfigMapper = $configMapper;
        $this->fieldTemplateMapper = $templateMapper;
        $this->fieldEntityMapper = $entityMapper;
    }

    /**
     * @param FieldObject $fieldObject
     * @param FieldConfigDTO $fieldConfigDTO
     */
    public function mapFieldConfigToDTO(FieldObject $fieldObject, FieldConfigDTO $fieldConfigDTO) : void
    {
        $this->fieldConfigMapper->mapToDTO($fieldObject, $fieldConfigDTO);
    }

    /**
     * @param FieldObject $fieldObject
     * @param FieldConfigDTO $fieldConfigDTO
     */
    public function mapDTOToFieldConfig(FieldObject $fieldObject, FieldConfigDTO $fieldConfigDTO) : void
    {
        $this->fieldConfigMapper->mapToFieldConfig($fieldConfigDTO, $fieldObject);
    }

    /**
     * @param FieldObject $fieldObject
     * @param FieldTemplateDTO $fieldTemplateDTO
     */
    public function mapFieldTemplateToDTO(FieldObject $fieldObject, FieldTemplateDTO $fieldTemplateDTO) : void
    {
        $this->fieldTemplateMapper->mapToDTO($fieldObject, $fieldTemplateDTO);
    }

    /**
     * @param FieldObject $fieldObject
     * @param FieldTemplateDTO $fieldTemplateDTO
     */
    public function mapDTOToFieldTemplate(FieldObject $fieldObject, FieldTemplateDTO $fieldTemplateDTO) : void
    {
        $this->fieldTemplateMapper->mapToFieldTemplate($fieldTemplateDTO, $fieldObject);
    }

    /**
     * @param FieldObject $fieldObject
     * @param FieldEntityDTO $fieldEntityDTO
     */
    public function mapFieldEntitiesToDTO(FieldObject $fieldObject, FieldEntityDTO $fieldEntityDTO) : void
    {
        $this->fieldEntityMapper->mapToDTO($fieldObject, $fieldEntityDTO);
    }

    /**
     * @param FieldObject $fieldObject
     * @param FieldEntityDTO $fieldEntityDTO
     */
    public function mapDTOToFieldEntities(FieldObject $fieldObject, FieldEntityDTO $fieldEntityDTO) : void
    {
        $this->fieldEntityMapper->mapToFieldEntities($fieldEntityDTO, $fieldObject);
    }
}
