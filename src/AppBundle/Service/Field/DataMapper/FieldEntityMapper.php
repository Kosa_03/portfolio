<?php

namespace AppBundle\Service\Field\DataMapper;

use AppBundle\Entity\Field\FieldType\FieldInterface;
use AppBundle\FormDTO\Field\FieldEntityDTO;
use AppBundle\Service\Field\Factory\FieldObjectDTOFactory;
use AppBundle\Service\Field\Factory\FieldObjectFactory;
use AppBundle\Service\Field\FieldObject;


/**
 * Class FieldEntityMapper
 */
class FieldEntityMapper
{
    private $fieldObjectFactory;
    private $fieldObjectDTOFactory;

    /**
     * FieldEntityMapper constructor.
     * @param FieldObjectFactory $fieldObjectFactory
     * @param FieldObjectDTOFactory $fieldObjectDTOFactory
     */
    public function __construct(FieldObjectFactory $fieldObjectFactory, FieldObjectDTOFactory $fieldObjectDTOFactory)
    {
        $this->fieldObjectFactory = $fieldObjectFactory;
        $this->fieldObjectDTOFactory = $fieldObjectDTOFactory;
    }

    /**
     * @param FieldEntityDTO $fieldEntityDTO
     * @param FieldObject $fieldObject
     */
    public function mapToFieldEntities(FieldEntityDTO $fieldEntityDTO, FieldObject $fieldObject) : void
    {
        $entitiesDTO = $fieldEntityDTO->getEntities();
        $fieldEntities = $fieldObject->getEntities();

        if ($entitiesDTO->isEmpty()) {
            return;
        }

        $fieldEntities->first();

        foreach ($entitiesDTO as $entityDTO) {
            if ($fieldEntities->current() == false) {
                $this->fieldObjectFactory->addEmptyEntity($fieldObject);
            }

            $reflection = new \ReflectionClass($entityDTO);
            $privateProperties = $reflection->getProperties(\ReflectionProperty::IS_PRIVATE);

            $this->mapDTOToEntity($fieldEntities->current(), $entityDTO, $privateProperties);

            $fieldEntities->next();
        }
    }

    /**
     * @param FieldInterface $fieldEntity
     * @param $entityDTO
     * @param array $privateProperties
     */
    private function mapDTOToEntity(FieldInterface $fieldEntity, $entityDTO, array $privateProperties) : void
    {
        foreach ($privateProperties as $property) {
            $propertyName = $property->getName();

            if ($propertyName != 'dataId') {
                $getValue = 'get' . strtoupper(substr($propertyName, 0, 1)) . substr($propertyName, 1);
                $setValue = 'set' . strtoupper(substr($propertyName, 0, 1)) . substr($propertyName, 1);

                if (method_exists($fieldEntity, $setValue) && method_exists($entityDTO, $getValue)) {
                    $fieldEntity->$setValue($entityDTO->$getValue());
                }
            }
        }
    }

    /**
     * @param FieldEntityDTO $fieldEntityDTO
     * @param FieldObject $fieldObject
     */
    public function mapToDTO(FieldObject $fieldObject, FieldEntityDTO $fieldEntityDTO) : void
    {
        $fieldEntitiesDTO = $fieldEntityDTO->getEntities();
        $fieldEntities = $fieldObject->getEntities();

        if ($fieldEntities->isEmpty()) {
            return;
        }

        $fieldClass = $fieldObject->getFieldClass();

        $fieldEntitiesDTO->first();

        foreach ($fieldEntities as $fieldEntity) {
            if ($fieldEntitiesDTO->current() == false) {
                $this->fieldObjectDTOFactory->addEmptyEntity($fieldEntityDTO, $fieldClass);
            }

            $reflection = new \ReflectionClass($fieldEntitiesDTO->current());
            $privateProperties = $reflection->getProperties(\ReflectionProperty::IS_PRIVATE);

            $this->mapEntityToDTO($fieldEntity, $fieldEntitiesDTO->current(), $privateProperties);

            $fieldEntitiesDTO->next();
        }
    }

    /**
     * @param FieldInterface $fieldEntity
     * @param $fieldEntityDTO
     * @param array $privateProperties
     */
    private function mapEntityToDTO(FieldInterface $fieldEntity, $fieldEntityDTO, array $privateProperties) : void
    {
        foreach ($privateProperties as $property) {
            $propertyName = $property->getName();

            $getValue = 'get' . strtoupper(substr($propertyName, 0, 1)) . substr($propertyName, 1);
            $setValue = 'set' . strtoupper(substr($propertyName, 0, 1)) . substr($propertyName, 1);

            if (method_exists($fieldEntityDTO, $setValue) && method_exists($fieldEntity, $getValue)) {
                $fieldEntityDTO->$setValue($fieldEntity->$getValue());
            }
        }
    }
}
