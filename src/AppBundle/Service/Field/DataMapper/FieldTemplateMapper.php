<?php

namespace AppBundle\Service\Field\DataMapper;

use AppBundle\Entity\Field\Template\FieldTemplate;
use AppBundle\FormDTO\Field\FieldTemplateDTO;
use AppBundle\Service\Field\FieldObject;


/**
 * Class FieldTemplateMapper
 */
class FieldTemplateMapper
{
    /**
     * @param FieldTemplateDTO $fieldTemplateDTO
     * @param FieldObject $fieldObject
     */
    public function mapToFieldTemplate(FieldTemplateDTO $fieldTemplateDTO, FieldObject $fieldObject) : void
    {
        /** @var FieldTemplate $fieldTemplate */
        $fieldTemplate = $fieldObject->getOrigTemplate();

        $fieldTemplate->setArrayCss($fieldTemplateDTO->getArrayCss());
        $fieldTemplate->setDividerBefore($fieldTemplateDTO->isDividerBefore());
        $fieldTemplate->setDividerAfter($fieldTemplateDTO->isDividerAfter());
        $fieldTemplate->setSmallDividerBefore($fieldTemplateDTO->isSmallDividerBefore());
        $fieldTemplate->setSmallDividerAfter($fieldTemplateDTO->isSmallDividerAfter());
        $fieldTemplate->setTwigFunctionParameters($fieldTemplateDTO->getTwigFunctionParameters());
    }

    /**
     * @param FieldObject $fieldObject
     * @param FieldTemplateDTO $fieldTemplateDTO
     */
    public function mapToDTO(FieldObject $fieldObject, FieldTemplateDTO $fieldTemplateDTO) : void
    {
        /** @var FieldTemplate $fieldTemplate */
        $fieldTemplate = $fieldObject->getOrigTemplate();

        $fieldTemplateDTO->setArrayCss($fieldTemplate->getArrayCss());
        $fieldTemplateDTO->setDividerBefore($fieldTemplate->isDividerBefore());
        $fieldTemplateDTO->setDividerAfter($fieldTemplate->isDividerAfter());
        $fieldTemplateDTO->setSmallDividerBefore($fieldTemplate->isSmallDividerBefore());
        $fieldTemplateDTO->setSmallDividerAfter($fieldTemplate->isSmallDividerAfter());
        $fieldTemplateDTO->setTwigFunctionParameters($fieldTemplate->getTwigFunctionParameters());
    }
}
