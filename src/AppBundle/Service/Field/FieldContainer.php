<?php

namespace AppBundle\Service\Field;

use AppBundle\Service\Field\Collection\FieldCollection;


/**
 * Class FieldContainer
 */
class FieldContainer
{
    private $collections = [];
    private $commandResults = [];

    /**
     * @param FieldCollection $fieldCollection
     * @param string $collectionName
     * @return bool
     */
    public function addCollection(FieldCollection $fieldCollection, string $collectionName) : bool
    {
        if (!key_exists($collectionName, $this->collections)) {
            $this->collections[$collectionName] = $fieldCollection;

            return true;
        }

        return false;
    }

    /**
     * @param string $collectionName
     * @return bool
     */
    public function removeCollection(string $collectionName) : bool
    {
        unset($this->collections[$collectionName]);

        return true;
    }

    /**
     * @param string $collectionName
     * @return FieldCollection|null
     */
    public function getCollection(string $collectionName) : ?FieldCollection
    {
        if (key_exists($collectionName, $this->collections)) {
            return $this->collections[$collectionName];
        }

        return null;
    }

    /**
     * @param string $collectionName
     * @return array
     */
    public function getArrayCollection(string $collectionName) : array
    {
        $collection = $this->getCollection($collectionName);

        if ($collection instanceof FieldCollection) {
            return $collection->getCollection();
        }

        return [];
    }

    /**
     * @param string $resultName
     * @param $result
     */
    public function addCommandResult(string $resultName, $result) : void
    {
        $this->commandResults[$resultName] = $result;
    }

    /**
     * @param string $resultName
     * @return mixed|null
     */
    public function getCommandResult(string $resultName)
    {
        if (array_key_exists($resultName, $this->commandResults)) {
            return $this->commandResults[$resultName];
        }

        return null;
    }
}
