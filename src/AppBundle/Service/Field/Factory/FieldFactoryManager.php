<?php

namespace AppBundle\Service\Field\Factory;


/**
 * Class FieldFactoryManager
 */
class FieldFactoryManager
{
    private $fieldObjectFactory;
    private $fieldObjectDTOFactory;
    private $formDTOFactory;

    /**
     * FieldFactoryManager constructor.
     * @param FieldObjectFactory $fieldObjectFactory
     * @param FieldObjectDTOFactory $fieldObjectDTOFactory
     * @param FormDTOFactory $formDTOFactory
     */
    public function __construct
    (
        FieldObjectFactory $fieldObjectFactory,
        FieldObjectDTOFactory $fieldObjectDTOFactory,
        FormDTOFactory $formDTOFactory
    )
    {
        $this->fieldObjectFactory = $fieldObjectFactory;
        $this->fieldObjectDTOFactory = $fieldObjectDTOFactory;
        $this->formDTOFactory = $formDTOFactory;
    }

    /**
     * @return FieldObjectFactory
     */
    public function getFieldObjectFactory() : FieldObjectFactory
    {
        return $this->fieldObjectFactory;
    }

    /**
     * @return FieldObjectDTOFactory
     */
    public function getFieldObjectDTOFactory() : FieldObjectDTOFactory
    {
        return $this->fieldObjectDTOFactory;
    }

    /**
     * @return FormDTOFactory
     */
    public function getFormDTOFactory() : FormDTOFactory
    {
        return $this->formDTOFactory;
    }
}
