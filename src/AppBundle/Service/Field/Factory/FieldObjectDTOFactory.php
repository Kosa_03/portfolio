<?php

namespace AppBundle\Service\Field\Factory;

use AppBundle\Entity\Field\FieldType\NotTranslatableFieldInterface;
use AppBundle\Entity\Field\FieldType\Translatable\TranslatableFieldInterface;
use AppBundle\FormDTO\Field\FieldConfigDTO;
use AppBundle\FormDTO\Field\FieldEntityDTO;
use AppBundle\FormDTO\Field\FieldTemplateDTO;


/**
 * Class FieldObjectDTOFactory
 */
class FieldObjectDTOFactory
{
    /**
     * @return FieldConfigDTO
     */
    public function createFieldConfig() : FieldConfigDTO
    {
        return new FieldConfigDTO();
    }

    /**
     * @return FieldTemplateDTO
     */
    public function createFieldTemplate() : FieldTemplateDTO
    {
        return new FieldTemplateDTO();
    }

    /**
     * @return FieldEntityDTO
     */
    public function createFieldEntity() : FieldEntityDTO
    {
        return new FieldEntityDTO();
    }

    /**
     * @param FieldEntityDTO $fieldEntityDTO
     * @param string $fieldClass
     * @return bool
     */
    public function addEmptyEntity(FieldEntityDTO $fieldEntityDTO, string $fieldClass) : bool
    {
        $fieldEntity = $this->getEntityDTOPath($fieldClass);

        if (array_key_exists(TranslatableFieldInterface::class, class_implements($fieldClass))) {
            $fieldEntityDTO->getEntities()->add(new $fieldEntity());
            return true;

        } elseif (array_key_exists(NotTranslatableFieldInterface::class, class_implements($fieldClass))) {
            if ($fieldEntityDTO->getEntities()->isEmpty()) {
                $fieldEntityDTO->getEntities()->add(new $fieldEntity());
                return true;
            }
        }

        return false;
    }

    /**
     * @param FieldEntityDTO $fieldEntityDTO
     * @param int $entityKey
     */
    public function deleteEntity(FieldEntityDTO $fieldEntityDTO, int $entityKey) : void
    {
        $fieldEntityDTO->getEntities()->remove($entityKey);
    }

    /**
     * @param string $fieldClass
     * @return string
     */
    private function getEntityDTOPath(string $fieldClass) : string
    {
        $arrayFieldClass = explode('\\', $fieldClass);
        $reverseArrayFieldClass = array_reverse($arrayFieldClass);

        $fieldClass = array_slice($reverseArrayFieldClass, 0, 1)[0];
        $fieldType = array_slice($reverseArrayFieldClass, 1, 1)[0];

        return 'AppBundle\FormDTO\Field\FieldType\\'
            . $fieldType . '\\' . $fieldClass . 'DTO';
    }
}
