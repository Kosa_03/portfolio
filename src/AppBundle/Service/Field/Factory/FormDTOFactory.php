<?php

namespace AppBundle\Service\Field\Factory;

use AppBundle\FormDTO\Field\SearchFormDTO;


/**
 * Class FormDTOFactory
 */
class FormDTOFactory
{
    /**
     * @return SearchFormDTO
     */
    public function createSearchForm() : SearchFormDTO
    {
        return new SearchFormDTO();
    }
}
