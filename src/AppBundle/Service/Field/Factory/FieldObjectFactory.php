<?php

namespace AppBundle\Service\Field\Factory;

use AppBundle\Entity\Field\FieldConfig;
use AppBundle\Entity\Field\FieldData;
use AppBundle\Entity\Field\FieldType\Component\NullField;
use AppBundle\Entity\Field\FieldType\FieldInterface;
use AppBundle\Entity\Field\FieldType\NotTranslatableFieldInterface;
use AppBundle\Entity\Field\FieldType\Translatable\TranslatableFieldInterface;
use AppBundle\Entity\Field\Group\FieldGroup;
use AppBundle\Entity\Field\Template\FieldTemplate;
use AppBundle\Service\Field\FieldEntityStrategy\StrategyManager;
use AppBundle\Service\Field\FieldObject;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;


/**
 * Class FieldObjectFactory
 */
class FieldObjectFactory
{
    private $entityManager;
    private $strategyManager;
    private $user;

    /**
     * FieldObjectFactory constructor.
     * @param EntityManagerInterface $entityManager
     * @param StrategyManager $strategyManager
     * @param Security $security
     */
    public function __construct(EntityManagerInterface $entityManager, StrategyManager $strategyManager, Security $security)
    {
        $this->entityManager = $entityManager;
        $this->strategyManager = $strategyManager;
        $this->user = $security->getUser();
    }

    /**
     * @return FieldObject
     */
    public function createEmptyFieldObject() : FieldObject
    {
        $fieldConfig = new FieldConfig();
        $fieldTemplate = new FieldTemplate();

        $fieldObject = new FieldObject($fieldConfig);
        $fieldObject->setOrigTemplate($fieldTemplate);

        return $fieldObject;
    }

    /**
     * @param FieldConfig $fieldConfig
     * @return FieldObject
     */
    public function createFieldObject(FieldConfig $fieldConfig) : FieldObject
    {
        $fieldObject = new FieldObject($fieldConfig);

        $arrayFieldData = $this->findFieldData($fieldConfig->getId());

        foreach ($arrayFieldData as $fieldData) {
            $fieldEntity = $this->findFieldEntity(
                $fieldData->getDataId(),
                $fieldConfig->getFieldClass()
            );

            if (!($fieldEntity instanceof NullField)) {
                $fieldObject->getEntities()->add($fieldEntity);
            }
        }

        $this->setExtraProperties($fieldObject);

        return $fieldObject;
    }

    /**
     * @param FieldObject $fieldObject
     * @return bool
     */
    public function addEmptyEntity(FieldObject $fieldObject) : bool
    {
        $fieldClass = $fieldObject->getFieldClass();

        /** @var FieldInterface $fieldEntity */
        $fieldEntity = new $fieldClass();
        $fieldEntity->setModified(new \DateTime());
        $fieldEntity->setUserId($this->user->getId());

        if (array_key_exists(TranslatableFieldInterface::class, class_implements($fieldClass))) {
            $fieldObject->getEntities()->add($fieldEntity);
            return true;

        } elseif (array_key_exists(NotTranslatableFieldInterface::class, class_implements($fieldClass))) {
            if ($fieldObject->getEntities()->isEmpty()) {
                $fieldObject->getEntities()->add($fieldEntity);
                return true;
            }
        }

        return false;
    }

    /**
     * @param int $configId
     * @return array
     */
    private function findFieldData(int $configId) : array
    {
        $fieldData = $this->entityManager->getRepository(FieldData::class)
            ->findByConfigId($configId);

        return $fieldData;
    }

    /**
     * @param int $dataId
     * @param string $fieldType
     * @return FieldInterface
     */
    private function findFieldEntity(int $dataId, string $fieldType) : FieldInterface
    {
        if (!class_exists($fieldType)) {
            return new NullField();
        }

        $fieldContent = $this->entityManager->getRepository($fieldType)
            ->findByDataId($dataId);

        return $fieldContent;
    }

    /**
     * @param FieldObject $fieldObject
     */
    private function setExtraProperties(FieldObject $fieldObject) : void
    {
        $fieldConfig = $fieldObject->getConfig();

        $this->setTemplate($fieldObject, $fieldConfig->getTemplateId());
        $this->setGroup($fieldObject, $fieldConfig->getGroupId());

        $this->strategyManager->selectStrategy($fieldObject);
    }

    /**
     * @param FieldObject $fieldObject
     * @param int $templateId
     */
    private function setTemplate(FieldObject $fieldObject, int $templateId) : void
    {
        $fieldTemplate = $this->entityManager->getRepository(FieldTemplate::class)
            ->findByTemplateId($templateId);

        $fieldObject->setTemplate($fieldTemplate);
        $fieldObject->setOrigTemplate($fieldTemplate);
    }

    /**
     * @param FieldObject $fieldObject
     * @param int $groupId
     */
    private function setGroup(FieldObject $fieldObject, int $groupId) : void
    {
        $fieldGroup = $this->entityManager->getRepository(FieldGroup::class)
            ->findByGroupId($groupId);

        $fieldObject->setGroup($fieldGroup);
    }
}
