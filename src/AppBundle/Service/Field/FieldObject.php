<?php

namespace AppBundle\Service\Field;

use AppBundle\Entity\Field\FieldConfig;
use AppBundle\Entity\Field\FieldType\FieldInterface;
use AppBundle\Entity\Field\Group\FieldGroupInterface;
use AppBundle\Entity\Field\Template\FieldTemplateInterface;
use AppBundle\Service\Field\FieldEntityStrategy\StrategyInterface;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Class FieldObject
 */
class FieldObject
{
    private $config;
    private $template;
    private $origTemplate;
    private $group;
    private $strategyClass;
    private $externalEntity;
    private $entity;
    private $entities;

    /**
     * FieldObject constructor.
     * @param FieldConfig $fieldConfig
     */
    public function __construct(FieldConfig $fieldConfig)
    {
        $this->config = $fieldConfig;
        $this->entities = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getFieldClass() : string
    {
        return $this->config->getFieldClass();
    }

    /**
     * @return string
     */
    public function getFieldName() : string
    {
        return $this->config->getFieldName();
    }

    /**
     * @return FieldInterface
     */
    public function getField() : FieldInterface
    {
        return $this->getEntity();
    }

    public function getExtField()
    {
        return $this->getExternalEntity();
    }

/*
 * Getters and setters
 */

    /**
     * @return FieldConfig
     */
    public function getConfig() : FieldConfig
    {
        return $this->config;
    }

    /**
     * @return FieldTemplateInterface
     */
    public function getTemplate() : FieldTemplateInterface
    {
        return $this->template;
    }

    /**
     * @param FieldTemplateInterface $template
     */
    public function setTemplate(FieldTemplateInterface $template) : void
    {
        $this->template = $template;
    }

    /**
     * @return FieldTemplateInterface
     */
    public function getOrigTemplate() : ?FieldTemplateInterface
    {
        return $this->origTemplate;
    }

    /**
     * @param FieldTemplateInterface $template
     */
    public function setOrigTemplate(FieldTemplateInterface $template) : void
    {
        $this->origTemplate = $template;
    }

    /**
     * @return FieldGroupInterface
     */
    public function getGroup() : FieldGroupInterface
    {
        return $this->group;
    }

    /**
     * @param FieldGroupInterface $group
     */
    public function setGroup(FieldGroupInterface $group) : void
    {
        $this->group = $group;
    }

    /**
     * @return StrategyInterface
     */
    public function getCreateEntityStrategy() : StrategyInterface
    {
        return $this->strategyClass;
    }

    /**
     * @param StrategyInterface $strategyClass
     */
    public function setCreateEntityStrategy(StrategyInterface $strategyClass) : void
    {
        $this->strategyClass = $strategyClass;
    }

    /**
     * @return mixed
     */
    public function getExternalEntity()
    {
        return $this->externalEntity;
    }

    /**
     * @param $externalEntity
     * @return FieldObject
     */
    public function setExternalEntity($externalEntity) : FieldObject
    {
        $this->externalEntity = $externalEntity;
        return $this;
    }

    /**
     * @return FieldInterface
     */
    public function getEntity() : FieldInterface
    {
        return $this->entity;
    }

    /**
     * @param FieldInterface $entity
     */
    public function setEntity(FieldInterface $entity) : void
    {
        $this->entity = $entity;
    }

    /**
     * @return ArrayCollection
     */
    public function getEntities() : ArrayCollection
    {
        return $this->entities;
    }
}
