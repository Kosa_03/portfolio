<?php

namespace AppBundle\Service\Field;

use AppBundle\Service\Field\Collection\FieldCollection;
use AppBundle\Service\Field\Command\CommandInterface;
use AppBundle\Service\Field\DataMapper\FieldObjectMapper;
use AppBundle\Service\Field\Factory\FieldFactoryManager;
use AppBundle\Service\Field\Options\FieldListOptions;
use AppBundle\Service\Field\Options\FieldObjectOptions;


/**
 * Class FieldManager
 */
class FieldManager
{
    /** @var CommandInterface */
    private $command;

    private $fieldListOptions;
    private $fieldObjectOptions;

    private $fieldFactoryManager;
    private $fieldObjectMapper;
    private $fieldContainer;

    /**
     * FieldManager constructor.
     * @param FieldListOptions $listOptions
     * @param FieldObjectOptions $fieldOptions
     * @param FieldFactoryManager $factoryManager
     * @param FieldObjectMapper $fieldMapper
     * @param FieldContainer $fieldContainer
     */
    public function __construct
    (
        FieldListOptions $listOptions,
        FieldObjectOptions $fieldOptions,
        FieldFactoryManager $factoryManager,
        FieldObjectMapper $fieldMapper,
        FieldContainer $fieldContainer
    )
    {
        $this->fieldListOptions = $listOptions;
        $this->fieldObjectOptions = $fieldOptions;
        $this->fieldFactoryManager = $factoryManager;
        $this->fieldObjectMapper = $fieldMapper;
        $this->fieldContainer = $fieldContainer;
    }

    /**
     * @param CommandInterface $command
     */
    public function setCommand(CommandInterface $command) : void
    {
        $this->command = $command;
    }

    /**
     * @param string $resultName
     * @return mixed|null
     */
    public function getCmdResult(string $resultName)
    {
        return $this->fieldContainer->getCommandResult($resultName);
    }

    public function run() : void
    {
        if ($this->command instanceof CommandInterface) {
            $this->command->execute();

        } else {
            throw new \RuntimeException('Invalid Command to run.');
        }
    }

    /**
     * @return FieldListOptions
     */
    public function getListOptions() : FieldListOptions
    {
        return $this->fieldListOptions;
    }

    /**
     * @return FieldObjectOptions
     */
    public function getFieldOptions() : FieldObjectOptions
    {
        return $this->fieldObjectOptions;
    }

    /**
     * @return FieldFactoryManager
     */
    public function getFactory() : FieldFactoryManager
    {
        return $this->fieldFactoryManager;
    }

    /**
     * @return FieldObjectMapper
     */
    public function getMapper() : FieldObjectMapper
    {
        return $this->fieldObjectMapper;
    }

    /**
     * @return FieldContainer
     */
    public function getContainer() : FieldContainer
    {
        return $this->fieldContainer;
    }

    /**
     * @param string $collectionName
     * @return FieldCollection|null
     */
    public function getCollection(string $collectionName) : ?FieldCollection
    {
        return $this->fieldContainer->getCollection($collectionName);
    }

    /**
     * @param string $collectionName
     * @return array
     */
    public function getArrayCollection(string $collectionName) : array
    {
        return $this->fieldContainer->getArrayCollection($collectionName);
    }
}
