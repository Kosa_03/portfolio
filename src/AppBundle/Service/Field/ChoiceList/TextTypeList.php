<?php

namespace AppBundle\Service\Field\ChoiceList;


/**
 * Class TextTypeList
 */
class TextTypeList
{
    public static function getList() : array
    {
        return self::textTypeList();
    }

    private function textTypeList() : array
    {
        $list = [
            'Text (label: value)' => 'text',
            'Date (label: d-m-Y)' => 'date',
            'DateTime (label: d-m-Y H:i:s)' => 'date_time',
            'Url (value as href)' => 'url',
            'Value (without label)' => 'value'
        ];

        return $list;
    }
}
