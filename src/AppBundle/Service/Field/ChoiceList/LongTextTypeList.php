<?php

namespace AppBundle\Service\Field\ChoiceList;


/**
 * Class LongTextTypeList
 */
class LongTextTypeList
{
    public static function getList() : array
    {
        return self::longTextTypeList();
    }

    private function longTextTypeList() : array
    {
        $list = [
            'Filtered HTML' => 'filtered_html',
            'Plain text' => 'plain_text'
        ];

        return $list;
    }
}
