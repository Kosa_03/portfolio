<?php

namespace AppBundle\Service\Field\ChoiceList;

use AppBundle\Entity\Field\FieldType\Component\PaginationField;
use AppBundle\Entity\Field\FieldType\Entity\BlogEntityField;
use AppBundle\Entity\Field\FieldType\File\FileField;
use AppBundle\Entity\Field\FieldType\File\ImageField;
use AppBundle\Entity\Field\FieldType\Form\FormField;
use AppBundle\Entity\Field\FieldType\Translatable\HeaderField;
use AppBundle\Entity\Field\FieldType\Translatable\ListField;
use AppBundle\Entity\Field\FieldType\Translatable\LongTextField;
use AppBundle\Entity\Field\FieldType\Translatable\SmallHeaderField;
use AppBundle\Entity\Field\FieldType\Translatable\TextField;


/**
 * Class FieldList
 */
class FieldList
{
    public static function getList() : array
    {
        return self::fieldList();
    }

    private function fieldList() : array
    {
        $list = [
            'Translatable' => [
                'HeaderField' => HeaderField::class,
                'ListField' => ListField::class,
                'LongTextField' => LongTextField::class,
                'SmallHeaderField' => SmallHeaderField::class,
                'TextField' => TextField::class
            ],
            'Entity' => [
                'BlogEntityField' => BlogEntityField::class
            ],
            'File' => [
                'FileField' => FileField::class,
                'ImageField' => ImageField::class
            ],
            'Form' => [
                'FormField' => FormField::class
            ],
            'Component' => [
                'PaginationField' => PaginationField::class
            ]
        ];

        return $list;
    }
}
