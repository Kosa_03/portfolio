<?php

namespace AppBundle\Service\Field\ChoiceList;

use AppBundle\Controller\AboutMeController;
use AppBundle\Controller\AccountController;
use AppBundle\Controller\Admin\AdminPanelController;
use AppBundle\Controller\Admin\Blog\BlogAddController;
use AppBundle\Controller\Admin\Blog\BlogDeleteController;
use AppBundle\Controller\Admin\Blog\BlogEditController;
use AppBundle\Controller\Admin\Blog\BlogListController;
use AppBundle\Controller\Admin\Blog\BlogPreviewController;
use AppBundle\Controller\Admin\Blog\BlogTagDeleteController;
use AppBundle\Controller\Admin\Blog\BlogTagListController;
use AppBundle\Controller\Admin\Field\FieldAddController;
use AppBundle\Controller\Admin\Field\FieldDeleteController;
use AppBundle\Controller\Admin\Field\FieldEditController;
use AppBundle\Controller\Admin\Field\FieldListController;
use AppBundle\Controller\Admin\Field\FieldNotFoundController;
use AppBundle\Controller\Admin\Field\FieldShowController;
use AppBundle\Controller\BlogArticleController;
use AppBundle\Controller\BlogArticleListController;
use AppBundle\Controller\ContactController;
use AppBundle\Controller\ExperienceController;
use AppBundle\Controller\HomeController;
use AppBundle\Controller\SecurityController;


/**
 * Class ControllerList
 */
class ControllerList
{
    public static function getList() : array
    {
        return self::controllerList();
    }

    private function controllerList() : array
    {
        $list = [
            'AboutMeController' => AboutMeController::class,
            'AccountController' => AccountController::class,
            'AdminPanelController' => AdminPanelController::class,
            'ContactController' => ContactController::class,
            'ExperienceController' => ExperienceController::class,
            'HomeController' => HomeController::class,
            'SecurityController' => SecurityController::class,
            'BlogArticleController' => BlogArticleController::class,
            'BlogArticleListController' => BlogArticleListController::class,
            'BlogAddController' => BlogAddController::class,
            'BlogDeleteController' => BlogDeleteController::class,
            'BlogEditController' => BlogEditController::class,
            'BlogListController' => BlogListController::class,
            'BlogPreviewController' => BlogPreviewController::class,
            'BlogTagDeleteController' => BlogTagDeleteController::class,
            'BlogTagListController' => BlogTagListController::class,
            'FieldAddController' => FieldAddController::class,
            'FieldDeleteController' => FieldDeleteController::class,
            'FieldEditController' => FieldEditController::class,
            'FieldListController' => FieldListController::class,
            'FieldNotFoundController' => FieldNotFoundController::class,
            'FieldShowController' => FieldShowController::class,
        ];

        return $list;
    }
}
