<?php

namespace AppBundle\Service\Field\ChoiceList;

use AppBundle\Service\Blog\EntityLoader\BlogArticleListLoader;
use AppBundle\Service\Blog\EntityLoader\BlogArticleLoader;
use AppBundle\Service\Blog\Factory\BlogEntityLoaderFactory;


/**
 * Class EntityLoaderList
 */
class EntityLoaderList
{
    private static $entityLoaderFactory;

    /**
     * @param string $entityLoaderFactory
     * @return array
     */
    public static function getList(string $entityLoaderFactory) : array
    {
        self::$entityLoaderFactory = $entityLoaderFactory;

        return self::entityLoaderList();
    }

    /**
     * @return array
     */
    private static function entityLoaderList() : array
    {
        $list = [
            BlogEntityLoaderFactory::class => [
                'BlogArticleLoader' => BlogArticleLoader::class,
                'BlogArticleListLoader' => BlogArticleListLoader::class
            ]
        ];

        if (key_exists(self::$entityLoaderFactory, $list)) {
            return $list[self::$entityLoaderFactory];
        }

        return [];
    }
}
