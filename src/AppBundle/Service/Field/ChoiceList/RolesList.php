<?php

namespace AppBundle\Service\Field\ChoiceList;


/**
 * Class RolesList
 */
class RolesList
{
    public static function getList() : array
    {
        return self::rolesList();
    }

    private function rolesList() : array
    {
        $list = [
            'IS_AUTHENTICATED_ANONYMOUSLY' => 'IS_AUTHENTICATED_ANONYMOUSLY',
            'IS_AUTHENTICATED_FULLY' => 'IS_AUTHENTICATED_FULLY',
            'ROLE_USER' => 'ROLE_USER',
            'ROLE_ADMIN' => 'ROLE_ADMIN'
        ];

        return $list;
    }
}
