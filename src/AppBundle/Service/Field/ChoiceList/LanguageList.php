<?php

namespace AppBundle\Service\Field\ChoiceList;


/**
 * Class LanguageList
 */
class LanguageList
{
    public static function getList() : array
    {
        return self::languageList();
    }

    private function languageList() : array
    {
        $list = [
            'Default' => 'und',
            'Polish' => 'pl',
            'English' => 'en'
        ];

        return $list;
    }
}
