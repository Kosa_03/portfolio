<?php

namespace AppBundle\Service\Field\Replacer;

use AppBundle\Entity\Field\FieldType\Translatable\TranslatableFieldInterface;
use AppBundle\Service\Field\FieldObject;


/**
 * Class PatternReplacer
 */
class PatternReplacer implements ReplacerInterface
{
    /** @var TranslatableFieldInterface */
    private $fieldItem;

    private $fieldName;
    private $fieldPatterns;

    private $originalText;
    private $replacedText;

    /**
     * @param FieldObject $fieldObject
     * @param array $fieldPatterns
     */
    public function replacePatterns(FieldObject $fieldObject, array $fieldPatterns) : void
    {
        $this->fieldItem = $fieldObject->getField();
        $this->fieldName = $fieldObject->getFieldClass();

        $this->fieldPatterns = $fieldPatterns;
        $this->originalText = null;
        $this->replacedText = null;

        if ($this->fieldItem instanceof TranslatableFieldInterface) {
            $this->originalText = $this->fieldItem->getContent();
            $this->replacedText = $this->originalText;

            $this->makeReplacePatterns();

            $this->fieldItem->setContent($this->replacedText);
        }
    }

    private function makeReplacePatterns() : void
    {
        foreach ($this->fieldPatterns as $pattern => $replacement) {
            if (is_string($this->originalText)) {
                $this->replacePatternsInText($pattern, $replacement);
            } elseif (is_array($this->originalText)) {
                $this->replacePatternsInArray($pattern, $replacement);
            } else {
                $this->replacedText = $this->originalText;
            }
        }
    }

    /**
     * @param string $pattern
     * @param mixed $replacement
     */
    private function replacePatternsInArray(string $pattern, $replacement) : void
    {
        foreach ($this->originalText as $key => $value) {
            $this->replacePatternsInText($pattern, $replacement, $key);
        }
    }

    /**
     * @param string $pattern
     * @param mixed $replacement
     * @param mixed $key
     */
    private function replacePatternsInText(string $pattern, $replacement, $key = null) : void
    {
        switch (true) {
            case is_array($replacement) :
                $replacement = implode(', ', $replacement);
                break;

            case ($replacement instanceof \DateTime) :
                $replacement = $replacement->getTimestamp();
                break;
        }

        if (!is_null($key)) {
            $this->replacedText[$key] = str_replace($pattern, $replacement, $this->originalText[$key]);
        } else {
            $this->replacedText = str_replace($pattern, $replacement, $this->originalText);
        }
    }
}
