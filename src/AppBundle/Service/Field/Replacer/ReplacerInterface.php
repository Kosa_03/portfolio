<?php

namespace AppBundle\Service\Field\Replacer;

use AppBundle\Service\Field\FieldObject;


/**
 * Interface ReplacerInterface
 */
interface ReplacerInterface
{
    public function replacePatterns(FieldObject $fieldObject, array $fieldsPatterns) : void;
}
