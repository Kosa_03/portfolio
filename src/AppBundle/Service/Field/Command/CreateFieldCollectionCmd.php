<?php

namespace AppBundle\Service\Field\Command;

use AppBundle\Entity\Field\Group\NullGroupTemplate;
use AppBundle\Service\Field\Collection\FieldCollection;
use AppBundle\Service\Field\FieldContainer;
use AppBundle\Service\Field\FieldObject;
use AppBundle\Service\Field\Options\FieldListOptions;
use AppBundle\Service\Field\Options\FieldObjectOptions;


/**
 * Class CreateFieldCollectionCmd
 */
class CreateFieldCollectionCmd implements CommandInterface
{
    /** @var FieldListOptions */
    private $fieldListOptions;
    /** @var FieldObjectOptions */
    private $fieldObjectOptions;

    /** @var CreateFieldCmd */
    private $createFieldCmd;
    /** @var FindByFieldListOptionsCmd */
    private $findByFieldListOptionsCmd;

    private $collectionName;
    /** @var FieldCollection */
    private $fieldCollection;
    private $fieldContainer;

    /**
     * CreateFieldCollectionCmd constructor.
     * @param FieldContainer $fieldContainer
     */
    public function __construct(FieldContainer $fieldContainer)
    {
        $this->fieldContainer = $fieldContainer;
    }

    /**
     * @required
     * @param FieldListOptions $listOptions
     * @param FieldObjectOptions $objectOptions
     */
    public function setOptions(FieldListOptions $listOptions, FieldObjectOptions $objectOptions) : void
    {
        /**
         * TODO: Remove this options, options not used.
         */
        $this->fieldListOptions = $listOptions;
        $this->fieldObjectOptions = $objectOptions;
    }

    /**
     * @required
     * @param CreateFieldCmd $createFieldCmd
     * @param FindByFieldListOptionsCmd $findByFieldListOptionsCmd
     */
    public function setCommands(CreateFieldCmd $createFieldCmd, FindByFieldListOptionsCmd $findByFieldListOptionsCmd) : void
    {
        $this->createFieldCmd = $createFieldCmd;
        $this->findByFieldListOptionsCmd = $findByFieldListOptionsCmd;
    }

    /**
     * @param string $collectionName
     */
    public function setCollectionName(string $collectionName) : void
    {
        $this->collectionName = $collectionName;
    }

    public function execute() : void
    {
        $this->fieldCollection = new FieldCollection();
        $this->fieldCollection->setTemplate(new NullGroupTemplate());

        $this->createCollection();

        if (is_null($this->collectionName)) {
            $this->collectionName = 'default_collection';
        }

        $this->fieldContainer->addCollection($this->fieldCollection, $this->collectionName);
    }

    private function createCollection() : void
    {
        $this->findByFieldListOptionsCmd->execute();
        $fieldConfigList = $this->fieldContainer->getCommandResult('fieldConfigList');

        foreach ($fieldConfigList as $fieldConfig) {
            $this->createFieldCmd->setFieldConfig($fieldConfig);
            $this->createFieldCmd->execute();

            $fieldObject = $this->fieldContainer->getCommandResult('fieldObject');

            if ($fieldObject instanceof FieldObject) {
                $this->addToCollection($fieldObject);
            }
        }
    }

    /**
     * @param FieldObject $fieldObject
     */
    private function addToCollection(FieldObject $fieldObject) : void
    {
        $this->fieldCollection->addField($fieldObject);
    }
}
