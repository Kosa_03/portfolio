<?php

namespace AppBundle\Service\Field\Command;

use AppBundle\Entity\Field\FieldConfig;
use AppBundle\Service\Field\FieldContainer;
use AppBundle\Service\Field\Options\FieldListOptions;
use Doctrine\ORM\EntityManagerInterface;


/**
 * Class FindByFieldListOptionsCmd
 */
class FindByFieldListOptionsCmd implements CommandInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    private $fieldContainer;
    private $listOptions;
    
    private $fieldConfigList;

    /**
     * CreateFieldConfigListCmd constructor.
     * @param FieldContainer $fieldContainer
     * @param FieldListOptions $listOptions
     */
    public function __construct(FieldContainer $fieldContainer, FieldListOptions $listOptions)
    {
        $this->fieldContainer = $fieldContainer;
        $this->listOptions = $listOptions;
    }

    /**
     * @required
     * @param EntityManagerInterface $entityManager
     */
    public function setEntityManager(EntityManagerInterface $entityManager) : void
    {
        $this->entityManager = $entityManager;
    }

    public function execute() : void
    {
        $this->createList();

        $this->fieldContainer->addCommandResult('fieldConfigList', $this->fieldConfigList);
    }

    private function createList() : void
    {
        $this->fieldConfigList = $this->entityManager->getRepository(FieldConfig::class)
            ->findByFieldListOptions($this->listOptions);
    }
}
