<?php

namespace AppBundle\Service\Field\Command;

use AppBundle\Entity\Field\FieldData;
use AppBundle\Service\Field\FieldContainer;
use AppBundle\Service\Field\FieldObject;
use Doctrine\ORM\EntityManagerInterface;


/**
 * Class InsertFieldCmd
 */
class InsertFieldCmd implements CommandInterface
{
    private $entityManager;
    private $fieldContainer;

    /** @var FieldObject */
    private $fieldObject;

    /**
     * UpdateFieldCommand constructor.
     * @param EntityManagerInterface $entityManager
     * @param FieldContainer $fieldContainer
     */
    public function __construct(EntityManagerInterface $entityManager, FieldContainer $fieldContainer)
    {
        $this->entityManager = $entityManager;
        $this->fieldContainer = $fieldContainer;
    }

    /**
     * @param FieldObject $fieldObject
     */
    public function setFieldObject(FieldObject $fieldObject) : void
    {
        $this->fieldObject = $fieldObject;
    }

    public function execute() : void
    {
        $this->insert();
    }

    private function insert() : void
    {
        $this->entityManager->getConnection()->beginTransaction();

        $fieldConfig = $this->fieldObject->getConfig();
        $fieldTemplate = $this->fieldObject->getOrigTemplate();
        $fieldEntities = $this->fieldObject->getEntities();

        try {
            $this->entityManager->persist($fieldTemplate);
            $this->entityManager->flush();

            $fieldConfig->setTemplateId($fieldTemplate->getId());
            $this->entityManager->persist($fieldConfig);
            $this->entityManager->flush();

            foreach ($fieldEntities as $fieldEntity) {
                $this->entityManager->persist($fieldEntity);
                $this->entityManager->flush();

                $fieldData = new FieldData();
                $fieldData->setConfigId($fieldConfig->getId());
                $fieldData->setDataId($fieldEntity->getDataId());
                $this->entityManager->persist($fieldData);
                $this->entityManager->flush();
            }

            $this->entityManager->getConnection()->commit();

            $this->fieldContainer->addCommandResult('insertError', false);

        } catch (\Exception $e) {
            $this->entityManager->rollback();

            $this->fieldContainer->addCommandResult('insertError', true);
        }
    }
}