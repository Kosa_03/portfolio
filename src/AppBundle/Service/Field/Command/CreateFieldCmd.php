<?php

namespace AppBundle\Service\Field\Command;

use AppBundle\Entity\Field\FieldConfig;
use AppBundle\Service\Field\Factory\FieldObjectFactory;
use AppBundle\Service\Field\FieldContainer;
use AppBundle\Service\Field\Options\FieldObjectOptions;
use AppBundle\Service\Field\Replacer\ReplacerInterface;


/**
 * Class CreateFieldCmd
 */
class CreateFieldCmd implements CommandInterface
{
    private $fieldContainer;
    private $fieldOptions;
    
    /** @var FieldObjectFactory */
    private $fieldFactory;
    /** @var ReplacerInterface */
    private $patternReplacer;

    private $fieldConfig;

    /**
     * CreateFieldCmd constructor.
     * @param FieldContainer $fieldContainer
     * @param FieldObjectOptions $fieldOptions
     */
    public function __construct(FieldContainer $fieldContainer, FieldObjectOptions $fieldOptions)
    {
        $this->fieldContainer = $fieldContainer;
        $this->fieldOptions = $fieldOptions;
    }

    /**
     * @required
     * @param FieldObjectFactory $fieldFactory
     */
    public function setFactory(FieldObjectFactory $fieldFactory) : void
    {
        $this->fieldFactory = $fieldFactory;
    }

    /**
     * @required
     * @param ReplacerInterface $patternReplacer
     */
    public function setReplacer(ReplacerInterface $patternReplacer) : void
    {
        $this->patternReplacer = $patternReplacer;
    }

    /**
     * @param FieldConfig $fieldConfig
     */
    public function setFieldConfig(FieldConfig $fieldConfig) : void
    {
        $this->fieldConfig = $fieldConfig;
    }

    public function execute() : void
    {
        $this->createFieldObject();
    }

    private function createFieldObject() : void
    {
        $patterns = $this->fieldOptions->getPatterns();

        $fieldObject = $this->fieldFactory->createFieldObject($this->fieldConfig);
        $fieldName = $fieldObject->getFieldName();

        $fieldObject->getCreateEntityStrategy()->createFieldEntity($fieldObject);

        if (key_exists($fieldName, $patterns)) {
            $this->patternReplacer->replacePatterns($fieldObject, $patterns[$fieldName]);
        }

        $this->fieldContainer->addCommandResult('fieldObject', $fieldObject);
    }
}
