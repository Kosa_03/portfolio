<?php

namespace AppBundle\Service\Field\Command;

use AppBundle\Entity\Field\FieldConfig;
use AppBundle\Service\Field\FieldContainer;
use AppBundle\Service\Field\Options\FieldListOptions;
use Doctrine\ORM\EntityManagerInterface;


/**
 * Class CountByFieldListOptionsCmd
 */
class CountByFieldListOptionsCmd implements CommandInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    private $listOptions;
    private $fieldContainer;

    private $amountFields;

    /**
     * CountByFieldListOptionsCmd constructor.
     * @param FieldContainer $fieldContainer
     * @param FieldListOptions $listOptions
     */
    public function __construct(FieldContainer $fieldContainer, FieldListOptions $listOptions)
    {
        $this->fieldContainer = $fieldContainer;
        $this->listOptions = $listOptions;
    }

    /**
     * @required
     * @param EntityManagerInterface $entityManager
     */
    public function setEntityManager(EntityManagerInterface $entityManager) : void
    {
        $this->entityManager = $entityManager;
    }

    public function execute() : void
    {
        $this->countFields();

        $this->fieldContainer->addCommandResult('amount_fields', $this->amountFields);
    }

    public function countFields() : void
    {
        $this->amountFields = $this->entityManager->getRepository(FieldConfig::class)
            ->countByFieldListOption($this->listOptions);
    }
}
