<?php

namespace AppBundle\Service\Field\Command;

use AppBundle\Entity\Field\FieldData;
use AppBundle\Service\Field\Factory\FieldObjectDTOFactory;
use AppBundle\Service\Field\Factory\FormDTOFactory;
use AppBundle\Service\Field\FieldContainer;
use AppBundle\Service\Field\FieldObject;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


/**
 * Class DeleteFieldEntityCmd
 */
class DeleteFieldEntityCmd implements CommandInterface
{
    private $session;
    private $entityManager;
    private $fieldObjectDTOFactory;
    private $fieldContainer;

    /** @var FieldObject */
    private $fieldObject;
    private $dataId;

    /**
     * DeleteFieldEntityCmd constructor.
     * @param SessionInterface $session
     * @param EntityManagerInterface $entityManager
     * @param FieldObjectDTOFactory $fieldObjectDTOFactory
     * @param FieldContainer $fieldContainer
     */
    public function __construct(
        SessionInterface $session,
        EntityManagerInterface $entityManager,
        FieldObjectDTOFactory $fieldObjectDTOFactory,
        FieldContainer $fieldContainer
    )
    {
        $this->session = $session;
        $this->entityManager = $entityManager;
        $this->fieldObjectDTOFactory = $fieldObjectDTOFactory;
        $this->fieldContainer = $fieldContainer;
    }

    /**
     * @param FieldObject $fieldObject
     * @param int $dataId
     */
    public function setFieldEntity(FieldObject $fieldObject, int $dataId) : void
    {
        $this->fieldObject = $fieldObject;
        $this->dataId = $dataId;
    }

    public function execute() : void
    {
        $this->deleteFieldEntity();

        $this->fieldObject = null;
        $this->dataId = null;
    }

    private function deleteFieldEntity() : void
    {
        $this->entityManager->getConnection()->beginTransaction();

        $fieldConfig = $this->fieldObject->getConfig();
        $fieldEntities = $this->fieldObject->getEntities();

        try {
            foreach ($fieldEntities as $fieldEntity) {
                if ($this->dataId == $fieldEntity->getDataId()) {
                    $this->entityManager->remove($fieldEntity);

                    $fieldData = $this->entityManager->getRepository(FieldData::class)
                        ->findOneBy([
                            'configId' => $fieldConfig->getId(),
                            'dataId' => $fieldEntity->getDataId()
                        ]);

                    $this->entityManager->remove($fieldData);
                    $this->entityManager->flush();
                }
            }

            $this->entityManager->getConnection()->commit();

            $this->deleteEntityFromSession($fieldConfig->getId());
            $this->fieldContainer->addCommandResult('deleteError', false);

        } catch (ORMException $e) {
            $this->entityManager->rollback();

            $this->fieldContainer->addCommandResult('deleteError', true);
        }
    }

    /**
     * @param int $configId
     */
    private function deleteEntityFromSession(int $configId) : void
    {
        $fieldEntityDTO = $this->fieldObjectDTOFactory->createFieldEntity();
        $serializedFieldEntityDTO = $this->session->get('edit_field/' . $configId . '/fieldEntityDTO');
        if (!is_null($serializedFieldEntityDTO)) {
            $fieldEntityDTO->unserialize($serializedFieldEntityDTO);
        }

        foreach ($fieldEntityDTO->getEntities() as $entityDTO) {
            if ($entityDTO->getDataId() == $this->dataId) {
                $fieldEntityDTO->getEntities()->removeElement($entityDTO);
            }
        }

        $this->session->set('edit_field/' . $configId . '/fieldEntityDTO', $fieldEntityDTO->serialize());
    }
}
