<?php

namespace AppBundle\Service\Field\Command;


/**
 * Interface CommandInterface
 */
interface CommandInterface
{
    public function execute() : void;
}
