<?php

namespace AppBundle\Service\Field\Command;

use AppBundle\Entity\Field\Group\FieldGroup;
use AppBundle\Entity\Field\Group\NullGroupTemplate;
use AppBundle\Service\Field\Collection\FieldCollection;
use AppBundle\Service\Field\FieldContainer;
use AppBundle\Service\Field\FieldObject;
use AppBundle\Service\Field\Options\FieldListOptions;
use AppBundle\Service\Field\Options\FieldObjectOptions;


/**
 * Class CreateControllerCollectionCmd
 */
class CreateControllerCollectionCmd implements CommandInterface
{
    /** @var FieldListOptions */
    private $fieldListOptions;
    /** @var FieldObjectOptions */
    private $fieldObjectOptions;

    /** @var CreateFieldCmd */
    private $createFieldCmd;
    /** @var FindByFieldListOptionsCmd */
    private $findByFieldListOptionsCmd;

    /** @var FieldCollection */
    private $fieldCollection;
    private $fieldContainer;

    /**
     * CreateControllerCollectionCmd constructor.
     * @param FieldContainer $fieldContainer
     */
    public function __construct(FieldContainer $fieldContainer)
    {
        $this->fieldContainer = $fieldContainer;
    }

    /**
     * @required
     * @param FieldListOptions $listOptions
     * @param FieldObjectOptions $objectOptions
     */
    public function setOptions(FieldListOptions $listOptions, FieldObjectOptions $objectOptions) : void
    {
        $this->fieldListOptions = $listOptions;
        $this->fieldObjectOptions = $objectOptions;
    }

    /**
     * @required
     * @param CreateFieldCmd $createFieldCmd
     * @param FindByFieldListOptionsCmd $findByFieldListOptionsCmd
     */
    public function setCommands(CreateFieldCmd $createFieldCmd, FindByFieldListOptionsCmd $findByFieldListOptionsCmd) : void
    {
        $this->createFieldCmd = $createFieldCmd;
        $this->findByFieldListOptionsCmd = $findByFieldListOptionsCmd;
    }

    public function execute() : void
    {
        $this->fieldCollection = new FieldCollection();
        $this->fieldCollection->setTemplate(new NullGroupTemplate());

        $this->createCollection();

        $this->fieldContainer->addCollection($this->fieldCollection, '_controller');
    }

    private function createCollection() : void
    {
        $this->findByFieldListOptionsCmd->execute();
        $fieldConfigList = $this->fieldContainer->getCommandResult('fieldConfigList');

        foreach ($fieldConfigList as $fieldConfig) {
            $this->createFieldCmd->setFieldConfig($fieldConfig);
            $this->createFieldCmd->execute();

            $fieldObject = $this->fieldContainer->getCommandResult('fieldObject');

            if ($fieldObject instanceof FieldObject) {
                $this->addFieldObject($fieldObject);
            }
        }
    }

    /**
     * @param FieldObject $fieldObject
     */
    private function addFieldObject(FieldObject $fieldObject) : void
    {
        if ($fieldObject->getGroup() instanceof FieldGroup) {
            $this->addToGroup($fieldObject);
        } else {
            $this->addToCollection($fieldObject);
        }
    }

    /**
     * @param FieldObject $fieldObject
     */
    private function addToGroup(FieldObject $fieldObject) : void
    {
        $groupName = $fieldObject->getGroup()->getGroupName();
        $fieldCollection = $this->fieldCollection->getCollection();

        if (in_array($groupName, $fieldCollection)) {
            $groupCollection = $this->fieldCollection->getGroup($groupName);

            $this->addToCollection($fieldObject, $groupCollection);
        } else {
            $this->fieldCollection->addGroup($groupName);

            $groupCollection = $this->fieldCollection->getGroup($groupName);
            $groupTemplate = $fieldObject->getGroup()->getGroupTemplate();

            $groupCollection->setTemplate($groupTemplate);

            $this->addToCollection($fieldObject, $groupCollection);
        }
    }

    /**
     * @param FieldObject $fieldObject
     * @param FieldCollection|null $fieldCollection
     */
    private function addToCollection(FieldObject $fieldObject, FieldCollection $fieldCollection = null) : void
    {
        $fieldName = $fieldObject->getFieldName();

        if (is_null($fieldCollection)) {
            $fieldCollection = $this->fieldCollection;
        }

        if (!in_array($fieldName, $fieldCollection->getCollection())) {
            $fieldCollection->addField($fieldObject);
        }
    }
}
