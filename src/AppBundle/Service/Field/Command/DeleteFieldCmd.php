<?php

namespace AppBundle\Service\Field\Command;

use AppBundle\Entity\Field\FieldData;
use AppBundle\Service\Field\FieldContainer;
use AppBundle\Service\Field\FieldObject;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


/**
 * Class DeleteFieldCmd
 */
class DeleteFieldCmd implements CommandInterface
{
    private $session;
    private $entityManager;
    private $fieldContainer;

    /** @var FieldObject */
    private $fieldObject;

    /**
     * DeleteFieldCmd constructor.
     * @param SessionInterface $session
     * @param EntityManagerInterface $entityManager
     * @param FieldContainer $fieldContainer
     */
    public function __construct(
        SessionInterface $session,
        EntityManagerInterface $entityManager,
        FieldContainer $fieldContainer
    )
    {
        $this->session = $session;
        $this->entityManager = $entityManager;
        $this->fieldContainer = $fieldContainer;
    }

    /**
     * @param FieldObject $fieldObject
     */
    public function setFieldObject(FieldObject $fieldObject) : void
    {
        $this->fieldObject = $fieldObject;
    }

    public function execute() : void
    {
        $this->deleteField();
    }

    private function deleteField() : void
    {
        $this->entityManager->getConnection()->beginTransaction();

        $fieldConfig = $this->fieldObject->getConfig();
        $configId = $fieldConfig->getId();
        $fieldTemplate = $this->fieldObject->getOrigTemplate();
        $fieldEntities = $this->fieldObject->getEntities();

        try {
            foreach ($fieldEntities as $fieldEntity) {
                $this->entityManager->remove($fieldEntity);

                $fieldData = $this->entityManager->getRepository(FieldData::class)
                    ->findOneBy([
                        'configId' => $fieldConfig->getId(),
                        'dataId' => $fieldEntity->getDataId()
                    ]);

                $this->entityManager->remove($fieldData);
                $this->entityManager->flush();
            }

            $this->entityManager->remove($fieldTemplate);
            $this->entityManager->remove($fieldConfig);
            $this->entityManager->flush();

            $this->entityManager->getConnection()->commit();

            $this->deleteEntityFromSession($configId);
            $this->fieldContainer->addCommandResult('deleteError', false);

        } catch (ORMException $e) {
            $this->entityManager->rollback();

            $this->fieldContainer->addCommandResult('deleteError', true);
        }
    }

    /**
     * @param int $configId
     */
    private function deleteEntityFromSession(int $configId) : void
    {
        $this->session->remove('edit_field/' . $configId . '/fieldEntityDTO');
    }
}
