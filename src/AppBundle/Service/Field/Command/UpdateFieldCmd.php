<?php

namespace AppBundle\Service\Field\Command;

use AppBundle\Entity\Field\FieldData;
use AppBundle\Service\Field\FieldContainer;
use AppBundle\Service\Field\FieldObject;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;


/**
 * Class UpdateFieldCmd
 */
class UpdateFieldCmd implements CommandInterface
{
    private $entityManager;
    private $fieldContainer;

    /** @var FieldObject */
    private $fieldObject;

    /**
     * UpdateFieldCommand constructor.
     * @param EntityManagerInterface $entityManager
     * @param FieldContainer $fieldContainer
     */
    public function __construct(EntityManagerInterface $entityManager, FieldContainer $fieldContainer)
    {
        $this->entityManager = $entityManager;
        $this->fieldContainer = $fieldContainer;
    }

    /**
     * @param FieldObject $fieldObject
     */
    public function setFieldObject(FieldObject $fieldObject) : void
    {
        $this->fieldObject = $fieldObject;
    }

    public function execute() : void
    {
        $this->update();
    }

    private function update() : void
    {
        $this->entityManager->getConnection()->beginTransaction();

        $fieldConfig = $this->fieldObject->getConfig();
        $fieldEntities = $this->fieldObject->getEntities();

        try {
            foreach ($fieldEntities as $fieldEntity) {
                if (!$this->entityManager->contains($fieldEntity)) {
                    $this->entityManager->persist($fieldEntity);
                    $this->entityManager->flush();

                    $fieldData = new FieldData();
                    $fieldData->setConfigId($fieldConfig->getId());
                    $fieldData->setDataId($fieldEntity->getDataId());
                    $this->entityManager->persist($fieldData);
                    $this->entityManager->flush();
                }
            }

            $this->entityManager->flush();
            $this->entityManager->getConnection()->commit();

            $this->fieldContainer->addCommandResult('updateError', false);

        } catch (ORMException $e) {
            $this->entityManager->rollback();

            $this->fieldContainer->addCommandResult('updateError', true);
        }
    }
}
