<?php

namespace AppBundle\Service\Field\EntityLoader;

use AppBundle\Service\Field\FieldObject;


/**
 * Interface EntityLoaderInterface
 */
interface EntityLoaderInterface
{
    public function loadExternalEntity(FieldObject $fieldObject) : void;
}
