<?php

namespace AppBundle\Service\Field\EntityLoader;


/**
 * Interface EntityLoaderFactoryInterface
 */
interface EntityLoaderFactoryInterface
{
    public function getEntityLoader(string $entityLoaderClassName) : EntityLoaderInterface;
}
