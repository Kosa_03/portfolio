<?php

namespace AppBundle\Service\Field\FieldEntityStrategy;

use AppBundle\Entity\Field\FieldType\Component\NullField;
use AppBundle\Entity\Field\Template\NullFieldTemplate;
use AppBundle\Service\Field\FieldObject;


/**
 * Class DefaultStrategy
 */
class DefaultStrategy implements StrategyInterface
{
    /**
     * @param FieldObject $fieldObject
     */
    public function createFieldEntity(FieldObject $fieldObject) : void
    {
        $fieldObjectEntities = $fieldObject->getEntities();
        $entityType = $fieldObject->getFieldClass();

        if (!class_exists($entityType)) {
            $fieldObject->setEntity(new NullField());
            $fieldObject->setTemplate(new NullFieldTemplate());
            return;
        }

        if (!$fieldObjectEntities->isEmpty()) {
            $fieldObjectEntity = clone $fieldObjectEntities->first();
            $fieldObject->setEntity($fieldObjectEntity);

        } else {
            $fieldObject->setEntity(new NullField());
            $fieldObject->setTemplate(new NullFieldTemplate());
        }
    }
}
