<?php

namespace AppBundle\Service\Field\FieldEntityStrategy;

use AppBundle\Entity\Field\FieldType\Entity\EntityFieldInterface;
use AppBundle\Service\Field\EntityLoader\EntityLoaderFactoryInterface;
use AppBundle\Service\Field\EntityLoader\EntityLoaderInterface;
use AppBundle\Service\Field\FieldObject;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Class EntityStrategy
 */
class EntityStrategy implements StrategyInterface
{
    private $container;

    private $defaultStrategy;
    private $entityManager;

    /**
     * EntityStrategy constructor.
     * @param ContainerInterface $container
     * @param DefaultStrategy $defaultStrategy
     * @param EntityManagerInterface $entityManager
     */
    public function __construct
    (
        ContainerInterface $container,
        DefaultStrategy $defaultStrategy,
        EntityManagerInterface $entityManager
    )
    {
        $this->container = $container;
        $this->defaultStrategy = $defaultStrategy;
        $this->entityManager = $entityManager;
    }

    /**
     * @param FieldObject $fieldObject
     */
    public function createFieldEntity(FieldObject $fieldObject)
    {
        $this->defaultStrategy->createFieldEntity($fieldObject);

        $entityLoader = $this->getEntityLoader($fieldObject);

        $entityLoader->loadExternalEntity($fieldObject);
    }

    /**
     * @param FieldObject $fieldObject
     * @return EntityLoaderInterface
     */
    private function getEntityLoader(FieldObject $fieldObject) : EntityLoaderInterface
    {
        /** @var EntityFieldInterface $entityField */
        $entityField = $fieldObject->getField();

        $entityLoaderFactoryClass = $entityField->getEntityLoaderFactory();
        /** @var EntityLoaderFactoryInterface $entityLoaderFactory */
        $entityLoaderFactory = $this->container->get($entityLoaderFactoryClass);

        $entityLoaderClass = $entityField->getEntityLoader();
        $entityLoader = $entityLoaderFactory->getEntityLoader($entityLoaderClass);

        return $entityLoader;
    }
}
