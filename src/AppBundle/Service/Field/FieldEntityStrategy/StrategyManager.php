<?php

namespace AppBundle\Service\Field\FieldEntityStrategy;

use AppBundle\Entity\Field\FieldType\Entity\EntityFieldInterface;
use AppBundle\Entity\Field\FieldType\Translatable\TranslatableFieldInterface;
use AppBundle\Service\Field\FieldObject;


/**
 * Class StrategyManager
 */
class StrategyManager
{
    private $strategy = [];

    /**
     * StrategyManager constructor.
     * @param DefaultStrategy $defaultStrategy
     * @param EntityStrategy $entityStrategy
     * @param TranslatableStrategy $translatableStrategy
     */
    public function __construct
    (
        DefaultStrategy $defaultStrategy,
        EntityStrategy $entityStrategy,
        TranslatableStrategy $translatableStrategy
    )
    {
        $this->strategy['default'] = $defaultStrategy;
        $this->strategy['entity'] = $entityStrategy;
        $this->strategy['translatable'] = $translatableStrategy;
    }

    /**
     * @param FieldObject $fieldObject
     */
    public function selectStrategy(FieldObject $fieldObject) : void
    {
        $entityType = $fieldObject->getFieldClass();

        if (in_array(TranslatableFieldInterface::class, class_implements($entityType))) {
            $fieldObject->setCreateEntityStrategy($this->strategy['translatable']);

        } elseif (in_array(EntityFieldInterface::class, class_implements($entityType))) {
            $fieldObject->setCreateEntityStrategy($this->strategy['entity']);

        } else {
            $fieldObject->setCreateEntityStrategy($this->strategy['default']);
        }
    }
}
