<?php

namespace AppBundle\Service\Field\FieldEntityStrategy;

use AppBundle\Entity\Field\FieldType\Component\NullField;
use AppBundle\Entity\Field\Template\NullFieldTemplate;
use AppBundle\Service\Field\FieldObject;
use Symfony\Component\HttpFoundation\RequestStack;


/**
 * Class TranslatableStrategy
 */
class TranslatableStrategy implements StrategyInterface
{
    private $locale;
    private $defaultLocale;

    /**
     * TranslatableStrategy constructor.
     * @param RequestStack $request
     */
    public function __construct(RequestStack $request)
    {
        $request = $request->getCurrentRequest();

        $this->locale = $request->getLocale();
        $this->defaultLocale = 'und'; //$request->getDefaultLocale();
    }

    /**
     * @param FieldObject $fieldObject
     */
    public function createFieldEntity(FieldObject $fieldObject) : void
    {
        $fieldObjectEntities = $fieldObject->getEntities();
        $entityType = $fieldObject->getFieldClass();

        if (!class_exists($entityType)) {
            $fieldObject->setEntity(new NullField());
            $fieldObject->setTemplate(new NullFieldTemplate());
            return;
        }

        $fieldContentLocale = null;
        $fieldContentDefaultLocale = null;

        foreach ($fieldObjectEntities as $fieldObjectEntity) {
            if ($fieldObjectEntity->getLanguage() == $this->locale) {
                $fieldContentLocale = clone $fieldObjectEntity;
            }

            if ($fieldObjectEntity->getLanguage() == $this->defaultLocale) {
                $fieldContentDefaultLocale = clone $fieldObjectEntity;
            }
        }

        if (!is_null($fieldContentLocale)) {
            $fieldObject->setEntity($fieldContentLocale);

        } elseif (!is_null($fieldContentDefaultLocale)) {
            $fieldObject->setEntity($fieldContentDefaultLocale);

        } else {
            $fieldObject->setEntity(new NullField());
            $fieldObject->setTemplate(new NullFieldTemplate());
        }
    }
}
