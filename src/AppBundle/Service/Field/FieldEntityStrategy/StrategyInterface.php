<?php

namespace AppBundle\Service\Field\FieldEntityStrategy;

use AppBundle\Service\Field\FieldObject;


/**
 * Interface StrategyInterface
 */
interface StrategyInterface
{
    public function createFieldEntity(FieldObject $fieldObject);
}
