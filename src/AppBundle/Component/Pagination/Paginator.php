<?php

namespace AppBundle\Component\Pagination;


/**
 * Class Pagination
 */
class Paginator
{
    private $numberOfItems;
    private $itemsPerPage;
    private $numberOfPages;
    private $pageNumber = 1;

    public function __construct(int $numberOfItems, int $itemsPerPage = 25)
    {
        $this->numberOfItems = $numberOfItems;
        $this->itemsPerPage = $itemsPerPage;
        $this->numberOfPages = ceil($numberOfItems / $itemsPerPage);
    }

    /**
     * @param int $pageNumber
     */
    public function setPageNumber(int $pageNumber) : void
    {
        $this->pageNumber = $pageNumber;
    }

    /**
     * @return int
     */
    public function getNumberOfPages() : int
    {
        return $this->numberOfPages;
    }

    /**
     * @return int|null
     */
    public function previousPage() : ?int
    {
        if ($this->pageNumber > 1) {
            return $this->pageNumber - 1;
        }

        return null;
    }

    /**
     * @return int
     */
    public function currentPage() : int
    {
        return $this->pageNumber;
    }

    /**
     * @return int|null
     */
    public function nextPage() : ?int
    {
        if ($this->pageNumber < $this->numberOfPages) {
            return $this->pageNumber + 1;
        }

        return null;
    }
}
