<?php

namespace AppBundle\FormDTO\Blog;


/**
 * Class SearchBlogDTO
 */
final class SearchBlogDTO implements \Serializable
{
    private $filterType    = 'all_articles';
    private $tags          = [];
    private $status        = 'all_status';
    private $sortBy        = 'created';
    private $sortMethod    = 'DESC';
    private $articlesPerPage = 25;

    /**
     * @return string
     */
    public function serialize() : string
    {
        return serialize([
            $this->filterType,
            $this->tags,
            $this->status,
            $this->sortBy,
            $this->sortMethod,
            $this->articlesPerPage
        ]);
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized) : void
    {
        list (
            $this->filterType,
            $this->tags,
            $this->status,
            $this->sortBy,
            $this->sortMethod,
            $this->articlesPerPage
        ) = unserialize($serialized);
    }

/*
 * Getters and setters
 */

    /**
     * @return mixed
     */
    public function getFilterType()
    {
        return $this->filterType;
    }

    /**
     * @param $filterType
     */
    public function setFilterType($filterType)
    {
        $this->filterType = $filterType;
    }

    /**
     * @return array
     */
    public function getTags() : array
    {
        return $this->tags;
    }

    /**
     * @param array $tags
     */
    public function setTags(array $tags)
    {
        $this->tags = $tags;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getSortBy()
    {
        return $this->sortBy;
    }

    /**
     * @param mixed $sortBy
     */
    public function setSortBy($sortBy)
    {
        $this->sortBy = $sortBy;
    }

    /**
     * @return mixed
     */
    public function getSortMethod()
    {
        return $this->sortMethod;
    }

    /**
     * @param mixed $sortMethod
     */
    public function setSortMethod($sortMethod)
    {
        $this->sortMethod = $sortMethod;
    }

    /**
     * @return int
     */
    public function getArticlesPerPage() : int
    {
        return $this->articlesPerPage;
    }

    /**
     * @param int $articlesPerPage
     */
    public function setArticlesPerPage(int $articlesPerPage)
    {
        $this->articlesPerPage = $articlesPerPage;
    }
}
