<?php

namespace AppBundle\FormDTO\Blog\CollectionType;


/**
 * Class BlogTagDTO
 */
final class BlogTagDTO implements \ArrayAccess, \Serializable
{
    use ArrayAccessTrait;

    private $tagId;
    private $name;
    private $weight;
    private $translations;

    /**
     * @return string
     */
    public function serialize() : string
    {
        return serialize([
            $this->tagId,
            $this->name,
            $this->weight,
            $this->translations
        ]);
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized) : void
    {
        list(
            $this->tagId,
            $this->name,
            $this->weight,
            $this->translations
        ) = unserialize($serialized);
    }

/*
 * Getters and setters
 */

    /**
     * @return mixed
     */
    public function getTagId()
    {
        return $this->tagId;
    }

    /**
     * @param mixed $tagId
     */
    public function setTagId($tagId)
    {
        $this->tagId = $tagId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param mixed $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * @return mixed
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @param mixed $translations
     */
    public function setTranslations($translations)
    {
        $this->translations = $translations;
    }
}
