<?php

namespace AppBundle\FormDTO\Blog\CollectionType;


/**
 * Class BlogTranslationDTO
 */
final class BlogTranslationDTO implements \ArrayAccess, \Serializable
{
    use ArrayAccessTrait;

    private $translationId;
    private $language;
    private $subject;
    private $content;

    /**
     * @return string
     */
    public function serialize() : string
    {
        return serialize([
            $this->translationId,
            $this->language,
            $this->subject,
            $this->content
        ]);
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized) : void
    {
        list(
            $this->translationId,
            $this->language,
            $this->subject,
            $this->content
        ) = unserialize($serialized);
    }

/*
 * Getters and setters
 */

    /**
     * @return mixed
     */
    public function getTranslationId()
    {
        return $this->translationId;
    }

    /**
     * @param mixed $translationId
     */
    public function setTranslationId($translationId)
    {
        $this->translationId = $translationId;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param mixed $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }
}
