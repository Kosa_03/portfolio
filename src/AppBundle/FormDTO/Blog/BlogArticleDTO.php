<?php

namespace AppBundle\FormDTO\Blog;

use Doctrine\Common\Collections\ArrayCollection;


/**
 * Class BlogArticleDTO
 */
final class BlogArticleDTO implements \Serializable
{
    private $amountTranslations;
    private $headerImageFile = 'default-bg-landscape.jpg';
    private $status;
    private $tags;
    private $translations;

    /**
     * BlogArticleDTO constructor.
     */
    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function serialize() : string
    {
        return serialize([
            $this->amountTranslations,
            $this->headerImageFile,
            $this->status,
            $this->tags,
            $this->translations
        ]);
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized) : void
    {
        list(
            $this->amountTranslations,
            $this->headerImageFile,
            $this->status,
            $this->tags,
            $this->translations
        ) = unserialize($serialized);
    }

/*
 * Getters and setters
 */

    /**
     * @return mixed
     */
    public function getAmountTranslations()
    {
        return $this->amountTranslations;
    }

    /**
     * @param mixed $amountTranslations
     */
    public function setAmountTranslations($amountTranslations)
    {
        $this->amountTranslations = $amountTranslations;
    }

    /**
     * @return mixed
     */
    public function getHeaderImageFile()
    {
        return $this->headerImageFile;
    }

    /**
     * @param mixed $headerImageFile
     */
    public function setHeaderImageFile($headerImageFile)
    {
        $this->headerImageFile = $headerImageFile;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return array|null
     */
    public function getTags() : ?array
    {
        return $this->tags;
    }

    /**
     * @param array $tags
     */
    public function setTags(array $tags)
    {
        $this->tags = $tags;
    }

    /**
     * @return ArrayCollection
     */
    public function getTranslations() : ArrayCollection
    {
        return $this->translations;
    }

    /**
     * @param ArrayCollection $translations
     */
    public function setTranslations(ArrayCollection $translations)
    {
        $this->translations = $translations;
    }
}
