<?php

namespace AppBundle\FormDTO\Blog;

use Doctrine\Common\Collections\ArrayCollection;


/**
 * Class BlogTagListDTO
 */
final class BlogTagListDTO implements \Serializable
{
    private $tags;

    /**
     * BlogTagListDTO constructor.
     */
    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function serialize() : string
    {
        return serialize([
            $this->tags
        ]);
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized) : void
    {
        list(
            $this->tags
        ) = unserialize($serialized);
    }

/*
 * Getters and setters
 */

    /**
     * @return ArrayCollection
     */
    public function getTags() : ArrayCollection
    {
        return $this->tags;
    }

    /**
     * @param ArrayCollection $tags
     */
    public function setTags(ArrayCollection $tags) : void
    {
        $this->tags = $tags;
    }
}
