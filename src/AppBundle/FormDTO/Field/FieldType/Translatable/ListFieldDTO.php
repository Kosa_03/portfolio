<?php

namespace AppBundle\FormDTO\Field\FieldType\Translatable;

use AppBundle\FormDTO\Field\FieldType\ArrayAccessTrait;


/**
 * Class ListFieldDTO
 */
final class ListFieldDTO implements \ArrayAccess, \Serializable
{
    use ArrayAccessTrait;

    private $dataId;
    private $language;
    private $list;

    /**
     * @return string
     */
    public function serialize()
    {
        return serialize([
            $this->dataId,
            $this->language,
            $this->list
        ]);
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        list (
            $this->dataId,
            $this->language,
            $this->list
        ) = unserialize($serialized);
    }

/*
 * Getters and setters
 */

    /**
     * @return mixed
     */
    public function getDataId()
    {
        return $this->dataId;
    }

    /**
     * @param mixed $dataId
     */
    public function setDataId($dataId)
    {
        $this->dataId = $dataId;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param mixed $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return mixed
     */
    public function getList()
    {
        return $this->list;
    }

    /**
     * @param mixed $list
     */
    public function setList($list)
    {
        $this->list = $list;
    }
}
