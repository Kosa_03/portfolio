<?php

namespace AppBundle\FormDTO\Field\FieldType\Translatable;

use AppBundle\FormDTO\Field\FieldType\ArrayAccessTrait;


/**
 * Class SmallHeaderFieldDTO
 */
final class SmallHeaderFieldDTO implements \ArrayAccess, \Serializable
{
    use ArrayAccessTrait;

    private $dataId;
    private $language;
    private $header;

    /**
     * @return string
     */
    public function serialize()
    {
        return serialize([
            $this->dataId,
            $this->language,
            $this->header
        ]);
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        list (
            $this->dataId,
            $this->language,
            $this->header
        ) = unserialize($serialized);
    }

/*
 * Getters and setters
 */

    /**
     * @return mixed
     */
    public function getDataId()
    {
        return $this->dataId;
    }

    /**
     * @param mixed $dataId
     */
    public function setDataId($dataId)
    {
        $this->dataId = $dataId;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param mixed $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return mixed
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * @param mixed $header
     */
    public function setHeader($header)
    {
        $this->header = $header;
    }
}
