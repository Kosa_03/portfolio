<?php

namespace AppBundle\FormDTO\Field\FieldType\Form;

use AppBundle\FormDTO\Field\FieldType\ArrayAccessTrait;


/**
 * Class FormFieldDTO
 */
class FormFieldDTO implements \ArrayAccess, \Serializable
{
    use ArrayAccessTrait;

    private $dataId;
    private $formTemplatePath;

    /**
     * @return string
     */
    public function serialize()
    {
        return serialize([
            $this->dataId,
            $this->formTemplatePath
        ]);
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        list (
            $this->dataId,
            $this->formTemplatePath
        ) = unserialize($serialized);
    }

/*
 * Getters and setters
 */

    /**
     * @return mixed
     */
    public function getDataId()
    {
        return $this->dataId;
    }

    /**
     * @param mixed $dataId
     */
    public function setDataId($dataId)
    {
        $this->dataId = $dataId;
    }

    /**
     * @return mixed
     */
    public function getFormTemplatePath()
    {
        return $this->formTemplatePath;
    }

    /**
     * @param mixed $formTemplatePath
     */
    public function setFormTemplatePath($formTemplatePath)
    {
        $this->formTemplatePath = $formTemplatePath;
    }
}
