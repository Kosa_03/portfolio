<?php

namespace AppBundle\FormDTO\Field\FieldType\Component;

use AppBundle\FormDTO\Field\FieldType\ArrayAccessTrait;


/**
 * Class PaginationFieldDTO
 */
final class PaginationFieldDTO implements \ArrayAccess, \Serializable
{
    use ArrayAccessTrait;

    private $dataId;
    private $paginationPath;

    /**
     * @return string
     */
    public function serialize()
    {
        return serialize([
            $this->dataId,
            $this->paginationPath
        ]);
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        list (
            $this->dataId,
            $this->paginationPath
        ) = unserialize($serialized);
    }

/*
 * Getters and setters
 */

    /**
     * @return mixed
     */
    public function getDataId()
    {
        return $this->dataId;
    }

    /**
     * @param mixed $dataId
     */
    public function setDataId($dataId)
    {
        $this->dataId = $dataId;
    }

    /**
     * @return mixed
     */
    public function getPaginationPath()
    {
        return $this->paginationPath;
    }

    /**
     * @param mixed $paginationPath
     */
    public function setPaginationPath($paginationPath)
    {
        $this->paginationPath = $paginationPath;
    }
}
