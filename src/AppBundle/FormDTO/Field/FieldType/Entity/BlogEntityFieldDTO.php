<?php

namespace AppBundle\FormDTO\Field\FieldType\Entity;

use AppBundle\FormDTO\Field\FieldType\ArrayAccessTrait;


/**
 * Class BlogEntityFieldDTO
 */
final class BlogEntityFieldDTO implements \ArrayAccess, \Serializable
{
    use ArrayAccessTrait;

    private $dataId;
    private $entityLoader;
    private $templatePath;

    /**
     * @return string
     */
    public function serialize()
    {
        return serialize([
            $this->dataId,
            $this->entityLoader,
            $this->templatePath
        ]);
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        list (
            $this->dataId,
            $this->entityLoader,
            $this->templatePath
        ) = unserialize($serialized);
    }

/*
 * Getters and setters
 */

    /**
     * @return mixed
     */
    public function getDataId()
    {
        return $this->dataId;
    }

    /**
     * @param mixed $dataId
     */
    public function setDataId($dataId)
    {
        $this->dataId = $dataId;
    }

    /**
     * @return mixed
     */
    public function getEntityLoader()
    {
        return $this->entityLoader;
    }

    /**
     * @param mixed $entityLoader
     */
    public function setEntityLoader($entityLoader)
    {
        $this->entityLoader = $entityLoader;
    }

    /**
     * @return mixed
     */
    public function getTemplatePath()
    {
        return $this->templatePath;
    }

    /**
     * @param mixed $templatePath
     */
    public function setTemplatePath($templatePath)
    {
        $this->templatePath = $templatePath;
    }
}