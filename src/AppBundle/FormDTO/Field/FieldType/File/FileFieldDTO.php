<?php

namespace AppBundle\FormDTO\Field\FieldType\File;

use AppBundle\FormDTO\Field\FieldType\ArrayAccessTrait;


/**
 * Class FileFieldDTO
 */
final class FileFieldDTO implements \ArrayAccess, \Serializable
{
    use ArrayAccessTrait;

    private $dataId;
    private $filePath;
    private $fileName;

    /**
     * @return string
     */
    public function serialize()
    {
        return serialize([
            $this->dataId,
            $this->filePath,
            $this->fileName
        ]);
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        list (
            $this->dataId,
            $this->filePath,
            $this->fileName
        ) = unserialize($serialized);
    }

/*
 * Getters and setters
 */

    /**
     * @return mixed
     */
    public function getDataId()
    {
        return $this->dataId;
    }

    /**
     * @param mixed $dataId
     */
    public function setDataId($dataId)
    {
        $this->dataId = $dataId;
    }

    /**
     * @return mixed
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * @param mixed $filePath
     */
    public function setFilePath($filePath)
    {
        $this->filePath = $filePath;
    }

    /**
     * @return mixed
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param mixed $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }
}
