<?php

namespace AppBundle\FormDTO\Field\FieldType\File;

use AppBundle\FormDTO\Field\FieldType\ArrayAccessTrait;


/**
 * Class ImageFieldDTO
 */
final class ImageFieldDTO implements \ArrayAccess, \Serializable
{
    use ArrayAccessTrait;

    private $dataId;
    private $redirectUrl;
    private $filePath;

    /**
     * @return string
     */
    public function serialize()
    {
        return serialize([
            $this->dataId,
            $this->redirectUrl,
            $this->filePath
        ]);
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        list (
            $this->dataId,
            $this->redirectUrl,
            $this->filePath
        ) = unserialize($serialized);
    }

/*
 * Getters and setters
 */

    /**
     * @return mixed
     */
    public function getDataId()
    {
        return $this->dataId;
    }

    /**
     * @param mixed $dataId
     */
    public function setDataId($dataId)
    {
        $this->dataId = $dataId;
    }

    /**
     * @return mixed
     */
    public function getRedirectUrl()
    {
        return $this->redirectUrl;
    }

    /**
     * @param mixed $redirectUrl
     */
    public function setRedirectUrl($redirectUrl)
    {
        $this->redirectUrl = $redirectUrl;
    }

    /**
     * @return mixed
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * @param mixed $filePath
     */
    public function setFilePath($filePath)
    {
        $this->filePath = $filePath;
    }
}
