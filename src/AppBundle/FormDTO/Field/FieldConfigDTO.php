<?php

namespace AppBundle\FormDTO\Field;


/**
 * Class FieldConfigDTO
 */
final class FieldConfigDTO implements \Serializable
{
    private $amountEntities;
    private $fieldClass;
    private $fieldName;
    private $groupId;
    private $controller;
    private $method;
    private $weight;
    private $roles;

    /**
     * @return string
     */
    public function serialize()
    {
        return serialize([
            $this->amountEntities,
            $this->fieldClass,
            $this->fieldName,
            $this->groupId,
            $this->controller,
            $this->method,
            $this->weight,
            $this->roles
        ]);
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        list (
            $this->amountEntities,
            $this->fieldClass,
            $this->fieldName,
            $this->groupId,
            $this->controller,
            $this->method,
            $this->weight,
            $this->roles
        ) = unserialize($serialized);
    }

/*
 * Getters and setters
 */

    /**
     * @return mixed
     */
    public function getAmountEntities()
    {
        return $this->amountEntities;
    }

    /**
     * @param mixed $amountEntities
     */
    public function setAmountEntities($amountEntities)
    {
        $this->amountEntities = $amountEntities;
    }

    /**
     * @return mixed
     */
    public function getFieldClass()
    {
        return $this->fieldClass;
    }

    /**
     * @param mixed $fieldClass
     */
    public function setFieldClass($fieldClass)
    {
        $this->fieldClass = $fieldClass;
    }

    /**
     * @return mixed
     */
    public function getFieldName()
    {
        return $this->fieldName;
    }

    /**
     * @param mixed $fieldName
     */
    public function setFieldName($fieldName)
    {
        $this->fieldName = $fieldName;
    }

    /**
     * @return mixed
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * @param mixed $groupId
     */
    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;
    }

    /**
     * @return mixed
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * @param mixed $controller
     */
    public function setController($controller)
    {
        $this->controller = $controller;
    }

    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param mixed $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * @return mixed
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param mixed $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @param mixed $roles
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;
    }
}
