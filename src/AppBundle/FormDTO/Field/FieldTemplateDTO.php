<?php

namespace AppBundle\FormDTO\Field;


/**
 * Class FieldTemplateDTO
 */
final class FieldTemplateDTO implements \Serializable
{
    public $arrayCss;
    public $dividerBefore;
    public $dividerAfter;
    public $smallDividerBefore;
    public $smallDividerAfter;
    public $twigFunctionParameters;

    /**
     * @return string
     */
    public function serialize()
    {
        return serialize([
            $this->arrayCss,
            $this->dividerBefore,
            $this->dividerAfter,
            $this->smallDividerBefore,
            $this->smallDividerAfter,
            $this->twigFunctionParameters
        ]);
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        list (
            $this->arrayCss,
            $this->dividerBefore,
            $this->dividerAfter,
            $this->smallDividerBefore,
            $this->smallDividerAfter,
            $this->twigFunctionParameters
        ) = unserialize($serialized);
    }

/*
 * Getters and setters
 */

    /**
     * @return mixed
     */
    public function getArrayCss()
    {
        return $this->arrayCss;
    }

    /**
     * @param mixed $arrayCss
     */
    public function setArrayCss($arrayCss)
    {
        $this->arrayCss = $arrayCss;
    }

    /**
     * @return mixed
     */
    public function isDividerBefore()
    {
        return $this->dividerBefore;
    }

    /**
     * @param mixed $dividerBefore
     */
    public function setDividerBefore($dividerBefore)
    {
        $this->dividerBefore = $dividerBefore;
    }

    /**
     * @return mixed
     */
    public function isDividerAfter()
    {
        return $this->dividerAfter;
    }

    /**
     * @param mixed $dividerAfter
     */
    public function setDividerAfter($dividerAfter)
    {
        $this->dividerAfter = $dividerAfter;
    }

    /**
     * @return mixed
     */
    public function isSmallDividerBefore()
    {
        return $this->smallDividerBefore;
    }

    /**
     * @param mixed $smallDividerBefore
     */
    public function setSmallDividerBefore($smallDividerBefore)
    {
        $this->smallDividerBefore = $smallDividerBefore;
    }

    /**
     * @return mixed
     */
    public function isSmallDividerAfter()
    {
        return $this->smallDividerAfter;
    }

    /**
     * @param mixed $smallDividerAfter
     */
    public function setSmallDividerAfter($smallDividerAfter)
    {
        $this->smallDividerAfter = $smallDividerAfter;
    }

    /**
     * @return mixed
     */
    public function getTwigFunctionParameters()
    {
        return $this->twigFunctionParameters;
    }

    /**
     * @param mixed $twigFunctionParameters
     */
    public function setTwigFunctionParameters($twigFunctionParameters)
    {
        $this->twigFunctionParameters = $twigFunctionParameters;
    }
}
