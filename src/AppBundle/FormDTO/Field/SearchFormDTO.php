<?php

namespace AppBundle\FormDTO\Field;


/**
 * Class SearchFormDTO
 */
final class SearchFormDTO implements \Serializable
{
    private $controller    = 'all_controllers';
    private $fieldClass    = 'all_fields';
    private $sortBy        = 'id';
    private $sortMethod    = 'ASC';
    private $fieldsPerPage = 25;

    /**
     * @return string
     */
    public function serialize()
    {
        return serialize([
            $this->controller,
            $this->fieldClass,
            $this->sortBy,
            $this->sortMethod,
            $this->fieldsPerPage
        ]);
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        list (
            $this->controller,
            $this->fieldClass,
            $this->sortBy,
            $this->sortMethod,
            $this->fieldsPerPage
        ) = unserialize($serialized);
    }

/*
 * Getters and setters
 */

    /**
     * @return mixed
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * @param mixed $controller
     */
    public function setController($controller)
    {
        $this->controller = $controller;
    }

    /**
     * @return mixed
     */
    public function getFieldClass()
    {
        return $this->fieldClass;
    }

    /**
     * @param mixed $fieldClass
     */
    public function setFieldClass($fieldClass)
    {
        $this->fieldClass = $fieldClass;
    }

    /**
     * @return mixed
     */
    public function getSortBy()
    {
        return $this->sortBy;
    }

    /**
     * @param mixed $sortBy
     */
    public function setSortBy($sortBy)
    {
        $this->sortBy = $sortBy;
    }

    /**
     * @return mixed
     */
    public function getSortMethod()
    {
        return $this->sortMethod;
    }

    /**
     * @param mixed $sortMethod
     */
    public function setSortMethod($sortMethod)
    {
        $this->sortMethod = $sortMethod;
    }

    /**
     * @return int
     */
    public function getFieldsPerPage(): int
    {
        return $this->fieldsPerPage;
    }

    /**
     * @param int $fieldsPerPage
     */
    public function setFieldsPerPage(int $fieldsPerPage)
    {
        $this->fieldsPerPage = $fieldsPerPage;
    }
}
