<?php

namespace AppBundle\FormDTO\Field;

use Doctrine\Common\Collections\ArrayCollection;


/**
 * Class FieldEntityDTO
 */
final class FieldEntityDTO implements \Serializable
{
    private $entities;

    /**
     * NewFieldEntitiesData constructor.
     */
    public function __construct()
    {
        $this->entities = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function serialize()
    {
        return serialize([
            $this->entities
        ]);
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        list (
            $this->entities
        ) = unserialize($serialized);
    }

/*
 * Getters and setters
 */

    /**
     * @return ArrayCollection
     */
    public function getEntities(): ArrayCollection
    {
        return $this->entities;
    }

    /**
     * @param ArrayCollection $entities
     */
    public function setEntities(ArrayCollection $entities)
    {
        $this->entities = $entities;
    }
}
