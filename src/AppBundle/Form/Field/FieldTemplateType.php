<?php

namespace AppBundle\Form\Field;

use AppBundle\Form\Field\DataTransformer\ArrayCssToStringCssTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;


/**
 * Class FieldTemplateType
 */
class FieldTemplateType extends AbstractType
{
    private $arrayCssTransformer;

    /**
     * FieldTemplateType constructor.
     * @param ArrayCssToStringCssTransformer $arrayCssTransformer
     */
    public function __construct(ArrayCssToStringCssTransformer $arrayCssTransformer)
    {
        $this->arrayCssTransformer = $arrayCssTransformer;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dividerBefore', CheckboxType::class, [
                'label' => 'Divider before',
                'required' => false
            ])
            ->add('dividerAfter', CheckboxType::class, [
                'label' => 'Divider after',
                'required' => false
            ])
            ->add('smallDividerBefore', CheckboxType::class, [
                'label' => 'Small divider before',
                'required' => false
            ])
            ->add('smallDividerAfter', CheckboxType::class, [
                'label' => 'Small divider after',
                'required' => false
            ])
            ->add('arrayCss', TextareaType::class, [
                'label' => 'CSS styles',
                'empty_data' => '',
                'required' => false,
            ])
            ->add('twigFunctionParameters', TextType::class, [
                'label' => 'Twig parameters',
                'required' => false,
                'empty_data' => '',
                'constraints' => [
                    new Length([
                        'max' => 96,
                        'maxMessage' => 'Field is too long. Max length is 96 chars.'
                    ])
                ]
            ])
        ;

        $builder->get('arrayCss')->addModelTransformer($this->arrayCssTransformer);
    }
}
