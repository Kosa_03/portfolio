<?php

namespace AppBundle\Form\Field;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


/**
 * Class AddFieldEntityType
 */
class AddFieldEntityType extends AbstractType
{
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'field_class' => ''
        ]);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('entities', CollectionType::class, [
                'entry_type' => $this->createEntityTypeFormPath($options['field_class'])
            ])
            ->add('submit_go_back', SubmitType::class, [
                'label' => 'Go back',
                'attr' => [
                    'class' => 'btn-outline-primary mr-2'
                ]
            ])
            ->add('submit_create', SubmitType::class, [
                'label' => 'Create field',
                'attr' => [
                    'class' => 'btn-outline-success'
                ]
            ])
        ;
    }

    /**
     * @param string $fieldClass
     * @return string
     */
    private function createEntityTypeFormPath(string $fieldClass) : string
    {
        $arrayFieldClass = explode('\\', $fieldClass);
        $reverseArrayFieldClass = array_reverse($arrayFieldClass);

        $fieldClass = array_slice($reverseArrayFieldClass, 0, 1)[0];
        $fieldType = array_slice($reverseArrayFieldClass, 1, 1)[0];

        return 'AppBundle\Form\Field\FieldType\\'
            . $fieldType . '\\' . $fieldClass . 'Type';
    }
}
