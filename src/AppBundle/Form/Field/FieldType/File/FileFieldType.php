<?php

namespace AppBundle\Form\Field\FieldType\File;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Regex;


/**
 * Class FileFieldType
 */
class FileFieldType extends AbstractType
{
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'delete_button' => false
        ]);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $deleteButton = $options['delete_button'];

        $builder
            ->add('fileName', TextType::class, [
                'label' => 'File name',
                'empty_data' => '',
                'required' => false,
                'constraints' => [
                    new Length([
                        'max' => 255,
                        'maxMessage' => 'File name field should has max 255 chars.'
                    ]),
                    new Regex([
                        'pattern' => '/^[a-zA-Z0-9_\-.]+$/i',
                        'message' => 'Field should contains only a-z, A-Z, 0-9, _, -, and . chars.'
                    ])
                ]
            ])
            ->add('filePath', TextType::class, [
                'label' => 'File path',
                'empty_data' => '',
                'required' => false,
                'constraints' => [
                    new Length([
                        'max' => 255,
                        'maxMessage' => 'File path field should has max 255 chars.'
                    ]),
                    new Regex([
                        'pattern' => '/^[a-zA-Z0-9_\-\/.]+$/i',
                        'message' => 'Field should contains only a-z, A-Z, 0-9, _, -, / and . chars.'
                    ])
                ]
            ])
        ;

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($deleteButton) {
                if (!$deleteButton) {
                    return;
                }

                $form = $event->getForm();

                $form->add('delete_entity', SubmitType::class, [
                    'label' => 'Delete',
                    'attr' => [
                        'class' => 'btn-outline-danger'
                    ]
                ]);
            }
        );
    }
}
