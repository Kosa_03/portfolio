<?php

namespace AppBundle\Form\Field\FieldType\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Regex;


/**
 * Class FormFieldType
 */
class FormFieldType extends AbstractType
{
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'delete_button' => false
        ]);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $deleteButton = $options['delete_button'];

        $builder
            ->add('formTemplatePath', TextType::class, [
                'label' => 'Form template path',
                'empty_data' => '',
                'required' => false,
                'constraints' => [
                    new Length([
                        'max' => 192,
                        'maxMessage' => 'Form template path should has max 192 chars.'
                    ]),
                    new Regex([
                        'pattern' => '/^[a-zA-Z0-9_\/.]+$/i',
                        'message' => 'Field should contains only a-z, A-Z, 0-9, _, / and . chars.'
                    ])
                ]
            ])
        ;

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($deleteButton) {
                if (!$deleteButton) {
                    return;
                }

                $form = $event->getForm();

                $form->add('delete_entity', SubmitType::class, [
                    'label' => 'Delete',
                    'attr' => [
                        'class' => 'btn-outline-danger'
                    ]
                ]);
            }
        );
    }
}
