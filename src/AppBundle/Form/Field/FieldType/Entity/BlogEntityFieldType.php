<?php

namespace AppBundle\Form\Field\FieldType\Entity;

use AppBundle\Service\Blog\Factory\BlogEntityLoaderFactory;
use AppBundle\Service\Field\ChoiceList\EntityLoaderList;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\Loader\CallbackChoiceLoader;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;


/**
 * Class BlogEntityFieldType
 */
class BlogEntityFieldType extends AbstractType
{
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'delete_button' => false
        ]);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $deleteButton = $options['delete_button'];

        $builder
            ->add('entityLoader', ChoiceType::class, [
                'label' => 'Entity loader class',
                'choice_loader' => new CallbackChoiceLoader(function() {
                    return EntityLoaderList::getList(BlogEntityLoaderFactory::class);
                }),
                'attr' => [
                    'class' => 'custom-select'
                ]
            ])
            ->add('templatePath', TextType::class, [
                'label' => 'Entity template path',
                'empty_data' => '',
                'required' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Template path should not be blank.'
                    ]),
                    new Length([
                        'max' => 255,
                        'maxMessage' => 'Header field should has max 255 chars.'
                    ])
                ]
            ])
        ;

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($deleteButton) {
                if (!$deleteButton) {
                    return;
                }

                $form = $event->getForm();

                $form->add('delete_entity', SubmitType::class, [
                    'label' => 'Delete',
                    'attr' => [
                        'class' => 'btn-outline-danger'
                    ]
                ]);
            }
        );
    }
}
