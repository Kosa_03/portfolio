<?php

namespace AppBundle\Form\Field\FieldType\Translatable;

use AppBundle\Service\Field\ChoiceList\LanguageList;
use AppBundle\Service\Field\ChoiceList\LongTextTypeList;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\Loader\CallbackChoiceLoader;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;


/**
 * Class LongTextFieldType
 */
class LongTextFieldType extends AbstractType
{
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'delete_button' => false
        ]);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $deleteButton = $options['delete_button'];

        $builder
            ->add('language', ChoiceType::class, [
                'label' => 'Language',
                'choice_loader' => new CallbackChoiceLoader(function() {
                    return LanguageList::getList();
                }),
                'attr' => [
                    'class' => 'custom-select'
                ]
            ])
            ->add('type', ChoiceType::class, [
                'label' => 'Field type',
                'choice_loader' => new CallbackChoiceLoader(function() {
                    return LongTextTypeList::getList();
                }),
                'attr' => [
                    'class' => 'custom-select'
                ]
            ])
            ->add('longText', TextareaType::class, [
                'label' => 'Long text',
                'empty_data' => '',
                'required' => false,
                'constraints' => [
                    new Length([
                        'max' => 55000,
                        'maxMessage' => 'Field is too long.'
                    ])
                ]
            ])
        ;

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($deleteButton) {
                if (!$deleteButton) {
                    return;
                }

                $form = $event->getForm();

                $form->add('delete_entity', SubmitType::class, [
                    'label' => 'Delete',
                    'attr' => [
                        'class' => 'btn-outline-danger'
                    ]
                ]);
            }
        );
    }
}
