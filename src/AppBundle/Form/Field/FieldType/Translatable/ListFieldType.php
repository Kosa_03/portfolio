<?php

namespace AppBundle\Form\Field\FieldType\Translatable;

use AppBundle\Form\Field\DataTransformer\ArrayToStringTransformer;
use AppBundle\Service\Field\ChoiceList\LanguageList;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\Loader\CallbackChoiceLoader;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;


/**
 * Class ListFieldType
 */
class ListFieldType extends AbstractType
{
    private $transformer;

    /**
     * ListFieldType constructor.
     * @param ArrayToStringTransformer $transformer
     */
    public function __construct(ArrayToStringTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'delete_button' => false
        ]);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $deleteButton = $options['delete_button'];

        $builder
            ->add('language', ChoiceType::class, [
                'label' => 'Language',
                'choice_loader' => new CallbackChoiceLoader(function() {
                    return LanguageList::getList();
                }),
                'attr' => [
                    'class' => 'custom-select'
                ]
            ])
            ->add('list', TextType::class, [
                'label' => 'List',
                'empty_data' => '',
                'required' => false,
            ])
        ;

        $builder->get('list')->addModelTransformer($this->transformer);

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($deleteButton) {
                if (!$deleteButton) {
                    return;
                }

                $form = $event->getForm();

                $form->add('delete_entity', SubmitType::class, [
                    'label' => 'Delete',
                    'attr' => [
                        'class' => 'btn-outline-danger'
                    ]
                ]);
            }
        );
    }
}
