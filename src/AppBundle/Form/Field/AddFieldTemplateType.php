<?php

namespace AppBundle\Form\Field;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;


/**
 * Class AddFieldTemplateType
 */
class AddFieldTemplateType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fieldTemplate', FieldTemplateType::class, [
                'mapped' => false,
                'data' => $options['data']
            ])
            ->add('submit_go_back', SubmitType::class, [
                'label' => 'Go back',
                'attr' => [
                    'class' => 'btn-outline-primary'
                ]
            ])
            ->add('submit_next_step', SubmitType::class, [
                'label' => 'Save and next',
                'attr' => [
                    'class' => 'btn-outline-primary'
                ]
            ])
        ;
    }
}
