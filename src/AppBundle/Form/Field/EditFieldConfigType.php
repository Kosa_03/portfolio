<?php

namespace AppBundle\Form\Field;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;


/**
 * Class EditFieldConfigType
 */
class EditFieldConfigType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fieldConfig', FieldConfigType::class, [
                'mapped' => false,
                'data' => $options['data']
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Update configuration',
                'attr' => [
                    'class' => 'btn-outline-primary'
                ]
            ])
        ;

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
                $form = $event->getForm()->get('fieldConfig');
                $data = $event->getData();

                $form->remove('fieldClass');

                $array = explode('\\', $data->getFieldClass());

                $form->add('fieldClass', TextType::class, [
                    'label' => 'Field class name',
                    'data' => end($array),
                    'disabled' => true
                ]);
            }
        );
    }
}
