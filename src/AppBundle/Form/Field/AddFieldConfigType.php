<?php

namespace AppBundle\Form\Field;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;


/**
 * Class AddFieldConfigType
 */
class AddFieldConfigType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('amountEntities', ChoiceType::class, [
                'label' => 'Number of entities (works only for Translatable fields)',
                'choices' => [
                    '1' => 1,
                    '2' => 2,
                    '3' => 3,
                    '4' => 4,
                    '5' => 5
                ],
                'attr' => [
                    'class' => 'custom-select'
                ]
            ])
            ->add('fieldConfig', FieldConfigType::class, [
                'mapped' => false,
                'data' => $options['data']
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Save and next',
                'attr' => [
                    'class' => 'btn-outline-primary'
                ]
            ])
        ;
    }
}
