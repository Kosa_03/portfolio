<?php

namespace AppBundle\Form\Field;

use AppBundle\Service\Field\ChoiceList\ControllerList;
use AppBundle\Service\Field\ChoiceList\FieldList;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\Loader\CallbackChoiceLoader;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;


/**
 * Class ListFieldType
 */
class ListFieldType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('controller', ChoiceType::class, [
                'label' => 'Select controller',
                'choice_loader' => new CallbackChoiceLoader(function() {
                    $allOption = [
                        'All controllers' => 'all_controllers'
                    ];
                    return array_merge($allOption, ControllerList::getList());
                }),
                'attr' => [
                    'class' => 'custom-select'
                ]
            ])
            ->add('fieldClass', ChoiceType::class, [
                'label' => 'Select field class',
                'choice_loader' => new CallbackChoiceLoader(function() {
                    $allOption = [
                        'All fields' => 'all_fields'
                    ];
                    return array_merge($allOption, FieldList::getList());
                }),
                'attr' => [
                    'class' => 'custom-select'
                ]
            ])
            ->add('sortBy', ChoiceType::class, [
                'label' => 'Sort by',
                'choices' => [
                    'Config Id' => 'id',
                    'FieldName' => 'fieldName',
                    'Controller' => 'controller',
                    'Method' => 'method',
                    'FieldClass' => 'fieldClass',
                    'Weight' => 'weight'
                ],
                'attr' => [
                    'class' => 'custom-select'
                ]
            ])
            ->add('sortMethod', ChoiceType::class, [
                'label' => 'Sort method',
                'choices' => [
                    'Increasing' => 'ASC',
                    'Decreasing' => 'DESC'
                ],
                'attr' => [
                    'class' => 'custom-select'
                ]
            ])
            ->add('fieldsPerPage', ChoiceType::class, [
                'label' => 'Fields per page',
                'choices' => [
                    '10' => 10,
                    '25' => 25,
                    '50' => 50,
                    '100' => 100
                ],
                'attr' => [
                    'class' => 'custom-select'
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Search',
                'attr' => [
                    'class' => 'btn-outline-primary'
                ]
            ])
        ;
    }
}
