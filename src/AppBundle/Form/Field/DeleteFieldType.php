<?php

namespace AppBundle\Form\Field;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;


/**
 * Class DeleteFieldType
 */
class DeleteFieldType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('confirm_yes', SubmitType::class, [
                'label' => 'Delete',
                'attr' => [
                    'class' => 'btn-outline-danger mr-2'
                ]
            ])
            ->add('confirm_no', SubmitType::class, [
                'label' => 'Go back',
                'attr' => [
                    'class' => 'btn-outline-primary'
                ]
            ])
        ;
    }
}
