<?php

namespace AppBundle\Form\Field;

use AppBundle\Service\Field\ChoiceList\ControllerList;
use AppBundle\Service\Field\ChoiceList\FieldList;
use AppBundle\Service\Field\ChoiceList\RolesList;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\Loader\CallbackChoiceLoader;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;


/**
 * Class FieldConfigType
 */
class FieldConfigType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fieldClass', ChoiceType::class, [
                'label' => 'Field class name',
                'choice_loader' => new CallbackChoiceLoader(function() {
                    return FieldList::getList();
                }),
                'attr' => [
                    'class' => 'custom-select'
                ]
            ])
            ->add('fieldName', TextType::class, [
                'label' => 'Field name',
                'required' => false,
                'empty_data' => '',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Field should not be blank.'
                    ]),
                    new Length([
                        'max' => 48,
                        'maxMessage' => 'Field is too long. Max length is 48 chars.'
                    ]),
                    new Regex([
                        'pattern' => '/^[a-zA-Z0-9_]+$/i',
                        'message' => 'Field should contains only a-z, A-Z, 0-9 and _ chars.'
                    ])
                ]
            ])
            ->add('groupId', TextType::class, [
                'label' => 'Group Id',
                'required' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Field should not be blank.'
                    ]),
                    new Regex([
                        'pattern' => '/^[0-9]+$/i',
                        'message' => 'Value should be an integer.'
                    ])
                ]
            ])
            ->add('controller', ChoiceType::class, [
                'label' => 'Controller',
                'choice_loader' => new CallbackChoiceLoader(function() {
                    return ControllerList::getList();
                }),
                'attr' => [
                    'class' => 'custom-select'
                ]
            ])
            ->add('method', TextType::class, [
                'label' => 'Method',
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Field should not be blank.'
                    ]),
                    new Length([
                        'max' => 32,
                        'maxMessage' => 'Field is too long. Max length is 32 chars.'
                    ]),
                    new Regex([
                        'pattern' => '/^[a-zA-Z_]+$/i',
                        'message' => 'Field should contains only a-z, A-Z, _ letters.'
                    ])
                ]
            ])
            ->add('weight', TextType::class, [
                'label' => 'Weight',
                'required' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Field should not be blank.'
                    ]),
                    new Regex([
                        'pattern' => '/^[0-9]+$/i',
                        'message' => 'Value should be an integer.'
                    ])
                ]
            ])
            ->add('roles', ChoiceType::class, [
                'label' => 'Roles',
                'choice_loader' => new CallbackChoiceLoader(function() {
                    return RolesList::getList();
                }),
                'multiple' => true,
                'expanded' => true
            ])
        ;
    }
}
