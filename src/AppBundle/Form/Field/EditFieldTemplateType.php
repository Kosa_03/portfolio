<?php

namespace AppBundle\Form\Field;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;


/**
 * Class EditFieldTemplateType
 */
class EditFieldTemplateType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fieldTemplate', FieldTemplateType::class, [
                'mapped' => false,
                'data' => $options['data']
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Update template',
                'attr' => [
                    'class' => 'btn-outline-primary'
                ]
            ])
        ;
    }
}
