<?php

namespace AppBundle\Form\Field\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;


/**
 * Class ArrayCssToStringCssTransformer
 */
class ArrayCssToStringCssTransformer implements DataTransformerInterface
{
    /**
     * @param array $arrayCss
     * @return string
     */
    public function transform($arrayCss) : string
    {
        if (is_null($arrayCss)) {
            $arrayCss = [];
        }

        $string = '';

        foreach ($arrayCss as $key => $value) {
            if (empty($key) && empty($value)) {
                $string .= '';
            }

            if (empty($key) && !empty($value)) {
                $string .= $value . "\r\n";
            }

            if (!empty($key) && empty($value)) {
                $string .= $key . "\r\n";
            }

            if (!empty($key) && !empty($value)) {
                $string .= $key . ': ' . $value . "\r\n";
            }
        }

        if (substr($string, -2, 2) == "\r\n") {
            $string = substr_replace($string, '', -2, 2);
        }

        return $string;
    }

    /**
     * @param string $stringCss
     * @return array
     */
    public function reverseTransform($stringCss) : array
    {
        if (empty($stringCss)) {
            return [];
        }

        $arrayRows = explode("\r\n", $stringCss);
        $arrayCss = [];

        foreach ($arrayRows as $row) {
            $row = explode(': ', $row);

            if (!array_key_exists(1, $row)) {
                $row[1] = '';
            }

            $arrayCss[$row[0]] = $row[1];
        }

        return $arrayCss;
    }
}
