<?php

namespace AppBundle\Form\Field\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;


/**
 * Class ArrayToStringTransformer
 */
class ArrayToStringTransformer implements DataTransformerInterface
{
    /**
     * @param array $array
     * @return string
     */
    public function transform($array) : string
    {
        if (is_null($array)) {
            return '';
        }

        return implode(';', $array);
    }

    /**
     * @param string $string
     * @return array
     */
    public function reverseTransform($string) : array
    {
        return explode(';', $string);
    }
}
