<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;


class ContactType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('from', EmailType::class, array(
                'label' => 'E-mail zwrotny',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array(
                        'message' => 'Pole "email" nie może być puste.'
                    )),
                    new Regex(array(
                        'pattern' => '/^[a-z\d_\.\-]+\@[a-z\d]{1}[a-z\d_\.\-]*[a-z\d]{1}\.[a-z]{2,8}$/i',
                        'message' => 'Podany adres email jest niepoprawny.'
                    ))
                )
            ))
            ->add('subject', TextType::class, array(
                'label' => 'Temat',
                'required' => false,
                'constraints' => array(
                    new Length(array(
                        'max' => 70,
                        'maxMessage' => 'Pole "temat" jest zbyt długie. Maksymalna długość to 70 znaków.'
                    ))
                )
            ))
            ->add('message', TextareaType::class, array(
                'label' => 'Wiadomość',
                'required' => false,
                'constraints' => array(
                    new Length(array(
                        'max' => 1024,
                        'maxMessage' => 'Pole "wiadomość" jest zbyt długie. Maksymalna długość to 1024 znaki.'
                    ))
                )
            ))
            ->add('send', SubmitType::class, array(
                'label' => 'Wyślij',
                'attr' => array(
                    'class' => 'btn-outline-primary'
                )
            ))
        ;
    }
}
