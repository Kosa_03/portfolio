<?php

namespace AppBundle\Form\Blog;

use AppBundle\Form\Blog\CollectionType\BlogTranslationType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;


/**
 * Class EditBlogTranslationType
 */
class EditBlogTranslationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) : void
    {
        $builder
            ->add('translations', CollectionType::class, [
                'entry_type' => BlogTranslationType::class,
                'entry_options' => [
                    'delete_button' => true
                ]
            ])
            ->add('add_translation', SubmitType::class, [
                'label' => 'Add translation',
                'attr' => [
                    'class' => 'btn btn-outline-success'
                ]
            ])
            ->add('article_preview', SubmitType::class, [
                'label' => 'Preview',
                'attr' => [
                    'class' => 'btn btn-outline-primary'
                ]
            ])
            ->add('clear_translations', SubmitType::class, [
                'label' => 'Clear translations',
                'attr' => [
                    'class' => 'btn btn-outline-danger'
                ]
            ])
            ->add('update_translations', SubmitType::class, [
                'label' => 'Update translations',
                'attr' => [
                    'class' => 'btn btn-outline-primary'
                ]
            ])
        ;
    }
}
