<?php

namespace AppBundle\Form\Blog\CustomType;

use AppBundle\Entity\Blog\Tag\BlogTag;
use AppBundle\Service\Blog\ChoiceList\BlogTagList;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\Loader\CallbackChoiceLoader;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;


/**
 * Class BlogTagsType
 */
class BlogTagsType extends AbstractType
{
    private $blogTagList;

    /**
     * SearchBlogType constructor.
     * @param BlogTagList $blogTagList
     */
    public function __construct(BlogTagList $blogTagList)
    {
        $this->blogTagList = $blogTagList;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) : void
    {
        $blogTagList = $this->blogTagList;

        $resolver->setDefaults([
            'label' => 'Select tags (optional)',
            'choice_value' => 'name',
            'choice_label' => function (?BlogTag $blogTag) {
                return $blogTag ? $blogTag->getName() : '';
            },
            'choice_loader' => new CallbackChoiceLoader(function () use ($blogTagList) {
                return $blogTagList->getList();
            }),
            'multiple' => true,
            'expanded' => true,
            'label_attr' => [
                'class' => 'pt-0'
            ],
            'attr' => [
                'class' => 'blog-tags-inline'
            ]
        ]);
    }

    public function getParent() : string
    {
        return ChoiceType::class;
    }
}
