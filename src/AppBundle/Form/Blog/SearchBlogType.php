<?php

namespace AppBundle\Form\Blog;

use AppBundle\Form\Blog\CustomType\BlogTagsType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;


/**
 * Class SearchBlogType
 */
class SearchBlogType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) : void
    {
        $builder
            ->add('status', ChoiceType::class, [
                'label' => 'Select article status',
                'choices' => [
                    'All status' => 'all_status',
                    'Published' => 'published',
                    'Not published' => 'not_published'
                ],
                'multiple' => false,
                'expanded' => true,
                'label_attr' => [
                    'class' => 'pt-0'
                ]
            ])
            ->add('filterType', ChoiceType::class, [
                'label' => 'Select filter',
                'choices' => [
                    'All articles' => 'all_articles',
                    'Articles with selected tags' => 'articles_contains_selected_tags',
                    'Articles without tags' => 'articles_without_tags'
                ],
                'multiple' => false,
                'expanded' => true,
                'label_attr' => [
                    'class' => 'pt-0'
                ]
            ])
            ->add('tags', BlogTagsType::class)
            ->add('sortBy', ChoiceType::class, [
                'label' => 'Sort by',
                'choices' => [
                    'Article Id' => 'id',
                    'Status' => 'status',
                    'Viewed' => 'amountOfDisplay',
                    'Modified' => 'modified',
                    'Created' => 'created'
                ],
                'attr' => [
                    'class' => 'custom-select'
                ]
            ])
            ->add('sortMethod', ChoiceType::class, [
                'label' => 'Sort method',
                'choices' => [
                    'Increasing' => 'ASC',
                    'Decreasing' => 'DESC'
                ],
                'attr' => [
                    'class' => 'custom-select'
                ]
            ])
            ->add('articlesPerPage', ChoiceType::class, [
                'label' => 'Articles per page',
                'choices' => [
                    '10' => 10,
                    '25' => 25,
                    '50' => 50,
                    '100' => 100
                ],
                'attr' => [
                    'class' => 'custom-select'
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Search',
                'attr' => [
                    'class' => 'btn-outline-primary'
                ]
            ])
        ;
    }
}
