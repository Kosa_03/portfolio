<?php

namespace AppBundle\Form\Blog;

use AppBundle\Form\Blog\CollectionType\BlogTagType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;


/**
 * Class EditBlogTagListType
 */
class EditBlogTagListType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) : void
    {
        $builder
            ->add('tags', CollectionType::class, [
                'entry_type' => BlogTagType::class
            ])
            ->add('add_tag', SubmitType::class, [
                'label' => 'Add Tag',
                'attr' => [
                    'class' => 'btn btn-outline-success'
                ]
            ])
            ->add('update_tags', SubmitType::class, [
                'label' => 'Update Tag List',
                'attr' => [
                    'class' => 'btn btn-outline-primary'
                ]
            ])
        ;
    }
}
