<?php

namespace AppBundle\Form\Blog;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;


/**
 * Class BlogLikeNeutralDislikeBarType
 */
class BlogLikeNeutralDislikeBarType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) : void
    {
        $builder
            ->add('vote_like', SubmitType::class, [
                'label' => 'Add Like',
                'attr' => [
                    'class' => 'blog-article-vote'
                ]
            ])
            ->add('vote_neutral', SubmitType::class, [
                'label' => 'Add Neutral',
                'attr' => [
                    'class' => 'blog-article-vote'
                ]
            ])
            ->add('vote_dislike', SubmitType::class, [
                'label' => 'Add Dislike',
                'attr' => [
                    'class' => 'blog-article-vote'
                ]
            ])
        ;
    }
}
