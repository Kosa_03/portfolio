<?php

namespace AppBundle\Form\Blog;

use AppBundle\Form\Blog\CustomType\BlogTagsType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;


/**
 * Class AddBlogArticleType
 */
class AddBlogArticleType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) : void
    {
        $builder
            ->add('amountTranslations', ChoiceType::class, [
                'label' => 'Number of translations',
                'choices' => [
                    '1' => 1,
                    '2' => 2,
                    '3' => 3,
                    '4' => 4,
                    '5' => 5
                ],
                'attr' => [
                    'class' => 'custom-select'
                ]
            ])
            ->add('status', ChoiceType::class, [
                'label' => 'Options',
                'choices' => [
                    'Not published' => 'not_published',
                    'Published' => 'published'
                ],
                'expanded' => true,
                'multiple' => false
            ])
            ->add('tags', BlogTagsType::class)
            ->add('headerImageFile', HiddenType::class, [
                'label' => 'Header background image',
                'error_bubbling' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Header background image should not be blank.'
                    ])
                ]
            ])
            ->add('imageFile', FileType::class, [
                'label' => 'No file selected',
                'mapped' => false,
                'required' => false,
                'label_attr' => [
                    'class' => 'custom-file-label'
                ],
                'attr' => [
                    'class' => 'custom-file-input'
                ],
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png'
                        ],
                        'mimeTypesMessage' => 'Selected file is not an JPEG or PNG image.'
                    ])
                ]
            ])
            ->add('upload_image', SubmitType::class, [
                'label' => 'Upload image',
                'attr' => [
                    'class' => 'btn btn-outline-primary'
                ]
            ])
            ->add('submit_next_step', SubmitType::class, [
                'label' => 'Save and next',
                'attr' => [
                    'class' => 'btn btn-outline-primary'
                ]
            ])
        ;
    }
}
