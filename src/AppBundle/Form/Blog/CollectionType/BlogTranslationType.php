<?php

namespace AppBundle\Form\Blog\CollectionType;

use AppBundle\Service\Field\ChoiceList\LanguageList;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\Loader\CallbackChoiceLoader;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;


/**
 * Class BlogTranslationType
 */
class BlogTranslationType extends AbstractType
{
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) : void
    {
        $resolver->setDefaults([
            'delete_button' => false
        ]);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) : void
    {
        $deleteButton = $options['delete_button'];

        $builder
            ->add('language', ChoiceType::class, [
                'label' => 'Language',
                'choice_loader' => new CallbackChoiceLoader(function() {
                    return LanguageList::getList();
                }),
                'attr' => [
                    'class' => 'custom-select'
                ]
            ])
            ->add('subject', TextType::class, [
                'label' => 'Subject',
                'required' => false,
                'empty_data' => '',
                'constraints' => [
                    new Length([
                        'max' => 512,
                        'maxMessage' => 'Subject should has max 512 chars.'
                    ])
                ]
            ])
            ->add('content', TextareaType::class, [
                'label' => 'Article content',
                'required' => false,
                'empty_data' => '',
                'constraints' => [
                    new Length([
                        'max' => 60000,
                        'maxMessage' => 'Article content is too long.'
                    ])
                ]
            ])
        ;

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($deleteButton) {
                if (!$deleteButton) {
                    return;
                }

                $form = $event->getForm();

                $form->add('delete_translation', SubmitType::class, [
                    'label' => 'Delete',
                    'attr' => [
                        'class' => 'btn-outline-danger'
                    ]
                ]);
            }
        );
    }
}
