<?php

namespace AppBundle\Form\Blog\CollectionType;

use AppBundle\Form\Blog\DataTransformer\ArrayLangToStringLangTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;


/**
 * Class BlogTagType
 */
class BlogTagType extends AbstractType
{
    private $arrayLangTransformer;

    /**
     * BlogTagType constructor.
     * @param ArrayLangToStringLangTransformer $arrayLangTransformer
     */
    public function __construct(ArrayLangToStringLangTransformer $arrayLangTransformer)
    {
        $this->arrayLangTransformer = $arrayLangTransformer;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) : void
    {
        $builder
            ->add('weight', TextType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'class' => 'form-control-sm',
                    'style' => 'max-width: 100px'
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Weight should not be blank.'
                    ])
                ]
            ])
            ->add('translations', TextareaType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'class' => 'form-control-sm',
                    'style' => 'max-width: 250px; max-height: 75px'
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Translations should not be blank.'
                    ])
                ]
            ])
            ->add('delete_tag', SubmitType::class, [
                'label' => 'Delete',
                'attr' => [
                    'class' => 'btn-sm btn-outline-danger'
                ]
            ])
        ;

        $builder->get('translations')->addModelTransformer($this->arrayLangTransformer);
    }
}
