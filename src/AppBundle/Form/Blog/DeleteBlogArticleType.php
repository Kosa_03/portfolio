<?php

namespace AppBundle\Form\Blog;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;


/**
 * Class DeleteBlogArticleType
 */
class DeleteBlogArticleType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) : void
    {
        $builder
            ->add('confirm_yes', SubmitType::class, [
                'label' => 'Delete',
                'attr' => [
                    'class' => 'btn btn-outline-danger mr-2'
                ]
            ])
            ->add('confirm_go_back', SubmitType::class, [
                'label' => 'Go back',
                'attr' => [
                    'class' => 'btn btn-outline-primary'
                ]
            ])
        ;
    }
}
