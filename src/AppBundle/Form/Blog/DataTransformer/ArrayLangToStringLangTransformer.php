<?php

namespace AppBundle\Form\Blog\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;


/**
 * Class ArrayLangToStringLangTransformer
 */
class ArrayLangToStringLangTransformer implements DataTransformerInterface
{
    /**
     * @param array $arrayLanguages
     * @return string
     */
    public function transform($arrayLanguages) : string
    {
        if (is_null($arrayLanguages)) {
            $arrayLanguages = [];
        }

        $stringLanguages = '';

        foreach ($arrayLanguages as $key => $value) {
            if (empty($key) && empty($value)) {
                $stringLanguages .= '';
            }

            if (empty($key) && !empty($value)) {
                $stringLanguages .= $value . "\r\n";
            }

            if (!empty($key) && empty($value)) {
                $stringLanguages .= $key . "\r\n";
            }

            if (!empty($key) && !empty($value)) {
                $stringLanguages .= $key . ': ' . $value . "\r\n";
            }
        }

        if (substr($stringLanguages, -2, 2) == "\r\n") {
            $stringLanguages = substr_replace($stringLanguages, '', -2, 2);
        }

        return $stringLanguages;
    }

    /**
     * @param string $stringLanguages
     * @return array
     */
    public function reverseTransform($stringLanguages) : array
    {
        if (empty($stringLanguages)) {
            return [];
        }

        $arrayRows = explode("\r\n", $stringLanguages);
        $arrayLanguages = [];

        foreach ($arrayRows as $row) {
            $row = explode(': ', $row);

            if (!array_key_exists(1, $row)) {
                $row[1] = '';
            }

            $arrayLanguages[$row[0]] = $row[1];
        }

        return $arrayLanguages;
    }
}
