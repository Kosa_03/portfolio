<?php

namespace AppBundle\Form\Blog;

use AppBundle\Form\Blog\CollectionType\BlogTranslationType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;


/**
 * Class AddBlogTranslationType
 */
class AddBlogTranslationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) : void
    {
        $builder
            ->add('translations', CollectionType::class, [
                'entry_type' => BlogTranslationType::class
            ])
            ->add('submit_go_back', SubmitType::class, [
                'label' => 'Go back',
                'attr' => [
                    'class' => 'btn btn-outline-primary'
                ]
            ])
            ->add('submit_create', SubmitType::class, [
                'label' => 'Create article',
                'attr' => [
                    'class' => 'btn btn-outline-success'
                ]
            ])
        ;
    }
}
