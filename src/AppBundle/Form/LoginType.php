<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;


class LoginType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('_username', TextType::class, array(
                'label' => 'Login',
                'required' => true
            ))
            ->add('_password', PasswordType::class, array(
                'label' => 'Hasło',
                'required' => true
            ))
            ->add('log_in', SubmitType::class, array(
                'label' => 'Loguj',
                'attr' => array(
                    'class' => 'btn-outline-primary'
                )
            ))
        ;
    }
}
