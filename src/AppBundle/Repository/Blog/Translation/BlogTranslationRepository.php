<?php

namespace AppBundle\Repository\Blog\Translation;

use AppBundle\Entity\Blog\Translation\NullBlogTranslation;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;


/**
 * Class BlogTranslationRepository
 */
class BlogTranslationRepository extends EntityRepository
{
    /**
     * @param int $articleId
     * @return array
     */
    public function findTranslationsByArticleId(int $articleId) : array
    {
        try {
            $query = $this->createQueryBuilder('blog_translation')
                ->where('blog_translation.articleId = :articleId')
                ->setParameters([
                    'articleId' => $articleId
                ])
                ->getQuery();

            return $query->getResult();

        } catch (ORMException $e) {
            return [
                0 => new NullBlogTranslation()
            ];
        }
    }
}
