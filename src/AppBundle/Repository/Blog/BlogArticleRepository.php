<?php

namespace AppBundle\Repository\Blog;

use AppBundle\Entity\Blog\BlogArticleInterface;
use AppBundle\Entity\Blog\NullBlogArticle;
use AppBundle\Service\Blog\Options\BlogFindOptions;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\QueryBuilder;


/**
 * Class BlogArticleRepository
 */
class BlogArticleRepository extends EntityRepository
{
    private $joinParamsMap;
    private $joinConditionsMap;
    private $whereConditionsMap;
    private $whereParamsMap;

    /**
     * @param BlogFindOptions $findOptions
     * @return array
     */
    public function findByBlogFindOptions(BlogFindOptions $findOptions) : array
    {
        $this->joinParamsMap = $findOptions->getJoinParams();
        $this->joinConditionsMap = $findOptions->getJoinConditions();
        $this->whereConditionsMap = $findOptions->getWhereConditions();
        $this->whereParamsMap = $findOptions->getWhereParams();

        try {
            $query = $this->createQueryBuilder('blog_article');

            $this->buildJoinTableQueryPart($query);
            $this->buildWhereQueryPart($query);

            if (count($this->whereParamsMap) > 0) {
                $query->setParameters(array_column($this->whereParamsMap, 'value', 'paramName'));
            }

            $query->orderBy('blog_article.' . $findOptions->getSortBy(), $findOptions->getSortMethod());

            if (!is_null($findOptions->getOffset())) {
                $query->setFirstResult($findOptions->getOffset());
            }

            if (!is_null($findOptions->getLimit())) {
                $query->setMaxResults($findOptions->getLimit());
            }

            $query = $query->getQuery();

            return $query->getResult();

        } catch (ORMException $e) {
            return [];
        }
    }

    /**
     * @param BlogFindOptions $findOptions
     * @return int
     */
    public function countByBlogFindOptions(BlogFindOptions $findOptions) : int
    {
        $this->joinParamsMap = $findOptions->getJoinParams();
        $this->joinConditionsMap = $findOptions->getJoinConditions();
        $this->whereConditionsMap = $findOptions->getWhereConditions();
        $this->whereParamsMap = $findOptions->getWhereParams();

        try {
            $query = $this->createQueryBuilder('blog_article');

            $this->buildJoinTableQueryPart($query);
            $this->buildWhereQueryPart($query);

            if (count($this->whereParamsMap) > 0) {
                $query->setParameters(array_column($this->whereParamsMap, 'value', 'paramName'));
            }

            $query->orderBy('blog_article.' . $findOptions->getSortBy(), $findOptions->getSortMethod());

            $query = $query->getQuery();

            return count($query->getResult());

        } catch (ORMException $e) {
            return 0;
        }
    }

    /**
     * @param QueryBuilder $query
     */
    private function buildJoinTableQueryPart(QueryBuilder $query) : void
    {
        $joinParamsMap = $this->joinParamsMap;
        $joinConditionsMap = $this->joinConditionsMap;

        foreach ($joinParamsMap as $key => $joinParamMap) {
            if (key_exists($key, $joinConditionsMap)) {
                $joinCondition = $joinParamMap['tableAlias'] . '.' . $joinParamMap['tableColumn'] . ' '
                    . $joinConditionsMap[$key]['operator'] . ' ' . $joinConditionsMap[$key]['tableAlias'] . '.'
                    . $joinConditionsMap[$key]['columnName'];

            } else {
                return;
            }

            if ($joinParamMap['joinType'] == 'left') {
                $query->leftJoin($joinParamMap['tableName'], $joinParamMap['tableAlias'], 'WITH', $joinCondition);

            } else {
                $query->join($joinParamMap['tableName'], $joinParamMap['tableAlias'], 'WITH', $joinCondition);
            }
        }
    }

    /**
     * @param QueryBuilder $query
     */
    private function buildWhereQueryPart(QueryBuilder $query) : void
    {
        $whereConditionsMap = $this->whereConditionsMap;
        $whereParamsMap = $this->whereParamsMap;

        $firstCondition = true;
        foreach ($whereConditionsMap as $key => $whereConditionMap) {
            $whereCondition = null;

            if ($whereConditionMap['operator'] == 'IS NULL') {
                $whereCondition = $whereConditionMap['tableAlias'] . '.' . $whereConditionMap['columnName'] . ' '
                    . 'IS NULL';
            }

            if (key_exists($key, $whereParamsMap)) {
                if ($whereConditionMap['operator'] == 'IN') {
                    $whereCondition = $whereConditionMap['tableAlias'] . '.' . $whereConditionMap['columnName'] . ' '
                        . $whereConditionMap['operator'] . ' (' . $whereParamsMap[$key]['paramName'] . ')';

                } else {
                    $whereCondition = $whereConditionMap['tableAlias'] . '.' . $whereConditionMap['columnName'] . ' '
                        . $whereConditionMap['operator'] . ' ' . $whereParamsMap[$key]['paramName'];
                }
            }

            if (is_null($whereCondition)) {
                return;
            }

            if ($firstCondition) {
                $query->where($whereCondition);
                $firstCondition = false;

            } else {
                if ($whereConditionMap['conditionType'] == 'OR') {
                    $query->orWhere($whereCondition);

                } else {
                    $query->andWhere($whereCondition);
                }
            }
        }
    }
}
