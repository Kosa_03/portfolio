<?php

namespace AppBundle\Repository\Field\FieldType;

use AppBundle\Entity\Field\FieldType\Component\NullField;
use AppBundle\Entity\Field\FieldType\FieldInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\ORMException;


/**
 * Class FieldTypeRepository
 */
class FieldTypeRepository extends EntityRepository
{
    /**
     * @param int $dataId
     * @return FieldInterface
     */
    public function findByDataId(int $dataId) : FieldInterface
    {
        try {
            $query = $this->createQueryBuilder('f_type')
                ->where('f_type.dataId = :dataId')
                ->setFirstResult(0)
                ->setMaxResults(1)
                ->setParameters(array(
                    'dataId' => $dataId
                ))
                ->getQuery();

            return $query->getSingleResult();

        } catch (NoResultException $e) {
            return new NullField();

        } catch (ORMException $e) {
            return new NullField();
        }
    }
}
