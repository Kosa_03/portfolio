<?php

namespace AppBundle\Repository\Field\Template;

use AppBundle\Entity\Field\Template\FieldTemplateInterface;
use AppBundle\Entity\Field\Template\NullFieldTemplate;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\ORMException;


/**
 * Class FieldTemplateRepository
 */
class FieldTemplateRepository extends EntityRepository
{
    /**
     * @param int $templateId
     * @return FieldTemplateInterface
     */
    public function findByTemplateId(int $templateId) : FieldTemplateInterface
    {
        try {
            $query = $this->createQueryBuilder('f_tpl')
                ->where('f_tpl.id = :templateId')
                ->setFirstResult(0)
                ->setMaxResults(1)
                ->setParameters(array(
                    'templateId' => $templateId
                ))
                ->getQuery();

            return $query->getSingleResult();

        } catch (NoResultException $e) {
            return new NullFieldTemplate();

        } catch (ORMException $e) {
            return new NullFieldTemplate();
        }
    }
}
