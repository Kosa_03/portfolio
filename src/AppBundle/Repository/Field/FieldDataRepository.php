<?php

namespace AppBundle\Repository\Field;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;


/**
 * Class FieldDataRepository
 */
class FieldDataRepository extends EntityRepository
{
    /**
     * @param int $configId
     * @return array
     */
    public function findByConfigId(int $configId) : array
    {
        try {
            $query = $this->createQueryBuilder('f_data')
                ->where('f_data.configId = :configId')
                ->setParameters(array(
                    'configId' => $configId
                ))
                ->getQuery();

            return $query->getResult();

        } catch (ORMException $e) {
            return [];
        }
    }

    /**
     * @param int $offset
     * @param int $limit
     * @param string $controllerName
     * @return array
     */
    public function findFieldsByController(int $offset, int $limit, string $controllerName = null) : array
    {
        /**
         * TODO: This method is for delete.
         */
        $query = $this->createQueryBuilder('f_data')
            ->leftJoin('f_data.config', 'f_conf')
            ->leftJoin('f_data.template', 'f_tpl')
            ->leftJoin('f_data.group', 'f_group')
            ->leftJoin('f_group.template', 'g_tpl')
            ->addSelect('f_conf')
            ->addSelect('f_tpl')
            ->addSelect('f_group')
            ->addSelect('g_tpl');

        if (!is_null($controllerName)) {
            $query->where('f_conf.controller = :controller')
                ->setParameters([
                    'controller' => $controllerName
                ]);
        }

        $query->orderBy('f_data.fieldId', 'ASC')
            ->setFirstResult($offset)
            ->setMaxResults($limit);

        $query = $query->getQuery();

        return $query->getResult();
    }
}
