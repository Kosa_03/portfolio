<?php

namespace AppBundle\Repository\Field;

use AppBundle\Service\Field\Options\FieldListOptions;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;


/**
 * Class FieldConfigRepository
 */
class FieldConfigRepository extends EntityRepository
{
    /**
     * @param FieldListOptions $listOptions
     * @return array
     */
    public function findByFieldListOptions(FieldListOptions $listOptions) : array
    {
        $optionsMap = $this->createOptionsMap($listOptions);

        try {
            $query = $this->createQueryBuilder('f_conf');

            $firstConditionAdded = false;
            foreach ($optionsMap as $key => $options) {
                $condition = 'f_conf.' . $options['columnName']
                    . ' ' . $options['operator'] . ' :' . $options['paramName'];

                if ($firstConditionAdded == true) {
                    if ($options['method'] == 'and') {
                        $query->andWhere($condition);
                    }

                    if ($options['method'] == 'or') {
                        $query->orWhere($condition);
                    }
                }

                if ($firstConditionAdded == false) {
                    $query->where($condition);
                    $firstConditionAdded = true;
                }
            }

            $query->orderBy('f_conf.' . $listOptions->getSortBy(), $listOptions->getSortMethod());

            if (count($optionsMap) > 0) {
                $query->setParameters(array_column($optionsMap, 'value', 'paramName'));
            }

            if (!is_null($listOptions->getOffset())) {
                $query->setFirstResult($listOptions->getOffset());
            }

            if (!is_null($listOptions->getLimit())) {
                $query->setMaxResults($listOptions->getLimit());
            }

            $query = $query->getQuery();

            return $query->getResult();

        } catch (ORMException $e) {
            return [];
        }
    }

    /**
     * @param FieldListOptions $listOptions
     * @return int
     */
    public function countByFieldListOption(FieldListOptions $listOptions) : int
    {
        $optionsMap = $this->createOptionsMap($listOptions);

        try {
            $query = $this->createQueryBuilder('f_conf');

            $firstConditionAdded = false;
            foreach ($optionsMap as $key => $options) {
                $condition = 'f_conf.' . $options['columnName']
                    . ' ' . $options['operator'] . ' :' . $options['paramName'];

                if ($firstConditionAdded == true) {
                    if ($options['method'] == 'and') {
                        $query->andWhere($condition);
                    }

                    if ($options['method'] == 'or') {
                        $query->orWhere($condition);
                    }
                }

                if ($firstConditionAdded == false) {
                    $query->where($condition);
                    $firstConditionAdded = true;
                }
            }

            if (count($optionsMap) > 0) {
                $query->setParameters(array_column($optionsMap, 'value', 'paramName'));
            }

            $query = $query->getQuery();

            return count($query->getResult());

        } catch (ORMException $e) {
            return 0;
        }
    }

    /**
     * @param FieldListOptions $listOptions
     * @return array
     */
    private function createOptionsMap(FieldListOptions $listOptions) : array
    {
        if (!empty($listOptions->getConditions())) {
            $optionsMap = $listOptions->getConditions();

        } else {
            $optionsMap = [
                [
                    'columnName' => 'id',
                    'paramName' => 'configId',
                    'value' => $listOptions->getConfigId(),
                    'operator' => '=',
                    'method' => 'and'
                ],
                [
                    'columnName' => 'controller',
                    'paramName' => 'controller',
                    'value' => $listOptions->getControllerName(),
                    'operator' => '=',
                    'method' => 'and'
                ],
                [
                    'columnName' => 'method',
                    'paramName' => 'method',
                    'value' => $listOptions->getMethodName(),
                    'operator' => '=',
                    'method' => 'and'
                ],
                [
                    'columnName' => 'fieldClass',
                    'paramName' => 'fieldClass',
                    'value' => $listOptions->getFieldClass(),
                    'operator' => '=',
                    'method' => 'and'
                ],
                [
                    'columnName' => 'fieldName',
                    'paramName' => 'fieldName',
                    'value' => $listOptions->getFieldName(),
                    'operator' => '=',
                    'method' => 'and'
                ]
            ];

            foreach ($optionsMap as $key => $options) {
                if (is_null($options['value'])) {
                    unset($optionsMap[$key]);
                }
            }
        }

        return $optionsMap;
    }

    /**
     * @return int
     */
    public function countNumberOfAllFields() : int
    {
        try {
            $query = $this->createQueryBuilder('f_conf')
                ->getQuery();

            return count($query->getResult());

        } catch (ORMException $e) {
            return 0;
        }
    }
}
