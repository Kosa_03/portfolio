<?php

namespace AppBundle\Repository\Field\Group;

use AppBundle\Entity\Field\Group\FieldGroupInterface;
use AppBundle\Entity\Field\Group\NullFieldGroup;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\ORMException;


/**
 * Class FieldGroupRepository
 */
class FieldGroupRepository extends EntityRepository
{
    /**
     * @param int $groupId
     * @return FieldGroupInterface
     */
    public function findByGroupId(int $groupId) : FieldGroupInterface
    {
        try {
            $query = $this->createQueryBuilder('f_group')
                ->where('f_group.id = :groupId')
                ->setFirstResult(0)
                ->setMaxResults(1)
                ->setParameters(array(
                    'groupId' => $groupId
                ))
                ->getQuery();

            return $query->getSingleResult();

        } catch (NoResultException $e) {
            return new NullFieldGroup();

        } catch (ORMException $e) {
            return new NullFieldGroup();
        }
    }
}
