<?php

namespace AppBundle\Entity\Blog;

use AppBundle\Entity\Blog\Translation\BlogTranslationInterface;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Interface BlogArticleInterface
 */
interface BlogArticleInterface
{
    public function getId() : int;
    public function getAuthorId() : int;
    public function setAuthorId(int $authorId);
    public function getEditorId() : ?int;
    public function setEditorId(int $editorId);
    public function getStatus() : string;
    public function setPublished();
    public function setNotPublished();
    public function getTags() : ArrayCollection;
    public function setTags(ArrayCollection $blogTagsCollection);
    public function getAmountOfDisplay() : int;
    public function setAmountOfDisplay(int $amountOfDisplay);
    public function getAmountOfLike() : int;
    public function setAmountOfLike(int $amountOfLike);
    public function getAmountOfNeutral() : int;
    public function setAmountOfNeutral(int $amountOfNeutral);
    public function getAmountOfDislike() : int;
    public function setAmountOfDislike(int $amountOfDislike);
    public function getCreated() : \DateTime;
    public function setCreated(\DateTime $created);
    public function getModified() : \DateTime;
    public function setModified(\DateTime $modified);
    public function getHeaderImageFile() : string;
    public function setHeaderImageFile(string $headerImageFile);
    public function getTranslation() : BlogTranslationInterface;
    public function setTranslation(BlogTranslationInterface $translation);
    public function getTranslations() : ArrayCollection;
}
