<?php

namespace AppBundle\Entity\Blog;

use AppBundle\Entity\Blog\Translation\BlogTranslationInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class BlogArticle
 *
 * @ORM\Table(name="blog_article")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Blog\BlogArticleRepository")
 */
class BlogArticle implements BlogArticleInterface
{
    const STATUS_PUBLISHED = 'published';
    const STATUS_NOT_PUBLISHED = 'not_published';

    /**
     * @var int
     *
     * @ORM\Column(name="article_id", type="integer", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="author_id", type="integer", options={"unsigned":true})
     */
    private $authorId;

    /**
     * @var int
     *
     * @ORM\Column(name="editor_id", type="integer", options={"unsigned":true})
     */
    private $editorId;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=16)
     */
    private $status;

    /**
     * @var ArrayCollection
     */
    private $tags;

    /**
     * @var int
     *
     * @ORM\Column(name="amount_of_display", type="integer", options={"unsigned":true})
     */
    private $amountOfDisplay = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="amount_of_like", type="integer", options={"unsigned":true})
     */
    private $amountOfLike = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="amount_of_neutral", type="integer", options={"unsigned":true})
     */
    private $amountOfNeutral = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="amount_of_dislike", type="integer", options={"unsigned":true})
     */
    private $amountOfDislike = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", options={"default":0})
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", options={"default":0})
     */
    private $modified;

    /**
     * @var string
     *
     * @ORM\Column(name="header_image_file", type="string", length=255)
     */
    private $headerImageFile;

    /**
     * @var BlogTranslationInterface
     */
    private $translation;

    /**
     * @var ArrayCollection
     */
    private $translations;

/*
 * Getters and setters
 */

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getAuthorId() : int
    {
        return $this->authorId;
    }

    /**
     * @param int $authorId
     * @return BlogArticle
     */
    public function setAuthorId(int $authorId) : BlogArticle
    {
        $this->authorId = $authorId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getEditorId() : ?int
    {
        return $this->editorId;
    }

    /**
     * @param int $editorId
     * @return BlogArticle
     */
    public function setEditorId(int $editorId) : BlogArticle
    {
        $this->editorId = $editorId;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus() : string
    {
        return $this->status;
    }

    /**
     * @return BlogArticle
     */
    public function setPublished() : BlogArticle
    {
        $this->status = self::STATUS_PUBLISHED;
        return $this;
    }

    /**
     * @return BlogArticle
     */
    public function setNotPublished() : BlogArticle
    {
        $this->status = self::STATUS_NOT_PUBLISHED;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getTags() : ArrayCollection
    {
        if (is_null($this->tags)) {
            $this->tags = new ArrayCollection();
        }

        return $this->tags;
    }

    /**
     * @param ArrayCollection $blogTagsCollection
     * @return BlogArticle
     */
    public function setTags(ArrayCollection $blogTagsCollection) : BlogArticle
    {
        $this->tags = $blogTagsCollection;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmountOfDisplay() : int
    {
        return $this->amountOfDisplay;
    }

    /**
     * @param int $amountOfDisplay
     * @return BlogArticle
     */
    public function setAmountOfDisplay(int $amountOfDisplay) : BlogArticle
    {
        $this->amountOfDisplay = $amountOfDisplay;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmountOfLike() : int
    {
        return $this->amountOfLike;
    }

    /**
     * @param int $amountOfLike
     * @return BlogArticle
     */
    public function setAmountOfLike(int $amountOfLike) : BlogArticle
    {
        $this->amountOfLike = $amountOfLike;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmountOfNeutral() : int
    {
        return $this->amountOfNeutral;
    }

    /**
     * @param int $amountOfNeutral
     * @return BlogArticle
     */
    public function setAmountOfNeutral(int $amountOfNeutral) : BlogArticle
    {
        $this->amountOfNeutral = $amountOfNeutral;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmountOfDislike() : int
    {
        return $this->amountOfDislike;
    }

    /**
     * @param int $amountOfDislike
     * @return BlogArticle
     */
    public function setAmountOfDislike(int $amountOfDislike) : BlogArticle
    {
        $this->amountOfDislike = $amountOfDislike;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated() : \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     * @return BlogArticle
     */
    public function setCreated(\DateTime $created) : BlogArticle
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModified() : \DateTime
    {
        return $this->modified;
    }

    /**
     * @param \DateTime $modified
     * @return BlogArticle
     */
    public function setModified(\DateTime $modified) : BlogArticle
    {
        $this->modified = $modified;
        return $this;
    }

    /**
     * @return string
     */
    public function getHeaderImageFile() : string
    {
        return $this->headerImageFile;
    }

    /**
     * @param string $headerImageFile
     * @return BlogArticle
     */
    public function setHeaderImageFile(string $headerImageFile) : BlogArticle
    {
        $this->headerImageFile = $headerImageFile;
        return $this;
    }

    /**
     * @return BlogTranslationInterface
     */
    public function getTranslation() : BlogTranslationInterface
    {
        return $this->translation;
    }

    /**
     * @param BlogTranslationInterface $translation
     * @return BlogArticle
     */
    public function setTranslation(BlogTranslationInterface $translation) : BlogArticle
    {
        $this->translation = $translation;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getTranslations() : ArrayCollection
    {
        if (is_null($this->translations)) {
            $this->translations = new ArrayCollection();
        }

        return $this->translations;
    }
}
