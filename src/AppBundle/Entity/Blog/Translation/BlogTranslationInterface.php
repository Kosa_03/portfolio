<?php

namespace AppBundle\Entity\Blog\Translation;


/**
 * Interface BlogTranslationInterface
 */
interface BlogTranslationInterface
{
    public function getId() : int;
    public function getArticleId() : int;
    public function setArticleId(int $articleId);
    public function getCreated() : \DateTime;
    public function setCreated(\DateTime $created);
    public function getModified() : \DateTime;
    public function setModified(\DateTime $modified);
    public function getLanguage() : string;
    public function setLanguage(string $language);
    public function getSubject() : string;
    public function setSubject(string $subject);
    public function getContent() : string;
    public function setContent(string $content);
}
