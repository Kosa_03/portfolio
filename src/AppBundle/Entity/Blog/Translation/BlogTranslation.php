<?php

namespace AppBundle\Entity\Blog\Translation;

use Doctrine\ORM\Mapping as ORM;


/**
 * Class BlogTranslation
 *
 * @ORM\Table(name="blog_translation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Blog\Translation\BlogTranslationRepository")
 */
class BlogTranslation implements BlogTranslationInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="translation_id", type="integer", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="article_id", type="integer", options={"unsigned":true})
     */
    private $articleId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", options={"default":0})
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", options={"default":0})
     */
    private $modified;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=3, options={"default":"und"})
     */
    private $language;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", length=512)
     */
    private $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @return int
     */
    public function getTranslationId() : int
    {
        return $this->getId();
    }

/*
 * Getters and setters
 */

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getArticleId() : int
    {
        return $this->articleId;
    }

    /**
     * @param int $articleId
     * @return BlogTranslation
     */
    public function setArticleId(int $articleId) : BlogTranslation
    {
        $this->articleId = $articleId;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated() : \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     * @return BlogTranslation
     */
    public function setCreated(\DateTime $created) : BlogTranslation
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModified() : \DateTime
    {
        return $this->modified;
    }

    /**
     * @param \DateTime $modified
     * @return BlogTranslation
     */
    public function setModified(\DateTime $modified) : BlogTranslation
    {
        $this->modified = $modified;
        return $this;
    }

    /**
     * @return string
     */
    public function getLanguage() : string
    {
        return $this->language;
    }

    /**
     * @param string $language
     * @return BlogTranslation
     */
    public function setLanguage(string $language) : BlogTranslation
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubject() : string
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     * @return BlogTranslation
     */
    public function setSubject(string $subject) : BlogTranslation
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent() : string
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return BlogTranslation
     */
    public function setContent(string $content) : BlogTranslation
    {
        $this->content = $content;
        return $this;
    }
}
