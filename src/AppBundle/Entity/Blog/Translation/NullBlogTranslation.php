<?php

namespace AppBundle\Entity\Blog\Translation;


/**
 * Class NullBlogTranslation
 */
class NullBlogTranslation implements BlogTranslationInterface
{
    private $id;
    private $articleId;
    private $created;
    private $modified;
    private $language;
    private $subject;
    private $content;

    public function __construct()
    {
        $this->id = -1;
        $this->articleId = -1;
        $this->created = new \DateTime();
        $this->modified = new \DateTime();
        $this->language = 'und';
        $this->subject = 'We are sorry. Article not found.';
        $this->content = 'We are sorry, but translation of this article can not be found.';
    }

    /**
     * @return int
     */
    public function getTranslationId() : int
    {
        return $this->getId();
    }

/*
 * Getters and setters
 */

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getArticleId() : int
    {
        return $this->articleId;
    }

    /**
     * @param int $articleId
     * @return NullBlogTranslation
     */
    public function setArticleId(int $articleId) : NullBlogTranslation
    {
        $this->articleId = $articleId;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated() : \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     * @return NullBlogTranslation
     */
    public function setCreated(\DateTime $created) : NullBlogTranslation
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModified() : \DateTime
    {
        return $this->modified;
    }

    /**
     * @param \DateTime $modified
     * @return NullBlogTranslation
     */
    public function setModified(\DateTime $modified) : NullBlogTranslation
    {
        $this->modified = $modified;
        return $this;
    }

    /**
     * @return string
     */
    public function getLanguage() : string
    {
        return $this->language;
    }

    /**
     * @param string $language
     * @return NullBlogTranslation
     */
    public function setLanguage(string $language) : NullBlogTranslation
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubject() : string
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     * @return NullBlogTranslation
     */
    public function setSubject(string $subject) : NullBlogTranslation
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent() : string
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return NullBlogTranslation
     */
    public function setContent(string $content) : NullBlogTranslation
    {
        $this->content = $content;
        return $this;
    }
}
