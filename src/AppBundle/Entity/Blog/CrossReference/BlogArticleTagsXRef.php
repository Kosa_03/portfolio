<?php

namespace AppBundle\Entity\Blog\CrossReference;

use Doctrine\ORM\Mapping as ORM;


/**
 * Class BlogArticleTagsXRef
 *
 * @ORM\Table(name="blog_article_tags_cross_ref", indexes={
 *     @ORM\Index(name="searching_by_article_idx", columns={"article_id"})
 * })
 * @ORM\Entity
 */
class BlogArticleTagsXRef
{
    /**
     * @var int
     *
     * @ORM\Column(name="article_id", type="integer", options={"unsigned":true})
     * @ORM\Id
     */
    private $articleId;

    /**
     * @var int
     *
     * @ORM\Column(name="tag_id", type="integer", options={"unsigned":true})
     * @ORM\Id
     */
    private $tagId;

/*
 * Getters and setters
 */

    /**
     * @return int
     */
    public function getArticleId() : int
    {
        return $this->articleId;
    }

    /**
     * @param int $articleId
     * @return BlogArticleTagsXRef
     */
    public function setArticleId(int $articleId) : BlogArticleTagsXRef
    {
        $this->articleId = $articleId;
        return $this;
    }

    /**
     * @return int
     */
    public function getTagId() : int
    {
        return $this->tagId;
    }

    /**
     * @param int $tagId
     * @return BlogArticleTagsXRef
     */
    public function setTagId(int $tagId) : BlogArticleTagsXRef
    {
        $this->tagId = $tagId;
        return $this;
    }
}
