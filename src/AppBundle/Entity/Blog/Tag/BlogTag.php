<?php

namespace AppBundle\Entity\Blog\Tag;

use Doctrine\ORM\Mapping as ORM;


/**
 * Class BlogTag
 *
 * @ORM\Table(name="blog_tag")
 * @ORM\Entity
 */
class BlogTag implements \ArrayAccess
{
    use ArrayAccessTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="tag_id", type="integer", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $tagId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="weight", type="integer")
     */
    private $weight;

    /**
     * @var array
     *
     * @ORM\Column(name="translations", type="json_array")
     */
    private $translations;

/*
 * Getters and setters
 */

    /**
     * @return int
     */
    public function getTagId() : int
    {
        return $this->tagId;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return BlogTag
     */
    public function setName(string $name) : BlogTag
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getWeight() : int
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     * @return BlogTag
     */
    public function setWeight(int $weight) : BlogTag
    {
        $this->weight = $weight;
        return $this;
    }

    /**
     * @return array
     */
    public function getTranslations() : array
    {
        return $this->translations;
    }

    /**
     * @param array $translations
     * @return BlogTag
     */
    public function setTranslations(array $translations) : BlogTag
    {
        $this->translations = $translations;
        return $this;
    }
}
