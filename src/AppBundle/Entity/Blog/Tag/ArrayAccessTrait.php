<?php

namespace AppBundle\Entity\Blog\Tag;


/**
 * Trait ArrayAccessTrait
 */
trait ArrayAccessTrait
{
    /**
     * @param $offset
     * @return bool
     */
    public function offsetExists($offset) : bool
    {
        return isset($this->$offset);
    }

    /**
     * @param $offset
     * @param $value
     */
    public function offsetSet($offset, $value) : void
    {
        $this->$offset = $value;
    }

    /**
     * @param $offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return $this->$offset;
    }

    /**
     * @param $offset
     */
    public function offsetUnset($offset) : void
    {
        $this->$offset = null;
    }
}
