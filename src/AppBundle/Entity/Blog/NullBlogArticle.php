<?php

namespace AppBundle\Entity\Blog;

use AppBundle\Entity\Blog\Translation\BlogTranslationInterface;
use AppBundle\Entity\Blog\Translation\NullBlogTranslation;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Class NullBlogArticle
 */
class NullBlogArticle implements BlogArticleInterface
{
    const STATUS_PUBLISHED = 'published';
    const STATUS_NOT_PUBLISHED = 'not_published';

    private $id;
    private $authorId;
    private $editorId;
    private $status;
    private $tags;
    private $amountOfDisplay;
    private $amountOfLike;
    private $amountOfNeutral;
    private $amountOfDislike;
    private $created;
    private $modified;
    private $headerImageFile;
    private $translation;
    private $translations;

    /**
     * NullBlogArticle constructor.
     */
    public function __construct()
    {
        $this->id = -1;
        $this->authorId = -1;
        $this->editorId = -1;
        $this->status = $this->setPublished();
        $this->tags = new ArrayCollection();
        $this->amountOfDisplay = 0;
        $this->amountOfLike = 0;
        $this->amountOfNeutral = 0;
        $this->amountOfDislike = 0;
        $this->created = new \DateTime();
        $this->modified = new \DateTime();
        $this->headerImageFile = '';
        $this->translation = new NullBlogTranslation();
        $this->translations = new ArrayCollection();
    }

/*
 * Getters and setters
 */

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getAuthorId() : int
    {
        return $this->authorId;
    }

    /**
     * @param int $authorId
     * @return NullBlogArticle
     */
    public function setAuthorId(int $authorId) : NullBlogArticle
    {
        $this->authorId = $authorId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getEditorId() : ?int
    {
        return $this->editorId;
    }

    /**
     * @param int $editorId
     * @return NullBlogArticle
     */
    public function setEditorId(int $editorId) : NullBlogArticle
    {
        $this->editorId = $editorId;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus() : string
    {
        return $this->status;
    }

    /**
     * @return NullBlogArticle
     */
    public function setPublished() : NullBlogArticle
    {
        $this->status = self::STATUS_PUBLISHED;
        return $this;
    }

    /**
     * @return NullBlogArticle
     */
    public function setNotPublished() : NullBlogArticle
    {
        $this->status = self::STATUS_NOT_PUBLISHED;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getTags() : ArrayCollection
    {
        return $this->tags;
    }

    /**
     * @param ArrayCollection $blogTagsCollection
     * @return NullBlogArticle
     */
    public function setTags(ArrayCollection $blogTagsCollection) : NullBlogArticle
    {
        $this->tags = $blogTagsCollection;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmountOfDisplay() : int
    {
        return $this->amountOfDisplay;
    }

    /**
     * @param int $amountOfDisplay
     * @return NullBlogArticle
     */
    public function setAmountOfDisplay(int $amountOfDisplay) : NullBlogArticle
    {
        $this->amountOfDisplay = $amountOfDisplay;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmountOfLike() : int
    {
        return $this->amountOfLike;
    }

    /**
     * @param int $amountOfLike
     * @return NullBlogArticle
     */
    public function setAmountOfLike(int $amountOfLike) : NullBlogArticle
    {
        $this->amountOfLike = $amountOfLike;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmountOfNeutral() : int
    {
        return $this->amountOfNeutral;
    }

    /**
     * @param int $amountOfNeutral
     * @return NullBlogArticle
     */
    public function setAmountOfNeutral(int $amountOfNeutral) : NullBlogArticle
    {
        $this->amountOfNeutral = $amountOfNeutral;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmountOfDislike() : int
    {
        return $this->amountOfDislike;
    }

    /**
     * @param int $amountOfDislike
     * @return NullBlogArticle
     */
    public function setAmountOfDislike(int $amountOfDislike) : NullBlogArticle
    {
        $this->amountOfDislike = $amountOfDislike;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated() : \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     * @return NullBlogArticle
     */
    public function setCreated(\DateTime $created) : NullBlogArticle
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModified() : \DateTime
    {
        return $this->modified;
    }

    /**
     * @param \DateTime $modified
     * @return NullBlogArticle
     */
    public function setModified(\DateTime $modified) : NullBlogArticle
    {
        $this->modified = $modified;
        return $this;
    }

    /**
     * @return string
     */
    public function getHeaderImageFile() : string
    {
        return $this->headerImageFile;
    }

    /**
     * @param string $headerImageFile
     * @return NullBlogArticle
     */
    public function setHeaderImageFile(string $headerImageFile) : NullBlogArticle
    {
        $this->headerImageFile = $headerImageFile;
        return $this;
    }

    /**
     * @return BlogTranslationInterface
     */
    public function getTranslation() : BlogTranslationInterface
    {
        return $this->translation;
    }

    /**
     * @param BlogTranslationInterface $translation
     * @return NullBlogArticle
     */
    public function setTranslation(BlogTranslationInterface $translation) : NullBlogArticle
    {
        $this->translation = $translation;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getTranslations() : ArrayCollection
    {
        return $this->translations;
    }
}
