<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;


/**
 * User
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User implements AdvancedUserInterface, \Serializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=50, unique=true)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=60)
     */
    private $password;

    /**
     * @var array
     *
     * @ORM\Column(name="roles", type="simple_array")
     */
    private $roles = [];

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100, unique=true)
     */
    private $email;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_locked", type="boolean")
     */
    private $isLocked;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_expired", type="boolean")
     */
    private $isExpired;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="login_time", type="datetime", options={"default":0})
     */
    private $loginTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_login_time", type="datetime", options={"default":0})
     */
    private $lastLoginTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", options={"default":0})
     */
    private $created;

    /**
     * @return string
     */
    public function serialize()
    {
        return serialize([
            $this->id,
            $this->username,
            $this->password,
            $this->isActive,
            $this->isExpired,
            $this->isLocked
        ]);
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            $this->isActive,
            $this->isExpired,
            $this->isLocked
        ) = unserialize($serialized);
    }

/*
 * Getters and setters
 */

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param string $username
     * @return User
     */
    public function setUsername(string $username) : User
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername() : string
    {
        return $this->username;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password) : User
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword() : string
    {
        return $this->password;
    }

    /**
     * @param array $roles
     * @return User
     */
    public function setRoles(array $roles) : User
    {
        $this->roles = $roles;
        return $this;
    }

    /**
     * @return array
     */
    public function getRoles() : array
    {
        return $this->roles;
    }

    /**
     * @param string $email
     * @return User
     */
    public function setEmail(string $email) : User
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail() : string
    {
        return $this->email;
    }

    /**
     * @param boolean $isActive
     * @return User
     */
    public function setActive(bool $isActive) : User
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive() : bool
    {
        return $this->isActive;
    }

    /**
     * @param boolean $isLocked
     * @return User
     */
    public function setLocked(bool $isLocked) : User
    {
        $this->isLocked = $isLocked;
        return $this;
    }

    /**
     * @return bool
     */
    public function isLocked() : bool
    {
        return $this->isLocked;
    }

    /**
     * @param boolean $isExpired
     * @return User
     */
    public function setExpired(bool $isExpired) : User
    {
        $this->isExpired = $isExpired;
        return $this;
    }

    /**
     * @return bool
     */
    public function isExpired() : bool
    {
        return $this->isExpired;
    }

    /**
     * @param \DateTime $loginTime
     * @return User
     */
    public function setLoginTime(\DateTime $loginTime) : User
    {
        $this->loginTime = $loginTime;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getLoginTime() : \DateTime
    {
        return $this->loginTime;
    }

    /**
     * @param \DateTime $lastLoginTime
     * @return User
     */
    public function setLastLoginTime(\DateTime $lastLoginTime) : User
    {
        $this->lastLoginTime = $lastLoginTime;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getLastLoginTime(): \DateTime
    {
        return $this->lastLoginTime;
    }

    /**
     * @param \DateTime $created
     * @return User
     */
    public function setCreated(\DateTime $created) : User
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated() : \DateTime
    {
        return $this->created;
    }

/*
 * Methods from UserInterface
 */

    /**
     * @return null
     */
    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
    }

/*
 * Methods from AdvancedUserInterface
 */

    /**
     * @return bool
     */
    public function isEnabled() : bool
    {
        return $this->isActive();
    }

    /**
     * @return bool
     */
    public function isAccountNonLocked() : bool
    {
        return !$this->isLocked();
    }

    /**
     * @return bool
     */
    public function isAccountNonExpired() : bool
    {
        return !$this->isExpired();
    }

    /**
     * @return bool
     */
    public function isCredentialsNonExpired() : bool
    {
        return true;
    }
}
