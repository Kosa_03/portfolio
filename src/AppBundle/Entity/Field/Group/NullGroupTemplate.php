<?php

namespace AppBundle\Entity\Field\Group;


/**
 * Class NullGroupTemplate
 */
class NullGroupTemplate implements GroupTemplateInterface
{
    private $id;
    private $arrayCss;
    private $dividerBefore;
    private $dividerAfter;
    private $smallDividerBefore;
    private $smallDividerAfter;

    /**
     * NullGroupTemplate constructor.
     */
    public function __construct()
    {
        $this->id = -1;
        $this->arrayCss = array();
        $this->dividerBefore = false;
        $this->smallDividerBefore = false;
        $this->dividerAfter = false;
        $this->smallDividerAfter = false;
    }

/*
 * Getters
 */

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getArrayCss() : array
    {
        return $this->arrayCss;
    }

    /**
     * @return bool
     */
    public function isDividerBefore() : bool
    {
        return $this->dividerBefore;
    }

    /**
     * @return bool
     */
    public function isDividerAfter() : bool
    {
        return $this->dividerAfter;
    }

    /**
     * @return bool
     */
    public function isSmallDividerBefore() : bool
    {
        return $this->smallDividerBefore;
    }

    /**
     * @return bool
     */
    public function isSmallDividerAfter() : bool
    {
        return $this->smallDividerAfter;
    }
}
