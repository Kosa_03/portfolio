<?php

namespace AppBundle\Entity\Field\Group;


/**
 * Interface GroupTemplateInterface
 */
interface GroupTemplateInterface
{
    public function getId() : int;
    public function getArrayCss() : array;
    public function isDividerBefore() : bool;
    public function isDividerAfter() : bool;
    public function isSmallDividerBefore() : bool;
    public function isSmallDividerAfter() : bool;
}
