<?php

namespace AppBundle\Entity\Field\Group;


/**
 * Interface FieldGroupInterface
 */
interface FieldGroupInterface
{
    public function getId() : int;
    public function getGroupName() : string;
    public function getGroupTemplate() : GroupTemplateInterface;
    public function getFields() : array;
}
