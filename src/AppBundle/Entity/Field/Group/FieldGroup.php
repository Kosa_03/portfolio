<?php

namespace AppBundle\Entity\Field\Group;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class FieldGroup
 *
 * @ORM\Table(name="field_group")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Field\Group\FieldGroupRepository")
 */
class FieldGroup implements FieldGroupInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="group_id", type="integer", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="group_name", type="string", length=48)
     */
    private $groupName;

    /**
     * @var GroupTemplateInterface
     *
     * @ORM\ManyToOne(targetEntity="GroupTemplate")
     * @ORM\JoinColumn(name="template_id", referencedColumnName="template_id")
     */
    private $groupTemplate;

    /**
     * @var ArrayCollection
     */
    private $fields;

    /**
     * FieldGroup constructor.
     */
    public function __construct()
    {
        $this->fields = new ArrayCollection();
    }

/*
 * Getters and setters
 */

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getGroupName() : string
    {
        return $this->groupName;
    }

    /**
     * @param string $groupName
     * @return FieldGroup
     */
    public function setGroupName(string $groupName) : FieldGroup
    {
        $this->groupName = $groupName;
        return $this;
    }

    /**
     * @return GroupTemplateInterface
     */
    public function getGroupTemplate() : GroupTemplateInterface
    {
        return $this->groupTemplate;
    }

    /**
     * @param GroupTemplateInterface $groupTemplate
     * @return FieldGroup
     */
    public function setGroupTemplate(GroupTemplateInterface $groupTemplate) : FieldGroup
    {
        $this->groupTemplate = $groupTemplate;
        return $this;
    }

    /**
     * @return array
     */
    public function getFields() : array
    {
        return $this->fields->getValues();
    }
}
