<?php

namespace AppBundle\Entity\Field\Group;

use Doctrine\ORM\Mapping as ORM;


/**
 * Class GroupTemplate
 *
 * @ORM\Table(name="group_template")
 * @ORM\Entity
 */
class GroupTemplate implements GroupTemplateInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="template_id", type="integer", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var array
     *
     * @ORM\Column(name="css_array", type="json_array")
     */
    private $arrayCss;

    /**
     * @var bool
     *
     * @ORM\Column(name="divider_before", type="boolean")
     */
    private $dividerBefore;

    /**
     * @var bool
     *
     * @ORM\Column(name="divider_after", type="boolean")
     */
    private $dividerAfter;

    /**
     * @var bool
     *
     * @ORM\Column(name="small_divider_before", type="boolean")
     */
    private $smallDividerBefore;

    /**
     * @var bool
     *
     * @ORM\Column(name="small_divider_after", type="boolean")
     */
    private $smallDividerAfter;

/*
 * Getters and setters
 */

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getArrayCss() : array
    {
        return $this->arrayCss;
    }

    /**
     * @param array $arrayCss
     * @return GroupTemplate
     */
    public function setArrayCss(array $arrayCss) : GroupTemplate
    {
        $this->arrayCss = $arrayCss;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDividerBefore() : bool
    {
        return $this->dividerBefore;
    }

    /**
     * @param bool $dividerBefore
     * @return GroupTemplate
     */
    public function setDividerBefore(bool $dividerBefore) : GroupTemplate
    {
        $this->dividerBefore = $dividerBefore;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDividerAfter() : bool
    {
        return $this->dividerAfter;
    }

    /**
     * @param bool $dividerAfter
     * @return GroupTemplate
     */
    public function setDividerAfter(bool $dividerAfter) : GroupTemplate
    {
        $this->dividerAfter = $dividerAfter;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSmallDividerBefore() : bool
    {
        return $this->smallDividerBefore;
    }

    /**
     * @param bool $smallDividerBefore
     * @return GroupTemplate
     */
    public function setSmallDividerBefore(bool $smallDividerBefore) : GroupTemplate
    {
        $this->smallDividerBefore = $smallDividerBefore;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSmallDividerAfter() : bool
    {
        return $this->smallDividerAfter;
    }

    /**
     * @param bool $smallDividerAfter
     * @return GroupTemplate
     */
    public function setSmallDividerAfter(bool $smallDividerAfter) : GroupTemplate
    {
        $this->smallDividerAfter = $smallDividerAfter;
        return $this;
    }
}
