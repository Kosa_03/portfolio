<?php

namespace AppBundle\Entity\Field\Group;

use Doctrine\Common\Collections\ArrayCollection;


/**
 * Class NullFieldGroup
 */
class NullFieldGroup implements FieldGroupInterface
{
    private $id;
    private $groupName;
    private $groupTemplate;
    private $fields;

    /**
     * NullFieldGroup constructor.
     */
    public function __construct()
    {
        $this->id = -1;
        $this->groupName = '';
        $this->groupTemplate = new NullGroupTemplate();
        $this->fields = new ArrayCollection();
    }

/*
 * Getters
 */

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getGroupName() : string
    {
        return $this->groupName;
    }

    /**
     * @return GroupTemplateInterface
     */
    public function getGroupTemplate() : GroupTemplateInterface
    {
        return $this->groupTemplate;
    }

    /**
     * @return array
     */
    public function getFields() : array
    {
        return $this->fields->getValues();
    }
}
