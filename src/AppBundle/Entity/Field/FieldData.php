<?php

namespace AppBundle\Entity\Field;

use Doctrine\ORM\Mapping as ORM;


/**
 * Class FieldData
 *
 * @ORM\Table(name="field_data")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Field\FieldDataRepository")
 */
class FieldData
{
    /**
     * @var int
     *
     * @ORM\Column(name="field_id", type="integer", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $fieldId;

    /**
     * @var int
     *
     * @ORM\Column(name="config_id", type="integer")
     */
    private $configId;

    /**
     * @var int
     *
     * @ORM\Column(name="data_id", type="integer")
     */
    private $dataId;

/*
 * Getters and setters
 */

    /**
     * @return int
     */
    public function getFieldId() : int
    {
        return $this->fieldId;
    }

    /**
     * @return int
     */
    public function getConfigId() : int
    {
        return $this->configId;
    }

    /**
     * @param int $configId
     * @return FieldData
     */
    public function setConfigId(int $configId) : FieldData
    {
        $this->configId = $configId;
        return $this;
    }

    /**
     * @return int
     */
    public function getDataId() : int
    {
        return $this->dataId;
    }

    /**
     * @param int $dataId
     * @return FieldData
     */
    public function setDataId(int $dataId) : FieldData
    {
        $this->dataId = $dataId;
        return $this;
    }
}
