<?php

namespace AppBundle\Entity\Field\Template;


/**
 * Interface FieldTemplateInterface
 */
interface FieldTemplateInterface
{
    public function getId() : int;
    public function getArrayCss() : array;
    public function isDividerBefore() : bool;
    public function isDividerAfter() : bool;
    public function isSmallDividerBefore() : bool;
    public function isSmallDividerAfter() : bool;
    public function getTwigFunctionParameters() : string;
}
