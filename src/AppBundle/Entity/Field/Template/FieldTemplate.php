<?php

namespace AppBundle\Entity\Field\Template;

use Doctrine\ORM\Mapping as ORM;


/**
 * Class FieldTemplate
 *
 * @ORM\Table(name="field_template")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Field\Template\FieldTemplateRepository")
 */
class FieldTemplate implements FieldTemplateInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="template_id", type="integer", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var array
     *
     * @ORM\Column(name="css_array", type="json_array")
     */
    private $arrayCss;

    /**
     * @var bool
     *
     * @ORM\Column(name="divider_before", type="boolean")
     */
    private $dividerBefore;

    /**
     * @var bool
     *
     * @ORM\Column(name="divider_after", type="boolean")
     */
    private $dividerAfter;

    /**
     * @var bool
     *
     * @ORM\Column(name="small_divider_before", type="boolean")
     */
    private $smallDividerBefore;

    /**
     * @var bool
     *
     * @ORM\Column(name="small_divider_after", type="boolean")
     */
    private $smallDividerAfter;

    /**
     * @var string
     *
     * @ORM\Column(name="twig_parameters", type="string", length=96)
     */
    private $twigFunctionParameters;

/*
 * Getters and setters
 */

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getArrayCss() : array
    {
        return $this->arrayCss;
    }

    /**
     * @param array $arrayCss
     * @return FieldTemplate
     */
    public function setArrayCss(array $arrayCss) : FieldTemplate
    {
        $this->arrayCss = $arrayCss;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDividerBefore() : bool
    {
        return $this->dividerBefore;
    }

    /**
     * @param bool $dividerBefore
     * @return FieldTemplate
     */
    public function setDividerBefore(bool $dividerBefore) : FieldTemplate
    {
        $this->dividerBefore = $dividerBefore;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDividerAfter() : bool
    {
        return $this->dividerAfter;
    }

    /**
     * @param bool $dividerAfter
     * @return FieldTemplate
     */
    public function setDividerAfter(bool $dividerAfter) : FieldTemplate
    {
        $this->dividerAfter = $dividerAfter;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSmallDividerBefore() : bool
    {
        return $this->smallDividerBefore;
    }

    /**
     * @param bool $smallDividerBefore
     * @return FieldTemplate
     */
    public function setSmallDividerBefore(bool $smallDividerBefore) : FieldTemplate
    {
        $this->smallDividerBefore = $smallDividerBefore;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSmallDividerAfter() : bool
    {
        return $this->smallDividerAfter;
    }

    /**
     * @param bool $smallDividerAfter
     * @return FieldTemplate
     */
    public function setSmallDividerAfter(bool $smallDividerAfter) : FieldTemplate
    {
        $this->smallDividerAfter = $smallDividerAfter;
        return $this;
    }

    /**
     * @return string
     */
    public function getTwigFunctionParameters() : string
    {
        return $this->twigFunctionParameters;
    }

    /**
     * @param string $twigFunctionParameters
     * @return FieldTemplate
     */
    public function setTwigFunctionParameters(string $twigFunctionParameters) : FieldTemplate
    {
        $this->twigFunctionParameters = $twigFunctionParameters;
        return $this;
    }
}
