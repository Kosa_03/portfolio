<?php

namespace AppBundle\Entity\Field\FieldType\File;

use AppBundle\Entity\Field\FieldType\NotTranslatableFieldInterface;


/**
 * Interface FileFieldInterface
 */
interface FileFieldInterface extends NotTranslatableFieldInterface
{
    public function getFilePath() : string;
    public function setFilePath(string $filePath);
}
