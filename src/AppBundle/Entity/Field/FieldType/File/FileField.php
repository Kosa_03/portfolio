<?php

namespace AppBundle\Entity\Field\FieldType\File;

use AppBundle\Entity\Field\FieldType\ArrayAccessTrait;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class FileField
 *
 * @ORM\Table(name="field_file_field")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Field\FieldType\FieldTypeRepository")
 */
class FileField implements FileFieldInterface, \ArrayAccess
{
    use ArrayAccessTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="data_id", type="integer", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $dataId;

    /**
     * @var string
     */
    private $templatePath = 'default/field/field_type/file_field.html.twig';

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    private $uid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", options={"default":0})
     */
    private $modified;

    /**
     * @var string
     *
     * @ORM\Column(name="file_path", type="string", length=255)
     */
    private $filePath;

    /**
     * @var string
     *
     * @ORM\Column(name="file_name", type="string", length=255)
     */
    private $fileName;

/*
 * Getters and setters
 */

    /**
     * @return int
     */
    public function getDataId() : int
    {
        return $this->dataId;
    }

    /**
     * @return string
     */
    public function getTemplatePath() : string
    {
        return $this->templatePath;
    }

    /**
     * @return int
     */
    public function getUserId() : int
    {
        return $this->uid;
    }

    /**
     * @param int $uid
     * @return FileField
     */
    public function setUserId(int $uid) : FileField
    {
        $this->uid = $uid;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModified() : \DateTime
    {
        return $this->modified;
    }

    /**
     * @param \DateTime $modified
     * @return FileField
     */
    public function setModified(\DateTime $modified) : FileField
    {
        $this->modified = $modified;
        return $this;
    }

    /**
     * @return string
     */
    public function getFilePath() : string
    {
        return $this->filePath;
    }

    /**
     * @param string $filePath
     * @return FileField
     */
    public function setFilePath(string $filePath) : FileField
    {
        $this->filePath = $filePath;
        return $this;
    }

    /**
     * @return string
     */
    public function getFileName() : string
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     * @return FileField
     */
    public function setFileName(string $fileName) : FileField
    {
        $this->fileName = $fileName;
        return $this;
    }
}
