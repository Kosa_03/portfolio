<?php

namespace AppBundle\Entity\Field\FieldType\Component;


/**
 * Class NullField
 */
class NullField implements ComponentFieldInterface
{
    private $dataId;
    private $templatePath = 'default/field/field_type/null_field.html.twig';
    private $uid;
    private $modified;
    private $content;

    /**
     * NullField constructor.
     */
    public function __construct()
    {
        $this->dataId = -1;
        $this->uid = -1;
        $this->modified = new \DateTime();
        $this->content = 'Translation not found for expected language.';
    }

    /**
     * @return string
     */
    public function getContent() : string
    {
        return $this->content;
    }

/*
 * Getters and setters
 */

    /**
     * @return int
     */
    public function getDataId() : int
    {
        return $this->dataId;
    }

    /**
     * @return string
     */
    public function getTemplatePath() : string
    {
        return $this->templatePath;
    }

    /**
     * @return int
     */
    public function getUserId() : int
    {
        return $this->uid;
    }

    /**
     * @param int $uid
     * @return NullField
     */
    public function setUserId(int $uid) : NullField
    {
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModified() : \DateTime
    {
        return $this->modified;
    }

    /**
     * @param \DateTime $modified
     * @return NullField
     */
    public function setModified(\DateTime $modified) : NullField
    {
        return $this;
    }
}
