<?php

namespace AppBundle\Entity\Field\FieldType\Component;

use AppBundle\Entity\Field\FieldType\ArrayAccessTrait;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class PaginationField
 *
 * @ORM\Table(name="field_pagination_field")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Field\FieldType\FieldTypeRepository")
 */
class PaginationField implements ComponentFieldInterface, \ArrayAccess
{
    use ArrayAccessTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="data_id", type="integer", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $dataId;

    /**
     * @var string
     */
    private $templatePath = 'default/field/field_type/pagination_field.html.twig';

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    private $uid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", options={"default":0})
     */
    private $modified;

    /**
     * @var string
     *
     * @ORM\Column(name="pagination_path", type="string", length=255)
     */
    private $paginationPath;

/*
 * Getters and setters
 */

    /**
     * @return int
     */
    public function getDataId() : int
    {
        return $this->dataId;
    }

    /**
     * @return string
     */
    public function getTemplatePath() : string
    {
        return $this->templatePath;
    }

    /**
     * @return int
     */
    public function getUserId() : int
    {
        return $this->uid;
    }

    /**
     * @param int $uid
     * @return PaginationField
     */
    public function setUserId(int $uid) : PaginationField
    {
        $this->uid = $uid;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModified() : \DateTime
    {
        return $this->modified;
    }

    /**
     * @param \DateTime $modified
     * @return PaginationField
     */
    public function setModified(\DateTime $modified) : PaginationField
    {
        $this->modified = $modified;
        return $this;
    }

    /**
     * @return string
     */
    public function getPaginationPath() : string
    {
        return $this->paginationPath;
    }

    /**
     * @param string $paginationPath
     * @return PaginationField
     */
    public function setPaginationPath(string $paginationPath) : PaginationField
    {
        $this->paginationPath = $paginationPath;
        return $this;
    }
}
