<?php

namespace AppBundle\Entity\Field\FieldType\Component;

use AppBundle\Entity\Field\FieldType\NotTranslatableFieldInterface;


/**
 * Interface ComponentFieldInterface
 */
interface ComponentFieldInterface extends NotTranslatableFieldInterface
{

}
