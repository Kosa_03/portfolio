<?php

namespace AppBundle\Entity\Field\FieldType\Entity;

use AppBundle\Entity\Blog\Blog;
use AppBundle\Entity\Blog\BlogArticle;
use AppBundle\Entity\Field\FieldType\ArrayAccessTrait;
use AppBundle\Service\Blog\Factory\BlogEntityLoaderFactory;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class BlogEntityField
 *
 * @ORM\Table(name="field_blog_entity_field")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Field\FieldType\FieldTypeRepository")
 */
class BlogEntityField implements \ArrayAccess, EntityFieldInterface
{
    use ArrayAccessTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="data_id", type="integer", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $dataId;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    private $uid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", options={"default":0})
     */
    private $modified;

    /**
     * @var string
     */
    private $entityClassName = BlogArticle::class;

    /**
     * @var string
     */
    private $entityLoaderFactory = BlogEntityLoaderFactory::class;

    /**
     * @var string
     *
     * @ORM\Column(name="entity_loader", type="string", length=128)
     */
    private $entityLoader;

    /**
     * @var string
     *
     * @ORM\Column(name="template_path", type="string", length=255)
     */
    private $templatePath;

/*
 * Getters and setters
 */

    /**
     * @return int
     */
    public function getDataId() : int
    {
        return $this->dataId;
    }

    /**
     * @return int
     */
    public function getUserId() : int
    {
        return $this->uid;
    }

    /**
     * @param int $userId
     * @return BlogEntityField
     */
    public function setUserId(int $userId) : BlogEntityField
    {
        $this->uid = $userId;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModified() : \DateTime
    {
        return $this->modified;
    }

    /**
     * @param \DateTime $modified
     * @return BlogEntityField
     */
    public function setModified(\DateTime $modified) : BlogEntityField
    {
        $this->modified = $modified;
        return $this;
    }

    /**
     * @return string
     */
    public function getEntityClassName() : string
    {
        return $this->entityClassName;
    }

    /**
     * @return string
     */
    public function getEntityLoaderFactory(): string
    {
        return $this->entityLoaderFactory;
    }

    /**
     * @return string
     */
    public function getEntityLoader() : string
    {
        return $this->entityLoader;
    }

    /**
     * @param string $entityLoader
     * @return BlogEntityField
     */
    public function setEntityLoader(string $entityLoader) : BlogEntityField
    {
        $this->entityLoader = $entityLoader;
        return $this;
    }

    /**
     * @return string
     */
    public function getTemplatePath() : string
    {
        return $this->templatePath;
    }

    /**
     * @param string $templatePath
     * @return BlogEntityField
     */
    public function setTemplatePath(string $templatePath) : BlogEntityField
    {
        $this->templatePath = $templatePath;
        return $this;
    }
}
