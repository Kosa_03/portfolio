<?php

namespace AppBundle\Entity\Field\FieldType\Entity;

use AppBundle\Entity\Field\FieldType\NotTranslatableFieldInterface;


/**
 * Interface EntityFieldInterface
 */
interface EntityFieldInterface extends NotTranslatableFieldInterface
{
    public function getEntityClassName() : string;
    public function getEntityLoaderFactory() : string;
    public function getEntityLoader() : string;
    public function setEntityLoader(string $entityLoader);
    public function getTemplatePath() : string;
    public function setTemplatePath(string $templatePath);
}
