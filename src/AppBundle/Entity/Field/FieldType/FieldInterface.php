<?php

namespace AppBundle\Entity\Field\FieldType;


/**
 * Interface FieldInterface
 */
interface FieldInterface
{
    public function getDataId() : int;
    public function getTemplatePath() : string;
    public function getUserId() : int;
    public function setUserId(int $uid);
    public function getModified() : \DateTime;
    public function setModified(\DateTime $modified);
}
