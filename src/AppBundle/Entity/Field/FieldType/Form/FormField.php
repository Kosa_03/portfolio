<?php

namespace AppBundle\Entity\Field\FieldType\Form;

use AppBundle\Entity\Field\FieldType\ArrayAccessTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Form\FormView;


/**
 * Class FormField
 *
 * @ORM\Table(name="field_form_field")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Field\FieldType\FieldTypeRepository")
 */
class FormField implements FormFieldInterface, \ArrayAccess
{
    use ArrayAccessTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="data_id", type="integer", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $dataId;

    /**
     * @var string
     */
    private $templatePath = 'default/field/field_type/form_field.html.twig';

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    private $uid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", options={"default":0})
     */
    private $modified;

    /**
     * @var string
     *
     * @ORM\Column(name="form_template_path", type="string", length=192)
     */
    private $formTemplatePath;

    /**
     * @var array
     */
    private $formErrors = [];

    /**
     * @var FormView
     */
    private $formView;

/*
 * Getters and setters
 */

    /**
     * @return int
     */
    public function getDataId() : int
    {
        return $this->dataId;
    }

    /**
     * @return string
     */
    public function getTemplatePath() : string
    {
        return $this->templatePath;
    }

    /**
     * @return int
     */
    public function getUserId() : int
    {
        return $this->uid;
    }

    /**
     * @param int $uid
     * @return FormField
     */
    public function setUserId(int $uid) : FormField
    {
        $this->uid = $uid;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModified() : \DateTime
    {
        return $this->modified;
    }

    /**
     * @param \DateTime $modified
     * @return FormField
     */
    public function setModified(\DateTime $modified) : FormField
    {
        $this->modified = $modified;
        return $this;
    }

    /**
     * @return string
     */
    public function getFormTemplatePath() : string
    {
        return $this->formTemplatePath;
    }

    /**
     * @param string $formTemplatePath
     * @return FormField
     */
    public function setFormTemplatePath(string $formTemplatePath) : FormField
    {
        $this->formTemplatePath = $formTemplatePath;
        return $this;
    }

    /**
     * @return array
     */
    public function getFormErrors() : array
    {
        return $this->formErrors;
    }

    /**
     * @param array $formErrors
     * @return FormField
     */
    public function setFormErrors(array $formErrors) : FormField
    {
        $this->formErrors = $formErrors;
        return $this;
    }

    /**
     * @return FormView
     */
    public function getFormView() : FormView
    {
        return $this->formView;
    }

    /**
     * @param FormView $formView
     * @return FormField
     */
    public function setFormView(FormView $formView) : FormField
    {
        $this->formView = $formView;
        return $this;
    }
}
