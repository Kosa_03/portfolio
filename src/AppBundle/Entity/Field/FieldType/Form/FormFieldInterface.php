<?php

namespace AppBundle\Entity\Field\FieldType\Form;

use AppBundle\Entity\Field\FieldType\NotTranslatableFieldInterface;
use Symfony\Component\Form\FormView;


/**
 * Interface FormFieldInterface
 */
interface FormFieldInterface extends NotTranslatableFieldInterface
{
    public function getFormTemplatePath() : string;
    public function setFormTemplatePath(string $templateFilePath);
    public function getFormErrors() : array;
    public function setFormErrors(array $formErrors);
    public function getFormView() : FormView;
    public function setFormView(FormView $formView);
}
