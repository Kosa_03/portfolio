<?php

namespace AppBundle\Entity\Field\FieldType\Translatable;

use AppBundle\Entity\Field\FieldType\FieldInterface;


/**
 * Interface TranslatableFieldInterface
 */
interface TranslatableFieldInterface extends FieldInterface
{
    public function getLanguage() : string;
    public function setLanguage(string $language);
    public function getContent();
    public function setContent($content) : void;
}
