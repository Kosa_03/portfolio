<?php

namespace AppBundle\Entity\Field\FieldType\Translatable;

use AppBundle\Entity\Field\FieldType\ArrayAccessTrait;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class HeaderField
 *
 * @ORM\Table(name="field_header_field")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Field\FieldType\FieldTypeRepository")
 */
class HeaderField implements TranslatableFieldInterface, \ArrayAccess
{
    use ArrayAccessTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="data_id", type="integer", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $dataId;

    /**
     * @var string
     */
    private $templatePath = 'default/field/field_type/header_field.html.twig';

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    private $uid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", options={"default":0})
     */
    private $modified;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=3, options={"default":"und"})
     */
    private $language;

    /**
     * @var string
     *
     * @ORM\Column(name="header", type="string", length=50)
     */
    private $header;

    /**
     * @return string
     */
    public function getContent() : string
    {
        return $this->getHeader();
    }

    /**
     * @param string $header
     */
    public function setContent($header) : void
    {
        if (is_string($header)) {
            $this->setHeader($header);
        }
    }

/*
 * Getters and setters
 */

    /**
     * @return int
     */
    public function getDataId() : int
    {
        return $this->dataId;
    }

    /**
     * @return string
     */
    public function getTemplatePath() : string
    {
        return $this->templatePath;
    }

    /**
     * @return int
     */
    public function getUserId() : int
    {
        return $this->uid;
    }

    /**
     * @param int $uid
     * @return HeaderField
     */
    public function setUserId(int $uid) : HeaderField
    {
        $this->uid = $uid;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModified() : \DateTime
    {
        return $this->modified;
    }

    /**
     * @param \DateTime $modified
     * @return HeaderField
     */
    public function setModified(\DateTime $modified) : HeaderField
    {
        $this->modified = $modified;
        return $this;
    }

    /**
     * @return string
     */
    public function getLanguage() : string
    {
        return $this->language;
    }

    /**
     * @param string $language
     * @return HeaderField
     */
    public function setLanguage(string $language) : HeaderField
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @return string
     */
    public function getHeader() : string
    {
        return $this->header;
    }

    /**
     * @param string $header
     * @return HeaderField
     */
    public function setHeader(string $header) : HeaderField
    {
        $this->header = $header;
        return $this;
    }
}
