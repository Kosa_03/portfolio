<?php

namespace AppBundle\Entity\Field\FieldType\Translatable;

use AppBundle\Entity\Field\FieldType\ArrayAccessTrait;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class LongTextField
 *
 * @ORM\Table(name="field_long_text_field")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Field\FieldType\FieldTypeRepository")
 */
class LongTextField implements TranslatableFieldInterface, \ArrayAccess
{
    use ArrayAccessTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="data_id", type="integer", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $dataId;

    /**
     * @var string
     */
    private $templatePath = 'default/field/field_type/long_text_field.html.twig';

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    private $uid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", options={"default":0})
     */
    private $modified;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=3, options={"default":"und"})
     */
    private $language;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=16, options={"default":"filtered_html"})
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    private $longText;

    /**
     * @return string
     */
    public function getContent() : string
    {
        return $this->getLongText();
    }

    /**
     * @param string $longText
     */
    public function setContent($longText) : void
    {
        if (is_string($longText)) {
            $this->setLongText($longText);
        }
    }

/*
 * Getters and setters
 */

    /**
     * @return int
     */
    public function getDataId() : int
    {
        return $this->dataId;
    }

    /**
     * @return string
     */
    public function getTemplatePath() : string
    {
        return $this->templatePath;
    }

    /**
     * @return int
     */
    public function getUserId() : int
    {
        return $this->uid;
    }

    /**
     * @param int $uid
     * @return LongTextField
     */
    public function setUserId(int $uid) : LongTextField
    {
        $this->uid = $uid;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModified() : \DateTime
    {
        return $this->modified;
    }

    /**
     * @param \DateTime $modified
     * @return LongTextField
     */
    public function setModified(\DateTime $modified) : LongTextField
    {
        $this->modified = $modified;
        return $this;
    }

    /**
     * @return string
     */
    public function getLanguage() : string
    {
        return $this->language;
    }

    /**
     * @param string $language
     * @return LongTextField
     */
    public function setLanguage(string $language) : LongTextField
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @return string
     */
    public function getType() : string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return LongTextField
     */
    public function setType(string $type) : LongTextField
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getLongText() : string
    {
        return $this->longText;
    }

    /**
     * @param string $longText
     * @return LongTextField
     */
    public function setLongText(string $longText) : LongTextField
    {
        $this->longText = $longText;
        return $this;
    }
}
