<?php

namespace AppBundle\Entity\Field\FieldType\Translatable;

use AppBundle\Entity\Field\FieldType\ArrayAccessTrait;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class ListField
 *
 * @ORM\Table(name="field_list_field")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Field\FieldType\FieldTypeRepository")
 */
class ListField implements TranslatableFieldInterface, \ArrayAccess
{
    use ArrayAccessTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="data_id", type="integer", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $dataId;

    /**
     * @var string
     */
    private $templatePath = 'default/field/field_type/list_field.html.twig';

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    private $uid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", options={"default":0})
     */
    private $modified;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=3, options={"default":"und"})
     */
    private $language;

    /**
     * @var array
     *
     * @ORM\Column(name="list", type="simple_array")
     */
    private $list;

    /**
     * @return array
     */
    public function getContent() : array
    {
        return $this->getList();
    }

    /**
     * @param array $content
     */
    public function setContent($content) : void
    {
        if (is_array($content)) {
            $this->setList($content);
        }
    }

/*
 * Getters and setters
 */

    /**
     * @return int
     */
    public function getDataId() : int
    {
        return $this->dataId;
    }

    /**
     * @return string
     */
    public function getTemplatePath() : string
    {
        return $this->templatePath;
    }

    /**
     * @return int
     */
    public function getUserId() : int
    {
        return $this->uid;
    }

    /**
     * @param int $uid
     * @return ListField
     */
    public function setUserId(int $uid) : ListField
    {
        $this->uid = $uid;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModified() : \DateTime
    {
        return $this->modified;
    }

    /**
     * @param \DateTime $modified
     * @return ListField
     */
    public function setModified(\DateTime $modified) : ListField
    {
        $this->modified = $modified;
        return $this;
    }

    /**
     * @return string
     */
    public function getLanguage() : string
    {
        return $this->language;
    }

    /**
     * @param string $language
     * @return ListField
     */
    public function setLanguage(string $language) : ListField
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @return array
     */
    public function getList() : array
    {
        return $this->list;
    }

    /**
     * @param array $items
     * @return ListField
     */
    public function setList(array $items) : ListField
    {
        $this->list = $items;
        return $this;
    }
}
