<?php

namespace AppBundle\Entity\Field\FieldType\Translatable;

use AppBundle\Entity\Field\FieldType\ArrayAccessTrait;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class TextField
 *
 * @ORM\Table(name="field_text_field")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Field\FieldType\FieldTypeRepository")
 */
class TextField implements TranslatableFieldInterface, \ArrayAccess
{
    use ArrayAccessTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="data_id", type="integer", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $dataId;

    /**
     * @var string
     */
    private $templatePath = 'default/field/field_type/text_field.html.twig';

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    private $uid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", options={"default":0})
     */
    private $modified;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=3, options={"default":"und"})
     */
    private $language;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=10, options={"default":"text"})
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=50)
     */
    private $label;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=100)
     */
    private $value;

    /**
     * @return string
     */
    public function getContent() : string
    {
        return $this->getValue();
    }

    /**
     * @param string $value
     */
    public function setContent($value) : void
    {
        if (is_string($value)) {
            $this->setValue($value);
        }
    }

/*
 * Getters and setters
 */

    /**
     * @return int
     */
    public function getDataId() : int
    {
        return $this->dataId;
    }

    /**
     * @return string
     */
    public function getTemplatePath() : string
    {
        return $this->templatePath;
    }

    /**
     * @return int
     */
    public function getUserId() : int
    {
        return $this->uid;
    }

    /**
     * @param int $uid
     * @return TextField
     */
    public function setUserId(int $uid) : TextField
    {
        $this->uid = $uid;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModified() : \DateTime
    {
        return $this->modified;
    }

    /**
     * @param \DateTime $modified
     * @return TextField
     */
    public function setModified(\DateTime $modified) : TextField
    {
        $this->modified = $modified;
        return $this;
    }

    /**
     * @return string
     */
    public function getLanguage() : string
    {
        return $this->language;
    }

    /**
     * @param string $language
     * @return TextField
     */
    public function setLanguage(string $language) : TextField
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @return string
     */
    public function getType() : string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return TextField
     */
    public function setType(string $type) : TextField
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getLabel() : string
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return TextField
     */
    public function setLabel(string $label) : TextField
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue() : string
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return TextField
     */
    public function setValue(string $value) : TextField
    {
        $this->value = $value;
        return $this;
    }
}
