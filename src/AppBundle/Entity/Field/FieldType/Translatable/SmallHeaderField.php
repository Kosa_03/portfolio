<?php

namespace AppBundle\Entity\Field\FieldType\Translatable;

use AppBundle\Entity\Field\FieldType\ArrayAccessTrait;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class SmallHeader
 *
 * @ORM\Table(name="field_small_header_field")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Field\FieldType\FieldTypeRepository")
 */
class SmallHeaderField implements TranslatableFieldInterface, \ArrayAccess
{
    use ArrayAccessTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="data_id", type="integer", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $dataId;

    /**
     * @var string
     */
    private $templatePath = 'default/field/field_type/small_header_field.html.twig';

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    private $uid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", options={"default":0})
     */
    private $modified;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=3, options={"default":"und"})
     */
    private $language;

    /**
     * @var string
     *
     * @ORM\Column(name="header", type="string", length=50)
     */
    private $header;

    /**
     * @return string
     */
    public function getContent() : string
    {
        return $this->getHeader();
    }

    /**
     * @param string $smallHeader
     */
    public function setContent($smallHeader) : void
    {
        if (is_string($smallHeader)) {
            $this->setHeader($smallHeader);
        }
    }

/*
 * Getters and setters
 */

    /**
     * @return int
     */
    public function getDataId() : int
    {
        return $this->dataId;
    }

    /**
     * @return string
     */
    public function getTemplatePath() : string
    {
        return $this->templatePath;
    }

    /**
     * @return int
     */
    public function getUserId() : int
    {
        return $this->uid;
    }

    /**
     * @param int $uid
     * @return SmallHeaderField
     */
    public function setUserId(int $uid) : SmallHeaderField
    {
        $this->uid = $uid;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModified() : \DateTime
    {
        return $this->modified;
    }

    /**
     * @param \DateTime $modified
     * @return SmallHeaderField
     */
    public function setModified(\DateTime $modified) : SmallHeaderField
    {
        $this->modified = $modified;
        return $this;
    }

    /**
     * @return string
     */
    public function getLanguage() : string
    {
        return $this->language;
    }

    /**
     * @param string $language
     * @return SmallHeaderField
     */
    public function setLanguage(string $language) : SmallHeaderField
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @return string
     */
    public function getHeader() : string
    {
        return $this->header;
    }

    /**
     * @param string $header
     * @return SmallHeaderField
     */
    public function setHeader(string $header) : SmallHeaderField
    {
        $this->header = $header;
        return $this;
    }
}
