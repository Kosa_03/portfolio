<?php

namespace AppBundle\Entity\Field\FieldType;


/**
 * Interface NotTranslatableFieldInterface
 */
interface NotTranslatableFieldInterface extends FieldInterface
{

}
