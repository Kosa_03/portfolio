<?php

namespace AppBundle\Entity\Field;

use Doctrine\ORM\Mapping as ORM;


/**
 * Class FieldConfig
 *
 * @ORM\Table(name="field_config")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Field\FieldConfigRepository")
 */
class FieldConfig
{
    /**
     * @var int
     *
     * @ORM\Column(name="config_id", type="integer", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="controller", type="string", length=100)
     */
    private $controller;

    /**
     * @var string
     *
     * @ORM\Column(name="method", type="string", length=32)
     */
    private $method;

    /**
     * @var int
     *
     * @ORM\Column(name="template_id", type="integer", options={"default":0})
     */
    private $templateId;

    /**
     * @var int
     *
     * @ORM\Column(name="group_id", type="integer", options={"default":0})
     */
    private $groupId;

    /**
     * @var array
     *
     * @ORM\Column(name="roles", type="simple_array")
     */
    private $roles = [];

    /**
     * @var int
     *
     * @ORM\Column(name="weight", type="integer")
     */
    private $weight;

    /**
     * @var string
     *
     * @ORM\Column(name="field_name", type="string", length=48, unique=true)
     */
    private $fieldName;

    /**
     * @var string
     *
     * @ORM\Column(name="field_class", type="string", length=100)
     */
    private $fieldClass;

/*
 * Getters and setters
 */

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getController() : string
    {
        return $this->controller;
    }

    /**
     * @param string $controller
     * @return FieldConfig
     */
    public function setController(string $controller) : FieldConfig
    {
        $this->controller = $controller;
        return $this;
    }

    /**
     * @return string
     */
    public function getMethod() : string
    {
        return $this->method;
    }

    /**
     * @param string $method
     * @return FieldConfig
     */
    public function setMethod(string $method) : FieldConfig
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @return int
     */
    public function getTemplateId() : int
    {
        return $this->templateId;
    }

    /**
     * @param int $templateId
     * @return FieldConfig
     */
    public function setTemplateId(int $templateId) : FieldConfig
    {
        $this->templateId = $templateId;
        return $this;
    }

    /**
     * @return int
     */
    public function getGroupId() : int
    {
        return $this->groupId;
    }

    /**
     * @param int $groupId
     * @return FieldConfig
     */
    public function setGroupId(int $groupId) : FieldConfig
    {
        $this->groupId = $groupId;
        return $this;
    }

    /**
     * @return array
     */
    public function getRoles() : array
    {
        return $this->roles;
    }

    /**
     * @param array $roles
     * @return FieldConfig
     */
    public function setRoles(array $roles) : FieldConfig
    {
        $this->roles = $roles;
        return $this;
    }

    /**
     * @return int
     */
    public function getWeight() : int
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     * @return FieldConfig
     */
    public function setWeight(int $weight) : FieldConfig
    {
        $this->weight = $weight;
        return $this;
    }

    /**
     * @return string
     */
    public function getFieldName() : string
    {
        return $this->fieldName;
    }

    /**
     * @param string $fieldName
     * @return FieldConfig
     */
    public function setFieldName(string $fieldName) : FieldConfig
    {
        $this->fieldName = $fieldName;
        return $this;
    }

    /**
     * @return string
     */
    public function getFieldClass() : string
    {
        return $this->fieldClass;
    }

    /**
     * @param string $fieldClass
     * @return FieldConfig
     */
    public function setFieldClass(string $fieldClass) : FieldConfig
    {
        $this->fieldClass = $fieldClass;
        return $this;
    }
}
