<?php

namespace AppBundle\EventListener;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


class ExceptionListener
{
    private $router;
    private $request;
    private $flashBag;

    public function __construct(RequestStack $request, UrlGeneratorInterface $router, FlashBagInterface $flashBag)
    {
        $this->router = $router;
        $this->request = $request->getCurrentRequest();
        $this->flashBag = $flashBag;
    }

    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event) : void
    {
        $exception = $event->getException();

        // StatusCode 403 - Access Denied
        if ($exception instanceof AccessDeniedHttpException) {
            $this->doAccessDeniedException();
        }

        // StatusCode 404 - NotFound
        if ($exception instanceof NotFoundHttpException) {
            $this->doPageNotFoundException();
        }
    }

    private function doAccessDeniedException() : void
    {
        $redirectUrl = $this->request->headers->get('referer');

        if (is_null($redirectUrl)) {
            $redirectUrl = $this->router->generate('app_home');
        }

        $this->flashBag->set('danger', 'Access denied for ' .
            $this->request->getHttpHost() . $this->request->getRequestUri()
        );

        $response = new RedirectResponse($redirectUrl);
        $response->send();
    }

    private function doPageNotFoundException() : void
    {
        $redirectUrl = $this->router->generate('app_home');

        $response = new RedirectResponse($redirectUrl);
        $response->send();
    }
}
