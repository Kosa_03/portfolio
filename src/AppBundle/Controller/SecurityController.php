<?php

namespace AppBundle\Controller;

use AppBundle\Form\LoginType;
use AppBundle\Menu\MainMenu;
use AppBundle\Service\Field\Command\CreateControllerCollectionCmd;
use AppBundle\Service\Field\FieldManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccountExpiredException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\DisabledException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Exception\LockedException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


/**
 * Class SecurityController
 */
class SecurityController extends Controller
{
    private $createCollectionCmd;

    /** @var Request */
    private $request;
    /** @var FieldManager */
    private $fieldManager;

    /**
     * SecurityController constructor.
     * @param CreateControllerCollectionCmd $createCollectionCmd
     */
    public function __construct(CreateControllerCollectionCmd $createCollectionCmd)
    {
        $this->createCollectionCmd = $createCollectionCmd;
    }

    /**
     * @Route("/login", name="app_login")
     *
     * @param Request $request
     * @param FieldManager $fieldManager
     * @param AuthenticationUtils $authUtils
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function loginAction(Request $request, FieldManager $fieldManager, AuthenticationUtils $authUtils)
    {
        $this->request = $request;
        $this->fieldManager = $fieldManager;

        if ($this->getUser() != null) {
            return $this->redirectToRoute('app_home');
        }

        $form = $this->createForm(LoginType::class, null, array(
            'action' => $this->generateUrl('app_login')
        ));

        $loginError = $authUtils->getLastAuthenticationError();
        if (!is_null($loginError)) {
            $loginError = $this->getLoginErrorMessage($loginError);
        }

        $this->createControllerFields();
        $menu = new MainMenu($request->get('_route'));

        $formField = $fieldManager->getCollection('_controller')->getFormField('app_login_form');
        if (!is_null($formField)) {
            $formField->setFormView($form->createView());

            if (!is_null($loginError)) {
                $formField->setFormErrors($loginError);
            }
        }

        return $this->render('default/controller/app_login.html.twig', array(
            'title' => 'Tomasz Bukowski - Portfolio - Logowanie',
            'menu' => $menu->getMenu(),
            'fields' => $fieldManager->getArrayCollection('_controller')
        ));
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logoutAction()
    {
        // Logout action
    }

    private function createControllerFields() : void
    {
        $fieldList = $this->fieldManager->getListOptions();
        $fieldList->setControllerName(SecurityController::class);
        $fieldList->setMethodName('loginAction');

        $this->fieldManager->setCommand($this->createCollectionCmd);
        $this->fieldManager->run();
    }

    /**
     * @param AuthenticationException $error
     * @return array
     */
    private function getLoginErrorMessage(AuthenticationException $error) : array
    {
        switch (true) {
            case $error instanceof InvalidCsrfTokenException :
                return array(
                    'en' => 'Invalid CSRF token. Please refresh this page and try sign in again.',
                    'pl' => 'Niepoprawny CSRF token. Prosimy odświeżyć stronę i spróbować ponownie.'
                );

            case $error instanceof BadCredentialsException :
                return array(
                    'en' => 'Invalid username or password.',
                    'pl' => 'Login lub hasło nie są poprawne.'
                );

            case $error instanceof UsernameNotFoundException :
                return array(
                    'en' => 'Username not found in database.',
                    'pl' => 'Podany użytkownik nieistnieje.'
                );

            case $error instanceof DisabledException :
                return array(
                    'en' => 'Account is not active. Please contact with administrator.',
                    'pl' => 'Konto jest nieaktywne. Prosimy skontaktować się z administratorem.'
                );

            case $error instanceof LockedException :
                return array(
                    'en' => 'Account is locked.',
                    'pl' => 'Konto zostało zablokowane.'
                );

            case $error instanceof AccountExpiredException :
                return array(
                    'en' => 'Account has expired.',
                    'pl' => 'Okres ważności konta minął.'
                );

            default :
                return array(
                    'en' => 'An unknown authorization error occurred.',
                    'pl' => 'Wystąpił nieoczekiwany błąd podczas autoryzacji użytkownika.'
                );
        }
    }
}
