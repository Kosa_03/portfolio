<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Blog\BlogArticle;
use AppBundle\Form\Blog\BlogLikeNeutralDislikeBarType;
use AppBundle\Menu\MainMenu;
use AppBundle\Service\Blog\BlogManager;
use AppBundle\Service\Blog\Command\BlogArticleVoteDislikeCmd;
use AppBundle\Service\Blog\Command\BlogArticleVoteLikeCmd;
use AppBundle\Service\Blog\Command\BlogArticleVoteNeutralCmd;
use AppBundle\Service\Blog\Command\IncreaseAmountOfDisplayCmd;
use AppBundle\Service\Field\Command\CreateControllerCollectionCmd;
use AppBundle\Service\Field\FieldManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class BlogArticleController
 */
class BlogArticleController extends Controller
{
    private $createCollectionCmd;
    private $increaseAmountOfDisplayCmd;
    private $blogArticleVoteLikeCmd;
    private $blogArticleVoteNeutralCmd;
    private $blogArticleVoteDislikeCmd;

    private $articleId;
    /** @var Request */
    private $request;
    /** @var FieldManager */
    private $fieldManager;
    /** @var BlogManager */
    private $blogManager;

    /**
     * BlogArticleController constructor.
     * @param CreateControllerCollectionCmd $createCollectionCmd
     * @param IncreaseAmountOfDisplayCmd $increaseAmountOfDisplayCmd
     * @param BlogArticleVoteLikeCmd $blogArticleVoteLikeCmd
     * @param BlogArticleVoteNeutralCmd $blogArticleVoteNeutralCmd
     * @param BlogArticleVoteDislikeCmd $blogArticleVoteDislikeCmd
     */
    public function __construct
    (
        CreateControllerCollectionCmd $createCollectionCmd,
        IncreaseAmountOfDisplayCmd $increaseAmountOfDisplayCmd,
        BlogArticleVoteLikeCmd $blogArticleVoteLikeCmd,
        BlogArticleVoteNeutralCmd $blogArticleVoteNeutralCmd,
        BlogArticleVoteDislikeCmd $blogArticleVoteDislikeCmd
    )
    {
        $this->createCollectionCmd = $createCollectionCmd;
        $this->increaseAmountOfDisplayCmd = $increaseAmountOfDisplayCmd;
        $this->blogArticleVoteLikeCmd = $blogArticleVoteLikeCmd;
        $this->blogArticleVoteNeutralCmd = $blogArticleVoteNeutralCmd;
        $this->blogArticleVoteDislikeCmd = $blogArticleVoteDislikeCmd;
    }

    /**
     * @Route("/blog/article/{articleId}", name="app_blog_article", requirements={"articleId"="\d+"})
     *
     * @param int $articleId
     * @param Request $request
     * @param FieldManager $fieldManager
     * @param BlogManager $blogManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(int $articleId, Request $request, FieldManager $fieldManager, BlogManager $blogManager)
    {
        $this->articleId = $articleId;
        $this->request = $request;
        $this->fieldManager = $fieldManager;
        $this->blogManager = $blogManager;

        $this->createBlogArticleOptions();
        $this->createControllerFields();

        $blogArticle = $this->getBlogArticleFromFieldCollection();
        if (is_null($blogArticle)) {
            return $this->redirectToRoute('app_blog_article_list');
        }

        $formLikeNeutralDislike = $this->createForm(BlogLikeNeutralDislikeBarType::class);
        $formLikeNeutralDislike->handleRequest($request);

        if ($formLikeNeutralDislike->isSubmitted() && $formLikeNeutralDislike->isValid()) {
            $redirectResponse = $this->checkVoteLikeButton($formLikeNeutralDislike, $blogArticle);
            if (!is_null($redirectResponse)) {
                return $redirectResponse;
            }

            $redirectResponse = $this->checkVoteNeutralButton($formLikeNeutralDislike, $blogArticle);
            if (!is_null($redirectResponse)) {
                return $redirectResponse;
            }

            $redirectResponse = $this->checkVoteDislikeButton($formLikeNeutralDislike, $blogArticle);
            if (!is_null($redirectResponse)) {
                return $redirectResponse;
            }
        }

        $this->increaseAmountOfDisplay($blogArticle);

        $menu = new MainMenu('app_blog_article_list');

        return $this->render('default/controller/app_blog_article.html.twig', array(
            'title' => 'Tomasz Bukowski - Portfolio - ' . $blogArticle->getTranslation()->getSubject(),
            'menu' => $menu->getMenu(),
            'fields' => $fieldManager->getArrayCollection('_controller'),
            'form_vote_like_dislike' => $formLikeNeutralDislike->createView()
        ));
    }

    private function createBlogArticleOptions() : void
    {
        $findOptions = $this->blogManager->getFindOptions();
        $findOptions->clear();

        $findOptions->addWhereCondition('article_id', 'id');
        $findOptions->addWhereParam('article_id', ':articleId', $this->articleId);
    }

    private function createControllerFields() : void
    {
        $fieldList = $this->fieldManager->getListOptions();
        $fieldList->setControllerName(BlogArticleController::class);
        $fieldList->setMethodName('indexAction');

        $this->fieldManager->setCommand($this->createCollectionCmd);
        $this->fieldManager->run();
    }

    /**
     * @return BlogArticle|null
     */
    private function getBlogArticleFromFieldCollection() : ?BlogArticle
    {
        $fieldCollection = $this->fieldManager->getCollection('_controller');
        $fieldObject = $fieldCollection->getField('app_blog_article_article_entity');

        if (is_null($fieldObject)) {
            return null;
        }

        $blogArticle = $fieldObject->getExtField();

        if ($blogArticle instanceof BlogArticle) {
            return $blogArticle;
        }

        return null;
    }

    /**
     * @param FormInterface $formLikeNeutralDislike
     * @param BlogArticle $blogArticle
     * @return null|RedirectResponse
     */
    private function checkVoteLikeButton(FormInterface $formLikeNeutralDislike, BlogArticle $blogArticle) : ?RedirectResponse
    {
        if ($formLikeNeutralDislike->get('vote_like')->isClicked()) {
            $blogArticleVoted = $this->request->getSession()->get('app_blog_article/' . $blogArticle->getId() . '/voted');

            if (is_null($blogArticleVoted)) {
                $this->blogArticleVoteLikeCmd->setBlogArticle($blogArticle);

                $this->blogManager->setCommand($this->blogArticleVoteLikeCmd);
                $this->blogManager->run();

                $this->request->getSession()->set('app_blog_article/' . $blogArticle->getId() . '/voted', true);
            }
        }

        return null;
    }

    /**
     * @param FormInterface $formLikeNeutralDislike
     * @param BlogArticle $blogArticle
     * @return null|RedirectResponse
     */
    private function checkVoteNeutralButton(FormInterface $formLikeNeutralDislike, BlogArticle $blogArticle) : ?RedirectResponse
    {
        if ($formLikeNeutralDislike->get('vote_neutral')->isClicked()) {
            $blogArticleVoted = $this->request->getSession()->get('app_blog_article/' . $blogArticle->getId() . '/voted');

            if (is_null($blogArticleVoted)) {
                $this->blogArticleVoteNeutralCmd->setBlogArticle($blogArticle);

                $this->blogManager->setCommand($this->blogArticleVoteNeutralCmd);
                $this->blogManager->run();

                $this->request->getSession()->set('app_blog_article/' . $blogArticle->getId() . '/voted', true);
            }
        }

        return null;
    }

    /**
     * @param FormInterface $formLikeNeutralDislike
     * @param BlogArticle $blogArticle
     * @return null|RedirectResponse
     */
    private function checkVoteDislikeButton(FormInterface $formLikeNeutralDislike, BlogArticle $blogArticle) : ?RedirectResponse
    {
        if ($formLikeNeutralDislike->get('vote_dislike')->isClicked()) {
            $blogArticleVoted = $this->request->getSession()->get('app_blog_article/' . $blogArticle->getId() . '/voted');

            if (is_null($blogArticleVoted)) {
                $this->blogArticleVoteDislikeCmd->setBlogArticle($blogArticle);

                $this->blogManager->setCommand($this->blogArticleVoteDislikeCmd);
                $this->blogManager->run();

                $this->request->getSession()->set('app_blog_article/' . $blogArticle->getId() . '/voted', true);
            }
        }

        return null;
    }

    /**
     * @param BlogArticle $blogArticle
     */
    private function increaseAmountOfDisplay(BlogArticle $blogArticle) : void
    {
        $blogArticleViewed = $this->request->getSession()->get('app_blog_article/' . $blogArticle->getId() . '/viewed');

        if (is_null($blogArticleViewed)) {
            $this->increaseAmountOfDisplayCmd->setBlogArticle($blogArticle);

            $this->blogManager->setCommand($this->increaseAmountOfDisplayCmd);
            $this->blogManager->run();

            $this->request->getSession()->set('app_blog_article/' . $blogArticle->getId() . '/viewed', true);
        }
    }
}
