<?php

namespace AppBundle\Controller;

use AppBundle\Menu\MainMenu;
use AppBundle\Service\Field\Command\CreateControllerCollectionCmd;
use AppBundle\Service\Field\FieldManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class AccountController
 */
class AccountController extends Controller
{
    private $createCollectionCmd;

    /** @var Request */
    private $request;
    /** @var FieldManager */
    private $fieldManager;

    /**
     * AccountController constructor.
     * @param CreateControllerCollectionCmd $createCollectionCmd
     */
    public function __construct(CreateControllerCollectionCmd $createCollectionCmd)
    {
        $this->createCollectionCmd = $createCollectionCmd;
    }

    /**
     * @Route("/account", name="app_my_account")
     *
     * @param Request $request
     * @param FieldManager $fieldManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, FieldManager $fieldManager)
    {
        $this->request = $request;
        $this->fieldManager = $fieldManager;

        $this->createControllerFields();
        $menu = new MainMenu($request->get('_route'));

        return $this->render('default/controller/app_my_account.html.twig', array(
            'title' => 'Tomasz Bukowski - Portfolio - Profil użytkownika',
            'menu' => $menu->getMenu(),
            'user' => $this->getUser(),
            'fields' => $fieldManager->getArrayCollection('_controller')
        ));
    }

    private function createControllerFields() : void
    {
        $fieldList = $this->fieldManager->getListOptions();
        $fieldList->setControllerName(AccountController::class);
        $fieldList->setMethodName('indexAction');

        $fieldOptions = $this->fieldManager->getFieldOptions();
        $fieldOptions->setPatterns($this->textPatterns());

        $this->fieldManager->setCommand($this->createCollectionCmd);
        $this->fieldManager->run();
    }

    /**
     * @return array
     */
    private function textPatterns() : array
    {
        $user = $this->getUser();

        $patterns = [
            'account_header' => [
                '%username%' => $user->getUsername()
            ],
            'account_user_username' => [
                '%username%' => $user->getUsername()
            ],
            'account_user_email' => [
                '%useremail%' => $user->getEmail()
            ],
            'account_user_roles' => [
                '%userroles%' => $user->getRoles()
            ],
            'account_user_active' => [
                '%user_active%' => $user->isActive() ? 'Yes' : 'No'
            ],
            'account_user_blocked' => [
                '%user_blocked%' => $user->isLocked() ? 'Yes' : 'No'
            ],
            'account_user_expired' => [
                '%user_expired%' => $user->isExpired() ? 'Yes' : 'No'
            ],
            'account_user_created' => [
                '%user_created%' => $user->getCreated()
            ],
            'account_user_login_time' => [
                '%user_login_time%' => $user->getLoginTime()
            ],
            'account_user_last_login_time' => [
                '%user_last_login_time%' => $user->getLastLoginTime()
            ],
        ];

        return $patterns;
    }
}
