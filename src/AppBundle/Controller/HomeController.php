<?php

namespace AppBundle\Controller;

use AppBundle\Menu\MainMenu;
use AppBundle\Service\Field\Command\CreateControllerCollectionCmd;
use AppBundle\Service\Field\FieldManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class HomeController
 */
class HomeController extends Controller
{
    private $createCollectionCmd;

    /** @var Request */
    private $request;
    /** @var FieldManager */
    private $fieldManager;

    private $articles;

    /**
     * HomeController constructor.
     * @param CreateControllerCollectionCmd $createCollectionCmd
     */
    public function __construct(CreateControllerCollectionCmd $createCollectionCmd)
    {
        $this->createCollectionCmd = $createCollectionCmd;
    }

    /**
     * @Route("/", name="app_home")
     *
     * @param Request $request
     * @param FieldManager $fieldManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, FieldManager $fieldManager)
    {
        $this->request = $request;
        $this->fieldManager = $fieldManager;

        $this->createControllerFields();
        $menu = new MainMenu($request->get('_route'));

        return $this->render('default/controller/app_home.html.twig', array(
            'title' => 'Tomasz Bukowski - Portfolio - Strona główna',
            'menu' => $menu->getMenu(),
            'fields' => $fieldManager->getArrayCollection('_controller'),
            'home_articles' => array(
                'content' => $this->getArticles()
            ),
        ));
    }

    private function createControllerFields() : void
    {
        $fieldList = $this->fieldManager->getListOptions();
        $fieldList->setControllerName(HomeController::class);
        $fieldList->setMethodName('indexAction');

        $this->fieldManager->setCommand($this->createCollectionCmd);
        $this->fieldManager->run();
    }

    private function getArticles() : array
    {
        $this->articles = array(
            array(
                'header' => 'Aktualizacja',
                'content' => 'Dodano panel administracyjny oraz listy pól w portfolio.',
                'posted_date' => '2019-10-27'
            ),
            array(
                'header' => 'Aktualizacja',
                'content' => 'Dodano dynamicznie ładowaną treść. Treść została podzielona na pola:<br> 
                            1. TranslatableInterface
                            <ul class="rectangle-list">
                                <li>HeaderField (Translatable)</li>
                                <li>SmallHeaderField (Translatable)</li>
                                <li>TextField (Translatable)</li>
                                <li>LongTextField (Translatable)</li>
                                <li>ListField (Translatable)</li>
                            </ul>
                            2. FormInterface
                            <ul class="rectangle-list">
                                <li>FormField (Form)</li>
                            </ul>
                            2. FileInterface
                            <ul class="rectangle-list">
                                <li>FileField (File)</li>
                                <li>ImageField (File)</li>
                            </ul>
                            Każde z pól otrzymało własny layout.',
                'posted_date' => '2019-10-16'
            ),
            array(
                'header' => 'Projekt',
                'content' => 'Wersja 1.1 jest już dostępna!<br>
                              W tym wydaniu dodano:
                            <ul class="rectangle-list">
                                <li>Multi-level dropdown menu.</li>
                                <li>Podstawowy system użytkowników.</li>
                             </ul>',
                'posted_date' => '2019-05-26'
            ),
            array(
                'header' => 'Projekt',
                'content' => 'Została wydana wersja 1.0 portfolio!
                              Następnie planuję napisać:
                            <ul class="rectangle-list">
                                <li>Możliwość wyboru języka (PL i EN).</li>
                                <li>Listę aktualności.</li>
                                <li>System użytkowników.</li>
                                <li>Mini panel administracyjny.</li>
                             </ul>',
                'posted_date' => '2019-03-09'
            ),
            array(
                'header' => 'Aktualizacja',
                'content' => 'Dodano podstronę "O mnie" i trochę informacji na mój temat :)<br><br>Pozdrawiam!',
                'posted_date' => '2019-02-21'
            ),
            array(
                'header' => 'Aktualizacja',
                'content' => 'Dodano formularz kontaktowy. Od dzisiaj można wysyłać maile korzystając
                    z zakładki "Kontakt".',
                'posted_date' => '2019-02-16'
            ),
            array(
                'header' => 'Projekt',
                'content' => 'Już niedługo zostanie wydana wersja 1.0 projektu.
                             Znajdować się będą:
                             <ul class="rectangle-list">
                                <li>Informacje o projekcie i nowościach na stronie.</li>
                                <li>Informacje o autorze.</li>
                                <li>Formularz kontaktowy.</li>
                             </ul>
                             ',
                'posted_date' => '2019-01-30'
            )
        );

        return $this->articles;
    }
}
