<?php

namespace AppBundle\Controller\Admin\Blog;

use AppBundle\Entity\Blog\BlogArticle;
use AppBundle\Form\Blog\EditBlogArticleType;
use AppBundle\Form\Blog\EditBlogTranslationType;
use AppBundle\FormDTO\Blog\BlogArticleDTO;
use AppBundle\Menu\MainMenu;
use AppBundle\Service\Blog\BlogManager;
use AppBundle\Service\Blog\Command\CreateBlogArticleCmd;
use AppBundle\Service\Blog\Command\FindByBlogFindOptionsCmd;
use AppBundle\Service\Blog\Command\UpdateBlogArticleCmd;
use AppBundle\Service\Blog\Command\UpdateBlogTranslationsCmd;
use AppBundle\Service\Blog\DataMapper\BlogArticleMapper;
use AppBundle\Service\Blog\Uploader\HeaderImageFileUploader;
use AppBundle\Service\Field\Command\CreateControllerCollectionCmd;
use AppBundle\Service\Field\FieldManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class BlogEditController
 */
class BlogEditController extends Controller
{
    private $createControllerCollectionCmd;
    private $findByBlogFindOptions;
    private $createBlogArticleCmd;
    private $updateBlogArticleCmd;
    private $updateBlogTranslationsCmd;

    private $articleId;
    /** @var Request */
    private $request;
    /** @var FieldManager */
    private $fieldManager;
    /** @var BlogManager */
    private $blogManager;

    /**
     * BlogEditController constructor.
     * @param CreateControllerCollectionCmd $createControllerCollectionCmd
     * @param FindByBlogFindOptionsCmd $findByBlogFindOptionsCmd
     * @param CreateBlogArticleCmd $createBlogArticleCmd
     * @param UpdateBlogArticleCmd $updateBlogArticleCmd
     * @param UpdateBlogTranslationsCmd $updateBlogTranslationsCmd
     */
    public function __construct
    (
        CreateControllerCollectionCmd $createControllerCollectionCmd,
        FindByBlogFindOptionsCmd $findByBlogFindOptionsCmd,
        CreateBlogArticleCmd $createBlogArticleCmd,
        UpdateBlogArticleCmd $updateBlogArticleCmd,
        UpdateBlogTranslationsCmd $updateBlogTranslationsCmd
    )
    {
        $this->createControllerCollectionCmd = $createControllerCollectionCmd;
        $this->findByBlogFindOptions = $findByBlogFindOptionsCmd;
        $this->createBlogArticleCmd = $createBlogArticleCmd;
        $this->updateBlogArticleCmd = $updateBlogArticleCmd;
        $this->updateBlogTranslationsCmd = $updateBlogTranslationsCmd;
    }

    /**
     * @Route("/admin/blog/article/edit/{articleId}", name="admin_blog_article_edit", requirements={"articleId"="\d+"})
     *
     * @param int $articleId
     * @param Request $request
     * @param FieldManager $fieldManager
     * @param BlogManager $blogManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(int $articleId, Request $request, FieldManager $fieldManager, BlogManager $blogManager)
    {
        $this->articleId = $articleId;
        $this->request = $request;
        $this->fieldManager = $fieldManager;
        $this->blogManager = $blogManager;

        $blogArticle = $this->createBlogArticleById();
        if (is_null($blogArticle)) {
            return $this->redirectToRoute('admin_blog_article_list');
        }

        $blogArticleDTO = $this->createBlogArticleDTO($blogArticle);

        $formBlogArticle = $this->createForm(EditBlogArticleType::class, $blogArticleDTO);
        $formBlogArticle->handleRequest($request);

        if ($formBlogArticle->isSubmitted() && $formBlogArticle->isValid()) {
            $redirectResponse = $this->checkArticlePreviewButton($formBlogArticle, $blogArticleDTO);
            if (!is_null($redirectResponse)) {
                return $redirectResponse;
            }

            $redirectResponse = $this->checkClearOptionsButton($formBlogArticle, $blogArticleDTO, $blogArticle);
            if (!is_null($redirectResponse)) {
                return $redirectResponse;
            }

            $redirectResponse = $this->checkUploadImageButton($formBlogArticle, $blogArticleDTO);
            if (!is_null($redirectResponse)) {
                return $redirectResponse;
            }

            $redirectResponse = $this->checkUpdateOptionsButton($formBlogArticle, $blogArticleDTO, $blogArticle);
            if (!is_null($redirectResponse)) {
                return $redirectResponse;
            }
        }

        $formBlogTranslations = $this->createForm(EditBlogTranslationType::class, $blogArticleDTO);
        $formBlogTranslations->handleRequest($request);

        if ($formBlogTranslations->isSubmitted() && $formBlogTranslations->isValid()) {
            $redirectResponse = $this->checkArticlePreviewButton($formBlogTranslations, $blogArticleDTO);
            if (!is_null($redirectResponse)) {
                return $redirectResponse;
            }

            $redirectResponse = $this->checkClearTranslationsButton($formBlogTranslations, $blogArticleDTO, $blogArticle);
            if (!is_null($redirectResponse)) {
                return $redirectResponse;
            }

            $redirectResponse = $this->checkAddTranslationButton($formBlogTranslations, $blogArticleDTO);
            if (!is_null($redirectResponse)) {
                return $redirectResponse;
            }

            $redirectResponse = $this->checkUpdateTranslationsButton($formBlogTranslations, $blogArticleDTO, $blogArticle);
            if (!is_null($redirectResponse)) {
                return $redirectResponse;
            }

            $redirectResponse = $this->checkDeleteTranslationButtons($formBlogTranslations, $blogArticleDTO);
            if (!is_null($redirectResponse)) {
                return $redirectResponse;
            }
        }

        $this->createControllerFields();
        $menu = new MainMenu('admin_panel');

        return $this->render('default/controller/admin/blog/blog_article_edit.html.twig', array(
            'title' => 'Tomasz Bukowski - Portfolio - Edit field',
            'menu' => $menu->getMenu(),
            'fields' => $fieldManager->getArrayCollection('_controller'),
            'form_blog_article' => $formBlogArticle->createView(),
            'form_blog_translations' => $formBlogTranslations->createView()
        ));
    }

    /**
     * @return BlogArticle|null
     */
    private function createBlogArticleById() : ?BlogArticle
    {
        $findOptions = $this->blogManager->getFindOptions();

        $findOptions->clear();
        $findOptions->addWhereCondition('article_id', 'id', 'blog_article');
        $findOptions->addWhereParam('article_id', ':articleId', $this->articleId);

        $this->blogManager->setCommand($this->findByBlogFindOptions);
        $this->blogManager->run();

        $blogArticle = $this->blogManager->getCmdResult('foundedBlogArticles');
        if (key_exists(0, $blogArticle)) {
            $this->createBlogArticleCmd->setBlogArticle($blogArticle[0]);

            $this->blogManager->setCommand($this->createBlogArticleCmd);
            $this->blogManager->run();

            return $blogArticle[0];
        }

        return null;
    }


    /**
     * @param BlogArticle $blogArticle
     * @return BlogArticleDTO
     */
    private function createBlogArticleDTO(BlogArticle $blogArticle) : BlogArticleDTO
    {
        /** @var BlogArticleMapper $blogArticleMapper */
        $blogArticleMapper = $this->blogManager->getDataMapper(BlogArticleMapper::class);
        $blogArticleDTOFactory = $this->blogManager->getFactory()->getBlogArticleDTOFactory();

        $blogArticleDTO = $blogArticleDTOFactory->createBlogArticle();
        $serializedBlogArticleDTO = $this->request->getSession()->get('admin_blog_article_edit/' . $this->articleId . '/blogArticleDTO');
        if (!is_null($serializedBlogArticleDTO)) {
            $blogArticleDTO->unserialize($serializedBlogArticleDTO);

        }

        if ($blogArticleDTO->getTranslations()->isEmpty()) {
            $blogArticleMapper->mapBlogArticleToDTO($blogArticle, $blogArticleDTO);
            $blogArticleMapper->mapBlogTranslationsToDTO($blogArticle, $blogArticleDTO);
        }

        return $blogArticleDTO;
    }

    private function createControllerFields() : void
    {
        $fieldList = $this->fieldManager->getListOptions();
        $fieldList->clear();
        $fieldList->setControllerName(BlogEditController::class);
        $fieldList->setMethodName('indexAction');

        $fieldOptions = $this->fieldManager->getFieldOptions();
        $fieldOptions->setPatterns([
            'blog_edit_btn_delete_article' => [
                '%article_id%' => $this->articleId
            ]
        ]);

        $this->fieldManager->setCommand($this->createControllerCollectionCmd);
        $this->fieldManager->run();
    }

    /**
     * @param FormInterface $formBlog
     * @param BlogArticleDTO $blogArticleDTO
     * @return null|RedirectResponse
     */
    private function checkArticlePreviewButton(FormInterface $formBlog, BlogArticleDTO $blogArticleDTO) : ?RedirectResponse
    {
        if ($formBlog->get('article_preview')->isClicked()) {
            $this->request->getSession()->set('admin_blog_article_edit/' . $this->articleId . '/blogArticleDTO', $blogArticleDTO->serialize());

            return $this->redirectToRoute('admin_blog_article_preview', ['articleId' => $this->articleId]);
        }

        return null;
    }

    /**
     * @param FormInterface $formBlogArticle
     * @param BlogArticleDTO $blogArticleDTO
     * @param BlogArticle $blogArticle
     * @return null|RedirectResponse
     */
    private function checkClearOptionsButton(FormInterface $formBlogArticle, BlogArticleDTO $blogArticleDTO, BlogArticle $blogArticle) : ?RedirectResponse
    {
        if ($formBlogArticle->get('clear_options')->isClicked()) {
            /** @var BlogArticleMapper $blogArticleMapper */
            $blogArticleMapper = $this->blogManager->getDataMapper(BlogArticleMapper::class);

            $oldImageFileName = $blogArticle->getHeaderImageFile();
            $newImageFileName = $blogArticleDTO->getHeaderImageFile();
            if ($oldImageFileName != $newImageFileName) {
                $imageUploader = $this->blogManager->getFileUploader(HeaderImageFileUploader::class);
                $imageUploader->remove($newImageFileName);
            }

            $blogArticleMapper->mapBlogArticleToDTO($blogArticle, $blogArticleDTO);

            $this->addFlash('warning', 'Blog article options has been set from database.');

            $this->request->getSession()->set('admin_blog_article_edit/' . $this->articleId . '/blogArticleDTO', $blogArticleDTO->serialize());

            return $this->redirectToRoute('admin_blog_article_edit', ['articleId' => $this->articleId]);
        }

        return null;
    }

    /**
     * @param FormInterface $formBlogArticle
     * @param BlogArticleDTO $blogArticleDTO
     * @return null|RedirectResponse
     */
    private function checkUploadImageButton(FormInterface $formBlogArticle, BlogArticleDTO $blogArticleDTO) : ?RedirectResponse
    {
        if ($formBlogArticle->get('upload_image')->isClicked()) {
            $imageUploader = $this->blogManager->getFileUploader(HeaderImageFileUploader::class);

            $uploadedFile = $formBlogArticle->get('imageFile')->getData();

            if ($uploadedFile) {
                if ($imageUploader->upload($uploadedFile)) {
                    $blogArticleDTO->setHeaderImageFile($imageUploader->getFileName());

                    $this->addFlash('success', 'Changed header image file on ' . $blogArticleDTO->getHeaderImageFile());

                } else {
                    $this->addFlash('warning', 'No image file was uploaded.');
                }

            } else {
                $this->addFlash('warning', 'No image file was uploaded.');
            }

            $this->request->getSession()->set('admin_blog_article_edit/' . $this->articleId . '/blogArticleDTO', $blogArticleDTO->serialize());

            return $this->redirectToRoute('admin_blog_article_edit', ['articleId' => $this->articleId]);
        }

        return null;
    }

    /**
     * @param FormInterface $formBlogArticle
     * @param BlogArticleDTO $blogArticleDTO
     * @param BlogArticle $blogArticle
     * @return null|RedirectResponse
     */
    private function checkUpdateOptionsButton(FormInterface $formBlogArticle, BlogArticleDTO $blogArticleDTO, BlogArticle $blogArticle) : ?RedirectResponse
    {
        if ($formBlogArticle->get('update_options')->isClicked()) {
            /** @var BlogArticleMapper $blogArticleMapper */
            $blogArticleMapper = $this->blogManager->getDataMapper(BlogArticleMapper::class);

            $oldImageFileName = $blogArticle->getHeaderImageFile();
            $newImageFileName = $blogArticleDTO->getHeaderImageFile();
            if ($oldImageFileName != $newImageFileName) {
                $imageUploader = $this->blogManager->getFileUploader(HeaderImageFileUploader::class);
                $imageUploader->remove($oldImageFileName);
            }

            $blogArticleMapper->mapDTOToBlogArticle($blogArticle, $blogArticleDTO);

            $this->updateBlogArticle($blogArticle);

            $updateError = $this->blogManager->getCmdResult('updateOptionsError');
            if (isset($updateError) && $updateError == false) {
                $this->addFlash('success',
                    'Article options was successfully updated! (BlogArticle id ' . $blogArticle->getId() . ')'
                );
            } else {
                $this->addFlash('danger',
                    'Upps...! Something went wrong. Article options was not updated! (BlogArticle id ' . $blogArticle->getId() . ')'
                );
            }

            $this->request->getSession()->set('admin_blog_article_edit/' . $this->articleId . '/blogArticleDTO', $blogArticleDTO->serialize());

            return $this->redirectToRoute('admin_blog_article_edit', ['articleId' => $this->articleId]);
        }

        return null;
    }

    /**
     * @param FormInterface $formBlogTranslations
     * @param BlogArticleDTO $blogArticleDTO
     * @param BlogArticle $blogArticle
     * @return null|RedirectResponse
     */
    private function checkClearTranslationsButton(FormInterface $formBlogTranslations, BlogArticleDTO $blogArticleDTO, BlogArticle $blogArticle) : ?RedirectResponse
    {
        if ($formBlogTranslations->get('clear_translations')->isClicked()) {
            /** @var BlogArticleMapper $blogArticleMapper */
            $blogArticleMapper = $this->blogManager->getDataMapper(BlogArticleMapper::class);

            $blogArticleMapper->mapBlogTranslationsToDTO($blogArticle, $blogArticleDTO);

            $this->addFlash('warning', 'Blog article translations has been set from database.');

            $this->request->getSession()->set('admin_blog_article_edit/' . $this->articleId . '/blogArticleDTO', $blogArticleDTO->serialize());

            return $this->redirectToRoute('admin_blog_article_edit', ['articleId' => $this->articleId]);
        }

        return null;
    }

    /**
     * @param FormInterface $formBlogTranslations
     * @param BlogArticleDTO $blogArticleDTO
     * @return null|RedirectResponse
     */
    private function checkAddTranslationButton(FormInterface $formBlogTranslations, BlogArticleDTO $blogArticleDTO) : ?RedirectResponse
    {
        if ($formBlogTranslations->get('add_translation')->isClicked()) {
            $blogArticleDTOFactory = $this->blogManager->getFactory()->getBlogArticleDTOFactory();
            $blogArticleDTOFactory->addEmptyBlogTranslation($blogArticleDTO);

            $this->addFlash('success', 'New translation was added.');

            $this->request->getSession()->set('admin_blog_article_edit/' . $this->articleId . '/blogArticleDTO', $blogArticleDTO->serialize());

            return $this->redirectToRoute('admin_blog_article_edit', ['articleId' => $this->articleId]);
        }

        return null;
    }

    /**
     * @param FormInterface $formBlogTranslations
     * @param BlogArticleDTO $blogArticleDTO
     * @param BlogArticle $blogArticle
     * @return null|RedirectResponse
     */
    private function checkUpdateTranslationsButton(FormInterface $formBlogTranslations, BlogArticleDTO $blogArticleDTO, BlogArticle $blogArticle) : ?RedirectResponse
    {
        if ($formBlogTranslations->get('update_translations')->isClicked()) {
            /** @var BlogArticleMapper $blogArticleMapper */
            $blogArticleMapper = $this->blogManager->getDataMapper(BlogArticleMapper::class);

            $blogArticleMapper->mapDTOToBlogTranslations($blogArticle, $blogArticleDTO);

            $this->updateBlogTranslations($blogArticle);

            $updateError = $this->blogManager->getCmdResult('updateTranslationsError');
            if (isset($updateError) && $updateError == false) {
                $this->request->getSession()->remove('admin_blog_article_edit/' . $this->articleId . '/blogArticleDTO');
                $this->addFlash('success',
                    'Article translations was successfully updated! (BlogArticle id ' . $blogArticle->getId() . ')'
                );
            } else {
                $this->addFlash('danger',
                    'Upps...! Something went wrong. Article translations was not updated! (BlogArticle id ' . $blogArticle->getId() . ')'
                );
            }

            return $this->redirectToRoute('admin_blog_article_edit', ['articleId' => $this->articleId]);
        }

        return null;
    }

    /**
     * @param FormInterface $formBlogTranslations
     * @param BlogArticleDTO $blogArticleDTO
     * @return null|RedirectResponse
     */
    private function checkDeleteTranslationButtons(FormInterface $formBlogTranslations, BlogArticleDTO $blogArticleDTO) : ?RedirectResponse
    {
        $blogTranslationsDTO = $blogArticleDTO->getTranslations();
        $blogTranslationsDTO->first();

        foreach ($formBlogTranslations->get('translations') as $formBlogTranslation) {
            if ($formBlogTranslation->get('delete_translation')->isClicked()) {
                $redirectResponse = $this->checkIsSingleTranslation($formBlogTranslations->get('translations'));
                if (!is_null($redirectResponse)) {
                    return $redirectResponse;
                }

                $translationId = $blogTranslationsDTO->current()->getTranslationId();
                $currentKey = $blogTranslationsDTO->key();

                $redirectResponse = $this->checkIsExistsInDataBase($blogArticleDTO, $translationId, $currentKey);
                if (!is_null($redirectResponse)) {
                    return $redirectResponse;
                }
            }

            $blogTranslationsDTO->next();
        }

        return null;
    }

    /**
     * @param FormInterface $formBlogTranslations
     * @return null|RedirectResponse
     */
    private function checkIsSingleTranslation(FormInterface $formBlogTranslations) : ?RedirectResponse
    {
        if ($formBlogTranslations->count() <= 1) {
            $this->addFlash('warning',
                'You should not delete this translation, because this article contains only one translation.'
            );

            return $this->redirectToRoute('admin_blog_article_edit', ['articleId' => $this->articleId]);
        }

        return null;
    }

    /**
     * @param BlogArticleDTO $blogArticleDTO
     * @param int|null $translationId
     * @param $currentKey
     * @return null|RedirectResponse
     */
    private function checkIsExistsInDataBase(BlogArticleDTO $blogArticleDTO, ?int $translationId, $currentKey) : ?RedirectResponse
    {
        if (!is_null($translationId)) {
            return $this->redirectToRoute('admin_blog_translation_delete', ['articleId' => $this->articleId, 'translationId' => $translationId]);
        }

        if (is_null($translationId)) {
            $blogArticleDTOFactory = $this->blogManager->getFactory()->getBlogArticleDTOFactory();
            $blogArticleDTOFactory->deleteBlogTranslation($blogArticleDTO, $currentKey);

            $this->request->getSession()->set('admin_blog_article_edit/' . $this->articleId . '/blogArticleDTO', $blogArticleDTO->serialize());

            $this->addFlash('success', 'Translation was deleted from this form. It was not in database.');

            return $this->redirectToRoute('admin_blog_article_edit', ['articleId' => $this->articleId]);
        }

        return null;
    }

    /**
     * @param BlogArticle $blogArticle
     */
    private function updateBlogArticle(BlogArticle $blogArticle) : void
    {
        $this->updateBlogArticleCmd->setBlogArticle($blogArticle);

        $this->blogManager->setCommand($this->updateBlogArticleCmd);
        $this->blogManager->run();
    }

    /**
     * @param BlogArticle $blogArticle
     */
    private function updateBlogTranslations(BlogArticle $blogArticle) : void
    {
        $this->updateBlogTranslationsCmd->setBlogArticle($blogArticle);

        $this->blogManager->setCommand($this->updateBlogTranslationsCmd);
        $this->blogManager->run();
    }
}
