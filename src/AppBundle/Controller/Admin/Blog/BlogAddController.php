<?php

namespace AppBundle\Controller\Admin\Blog;

use AppBundle\Entity\Blog\BlogArticle;
use AppBundle\Form\Blog\AddBlogArticleType;
use AppBundle\Form\Blog\AddBlogTranslationType;
use AppBundle\FormDTO\Blog\BlogArticleDTO;
use AppBundle\Menu\MainMenu;
use AppBundle\Service\Blog\BlogManager;
use AppBundle\Service\Blog\Command\InsertBlogArticleCmd;
use AppBundle\Service\Blog\DataMapper\BlogArticleMapper;
use AppBundle\Service\Blog\Uploader\HeaderImageFileUploader;
use AppBundle\Service\Field\Command\CreateControllerCollectionCmd;
use AppBundle\Service\Field\FieldManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class BlogAddController
 */
class BlogAddController extends Controller
{
    private $createCollectionCmd;
    private $insertBlogArticleCmd;

    /** @var Request */
    private $request;
    /** @var FieldManager */
    private $fieldManager;
    /** @var BlogManager */
    private $blogManager;

    /**
     * BlogAddController constructor.
     * @param CreateControllerCollectionCmd $createCollectionCmd
     * @param InsertBlogArticleCmd $insertBlogArticleCmd
     */
    public function __construct
    (
        CreateControllerCollectionCmd $createCollectionCmd,
        InsertBlogArticleCmd $insertBlogArticleCmd
    )
    {
        $this->createCollectionCmd = $createCollectionCmd;
        $this->insertBlogArticleCmd = $insertBlogArticleCmd;
    }

    /**
     * @Route("/admin/blog/article/add", name="admin_blog_article_add")
     *
     * @param Request $request
     * @param FieldManager $fieldManager
     * @param BlogManager $blogManager
     * @return null|RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addBlogArticleAction(Request $request, FieldManager $fieldManager, BlogManager $blogManager)
    {
        $this->request = $request;
        $this->fieldManager = $fieldManager;
        $this->blogManager = $blogManager;

        $blogArticleDTO = $this->getBlogArticleDTO();

        $formBlogArticle = $this->createForm(AddBlogArticleType::class, $blogArticleDTO);
        $formBlogArticle->handleRequest($request);

        if ($formBlogArticle->isSubmitted() && $formBlogArticle->isValid()) {
            $redirectResponse = $this->checkUploadImageButton($formBlogArticle, $blogArticleDTO);
            if (!is_null($redirectResponse)) {
                return $redirectResponse;
            }

            $redirectResponse = $this->checkNextStepButton($formBlogArticle, $blogArticleDTO);
            if (!is_null($redirectResponse)) {
                return $redirectResponse;
            }
        }

        $this->createControllerFields();
        $menu = new MainMenu($request->get('_route'));

        $formStep = $request->getSession()->get('admin_blog_article_add/form_step');
        if (!is_null($formStep) && $formStep > 1) {
            return $this->forward(BlogAddController::class . '::addBlogTranslationsAction');
        }

        return $this->render('default/controller/admin/blog/blog_article_add.html.twig', array(
            'title' => 'Tomasz Bukowski - Portfolio - Strona główna',
            'menu' => $menu->getMenu(),
            'fields' => $fieldManager->getArrayCollection('_controller'),
            'form_blog_article' => $formBlogArticle->createView()
        ));
    }

    /**
     * @return null|RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addBlogTranslationsAction()
    {
        $blogArticleDTO = $this->getBlogArticleDTO();

        $formBlogTranslations = $this->createForm(AddBlogTranslationType::class, $blogArticleDTO);
        $formBlogTranslations->handleRequest($this->request);

        if ($formBlogTranslations->isSubmitted() && $formBlogTranslations->isValid()) {
            $redirectResponse = $this->checkGoBackButton($formBlogTranslations, $blogArticleDTO);
            if (!is_null($redirectResponse)) {
                return $redirectResponse;
            }

            $redirectResponse = $this->checkCreateBlogArticleButton($formBlogTranslations, $blogArticleDTO);
            if (!is_null($redirectResponse)) {
                return $redirectResponse;
            }
        }

        $this->createControllerFields();
        $menu = new MainMenu('admin_panel');

        return $this->render('default/controller/admin/blog/blog_article_add.html.twig', array(
            'title' => 'Tomasz Bukowski - Portfolio - Strona główna',
            'menu' => $menu->getMenu(),
            'fields' => $this->fieldManager->getArrayCollection('_controller'),
            'form_blog_translations' => $formBlogTranslations->createView()
        ));
    }

    private function createControllerFields() : void
    {
        $step = $this->request->getSession()->get('admin_blog_article_add/form_step');

        $fieldList = $this->fieldManager->getListOptions();
        $fieldList->setControllerName(BlogAddController::class);
        $fieldList->setMethodName('indexAction');

        $fieldOptions = $this->fieldManager->getFieldOptions();
        $fieldOptions->setPatterns([
            'blog_add_header' => [
                '%step%' => is_null($step) ? 1 : $step
            ]
        ]);

        $this->fieldManager->setCommand($this->createCollectionCmd);
        $this->fieldManager->run();
    }

    /**
     * @return BlogArticleDTO
     */
    private function getBlogArticleDTO() : BlogArticleDTO
    {
        $blogArticleDTOFactory = $this->blogManager->getFactory()->getBlogArticleDTOFactory();
        $blogArticleDTO = $blogArticleDTOFactory->createBlogArticle();

        $serializedBlogArticleDTO = $this->request->getSession()->get('admin_blog_article_add/blogArticleDTO');
        if (!is_null($serializedBlogArticleDTO)) {
            $blogArticleDTO->unserialize($serializedBlogArticleDTO);
        }

        return $blogArticleDTO;
    }

    /**
     * @param FormInterface $formBlogArticle
     * @param BlogArticleDTO $blogArticleDTO
     */
    private function checkUploadImageButton(FormInterface $formBlogArticle, BlogArticleDTO $blogArticleDTO) : ?RedirectResponse
    {
        if ($formBlogArticle->get('upload_image')->isClicked()) {
            $imageUploader = $this->blogManager->getFileUploader(HeaderImageFileUploader::class);
            $uploadedFile = $formBlogArticle->get('imageFile')->getData();

            if ($uploadedFile) {
                if ($imageUploader->upload($uploadedFile)) {
                    $newImageFileName = $imageUploader->getFileName();
                    $oldImageFileName = $blogArticleDTO->getHeaderImageFile();

                    $blogArticleDTO->setHeaderImageFile($newImageFileName);

                    if (!is_null($oldImageFileName)) {
                        $imageUploader->remove($oldImageFileName);
                    }

                    $this->addFlash('success', 'Changed header image file on ' . $blogArticleDTO->getHeaderImageFile());

                } else {
                    $this->addFlash('warning', 'No image file was uploaded.');
                }

            } else {
                $this->addFlash('warning', 'No image file was uploaded.');
            }

            $this->request->getSession()->set('admin_blog_article_add/form_step', 1);
            $this->request->getSession()->set('admin_blog_article_add/blogArticleDTO', $blogArticleDTO->serialize());

            return $this->redirectToRoute('admin_blog_article_add');
        }

        return null;
    }

    /**
     * @param FormInterface $formBlogArticle
     * @param BlogArticleDTO $blogArticleDTO
     * @return null|RedirectResponse
     */
    private function checkNextStepButton(FormInterface $formBlogArticle, BlogArticleDTO $blogArticleDTO) : ?RedirectResponse
    {
        if ($formBlogArticle->get('submit_next_step')->isClicked()) {
            $blogArticleDTOFactory = $this->blogManager->getFactory()->getBlogArticleDTOFactory();

            if ($blogArticleDTO->getTranslations()->isEmpty()) {
                $blogArticleDTOFactory->createBlogTranslations($blogArticleDTO);

            } else {
                $blogArticleDTOFactory->refreshBlogTranslations($blogArticleDTO);
            }

            $this->request->getSession()->set('admin_blog_article_add/form_step', 2);
            $this->request->getSession()->set('admin_blog_article_add/blogArticleDTO', $blogArticleDTO->serialize());
        }

        return null;
    }

    /**
     * @param FormInterface $formBlogTranslations
     * @param BlogArticleDTO $blogArticleDTO
     * @return null|RedirectResponse
     */
    private function checkGoBackButton(FormInterface $formBlogTranslations, BlogArticleDTO $blogArticleDTO) : ?RedirectResponse
    {
        if ($formBlogTranslations->get('submit_go_back')->isClicked()) {
            $this->request->getSession()->set('admin_blog_article_add/form_step', 1);
            $this->request->getSession()->set('admin_blog_article_add/blogArticleDTO', $blogArticleDTO->serialize());

            return $this->redirectToRoute('admin_blog_article_add');
        }

        return null;
    }

    /**
     * @param FormInterface $formBlogTranslations
     * @param BlogArticleDTO $blogArticleDTO
     * @return null|RedirectResponse
     */
    private function checkCreateBlogArticleButton(FormInterface $formBlogTranslations, BlogArticleDTO $blogArticleDTO) : ?RedirectResponse
    {
        if ($formBlogTranslations->get('submit_create')->isClicked()) {
            $blogArticleFactory = $this->blogManager->getFactory()->getBlogArticleFactory();
            /** @var BlogArticleMapper $blogArticleMapper */
            $blogArticleMapper = $this->blogManager->getDataMapper(BlogArticleMapper::class);

            $blogArticle = $blogArticleFactory->createBlogArticle();
            $blogArticleMapper->mapDTOToBlogArticle($blogArticle, $blogArticleDTO);
            $blogArticleMapper->mapDTOToBlogTranslations($blogArticle, $blogArticleDTO);

            $this->insertBlogArticleIntoDataBase($blogArticle);

            $insertError = $this->blogManager->getCmdResult('insertError');
            if (isset($insertError) && $insertError == false) {
                $this->request->getSession()->remove('admin_blog_article_add/form_step');
                $this->request->getSession()->remove('admin_blog_article_add/blogArticleDTO');

                $this->addFlash('success', 'Blog article was insert correctly!');
            } else {
                $this->addFlash('danger',
                    'Something went wrong during insert new blog article into database! No blog article added.'
                );
            }

            return $this->redirectToRoute('admin_blog_article_list');
        }

        return null;
    }

    /**
     * @param BlogArticle $blogArticle
     */
    private function insertBlogArticleIntoDataBase(BlogArticle $blogArticle) : void
    {
        $this->insertBlogArticleCmd->setBlogArticle($blogArticle);

        $this->blogManager->setCommand($this->insertBlogArticleCmd);
        $this->blogManager->run();
    }
}
