<?php

namespace AppBundle\Controller\Admin\Blog;

use AppBundle\Entity\Blog\Tag\BlogTag;
use AppBundle\Form\Blog\DeleteBlogTagType;
use AppBundle\Menu\MainMenu;
use AppBundle\Service\Blog\BlogManager;
use AppBundle\Service\Blog\Command\CreateBlogTagCmd;
use AppBundle\Service\Blog\Command\DeleteBlogTagCmd;
use AppBundle\Service\Blog\Command\FindBlogTagByIdCmd;
use AppBundle\Service\Field\Command\CreateControllerCollectionCmd;
use AppBundle\Service\Field\FieldManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class BlogTagDeleteController
 */
class BlogTagDeleteController extends Controller
{
    private $createControllerCollectionCmd;
    private $findBlogTagByIdCmd;
    private $createBlogTagCmd;
    private $deleteBlogTagCmd;

    private $tagId;
    /** @var Request */
    private $request;
    /** @var FieldManager */
    private $fieldManager;
    /** @var BlogManager */
    private $blogManager;

    /**
     * BlogTagDeleteController constructor.
     * @param CreateControllerCollectionCmd $createControllerCollectionCmd
     * @param FindBlogTagByIdCmd $findBlogTagByIdCmd
     * @param CreateBlogTagCmd $createBlogTagCmd
     * @param DeleteBlogTagCmd $deleteBlogTagCmd
     */
    public function __construct
    (
        CreateControllerCollectionCmd $createControllerCollectionCmd,
        FindBlogTagByIdCmd $findBlogTagByIdCmd,
        CreateBlogTagCmd $createBlogTagCmd,
        DeleteBlogTagCmd $deleteBlogTagCmd
    )
    {
        $this->createControllerCollectionCmd = $createControllerCollectionCmd;
        $this->findBlogTagByIdCmd = $findBlogTagByIdCmd;
        $this->createBlogTagCmd = $createBlogTagCmd;
        $this->deleteBlogTagCmd = $deleteBlogTagCmd;
    }

    /**
     * @Route("/admin/blog/tag/delete/{tagId}", name="admin_blog_tag_delete", requirements={"tagId"="\d+"})
     *
     * @param int $tagId
     * @param Request $request
     * @param FieldManager $fieldManager
     * @param BlogManager $blogManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(int $tagId, Request $request, FieldManager $fieldManager, BlogManager $blogManager)
    {
        $this->tagId = $tagId;
        $this->request = $request;
        $this->fieldManager = $fieldManager;
        $this->blogManager = $blogManager;

        $blogTag = $this->createBlogTagByTagId($tagId);
        if (is_null($blogTag)) {
            return $this->redirectToRoute('admin_blog_tag_list');
        }

        $formConfirmDelete = $this->createForm(DeleteBlogTagType::class);
        $formConfirmDelete->handleRequest($request);

        if ($formConfirmDelete->isSubmitted() && $formConfirmDelete->isValid()) {
            $redirectResponse = $this->checkConfirmYesButton($formConfirmDelete, $blogTag);
            if (!is_null($redirectResponse)) {
                return $redirectResponse;
            }

            $redirectResponse = $this->checkConfirmGoBackButton($formConfirmDelete);
            if (!is_null($redirectResponse)) {
                return $redirectResponse;
            }
        }

        $this->createControllerFields('indexAction', $blogTag);
        $menu = new MainMenu('admin_panel');

        return $this->render('default/controller/admin/blog/blog_tag_delete.html.twig', array(
            'title' => 'Tomasz Bukowski - Portfolio - Delete blog tag',
            'menu' => $menu->getMenu(),
            'fields' => $fieldManager->getArrayCollection('_controller'),
            'form_confirm_delete_blog_tag' => $formConfirmDelete->createView()
        ));
    }

    /**
     * @param int $tagId
     * @return BlogTag|null
     */
    private function createBlogTagByTagId(int $tagId) : ?BlogTag
    {
        $this->findBlogTagByIdCmd->setTagId($tagId);

        $this->blogManager->setCommand($this->findBlogTagByIdCmd);
        $this->blogManager->run();

        $blogTag = $this->blogManager->getCmdResult('foundedBlogTag');

        if (!is_null($blogTag)) {
            $this->createBlogTagCmd->setBlogTag($blogTag);

            $this->blogManager->setCommand($this->createBlogTagCmd);
            $this->blogManager->run();
        }

        return $blogTag;
    }

    /**
     * @param string $actionMethod
     * @param BlogTag $blogTag
     */
    private function createControllerFields(string $actionMethod, BlogTag $blogTag) : void
    {
        $fieldList = $this->fieldManager->getListOptions();
        $fieldList->clear();
        $fieldList->setControllerName(BlogTagDeleteController::class);
        $fieldList->setMethodName($actionMethod);

        $fieldOptions = $this->fieldManager->getFieldOptions();
        $fieldOptions->setPatterns([
            'delete_blog_tag_question' => [
                '%tag_name%' => '#' . $blogTag->getName()
            ]
        ]);

        $this->fieldManager->setCommand($this->createControllerCollectionCmd);
        $this->fieldManager->run();
    }

    /**
     * @param FormInterface $formConfirmDelete
     * @param BlogTag $blogTag
     * @return null|RedirectResponse
     */
    private function checkConfirmYesButton(FormInterface $formConfirmDelete, BlogTag $blogTag) : ?RedirectResponse
    {
        if ($formConfirmDelete->get('confirm_yes')->isClicked()) {
            $this->deleteBlogTagFromDataBase($blogTag);

            $deleteError = $this->blogManager->getCmdResult('deleteError');
            if (isset($deleteError) && $deleteError == false) {
                $this->addFlash('success', 'Blog tag was deleted correctly.');
            } else {
                $this->addFlash('danger', 'Upps...! Something went wrong. Blog tag was not deleted.');
            }

            return $this->redirectToRoute('admin_blog_tag_list');
        }

        return null;
    }

    /**
     * @param FormInterface $formConfirmDelete
     * @return null|RedirectResponse
     */
    private function checkConfirmGoBackButton(FormInterface $formConfirmDelete) : ?RedirectResponse
    {
        if ($formConfirmDelete->get('confirm_go_back')->isClicked()) {
            $this->addFlash('warning', 'Blog tag was not deleted.');

            return $this->redirectToRoute('admin_blog_tag_list');
        }

        return null;
    }

    /**
     * @param BlogTag $blogTag
     */
    private function deleteBlogTagFromDataBase(BlogTag $blogTag) : void
    {
        $this->deleteBlogTagCmd->setBlogTag($blogTag);

        $this->blogManager->setCommand($this->deleteBlogTagCmd);
        $this->blogManager->run();
    }
}
