<?php

namespace AppBundle\Controller\Admin\Blog;

use AppBundle\Entity\Blog\BlogArticle;
use AppBundle\Form\Blog\DeleteBlogArticleType;
use AppBundle\Menu\MainMenu;
use AppBundle\Service\Blog\BlogManager;
use AppBundle\Service\Blog\Command\CreateBlogArticleCmd;
use AppBundle\Service\Blog\Command\DeleteBlogArticleCmd;
use AppBundle\Service\Blog\Command\DeleteBlogTranslationCmd;
use AppBundle\Service\Blog\Command\FindByBlogFindOptionsCmd;
use AppBundle\Service\Field\Command\CreateControllerCollectionCmd;
use AppBundle\Service\Field\FieldManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class BlogDeleteController
 */
class BlogDeleteController extends Controller
{
    private $createControllerCollectionCmd;
    private $findByBlogFindOptions;
    private $createBlogArticleCmd;
    private $deleteBlogArticleCmd;
    private $deleteBlogTranslationCmd;

    private $articleId;
    private $translationId;
    /** @var Request */
    private $request;
    /** @var FieldManager */
    private $fieldManager;
    /** @var BlogManager */
    private $blogManager;

    /**
     * BlogDeleteController constructor.
     * @param CreateControllerCollectionCmd $createControllerCollectionCmd
     * @param FindByBlogFindOptionsCmd $findByBlogFindOptionsCmd
     * @param CreateBlogArticleCmd $createBlogArticleCmd
     * @param DeleteBlogArticleCmd $deleteBlogArticleCmd
     * @param DeleteBlogTranslationCmd $deleteBlogTranslationCmd
     */
    public function __construct
    (
        CreateControllerCollectionCmd $createControllerCollectionCmd,
        FindByBlogFindOptionsCmd $findByBlogFindOptionsCmd,
        CreateBlogArticleCmd $createBlogArticleCmd,
        DeleteBlogArticleCmd $deleteBlogArticleCmd,
        DeleteBlogTranslationCmd $deleteBlogTranslationCmd
    )
    {
        $this->createControllerCollectionCmd = $createControllerCollectionCmd;
        $this->findByBlogFindOptions = $findByBlogFindOptionsCmd;
        $this->createBlogArticleCmd = $createBlogArticleCmd;
        $this->deleteBlogArticleCmd = $deleteBlogArticleCmd;
        $this->deleteBlogTranslationCmd = $deleteBlogTranslationCmd;
    }

    /**
     * @Route("/admin/blog/article/delete/{articleId}", name="admin_blog_article_delete", requirements={"articleId"="\d+"})
     *
     * @param int $articleId
     * @param Request $request
     * @param FieldManager $fieldManager
     * @param BlogManager $blogManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteBlogArticleAction(int $articleId, Request $request, FieldManager $fieldManager, BlogManager $blogManager)
    {
        $this->articleId = $articleId;
        $this->request = $request;
        $this->fieldManager = $fieldManager;
        $this->blogManager = $blogManager;

        $blogArticle = $this->createBlogArticleById();
        if (is_null($blogArticle)) {
            return $this->redirectToRoute('admin_blog_article_list');
        }

        $formConfirmDelete = $this->createForm(DeleteBlogArticleType::class);
        $formConfirmDelete->handleRequest($request);

        if ($formConfirmDelete->isSubmitted() && $formConfirmDelete->isValid()) {
            $redirectResponse = $this->checkConfirmYesButton($formConfirmDelete, $blogArticle, 'deleteBlogArticleAction');
            if (!is_null($redirectResponse)) {
                return $redirectResponse;
            }

            $redirectResponse = $this->checkConfirmGoBackButton($formConfirmDelete, 'deleteBlogArticleAction');
            if (!is_null($redirectResponse)) {
                return $redirectResponse;
            }
        }

        $this->createControllerFields('deleteBlogArticleAction');
        $menu = new MainMenu('admin_panel');

        return $this->render('default/controller/admin/blog/blog_article_delete.html.twig', array(
            'title' => 'Tomasz Bukowski - Portfolio - Article preview',
            'menu' => $menu->getMenu(),
            'fields' => $fieldManager->getArrayCollection('_controller'),
            'form_confirm_delete' => $formConfirmDelete->createView()
        ));
    }

    /**
     * @Route("/admin/blog/article/delete/{articleId}/translation/{translationId}",
     *     name="admin_blog_translation_delete", requirements={"articleId"="\d+", "translationId"="\d+"})
     *
     * @param int $articleId
     * @param int $translationId
     * @param Request $request
     * @param FieldManager $fieldManager
     * @param BlogManager $blogManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteBlogTranslationAction(int $articleId, int $translationId, Request $request, FieldManager $fieldManager, BlogManager $blogManager)
    {
        $this->articleId = $articleId;
        $this->translationId = $translationId;
        $this->request = $request;
        $this->fieldManager = $fieldManager;
        $this->blogManager = $blogManager;

        $blogArticle = $this->createBlogArticleById();
        if (is_null($blogArticle)) {
            return $this->redirectToRoute('admin_blog_article_list');
        }

        $formConfirmDelete = $this->createForm(DeleteBlogArticleType::class);
        $formConfirmDelete->handleRequest($request);

        if ($formConfirmDelete->isSubmitted() && $formConfirmDelete->isValid()) {
            $redirectResponse = $this->checkConfirmYesButton($formConfirmDelete, $blogArticle, 'deleteBlogTranslationAction');
            if (!is_null($redirectResponse)) {
                return $redirectResponse;
            }

            $redirectResponse = $this->checkConfirmGoBackButton($formConfirmDelete, 'deleteBlogTranslationAction');
            if (!is_null($redirectResponse)) {
                return $redirectResponse;
            }
        }

        $this->createControllerFields('deleteBlogTranslationAction');
        $menu = new MainMenu('admin_panel');

        return $this->render('default/controller/admin/blog/blog_article_delete.html.twig', array(
            'title' => 'Tomasz Bukowski - Portfolio - Article preview',
            'menu' => $menu->getMenu(),
            'fields' => $fieldManager->getArrayCollection('_controller'),
            'form_confirm_delete' => $formConfirmDelete->createView()
        ));
    }

    /**
     * @return BlogArticle|null
     */
    private function createBlogArticleById() : ?BlogArticle
    {
        $findOptions = $this->blogManager->getFindOptions();

        $findOptions->clear();
        $findOptions->addWhereCondition('article_id', 'id', 'blog_article');
        $findOptions->addWhereParam('article_id', ':articleId', $this->articleId);

        $this->blogManager->setCommand($this->findByBlogFindOptions);
        $this->blogManager->run();

        $blogArticle = $this->blogManager->getCmdResult('foundedBlogArticles');
        if (key_exists(0, $blogArticle)) {
            $this->createBlogArticleCmd->setBlogArticle($blogArticle[0]);

            $this->blogManager->setCommand($this->createBlogArticleCmd);
            $this->blogManager->run();

            return $blogArticle[0];
        }

        return null;
    }

    /**
     * @param string $methodName
     */
    private function createControllerFields(string $methodName) : void
    {
        $fieldList = $this->fieldManager->getListOptions();
        $fieldList->clear();
        $fieldList->setControllerName(BlogDeleteController::class);
        $fieldList->setMethodName($methodName);

        $this->fieldManager->setCommand($this->createControllerCollectionCmd);
        $this->fieldManager->run();
    }

    /**
     * @param FormInterface $formConfirmDelete
     * @param BlogArticle $blogArticle
     * @param string $action
     * @return null|RedirectResponse
     */
    private function checkConfirmYesButton(FormInterface $formConfirmDelete, BlogArticle $blogArticle, string $action) : ?RedirectResponse
    {
        if ($formConfirmDelete->get('confirm_yes')->isClicked()) {
            if ($action == 'deleteBlogArticleAction') {
                $this->deleteBlogArticleFromDataBase($blogArticle);

                $deleteError = $this->blogManager->getCmdResult('deleteError');
                if (isset($deleteError) && $deleteError == false) {
                    $this->addFlash('success', 'Blog article was deleted correctly.');
                } else {
                    $this->addFlash('danger', 'Upps...! Something went wrong. Blog article was not deleted.');
                }

                return $this->redirectToRoute('admin_blog_article_list');
            }

            if ($action == 'deleteBlogTranslationAction') {
                $this->deleteBlogTranslationFromDataBase($blogArticle);

                $deleteError = $this->blogManager->getCmdResult('deleteError');
                if (isset($deleteError) && $deleteError == false) {
                    $this->addFlash('success', 'Blog translation was deleted correctly.');
                } else {
                    $this->addFlash('danger', 'Upps...! Something went wrong. Blog translation was not deleted.');
                }

                return $this->redirectToRoute('admin_blog_article_edit', ['articleId' => $this->articleId]);
            }
        }

        return null;
    }

    /**
     * @param FormInterface $formConfirmDelete
     * @param string $action
     * @return null|RedirectResponse
     */
    private function checkConfirmGoBackButton(FormInterface $formConfirmDelete, string $action) : ?RedirectResponse
    {
        if ($formConfirmDelete->get('confirm_go_back')->isClicked()) {
            if ($action == 'deleteBlogArticleAction') {
                $this->addFlash('warning', 'Blog article was not deleted.');

                return $this->redirectToRoute('admin_blog_article_list');
            }

            if ($action == 'deleteBlogTranslationAction') {
                $this->addFlash('warning', 'Blog translation was not deleted.');

                return $this->redirectToRoute('admin_blog_article_edit', ['articleId' => $this->articleId]);
            }
        }

        return null;
    }

    /**
     * @param BlogArticle $blogArticle
     */
    private function deleteBlogArticleFromDataBase(BlogArticle $blogArticle) : void
    {
        $this->deleteBlogArticleCmd->setBlogArticle($blogArticle);

        $this->blogManager->setCommand($this->deleteBlogArticleCmd);
        $this->blogManager->run();
    }

    /**
     * @param BlogArticle $blogArticle
     */
    private function deleteBlogTranslationFromDataBase(BlogArticle $blogArticle) : void
    {
        $this->deleteBlogTranslationCmd->setBlogArticle($blogArticle, $this->translationId);

        $this->blogManager->setCommand($this->deleteBlogTranslationCmd);
        $this->blogManager->run();
    }
}
