<?php

namespace AppBundle\Controller\Admin\Blog;

use AppBundle\Component\Pagination\Paginator;
use AppBundle\Form\Blog\SearchBlogType;
use AppBundle\FormDTO\Blog\SearchBlogDTO;
use AppBundle\Menu\MainMenu;
use AppBundle\Service\Blog\BlogManager;
use AppBundle\Service\Blog\Command\CountByBlogFindOptionsCmd;
use AppBundle\Service\Blog\Command\CreateBlogArticleCollectionCmd;
use AppBundle\Service\Field\Command\CreateControllerCollectionCmd;
use AppBundle\Service\Field\FieldManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class BlogListController
 */
class BlogListController extends Controller
{
    private $createControllerCollectionCmd;
    private $createBlogArticleCollectionCmd;
    private $countByBlogFindOptionsCmd;

    /** @var Request */
    private $request;
    /** @var FieldManager */
    private $fieldManager;
    /** @var BlogManager */
    private $blogManager;

    /**
     * BlogListController constructor.
     * @param CreateControllerCollectionCmd $createControllerCollectionCmd
     * @param CreateBlogArticleCollectionCmd $createBlogArticleCollectionCmd
     * @param CountByBlogFindOptionsCmd $countByBlogFindOptionsCmd
     */
    public function __construct
    (
        CreateControllerCollectionCmd $createControllerCollectionCmd,
        CreateBlogArticleCollectionCmd $createBlogArticleCollectionCmd,
        CountByBlogFindOptionsCmd $countByBlogFindOptionsCmd
    )
    {
        $this->createControllerCollectionCmd = $createControllerCollectionCmd;
        $this->createBlogArticleCollectionCmd = $createBlogArticleCollectionCmd;
        $this->countByBlogFindOptionsCmd = $countByBlogFindOptionsCmd;
    }

    /**
     * @Route("/admin/blog/article/list/{page}", name="admin_blog_article_list", requirements={"page"="\d+"})
     *
     * @param int $page
     * @param Request $request
     * @param FieldManager $fieldManager
     * @param BlogManager $blogManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(int $page = 1, Request $request, FieldManager $fieldManager, BlogManager $blogManager)
    {
        $this->request = $request;
        $this->fieldManager = $fieldManager;
        $this->blogManager = $blogManager;

        $searchBlogFormDTO = $this->getSearchBlogDTO();
        $searchBlogForm = $this->createForm(SearchBlogType::class, $searchBlogFormDTO, [
            'action' => $this->generateUrl('admin_blog_article_list')
        ]);
        $searchBlogForm->handleRequest($request);

        if ($searchBlogForm->isSubmitted() && $searchBlogForm->isValid()) {
            $this->request->getSession()->set('blog_list/search_blog_form', $searchBlogFormDTO->serialize());
        }

        $this->createBlogArticleList($page, $searchBlogFormDTO);
        $amountOfBlogArticles = $this->getAmountOfAllBlogArticles();

        $this->createControllerFields();
        $menu = new MainMenu('admin_panel');

        $blogListPaginator = new Paginator($amountOfBlogArticles, $searchBlogFormDTO->getArticlesPerPage());
        $blogListPaginator->setPageNumber($page);

        return $this->render('default/controller/admin/blog/blog_article_list.html.twig', array(
            'title' => 'Tomasz Bukowski - Portfolio - Lista wszystkich pól',
            'menu' => $menu->getMenu(),
            'fields' => $fieldManager->getArrayCollection('_controller'),
            'form_search_blog' => $searchBlogForm->createView(),
            'blog_list' => $blogManager->getArrayCollection('blog_list'),
            'blog_list_pagination' => $blogListPaginator
        ));
    }

    private function createControllerFields() : void
    {
        $fieldList = $this->fieldManager->getListOptions();
        $fieldList->clear();
        $fieldList->setControllerName(BlogListController::class);
        $fieldList->setMethodName('indexAction');

        $this->fieldManager->setCommand($this->createControllerCollectionCmd);
        $this->fieldManager->run();
    }

    /**
     * @return SearchBlogDTO
     */
    private function getSearchBlogDTO() : SearchBlogDTO
    {
        $formDTOFactory = $this->blogManager->getFactory()->getFormDTOFactory();
        $searchBlogFormDTO = $formDTOFactory->createSearchBlog();

        $serializedSearchBlogForm = $this->request->getSession()->get('blog_list/search_blog_form');
        if (!is_null($serializedSearchBlogForm)) {
            $searchBlogFormDTO->unserialize($serializedSearchBlogForm);
        }

        return $searchBlogFormDTO;
    }

    /**
     * @param int $pageNumber
     * @param SearchBlogDTO $searchBlogDTO
     */
    private function createBlogArticleList(int $pageNumber, SearchBlogDTO $searchBlogDTO) : void
    {
        if (!is_null($searchBlogDTO->getArticlesPerPage())) {
            $articlesPerPage = $searchBlogDTO->getArticlesPerPage();
        } else {
            $articlesPerPage = 25;
        }

        $offset = ($pageNumber - 1) * $articlesPerPage;
        $limit = $articlesPerPage;

        $findOptions = $this->blogManager->getFindOptions();
        $findOptions->clear();

        if ($searchBlogDTO->getStatus() != 'all_status') {
            $findOptions->addWhereCondition('status', 'status');
            $findOptions->addWhereParam('status', ':status', $searchBlogDTO->getStatus());
        }

        if ($searchBlogDTO->getFilterType() == 'articles_contains_selected_tags') {
            $findOptions->joinBlogTagDataTable('btd');
            $findOptions->addWhereCondition('tag_list', 'tagId', 'btd', 'IN');
            $findOptions->addWhereParam('tag_list', ':tags', $searchBlogDTO->getTags());
        }
        if ($searchBlogDTO->getFilterType() == 'articles_without_tags') {
            $findOptions->joinBlogTagDataTable('btd');
            $findOptions->addWhereCondition('null_article', 'articleId', 'btd', 'IS NULL');
        }

        $findOptions->setOffset($offset);
        $findOptions->setLimit($limit);
        $findOptions->setSortBy($searchBlogDTO->getSortBy());

        if ($searchBlogDTO->getSortMethod() == 'ASC') {
            $findOptions->sortASC();
        }
        if ($searchBlogDTO->getSortMethod() == 'DESC') {
            $findOptions->sortDESC();
        }

        $this->createBlogArticleCollectionCmd->setCollectionName('blog_list');
        $this->blogManager->setCommand($this->createBlogArticleCollectionCmd);
        $this->blogManager->run();
    }

    /**
     * @return int
     */
    private function getAmountOfAllBlogArticles() : int
    {
        $findOptions = $this->blogManager->getFindOptions();
        $findOptions->clear();

        $this->blogManager->setCommand($this->countByBlogFindOptionsCmd);
        $this->blogManager->run();

        return $this->blogManager->getCmdResult('amountOfBlogArticles');
    }
}
