<?php

namespace AppBundle\Controller\Admin\Blog;

use AppBundle\Form\Blog\EditBlogTagListType;
use AppBundle\FormDTO\Blog\BlogTagListDTO;
use AppBundle\Menu\MainMenu;
use AppBundle\Service\Blog\BlogManager;
use AppBundle\Service\Blog\Command\CreateBlogTagCollectionCmd;
use AppBundle\Service\Blog\Command\UpdateBlogTagListCmd;
use AppBundle\Service\Blog\DataMapper\BlogTagListMapper;
use AppBundle\Service\Blog\Factory\BlogTagListDTOFactory;
use AppBundle\Service\Field\Command\CreateControllerCollectionCmd;
use AppBundle\Service\Field\FieldManager;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class BlogTagListController
 */
class BlogTagListController extends Controller
{
    private $createControllerCollectionCmd;
    private $createBlogTagCollectionCmd;
    private $updateBlogTagListCmd;

    /** @var Request */
    private $request;
    /** @var FieldManager */
    private $fieldManager;
    /** @var BlogManager */
    private $blogManager;

    /**
     * BlogTagListController constructor.
     * @param CreateControllerCollectionCmd $createControllerCollectionCmd
     * @param CreateBlogTagCollectionCmd $createBlogTagCollectionCmd
     * @param UpdateBlogTagListCmd $updateBlogTagListCmd
     */
    public function __construct
    (
        CreateControllerCollectionCmd $createControllerCollectionCmd,
        CreateBlogTagCollectionCmd $createBlogTagCollectionCmd,
        UpdateBlogTagListCmd $updateBlogTagListCmd
    )
    {
        $this->createControllerCollectionCmd = $createControllerCollectionCmd;
        $this->createBlogTagCollectionCmd = $createBlogTagCollectionCmd;
        $this->updateBlogTagListCmd = $updateBlogTagListCmd;
    }

    /**
     * @Route("/admin/blog/tag/list", name="admin_blog_tag_list")
     *
     * @param Request $request
     * @param FieldManager $fieldManager
     * @param BlogManager $blogManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, FieldManager $fieldManager, BlogManager $blogManager)
    {
        $this->request = $request;
        $this->fieldManager = $fieldManager;
        $this->blogManager = $blogManager;

        $blogTagListDTOFactory = $blogManager->getFactory()->getBlogTagListDTOFactory();
        $blogTagListDTO = $this->getBlogTagListDTO($blogTagListDTOFactory);

        $formBlogTagList = $this->createForm(EditBlogTagListType::class, $blogTagListDTO);
        $formBlogTagList->handleRequest($request);

        if ($formBlogTagList->isSubmitted()) {
            $redirectResponse = $this->checkAddTagButton($formBlogTagList, $blogTagListDTO);
            if (!is_null($redirectResponse)) {
                return $redirectResponse;
            }

            $redirectResponse = $this->checkDeleteTagButtons($formBlogTagList, $blogTagListDTO);
            if (!is_null($redirectResponse)) {
                return $redirectResponse;
            }

            $redirectResponse = $this->checkUpdateTagsButton($formBlogTagList, $blogTagListDTO);
            if (!is_null($redirectResponse)) {
                return $redirectResponse;
            }
        }

        $this->createControllerFields();
        $menu = new MainMenu('admin_panel');

        return $this->render('default/controller/admin/blog/blog_tag_list.html.twig', array(
            'title' => 'Tomasz Bukowski - Portfolio - Lista wszystkich tagów',
            'menu' => $menu->getMenu(),
            'fields' => $fieldManager->getArrayCollection('_controller'),
            'blog_tags' => $blogTagListDTO->getTags()->getValues(),
            'form_blog_tags' => $formBlogTagList->createView()
        ));
    }

    private function createControllerFields() : void
    {
        $fieldList = $this->fieldManager->getListOptions();
        $fieldList->clear();
        $fieldList->setControllerName(BlogTagListController::class);
        $fieldList->setMethodName('indexAction');

        $this->fieldManager->setCommand($this->createControllerCollectionCmd);
        $this->fieldManager->run();
    }

    /**
     * @param BlogTagListDTOFactory $blogTagListDTOFactory
     * @return BlogTagListDTO
     */
    private function getBlogTagListDTO(BlogTagListDTOFactory $blogTagListDTOFactory) : BlogTagListDTO
    {
        $blogTagListDTO = $blogTagListDTOFactory->createBlogTagList();

        $serializedBlogTagListDTO = $this->request->getSession()->get('admin_blog_tag_list/blogTagListDTO');
        if (!is_null($serializedBlogTagListDTO)) {
            $blogTagListDTO->unserialize($serializedBlogTagListDTO);
        } else {
            $this->createBlogTagCollection();

            $blogTagList = $this->blogManager->getContainer()->getCollection('blog_tags');

            /** @var BlogTagListMapper $blogTagListMapper */
            $blogTagListMapper = $this->blogManager->getDataMapper(BlogTagListMapper::class);
            $blogTagListMapper->mapBlogTagListToDTO($blogTagList, $blogTagListDTO);
        }

        return $blogTagListDTO;
    }

    private function createBlogTagCollection() : void
    {
        $this->createBlogTagCollectionCmd->setCollectionName('blog_tags');

        $this->blogManager->setCommand($this->createBlogTagCollectionCmd);
        $this->blogManager->run();
    }

    /**
     * @param FormInterface $formBlogTagList
     * @param BlogTagListDTO $blogTagListDTO
     * @return RedirectResponse|null
     */
    private function checkAddTagButton(FormInterface $formBlogTagList, BlogTagListDTO $blogTagListDTO) : ?RedirectResponse
    {
        if ($formBlogTagList->get('add_tag')->isClicked()) {
            $blogTagListDTOFactory = $this->blogManager->getFactory()->getBlogTagListDTOFactory();
            $blogTagListDTOFactory->addEmptyBlogTag($blogTagListDTO);

            $this->addFlash('success', 'New blog tag was added.');
            $this->request->getSession()->set('admin_blog_tag_list/blogTagListDTO', $blogTagListDTO->serialize());

            return $this->redirectToRoute('admin_blog_tag_list');
        }

        return null;
    }

    /**
     * @param FormInterface $formBlogTagList
     * @param BlogTagListDTO $blogTagListDTO
     * @return null|RedirectResponse
     */
    private function checkUpdateTagsButton(FormInterface $formBlogTagList, BlogTagListDTO $blogTagListDTO) : ?RedirectResponse
    {
        if ($formBlogTagList->get('update_tags')->isClicked()) {
            if (!$this->isValidForm($formBlogTagList)) {
                $this->addFlash('danger', 'Form has invalid fields.');

                return null;
            }

            /** @var BlogTagListMapper $blogTagListMapper */
            $blogTagListMapper = $this->blogManager->getDataMapper(BlogTagListMapper::class);

            if (!$this->blogManager->getContainer()->existsCollection('blog_tags')) {
                $this->createBlogTagCollection();
            }
            $blogTagList = $this->blogManager->getContainer()->getCollection('blog_tags');

            $blogTagListMapper->mapDTOToBlogTagList($blogTagList, $blogTagListDTO);
            $this->updateBlogTagList($blogTagList);

            $updateError = $this->blogManager->getCmdResult('updateError');
            if (isset($updateError) && $updateError == false) {
                $this->addFlash('success', 'Blog tag list was updated correctly.');

                $this->request->getSession()->remove('admin_blog_tag_list/blogTagListDTO');

            } else {
                $this->addFlash('danger', 'Upps...! Something went wrong. Blog tag list was not updated.');
            }

            return $this->redirectToRoute('admin_blog_tag_list');
        }

        return null;
    }

    /**
     * @param FormInterface $formBlogTagList
     * @param BlogTagListDTO $blogTagListDTO
     * @return null|RedirectResponse
     */
    private function checkDeleteTagButtons(FormInterface $formBlogTagList, BlogTagListDTO $blogTagListDTO) : ?RedirectResponse
    {
        $blogTagsDTO = $blogTagListDTO->getTags();
        $blogTagsDTO->first();

        foreach ($formBlogTagList->get('tags') as $formBlogTag) {
            if ($formBlogTag->get('delete_tag')->isClicked()) {
                $tagId = $blogTagsDTO->current()->getTagId();
                $currentKey = $blogTagsDTO->key();

                $redirectResponse = $this->checkIsExistsInDataBase($blogTagListDTO, $tagId, $currentKey);
                if (!is_null($redirectResponse)) {
                    return $redirectResponse;
                }
            }

            $blogTagsDTO->next();
        }

        return null;
    }

    /**
     * @param FormInterface $formBlogTagList
     * @return bool
     */
    private function isValidForm(FormInterface $formBlogTagList) : bool
    {
        if ($formBlogTagList->isValid()) {
            return true;
        }

        return false;
    }

    /**
     * @param ArrayCollection $blogTagList
     */
    private function updateBlogTagList(ArrayCollection $blogTagList) : void
    {
        $this->updateBlogTagListCmd->setBlogTagList($blogTagList);

        $this->blogManager->setCommand($this->updateBlogTagListCmd);
        $this->blogManager->run();
    }

    /**
     * @param BlogTagListDTO $blogTagListDTO
     * @param int|null $tagId
     * @param $currentKey
     * @return null|RedirectResponse
     */
    private function checkIsExistsInDataBase(BlogTagListDTO $blogTagListDTO, ?int $tagId, $currentKey) : ?RedirectResponse
    {
        if (!is_null($tagId)) {
            $this->request->getSession()->set('admin_blog_tag_list/blogTagListDTO', $blogTagListDTO->serialize());

            return $this->redirectToRoute('admin_blog_tag_delete', ['tagId' => $tagId]);
        }

        if (is_null($tagId)) {
            $blogTagListDTOFactory = $this->blogManager->getFactory()->getBlogTagListDTOFactory();
            $blogTagListDTOFactory->deleteBlogTag($blogTagListDTO, $currentKey);

            $this->addFlash('success', 'Blog tag was deleted from this form. It was not in database.');
            $this->request->getSession()->set('admin_blog_tag_list/blogTagListDTO', $blogTagListDTO->serialize());

            return $this->redirectToRoute('admin_blog_tag_list');
        }

        return null;
    }
}
