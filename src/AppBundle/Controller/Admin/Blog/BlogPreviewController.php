<?php

namespace AppBundle\Controller\Admin\Blog;

use AppBundle\Entity\Blog\BlogArticle;
use AppBundle\Menu\MainMenu;
use AppBundle\Service\Blog\BlogManager;
use AppBundle\Service\Blog\Command\CreateBlogArticleCmd;
use AppBundle\Service\Blog\Command\FindByBlogFindOptionsCmd;
use AppBundle\Service\Blog\DataMapper\BlogArticleMapper;
use AppBundle\Service\Field\Command\CreateControllerCollectionCmd;
use AppBundle\Service\Field\FieldManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class BlogPreviewController
 */
class BlogPreviewController extends Controller
{
    private $createControllerCollectionCmd;
    private $findByBlogFindOptions;
    private $createBlogArticleCmd;

    private $articleId;
    /** @var Request */
    private $request;
    /** @var FieldManager */
    private $fieldManager;
    /** @var BlogManager */
    private $blogManager;

    /**
     * BlogEditController constructor.
     * @param CreateControllerCollectionCmd $createControllerCollectionCmd
     * @param FindByBlogFindOptionsCmd $findByBlogFindOptionsCmd
     * @param CreateBlogArticleCmd $createBlogArticleCmd
     */
    public function __construct
    (
        CreateControllerCollectionCmd $createControllerCollectionCmd,
        FindByBlogFindOptionsCmd $findByBlogFindOptionsCmd,
        CreateBlogArticleCmd $createBlogArticleCmd
    )
    {
        $this->createControllerCollectionCmd = $createControllerCollectionCmd;
        $this->findByBlogFindOptions = $findByBlogFindOptionsCmd;
        $this->createBlogArticleCmd = $createBlogArticleCmd;
    }

    /**
     * @Route("/admin/blog/article/preview/{articleId}", name="admin_blog_article_preview", requirements={"articleId"="\d+"})
     *
     * @param int $articleId
     * @param Request $request
     * @param FieldManager $fieldManager
     * @param BlogManager $blogManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(int $articleId, Request $request, FieldManager $fieldManager, BlogManager $blogManager)
    {
        $this->articleId = $articleId;
        $this->request = $request;
        $this->fieldManager = $fieldManager;
        $this->blogManager = $blogManager;

        $blogArticle = $this->createBlogArticleById();
        if (is_null($blogArticle)) {
            return $this->redirectToRoute('admin_blog_article_list');
        }

        $blogArticle = $this->createBlogArticleFromSession($blogArticle);

        $this->createControllerFields();
        $menu = new MainMenu('admin_panel');

        return $this->render('default/controller/admin/blog/blog_article_preview.html.twig', array(
            'title' => 'Tomasz Bukowski - Portfolio - Article preview',
            'menu' => $menu->getMenu(),
            'fields' => $fieldManager->getArrayCollection('_controller'),
            'blog_article_dto' => $blogArticle
        ));
    }

    /**
     * @return BlogArticle|null
     */
    private function createBlogArticleById() : ?BlogArticle
    {
        $findOptions = $this->blogManager->getFindOptions();

        $findOptions->clear();
        $findOptions->addWhereCondition('article_id', 'id', 'blog_article');
        $findOptions->addWhereParam('article_id', ':articleId', $this->articleId);

        $this->blogManager->setCommand($this->findByBlogFindOptions);
        $this->blogManager->run();

        $blogArticle = $this->blogManager->getCmdResult('foundedBlogArticles');
        if (key_exists(0, $blogArticle)) {
            $this->createBlogArticleCmd->setBlogArticle($blogArticle[0]);

            $this->blogManager->setCommand($this->createBlogArticleCmd);
            $this->blogManager->run();

            return $blogArticle[0];
        }

        return null;
    }

    /**
     * @param BlogArticle $blogArticle
     * @return BlogArticle
     */
    private function createBlogArticleFromSession(BlogArticle $blogArticle) : BlogArticle
    {
        /** @var BlogArticleMapper $blogArticleMapper */
        $blogArticleMapper = $this->blogManager->getDataMapper(BlogArticleMapper::class);
        $blogArticleDTOFactory = $this->blogManager->getFactory()->getBlogArticleDTOFactory();

        $blogArticleDTO = $blogArticleDTOFactory->createBlogArticle();
        $serializedBlogArticleDTO = $this->request->getSession()->get('admin_blog_article_edit/' . $this->articleId . '/blogArticleDTO');
        if (!is_null($serializedBlogArticleDTO)) {
            $blogArticleDTO->unserialize($serializedBlogArticleDTO);
        }

        $blogArticleMapper->mapDTOToBlogArticle($blogArticle, $blogArticleDTO);
        $blogArticleMapper->mapDTOToBlogTranslations($blogArticle, $blogArticleDTO);

        return $blogArticle;
    }

    private function createControllerFields() : void
    {
        $fieldList = $this->fieldManager->getListOptions();
        $fieldList->clear();
        $fieldList->setControllerName(BlogPreviewController::class);
        $fieldList->setMethodName('indexAction');

        $fieldOptions = $this->fieldManager->getFieldOptions();
        $fieldOptions->setPatterns([
            'blog_preview_btn_edit_article' => [
                '%article_id%' => $this->articleId
            ]
        ]);

        $this->fieldManager->setCommand($this->createControllerCollectionCmd);
        $this->fieldManager->run();
    }
}
