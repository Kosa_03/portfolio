<?php

namespace AppBundle\Controller\Admin\Field;

use AppBundle\Form\Field\AddFieldConfigType;
use AppBundle\Form\Field\AddFieldEntityType;
use AppBundle\Form\Field\AddFieldTemplateType;
use AppBundle\FormDTO\Field\FieldConfigDTO;
use AppBundle\FormDTO\Field\FieldEntityDTO;
use AppBundle\FormDTO\Field\FieldTemplateDTO;
use AppBundle\Menu\MainMenu;
use AppBundle\Service\Field\Command\CreateControllerCollectionCmd;
use AppBundle\Service\Field\Command\InsertFieldCmd;
use AppBundle\Service\Field\Factory\FieldObjectDTOFactory;
use AppBundle\Service\Field\FieldManager;
use AppBundle\Service\Field\FieldObject;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class FieldAddController
 */
class FieldAddController extends Controller
{
    private $createControllerCollectionCmd;
    private $insertFieldCmd;

    /** @var Request */
    private $request;
    /** @var FieldManager */
    private $fieldManager;

    /**
     * FieldAddController constructor.
     * @param CreateControllerCollectionCmd $createControllerCollectionCmd
     * @param InsertFieldCmd $insertFieldCmd
     */
    public function __construct(
        CreateControllerCollectionCmd $createControllerCollectionCmd,
        InsertFieldCmd $insertFieldCmd
    )
    {
        $this->createControllerCollectionCmd = $createControllerCollectionCmd;
        $this->insertFieldCmd = $insertFieldCmd;
    }

    /**
     * @Route("/admin/field/add", name="add_field")
     *
     * @param Request $request
     * @param FieldManager $fieldManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addConfigAction(Request $request, FieldManager $fieldManager)
    {
        $this->request = $request;
        $this->fieldManager = $fieldManager;

        $fieldObjectDTOFactory = $fieldManager->getFactory()->getFieldObjectDTOFactory();
        $fieldConfigDTO = $this->getFieldConfigDTO($fieldObjectDTOFactory);

        $fieldConfigForm = $this->createForm(AddFieldConfigType::class, $fieldConfigDTO);
        $fieldConfigForm->handleRequest($request);

        if ($fieldConfigForm->isSubmitted() && $fieldConfigForm->isValid()) {
            $request->getSession()->set('add_field/form_step', 2);
            $request->getSession()->set('add_field/fieldConfigDTO', $fieldConfigDTO->serialize());
        }

        $formStep = $request->getSession()->get('add_field/form_step');
        if (!is_null($formStep) && $formStep > 1) {
            return $this->forward(FieldAddController::class . '::addTemplateAction');
        }

        $this->createControllerFields();
        $menu = new MainMenu('admin_panel');

        return $this->render('default/controller/admin/field/field_add.html.twig', array(
            'title' => 'Tomasz Bukowski - Portfolio - Creating new field',
            'menu' => $menu->getMenu(),
            'fields' => $fieldManager->getArrayCollection('_controller'),
            'form_field_config' => $fieldConfigForm->createView()
        ));
    }

    /**
     * @param Request $request
     * @param FieldManager $fieldManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addTemplateAction(Request $request, FieldManager $fieldManager)
    {
        $fieldObjectDTOFactory = $fieldManager->getFactory()->getFieldObjectDTOFactory();
        $fieldTemplateDTO = $this->getFieldTemplateDTO($fieldObjectDTOFactory);

        $fieldTemplateForm = $this->createForm(AddFieldTemplateType::class, $fieldTemplateDTO);
        $fieldTemplateForm->handleRequest($request);

        if ($fieldTemplateForm->isSubmitted() && $fieldTemplateForm->isValid()) {
            if ($fieldTemplateForm->get('submit_go_back')->isClicked()) {
                $request->getSession()->set('add_field/form_step', 1);
                $request->getSession()->set('add_field/fieldTemplateDTO', $fieldTemplateDTO->serialize());

                return $this->redirectToRoute('add_field');
            }

            if ($fieldTemplateForm->get('submit_next_step')->isClicked()) {
                $request->getSession()->set('add_field/form_step', 3);
                $request->getSession()->set('add_field/fieldTemplateDTO', $fieldTemplateDTO->serialize());

                return $this->forward(FieldAddController::class . '::addEntitiesAction');
            }
        }

        $formStep = $request->getSession()->get('add_field/form_step');
        if (!is_null($formStep) && $formStep > 2) {
            return $this->forward(FieldAddController::class . '::addEntitiesAction');
        }

        $this->createControllerFields();
        $menu = new MainMenu('admin_panel');

        return $this->render('default/controller/admin/field/field_add.html.twig', array(
            'title' => 'Tomasz Bukowski - Portfolio - Creating new field',
            'menu' => $menu->getMenu(),
            'fields' => $fieldManager->getArrayCollection('_controller'),
            'form_field_template' => $fieldTemplateForm->createView()
        ));
    }

    /**
     * @param Request $request
     * @param FieldManager $fieldManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addEntitiesAction(Request $request, FieldManager $fieldManager)
    {
        $fieldObjectFactory = $fieldManager->getFactory()->getFieldObjectFactory();
        $fieldObjectDTOFactory = $fieldManager->getFactory()->getFieldObjectDTOFactory();
        $fieldObjectMapper = $fieldManager->getMapper();

        $fieldConfigDTO = $this->getFieldConfigDTO($fieldObjectDTOFactory);
        $fieldTemplateDTO = $this->getFieldTemplateDTO($fieldObjectDTOFactory);
        $fieldEntityDTO = $this->getFieldEntityDTO($fieldObjectDTOFactory, $fieldConfigDTO);

        $formFieldEntities = $this->createForm(AddFieldEntityType::class, $fieldEntityDTO, array(
            'field_class' => $fieldConfigDTO->getFieldClass()
        ));
        $formFieldEntities->handleRequest($request);

        if ($formFieldEntities->isSubmitted() && $formFieldEntities->isValid()) {
            if ($formFieldEntities->get('submit_go_back')->isClicked()) {
                $request->getSession()->set('add_field/form_step', 2);
                $request->getSession()->set('add_field/fieldEntityDTO', $fieldEntityDTO->serialize());

                return $this->redirectToRoute('add_field');
            }

            if ($formFieldEntities->get('submit_create')->isClicked()) {
                $request->getSession()->set('add_field/fieldEntityDTO', $fieldEntityDTO->serialize());

                $fieldObject = $fieldObjectFactory->createEmptyFieldObject();
                $fieldObjectMapper->mapDTOToFieldConfig($fieldObject, $fieldConfigDTO);
                $fieldObjectMapper->mapDTOToFieldTemplate($fieldObject, $fieldTemplateDTO);
                $fieldObjectMapper->mapDTOToFieldEntities($fieldObject, $fieldEntityDTO);

                $this->insertFieldObjectIntoDataBase($fieldObject);

                $insertError = $fieldManager->getCmdResult('insertError');

                if (isset($insertError) && $insertError == false) {
                    $request->getSession()->remove('add_field/form_step');
                    $request->getSession()->remove('add_field/fieldConfigDTO');
                    $request->getSession()->remove('add_field/fieldTemplateDTO');
                    $request->getSession()->remove('add_field/fieldEntityDTO');

                    $this->addFlash('success',
                        'Field was insert correctly! Added field with id '
                        . $fieldObject->getConfig()->getId() . '.'
                    );

                    return $this->redirectToRoute('list_fields');

                } else {
                    $this->addFlash('danger',
                        'Something went wrong during insert new field into database! No field added.'
                    );
                }

                return $this->redirectToRoute('add_field');
            }
        }

        $this->createControllerFields();
        $menu = new MainMenu('admin_panel');

        return $this->render('default/controller/admin/field/field_add.html.twig', array(
            'title' => 'Tomasz Bukowski - Portfolio - Creating new field',
            'menu' => $menu->getMenu(),
            'fields' => $fieldManager->getArrayCollection('_controller'),
            'entity_template_name' => $this->generateEntityTemplateName($fieldConfigDTO->getFieldClass()),
            'form_field_entities' => $formFieldEntities->createView()
        ));
    }

    private function createControllerFields() : void
    {
        $step = $this->request->getSession()->get('add_field/form_step');

        $fieldList = $this->fieldManager->getListOptions();
        $fieldList->setControllerName(FieldAddController::class);
        $fieldList->setMethodName('addConfigAction');

        $fieldOptions = $this->fieldManager->getFieldOptions();
        $fieldOptions->setPatterns([
            'add_field_header' => [
                '%step%' => is_null($step) ? 1 : $step
            ]
        ]);

        $this->fieldManager->setCommand($this->createControllerCollectionCmd);
        $this->fieldManager->run();
    }

    /**
     * @param FieldObjectDTOFactory $fieldObjectDTOFactory
     * @return FieldConfigDTO
     */
    private function getFieldConfigDTO(FieldObjectDTOFactory $fieldObjectDTOFactory) : FieldConfigDTO
    {
        $fieldConfigDTO = $fieldObjectDTOFactory->createFieldConfig();

        $serializedFieldConfigDTO = $this->request->getSession()->get('add_field/fieldConfigDTO');
        if (!is_null($serializedFieldConfigDTO)) {
            $fieldConfigDTO->unserialize($serializedFieldConfigDTO);
        }

        return $fieldConfigDTO;
    }

    /**
     * @param FieldObjectDTOFactory $fieldObjectDTOFactory
     * @return FieldTemplateDTO
     */
    private function getFieldTemplateDTO(FieldObjectDTOFactory $fieldObjectDTOFactory) : FieldTemplateDTO
    {
        $fieldTemplateDTO = $fieldObjectDTOFactory->createFieldTemplate();

        $serializedFieldTemplateDTO = $this->request->getSession()->get('add_field/fieldTemplateDTO');
        if (!is_null($serializedFieldTemplateDTO)) {
            $fieldTemplateDTO->unserialize($serializedFieldTemplateDTO);
        }

        return $fieldTemplateDTO;
    }

    /**
     * @param FieldObjectDTOFactory $fieldObjectDTOFactory
     * @param FieldConfigDTO $fieldConfigDTO
     * @return FieldEntityDTO
     */
    private function getFieldEntityDTO(FieldObjectDTOFactory $fieldObjectDTOFactory, FieldConfigDTO $fieldConfigDTO) : FieldEntityDTO
    {
        $oldFieldEntityDTO = $fieldObjectDTOFactory->createFieldEntity();
        $serializedFieldEntityDTO = $this->request->getSession()->get('add_field/fieldEntityDTO');
        if (!is_null($serializedFieldEntityDTO)) {
            $oldFieldEntityDTO->unserialize($serializedFieldEntityDTO);
        }

        $newFieldEntityDTO = $fieldObjectDTOFactory->createFieldEntity();
        for ($i = 0; $i < $fieldConfigDTO->getAmountEntities(); $i++) {
            $fieldObjectDTOFactory->addEmptyEntity($newFieldEntityDTO, $fieldConfigDTO->getFieldClass());
        }

        if ($oldFieldEntityDTO->getEntities()->isEmpty()) {
            return $newFieldEntityDTO;
        }

        $oldFieldClass = get_class($oldFieldEntityDTO->getEntities()->first());
        $newFieldClass = get_class($newFieldEntityDTO->getEntities()->first());
        $oldAmountEntities = $oldFieldEntityDTO->getEntities()->count();
        $newAmountEntities = $newFieldEntityDTO->getEntities()->count();

        if (($oldFieldClass == $newFieldClass) && ($oldAmountEntities == $newAmountEntities)) {
            return $oldFieldEntityDTO;
        } else {
            return $newFieldEntityDTO;
        }
    }

    /**
     * @param FieldObject $fieldObject
     */
    private function insertFieldObjectIntoDataBase(FieldObject $fieldObject) : void
    {
        $this->insertFieldCmd->setFieldObject($fieldObject);

        $this->fieldManager->setCommand($this->insertFieldCmd);
        $this->fieldManager->run();
    }

    /**
     * @param string $fieldClass
     * @return string
     */
    private function generateEntityTemplateName(string $fieldClass) : string
    {
        $entityTemplateName = explode('\\', $fieldClass);
        $entityTemplateName = end($entityTemplateName);
        $entityTemplateName = preg_replace('/([A-Z])/', '_$1', $entityTemplateName);
        $entityTemplateName = substr(strtolower($entityTemplateName), 1);

        return $entityTemplateName;
    }
}
