<?php

namespace AppBundle\Controller\Admin\Field;

use AppBundle\Form\Field\EditFieldConfigType;
use AppBundle\Form\Field\EditFieldEntityType;
use AppBundle\Form\Field\EditFieldTemplateType;
use AppBundle\FormDTO\Field\FieldEntityDTO;
use AppBundle\Menu\MainMenu;
use AppBundle\Service\Field\Command\CreateControllerCollectionCmd;
use AppBundle\Service\Field\Command\CreateFieldCmd;
use AppBundle\Service\Field\Command\FindByFieldListOptionsCmd;
use AppBundle\Service\Field\Command\UpdateFieldCmd;
use AppBundle\Service\Field\Factory\FieldObjectDTOFactory;
use AppBundle\Service\Field\FieldManager;
use AppBundle\Service\Field\FieldObject;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class FieldEditController
 */
class FieldEditController extends Controller
{
    private $createControllerCollectionCmd;
    private $findByFieldListOptionsCmd;
    private $createFieldCmd;
    private $updateFieldCmd;

    private $configId;
    /** @var Request */
    private $request;
    /** @var FieldManager */
    private $fieldManager;


    /**
     * FieldEditController constructor.
     * @param CreateControllerCollectionCmd $createControllerCollectionCmd
     * @param FindByFieldListOptionsCmd $findByFieldListOptionsCmd
     * @param CreateFieldCmd $createFieldCmd
     * @param UpdateFieldCmd $updateFieldCmd
     */
    public function __construct
    (
        CreateControllerCollectionCmd $createControllerCollectionCmd,
        FindByFieldListOptionsCmd $findByFieldListOptionsCmd,
        CreateFieldCmd $createFieldCmd,
        UpdateFieldCmd $updateFieldCmd
    )
    {
        $this->createControllerCollectionCmd = $createControllerCollectionCmd;
        $this->findByFieldListOptionsCmd = $findByFieldListOptionsCmd;
        $this->createFieldCmd = $createFieldCmd;
        $this->updateFieldCmd = $updateFieldCmd;
    }

    /**
     * @Route("/admin/field/edit/{configId}", name="edit_field", requirements={"configId"="\d+"})
     *
     * @param int $configId
     * @param Request $request
     * @param FieldManager $fieldManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editFieldAction(int $configId, Request $request, FieldManager $fieldManager)
    {
        $this->configId = $configId;
        $this->request = $request;
        $this->fieldManager = $fieldManager;
        $fieldObjectDTOFactory = $fieldManager->getFactory()->getFieldObjectDTOFactory();
        $fieldObjectMapper = $fieldManager->getMapper();

        $fieldObject = $this->createFieldObjectByConfigId();
        if (is_null($fieldObject)) {
            return $this->redirectToRoute('field_not_found');
        }

        $fieldConfigDTO = $fieldObjectDTOFactory->createFieldConfig();
        $fieldObjectMapper->mapFieldConfigToDTO($fieldObject, $fieldConfigDTO);

        $formFieldConfig = $this->createForm(EditFieldConfigType::class, $fieldConfigDTO);
        $formFieldConfig->handleRequest($request);

        if ($formFieldConfig->isSubmitted() && $formFieldConfig->isValid()) {
            $fieldObjectMapper->mapDTOToFieldConfig($fieldObject, $fieldConfigDTO);
            $this->updateFieldConfig($fieldObject);

            return $this->redirectToRoute('edit_field', ['configId' => $configId]);
        }

        $fieldTemplateDTO = $fieldObjectDTOFactory->createFieldTemplate();
        $fieldObjectMapper->mapFieldTemplateToDTO($fieldObject, $fieldTemplateDTO);

        $formFieldTemplate = $this->createForm(EditFieldTemplateType::class, $fieldTemplateDTO);
        $formFieldTemplate->handleRequest($request);

        if ($formFieldTemplate->isSubmitted() && $formFieldTemplate->isValid()) {
            $fieldObjectMapper->mapDTOToFieldTemplate($fieldObject, $fieldTemplateDTO);
            $this->updateFieldTemplate($fieldObject);

            return $this->redirectToRoute('edit_field', ['configId' => $configId]);
        }

        $fieldEntityDTO = $this->getFieldEntityDTO($fieldObjectDTOFactory, $request, $configId);
        if ($fieldEntityDTO->getEntities()->isEmpty()) {
            $fieldObjectMapper->mapFieldEntitiesToDTO($fieldObject, $fieldEntityDTO);
        }

        $formFieldEntity = $this->createForm(EditFieldEntityType::class, $fieldEntityDTO, [
            'field_class' => $fieldObject->getFieldClass()
        ]);
        $formFieldEntity->handleRequest($request);

        if ($formFieldEntity->isSubmitted() && $formFieldEntity->isValid()) {
            $redirectResponse = $this->checkAddEntityButton($formFieldEntity, $fieldEntityDTO, $fieldObject);
            if (!is_null($redirectResponse)) {
                return $redirectResponse;
            }

            $redirectResponse = $this->checkUpdateEntitiesButton($formFieldEntity, $fieldEntityDTO, $fieldObject);
            if (!is_null($redirectResponse)) {
                return $redirectResponse;
            }

            $redirectResponse = $this->checkDeleteButtons($formFieldEntity, $fieldEntityDTO);
            if (!is_null($redirectResponse)) {
                return $redirectResponse;
            }
        }

        $this->createControllerFields();
        $menu = new MainMenu('admin_panel');

        return $this->render('default/controller/admin/field/field_edit.html.twig', array(
            'title' => 'Tomasz Bukowski - Portfolio - Edit field',
            'menu' => $menu->getMenu(),
            'fields' => $fieldManager->getArrayCollection('_controller'),
            'entity_template_name' => $this->generateEntityTemplateName($fieldObject->getFieldClass()),
            'form_field_config' => $formFieldConfig->createView(),
            'form_field_template' => $formFieldTemplate->createView(),
            'form_field_entities' => $formFieldEntity->createView()
        ));
    }

    /**
     * @return FieldObject|null
     */
    private function createFieldObjectByConfigId() : ?FieldObject
    {
        $fieldList = $this->fieldManager->getListOptions();
        $fieldList->setConfigId($this->configId);

        $this->fieldManager->setCommand($this->findByFieldListOptionsCmd);
        $this->fieldManager->run();

        $fieldConfigList = $this->fieldManager->getCmdResult('fieldConfigList');
        if (key_exists(0, $fieldConfigList)) {
            $this->createFieldCmd->setFieldConfig($fieldConfigList[0]);

            $this->fieldManager->setCommand($this->createFieldCmd);
            $this->fieldManager->run();
        }

        return $this->fieldManager->getCmdResult('fieldObject');
    }

    private function createControllerFields() : void
    {
        $fieldList = $this->fieldManager->getListOptions();
        $fieldList->clear();
        $fieldList->setControllerName(FieldEditController::class);
        $fieldList->setMethodName('editFieldAction');

        /** @var FieldObject $fieldObject */
        $fieldObject = $this->fieldManager->getCmdResult('fieldObject');

        $fieldOptions = $this->fieldManager->getFieldOptions();
        $fieldOptions->setPatterns([
            'edit_field_header' => [
                '%field_name%' => $fieldObject->getFieldName()
            ],
            'edit_field_delete_btn' => [
                '%config_id%' => $fieldObject->getConfig()->getId()
            ]
        ]);

        $this->fieldManager->setCommand($this->createControllerCollectionCmd);
        $this->fieldManager->run();
    }

    /**
     * @param FieldObjectDTOFactory $fieldObjectDTOFactory
     * @param Request $request
     * @param int $configId
     * @return FieldEntityDTO
     */
    private function getFieldEntityDTO(FieldObjectDTOFactory $fieldObjectDTOFactory, Request $request, int $configId): FieldEntityDTO
    {
        $fieldEntityDTO = $fieldObjectDTOFactory->createFieldEntity();

        $serializedFieldEntity = $request->getSession()->get('edit_field/' . $configId . '/fieldEntityDTO');
        if (!is_null($serializedFieldEntity)) {
            $fieldEntityDTO->unserialize($serializedFieldEntity);
        }

        return $fieldEntityDTO;
    }

    /**
     * @param FormInterface $formFieldEntity
     * @param FieldEntityDTO $fieldEntityDTO
     * @param FieldObject $fieldObject
     * @return null|RedirectResponse
     */
    private function checkAddEntityButton(FormInterface $formFieldEntity, FieldEntityDTO $fieldEntityDTO, FieldObject $fieldObject): ?RedirectResponse
    {
        $fieldObjectDTOFactory = $this->fieldManager->getFactory()->getFieldObjectDTOFactory();

        if ($formFieldEntity->get('add_entity')->isClicked()) {
            $addedEntityResult = $fieldObjectDTOFactory->addEmptyEntity($fieldEntityDTO, $fieldObject->getFieldClass());

            if ($addedEntityResult) {
                $this->request->getSession()->set(
                    'edit_field/' . $this->configId . '/fieldEntityDTO',
                    $fieldEntityDTO->serialize())
                ;
                $this->addFlash('success', 'Added new empty entity.');

            } else {
                $this->addFlash('warning', 'You should not add more entities to this field type.');
            }

            return $this->redirectToRoute('edit_field', ['configId' => $this->configId]);
        }

        return null;
    }

    /**
     * @param FormInterface $formFieldEntity
     * @param FieldEntityDTO $fieldEntityDTO
     * @param FieldObject $fieldObject
     * @return null|RedirectResponse
     */
    private function checkUpdateEntitiesButton(FormInterface $formFieldEntity, FieldEntityDTO $fieldEntityDTO, FieldObject $fieldObject): ?RedirectResponse
    {
        $fieldObjectMapper = $this->fieldManager->getMapper();

        if ($formFieldEntity->get('update_entities')->isClicked()) {
            $fieldObjectMapper->mapDTOToFieldEntities($fieldObject, $fieldEntityDTO);

            $this->updateFieldObject($fieldObject);

            $updateError = $this->fieldManager->getCmdResult('updateError');
            if (isset($updateError) && $updateError == false) {
                $this->request->getSession()->remove('edit_field/' . $this->configId . '/fieldEntityDTO');
            }

            return $this->redirectToRoute('edit_field', ['configId' => $this->configId]);
        }

        return null;
    }

    /**
     * @param FormInterface $formFieldEntity
     * @param FieldEntityDTO $fieldEntityDTO
     * @return null|RedirectResponse
     */
    private function checkDeleteButtons(FormInterface $formFieldEntity, FieldEntityDTO $fieldEntityDTO): ?RedirectResponse
    {
        $entitiesDTO = $fieldEntityDTO->getEntities();
        $entitiesDTO->first();

        foreach ($formFieldEntity->get('entities') as $singleFormFieldEntity) {
            if ($singleFormFieldEntity->get('delete_entity')->isClicked()) {
                $redirectResponse = $this->checkIsSingleEntity($formFieldEntity->get('entities'));
                if (!is_null($redirectResponse)) {
                    return $redirectResponse;
                }

                $dataId = $entitiesDTO->current()->getDataId();
                $entityKey = $entitiesDTO->key();

                $redirectResponse = $this->checkDataId($fieldEntityDTO, $dataId, $entityKey);
                if (!is_null($redirectResponse)) {
                    return $redirectResponse;
                }
            }

            $entitiesDTO->next();
        }

        return null;
    }

    /**
     * @param FormInterface $formFieldEntity
     * @return null|RedirectResponse
     */
    private function checkIsSingleEntity(FormInterface $formFieldEntity): ?RedirectResponse
    {
        if ($formFieldEntity->count() <= 1) {
            $this->addFlash('warning',
                'You should not delete this entity, because this field contains only one entity.'
            );

            return $this->redirectToRoute('edit_field', [
                'configId' => $this->configId
            ]);
        }

        return null;
    }

    /**
     * @param FieldEntityDTO $fieldEntityDTO
     * @param int|null $dataId
     * @param $entityKey
     * @return RedirectResponse
     */
    private function checkDataId(FieldEntityDTO $fieldEntityDTO, ?int $dataId, $entityKey): RedirectResponse
    {
        $fieldObjectDTOFactory = $this->fieldManager->getFactory()->getFieldObjectDTOFactory();

        if (!is_null($dataId)) {
            $this->request->getSession()->set(
                'edit_field/' . $this->configId . '/fieldEntityDTO',
                $fieldEntityDTO->serialize()
            );

            return $this->redirectToRoute('delete_entity', [
                'configId' => $this->configId,
                'dataId' => $dataId
            ]);
        } else {
            $fieldObjectDTOFactory->deleteEntity($fieldEntityDTO, $entityKey);
            $this->request->getSession()->set(
                'edit_field/' . $this->configId . '/fieldEntityDTO',
                $fieldEntityDTO->serialize()
            );

            $this->addFlash('success',
                'Entity was deleted from this form. It was not in database.'
            );

            return $this->redirectToRoute('edit_field', [
                'configId' => $this->configId
            ]);
        }
    }

    /**
     * @param FieldObject $fieldObject
     */
    private function updateFieldConfig(FieldObject $fieldObject) : void
    {
        $this->updateIntoDataBase($fieldObject);

        $updateError = $this->fieldManager->getCmdResult('updateError');
        if (isset($updateError) && $updateError == false) {
            $this->addFlash('success',
                'FieldConfig with id ' . $fieldObject->getConfig()->getId() . ' was successfully updated!'
            );
        } else {
            $this->addFlash('danger',
                'FieldConfig with id ' . $fieldObject->getConfig()->getId() . ' was not updated!'
            );
        }
    }

    /**
     * @param FieldObject $fieldObject
     */
    private function updateFieldTemplate(FieldObject $fieldObject) : void
    {
        $this->updateIntoDataBase($fieldObject);

        $updateError = $this->fieldManager->getCmdResult('updateError');
        if (isset($updateError) && $updateError == false) {
            $this->addFlash('success',
                'FieldTemplate with id ' . $fieldObject->getOrigTemplate()->getId() . ' was successfully updated!'
            );
        } else {
            $this->addFlash('danger',
                'FieldTemplate with id ' . $fieldObject->getOrigTemplate()->getId() . ' was not updated!'
            );
        }
    }

    /**
     * @param FieldObject $fieldObject
     */
    private function updateFieldObject(FieldObject $fieldObject) : void
    {
        $this->updateIntoDataBase($fieldObject);

        $updateError = $this->fieldManager->getCmdResult('updateError');
        if (isset($updateError) && $updateError == false) {
            $this->addFlash('success', 'Field Entities were successfully updated!');
        } else {
            $this->addFlash('danger', 'Field Entities were not updated!');
        }
    }

    /**
     * @param FieldObject $fieldObject
     */
    private function updateIntoDataBase(FieldObject $fieldObject) : void
    {
        $this->updateFieldCmd->setFieldObject($fieldObject);

        $this->fieldManager->setCommand($this->updateFieldCmd);
        $this->fieldManager->run();
    }

    /**
     * @param string $fieldClass
     * @return string
     */
    private function generateEntityTemplateName(string $fieldClass) : string
    {
        $entityTemplateName = explode('\\', $fieldClass);
        $entityTemplateName = end($entityTemplateName);
        $entityTemplateName = preg_replace('/([A-Z])/', '_$1', $entityTemplateName);
        $entityTemplateName = substr(strtolower($entityTemplateName), 1);

        return $entityTemplateName;
    }
}
