<?php

namespace AppBundle\Controller\Admin\Field;

use AppBundle\Menu\MainMenu;
use AppBundle\Service\Field\Command\CreateControllerCollectionCmd;
use AppBundle\Service\Field\Command\CreateFieldCmd;
use AppBundle\Service\Field\Command\FindByFieldListOptionsCmd;
use AppBundle\Service\Field\FieldObject;
use AppBundle\Service\Field\FieldManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class FieldShowController
 */
class FieldShowController extends Controller
{
    private $createControllerCollectionCmd;
    private $findByFieldListOptionsCmd;
    private $createFieldCmd;

    private $configId;
    /** @var Request */
    private $request;
    /** @var FieldManager */
    private $fieldManager;


    /**
     * FieldShowController constructor.
     * @param CreateControllerCollectionCmd $createControllerCollectionCmd
     * @param FindByFieldListOptionsCmd $findByFieldListOptionsCmd
     * @param CreateFieldCmd $createFieldCmd
     */
    public function __construct
    (
        CreateControllerCollectionCmd $createControllerCollectionCmd,
        FindByFieldListOptionsCmd $findByFieldListOptionsCmd,
        CreateFieldCmd $createFieldCmd
    )
    {
        $this->createControllerCollectionCmd = $createControllerCollectionCmd;
        $this->findByFieldListOptionsCmd = $findByFieldListOptionsCmd;
        $this->createFieldCmd = $createFieldCmd;
    }

    /**
     * @Route("/admin/field/show/{configId}", name="show_field", requirements={"id"="\d+"})
     *
     * @param int $configId
     * @param Request $request
     * @param FieldManager $fieldManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showFieldAction(int $configId, Request $request, FieldManager $fieldManager)
    {
        $this->configId = $configId;
        $this->request = $request;
        $this->fieldManager = $fieldManager;

        $fieldObject = $this->createFieldObjectByConfigId();
        if (is_null($fieldObject)) {
            return $this->redirectToRoute('field_not_found');
        }

        $this->createControllerFields();
        $menu = new MainMenu('admin_panel');

        return $this->render('default/controller/admin/field/field_show.html.twig', array(
            'title' => 'Tomasz Bukowski - Portfolio - ',
            'menu' => $menu->getMenu(),
            'fields' => $fieldManager->getArrayCollection('_controller'),
            'described_field' => $fieldObject
        ));
    }

    /**
     * @return FieldObject|null
     */
    private function createFieldObjectByConfigId() : ?FieldObject
    {
        $fieldList = $this->fieldManager->getListOptions();
        $fieldList->setConfigId($this->configId);

        $this->fieldManager->setCommand($this->findByFieldListOptionsCmd);
        $this->fieldManager->run();

        $fieldConfigList = $this->fieldManager->getCmdResult('fieldConfigList');
        if (key_exists(0, $fieldConfigList)) {
            $this->createFieldCmd->setFieldConfig($fieldConfigList[0]);

            $this->fieldManager->setCommand($this->createFieldCmd);
            $this->fieldManager->run();
        }

        return $this->fieldManager->getCmdResult('fieldObject');
    }

    private function createControllerFields() : void
    {
        $fieldList = $this->fieldManager->getListOptions();
        $fieldList->clear();
        $fieldList->setControllerName(FieldShowController::class);
        $fieldList->setMethodName('showFieldAction');

        $fieldObject = $this->fieldManager->getCmdResult('fieldObject');

        $fieldOptions = $this->fieldManager->getFieldOptions();
        $fieldOptions->setPatterns($this->textPatterns($fieldObject));

        $this->fieldManager->setCommand($this->createControllerCollectionCmd);
        $this->fieldManager->run();
    }

    /**
     * @param FieldObject $fieldObject
     * @return array
     */
    private function textPatterns(FieldObject $fieldObject) : array
    {
        $patterns = [
            'show_field_header' => [
                '%field_name%' => $fieldObject->getFieldName()
            ],
            'show_field_edit_btn' => [
                '%config_id%' => $fieldObject->getConfig()->getId()
            ],
            'show_field_delete_btn' => [
                '%config_id%' => $fieldObject->getConfig()->getId()
            ]
        ];

        return $patterns;
    }
}
