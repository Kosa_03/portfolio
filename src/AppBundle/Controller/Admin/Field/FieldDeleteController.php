<?php

namespace AppBundle\Controller\Admin\Field;

use AppBundle\Form\Field\DeleteFieldType;
use AppBundle\Menu\MainMenu;
use AppBundle\Service\Field\Command\CreateControllerCollectionCmd;
use AppBundle\Service\Field\Command\CreateFieldCmd;
use AppBundle\Service\Field\Command\DeleteFieldCmd;
use AppBundle\Service\Field\Command\DeleteFieldEntityCmd;
use AppBundle\Service\Field\Command\FindByFieldListOptionsCmd;
use AppBundle\Service\Field\FieldManager;
use AppBundle\Service\Field\FieldObject;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class FieldDeleteController
 */
class FieldDeleteController extends Controller
{
    private $createControllerCollectionCmd;
    private $findByFieldListOptionsCmd;
    private $createFieldCmd;
    private $deleteFieldCmd;
    private $deleteFieldEntityCmd;

    private $configId;
    /** @var Request */
    private $request;
    /** @var FieldManager */
    private $fieldManager;

    /**
     * FieldDeleteController constructor.
     * @param CreateControllerCollectionCmd $createControllerCollectionCmd
     * @param FindByFieldListOptionsCmd $findByFieldListOptionsCmd
     * @param CreateFieldCmd $createFieldCmd
     * @param DeleteFieldCmd $deleteFieldCmd
     * @param DeleteFieldEntityCmd $deleteFieldEntityCmd
     */
    public function __construct
    (
        CreateControllerCollectionCmd $createControllerCollectionCmd,
        FindByFieldListOptionsCmd $findByFieldListOptionsCmd,
        CreateFieldCmd $createFieldCmd,
        DeleteFieldCmd $deleteFieldCmd,
        DeleteFieldEntityCmd $deleteFieldEntityCmd
    )
    {
        $this->createControllerCollectionCmd = $createControllerCollectionCmd;
        $this->findByFieldListOptionsCmd = $findByFieldListOptionsCmd;
        $this->createFieldCmd = $createFieldCmd;
        $this->deleteFieldCmd = $deleteFieldCmd;
        $this->deleteFieldEntityCmd = $deleteFieldEntityCmd;
    }

    /**
     * @Route("/admin/field/delete/{configId}", name="delete_field", requirements={"configId"="\d+"})
     *
     * @param int $configId
     * @param Request $request
     * @param FieldManager $fieldManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteFieldAction(int $configId, Request $request, FieldManager $fieldManager)
    {
        $this->configId = $configId;
        $this->request = $request;
        $this->fieldManager = $fieldManager;

        $fieldObject = $this->createFieldObjectByConfigId();
        if (is_null($fieldObject)) {
            return $this->redirectToRoute('field_not_found');
        }

        $formConfirmDelete = $this->createForm(DeleteFieldType::class);
        $formConfirmDelete->handleRequest($request);

        if ($formConfirmDelete->isSubmitted()) {
            if ($formConfirmDelete->get('confirm_yes')->isClicked()) {
                $this->deleteFieldFromDataBase($fieldObject);

                $deleteError = $fieldManager->getContainer()->getCommandResult('deleteError');

                if (isset($deleteError) && $deleteError == false) {
                    $this->addFlash('success', 'Field was deleted correctly.');
                } else {
                    $this->addFlash('danger', 'Field was not deleted. Something went wrong.');
                }
            }

            if ($formConfirmDelete->get('confirm_no')->isClicked()) {
                $this->addFlash('warning', 'No field was deleted.');
            }

            return $this->redirectToRoute('list_fields');
        }

        $this->createControllerFields('deleteFieldAction');
        $menu = new MainMenu('admin_panel');

        return $this->render('default/controller/admin/field/field_delete.html.twig', array(
            'title' => 'Tomasz Bukowski - Portfolio - Delete field',
            'menu' => $menu->getMenu(),
            'fields' => $fieldManager->getArrayCollection('_controller'),
            'form_confirm_delete_field' => $formConfirmDelete->createView()
        ));
    }

    /**
     * @Route("/admin/field/delete/{configId}/entity/{dataId}", name="delete_entity",
     *     requirements={"configId"="\d+", "dataId"="\d+"})
     *
     * @param int $configId
     * @param int $dataId
     * @param Request $request
     * @param FieldManager $fieldManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteFieldEntityAction(int $configId, int $dataId, Request $request, FieldManager $fieldManager)
    {
        $this->configId = $configId;
        $this->request = $request;
        $this->fieldManager = $fieldManager;

        $fieldObject = $this->createFieldObjectByConfigId();
        if (is_null($fieldObject)) {
            return $this->redirectToRoute('field_not_found');
        }

        $formConfirmDelete = $this->createForm(DeleteFieldType::class);
        $formConfirmDelete->handleRequest($request);

        if ($formConfirmDelete->isSubmitted()) {
            if ($formConfirmDelete->get('confirm_yes')->isClicked()) {
                $this->deleteFieldEntityFromDataBase($fieldObject, $dataId);

                $deleteError = $fieldManager->getCmdResult('deleteError');

                if (isset($deleteError) && $deleteError == false) {
                    $this->addFlash('success', 'Entity was deleted correctly.');
                } else {
                    $this->addFlash('danger', 'Entity was not deleted. Something went wrong.');
                }
            }

            if ($formConfirmDelete->get('confirm_no')->isClicked()) {
                $this->addFlash('warning', 'No entity was deleted.');
            }

            return $this->redirectToRoute('edit_field', ['configId' => $configId]);
        }

        $this->createControllerFields('deleteEntityAction');
        $menu = new MainMenu('admin_panel');

        return $this->render('default/controller/admin/field/field_delete.html.twig', array(
            'title' => 'Tomasz Bukowski - Portfolio - Delete entity field',
            'menu' => $menu->getMenu(),
            'fields' => $fieldManager->getArrayCollection('_controller'),
            'form_confirm_delete_field' => $formConfirmDelete->createView()
        ));
    }

    /**
     * @return FieldObject|null
     */
    private function createFieldObjectByConfigId() : ?FieldObject
    {
        $fieldList = $this->fieldManager->getListOptions();
        $fieldList->setConfigId($this->configId);

        $this->fieldManager->setCommand($this->findByFieldListOptionsCmd);
        $this->fieldManager->run();

        $fieldConfigList = $this->fieldManager->getCmdResult('fieldConfigList');
        if (key_exists(0, $fieldConfigList)) {
            $this->createFieldCmd->setFieldConfig($fieldConfigList[0]);

            $this->fieldManager->setCommand($this->createFieldCmd);
            $this->fieldManager->run();
        }

        return $this->fieldManager->getCmdResult('fieldObject');
    }

    /**
     * @param string $actionMethod
     */
    private function createControllerFields(string $actionMethod) : void
    {
        $fieldList = $this->fieldManager->getListOptions();
        $fieldList->clear();
        $fieldList->setControllerName(FieldDeleteController::class);
        $fieldList->setMethodName($actionMethod);

        $fieldObject = $this->fieldManager->getCmdResult('fieldObject');

        $fieldOptions = $this->fieldManager->getFieldOptions();
        if ($actionMethod == 'deleteFieldAction') {
            $fieldOptions->setPatterns([
                'delete_field_header' => [
                    '%field_name%' => $fieldObject->getFieldName()
                ]
            ]);
        }

        $this->fieldManager->setCommand($this->createControllerCollectionCmd);
        $this->fieldManager->run();
    }

    /**
     * @param FieldObject $fieldObject
     */
    private function deleteFieldFromDataBase(FieldObject $fieldObject) : void
    {
        $this->deleteFieldCmd->setFieldObject($fieldObject);

        $this->fieldManager->setCommand($this->deleteFieldCmd);
        $this->fieldManager->run();
    }

    /**
     * @param FieldObject $fieldObject
     * @param int $entityId
     */
    private function deleteFieldEntityFromDataBase(FieldObject $fieldObject, int $entityId) : void
    {
        $this->deleteFieldEntityCmd->setFieldEntity($fieldObject, $entityId);

        $this->fieldManager->setCommand($this->deleteFieldEntityCmd);
        $this->fieldManager->run();
    }
}
