<?php

namespace AppBundle\Controller\Admin\Field;

use AppBundle\Menu\MainMenu;
use AppBundle\Service\Field\Command\CreateControllerCollectionCmd;
use AppBundle\Service\Field\FieldManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class FieldNotFoundController
 */
class FieldNotFoundController extends Controller
{
    private $createControllerCollectionCmd;

    /** @var Request */
    private $request;
    /** @var FieldManager */
    private $fieldManager;

    /**
     * FieldListAllController constructor.
     * @param CreateControllerCollectionCmd $createControllerCollectionCmd
     */
    public function __construct(CreateControllerCollectionCmd $createControllerCollectionCmd)
    {
        $this->createControllerCollectionCmd = $createControllerCollectionCmd;
    }

    /**
     * @Route("/admin/field/not_found", name="field_not_found")
     *
     * @param Request $request
     * @param FieldManager $fieldManager
     */
    public function indexAction(Request $request, FieldManager $fieldManager)
    {
        $this->request = $request;
        $this->fieldManager = $fieldManager;

        $this->createControllerFields();
        $menu = new MainMenu('admin_panel');

        return $this->render('default/controller/admin/field/field_not_found.html.twig', array(
            'title' => 'Tomasz Bukowski - Portfolio - Field not found',
            'menu' => $menu->getMenu(),
            'fields' => $fieldManager->getArrayCollection('_controller')
        ));
    }

    private function createControllerFields() : void
    {
        $fieldList = $this->fieldManager->getListOptions();
        $fieldList->setControllerName(FieldNotFoundController::class);
        $fieldList->setMethodName('indexAction');

        $this->fieldManager->setCommand($this->createControllerCollectionCmd);
        $this->fieldManager->run();
    }
}
