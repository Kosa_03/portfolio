<?php

namespace AppBundle\Controller\Admin\Field;

use AppBundle\Component\Pagination\Paginator;
use AppBundle\Form\Field\ListFieldType;
use AppBundle\FormDTO\Field\SearchFormDTO;
use AppBundle\Menu\MainMenu;
use AppBundle\Service\Field\Command\CountByFieldListOptionsCmd;
use AppBundle\Service\Field\Command\CreateControllerCollectionCmd;
use AppBundle\Service\Field\Command\CreateFieldCollectionCmd;
use AppBundle\Service\Field\FieldManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class FieldListController
 */
class FieldListController extends Controller
{
    private $createControllerCollectionCmd;
    private $createFieldCollectionCmd;
    private $countByFieldListOptionsCmd;

    /** @var Request */
    private $request;
    /** @var FieldManager */
    private $fieldManager;

    /**
     * FieldListAllController constructor.
     * @param CreateControllerCollectionCmd $createControllerCollectionCmd
     * @param CreateFieldCollectionCmd $createFieldCollectionCmd
     * @param CountByFieldListOptionsCmd $countByFieldListOptionsCmd
     */
    public function __construct
    (
        CreateControllerCollectionCmd $createControllerCollectionCmd,
        CreateFieldCollectionCmd $createFieldCollectionCmd,
        CountByFieldListOptionsCmd $countByFieldListOptionsCmd
    )
    {
        $this->createControllerCollectionCmd = $createControllerCollectionCmd;
        $this->createFieldCollectionCmd = $createFieldCollectionCmd;
        $this->countByFieldListOptionsCmd = $countByFieldListOptionsCmd;
    }

    /**
     * @Route("/admin/field/list/{page}", name="list_fields", requirements={"page"="\d+"})
     *
     * @param int $page
     * @param Request $request
     * @param FieldManager $fieldManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(int $page = 1, Request $request, FieldManager $fieldManager)
    {
        $this->request = $request;
        $this->fieldManager = $fieldManager;

        $searchFormDTO = $this->getSearchFormDTO();
        $searchForm = $this->createForm(ListFieldType::class, $searchFormDTO, [
            'action' => $this->generateUrl('list_fields')
        ]);
        $searchForm->handleRequest($request);

        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $this->request->getSession()->set('list_field/search_form', $searchFormDTO->serialize());
        }

        $this->createListFields($page, $searchFormDTO);
        $this->countByFieldListOptions();

        $this->createControllerFields();
        $menu = new MainMenu('admin_panel');

        $paginator = new Paginator($fieldManager->getCmdResult('amount_fields'), $searchFormDTO->getFieldsPerPage());
        $paginator->setPageNumber($page);

        return $this->render('default/controller/admin/field/field_list.html.twig', array(
            'title' => 'Tomasz Bukowski - Portfolio - Lista wszystkich pól',
            'menu' => $menu->getMenu(),
            'fields' => $fieldManager->getArrayCollection('_controller'),
            'form_search_field' => $searchForm->createView(),
            'list_fields' => $fieldManager->getArrayCollection('list_fields'),
            'pagination' => $paginator
        ));
    }

    private function createControllerFields() : void
    {
        $fieldList = $this->fieldManager->getListOptions();
        $fieldList->clear();
        $fieldList->setControllerName(FieldListController::class);
        $fieldList->setMethodName('indexAction');

        $this->fieldManager->setCommand($this->createControllerCollectionCmd);
        $this->fieldManager->run();
    }

    /**
     * @return SearchFormDTO
     */
    private function getSearchFormDTO() : SearchFormDTO
    {
        $formDTOFactory = $this->fieldManager->getFactory()->getFormDTOFactory();
        $searchFormDTO = $formDTOFactory->createSearchForm();

        $serializedSearchForm = $this->request->getSession()->get('list_field/search_form');
        if (!is_null($serializedSearchForm)) {
            $searchFormDTO->unserialize($serializedSearchForm);
        }

        return $searchFormDTO;
    }

    /**
     * @param int $pageNumber
     * @param SearchFormDTO $searchFormDTO
     */
    private function createListFields(int $pageNumber = 1, SearchFormDTO $searchFormDTO) : void
    {
        if (!is_null($searchFormDTO->getFieldsPerPage())) {
            $fieldsPerPage = $searchFormDTO->getFieldsPerPage();
        } else {
            $fieldsPerPage = 25;
        }

        $offset = ($pageNumber - 1) * $fieldsPerPage;
        $limit = $fieldsPerPage;

        $fieldList = $this->fieldManager->getListOptions();
        $fieldList->clear();

        if (!is_null($searchFormDTO->getController()) && ($searchFormDTO->getController() != 'all_controllers')) {
            $fieldList->setControllerName($searchFormDTO->getController());
        }

        if (!is_null($searchFormDTO->getFieldClass()) && ($searchFormDTO->getFieldClass() != 'all_fields')) {
            $fieldList->setFieldClass($searchFormDTO->getFieldClass());
        }

        $fieldList->setOffset($offset);
        $fieldList->setLimit($limit);
        $fieldList->setSortBy($searchFormDTO->getSortBy());

        if ($searchFormDTO->getSortMethod() == 'ASC') {
            $fieldList->sortASC();
        }
        if ($searchFormDTO->getSortMethod() == 'DESC') {
            $fieldList->sortDESC();
        }

        $this->createFieldCollectionCmd->setCollectionName('list_fields');
        $this->fieldManager->setCommand($this->createFieldCollectionCmd);
        $this->fieldManager->run();
    }


    private function countByFieldListOptions() : void
    {
        $this->fieldManager->setCommand($this->countByFieldListOptionsCmd);
        $this->fieldManager->run();
    }
}
