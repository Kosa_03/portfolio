<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Field\FieldConfig;
use AppBundle\Menu\MainMenu;
use AppBundle\Service\Field\Command\CreateControllerCollectionCmd;
use AppBundle\Service\Field\FieldManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class AdminPanelController
 */
class AdminPanelController extends Controller
{
    private $createCollectionCmd;

    /** @var Request */
    private $request;
    /** @var FieldManager */
    private $fieldManager;

    /**
     * AdminController constructor.
     * @param CreateControllerCollectionCmd $createCollectionCmd
     */
    public function __construct(CreateControllerCollectionCmd $createCollectionCmd)
    {
        $this->createCollectionCmd = $createCollectionCmd;
    }

    /**
     * @Route("/admin/panel", name="admin_panel")
     *
     * @param Request $request
     * @param FieldManager $fieldManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, FieldManager $fieldManager)
    {
        $this->request = $request;
        $this->fieldManager = $fieldManager;

        $this->createControllerFields();
        $menu = new MainMenu($request->get('_route'));

        return $this->render('default/controller/admin/admin_panel.html.twig', array(
            'title' => 'Tomasz Bukowski - Portfolio - Panel administracyjny',
            'menu' => $menu->getMenu(),
            'fields' => $fieldManager->getArrayCollection('_controller')
        ));
    }

    private function createControllerFields() : void
    {
        $fieldList = $this->fieldManager->getListOptions();
        $fieldList->setControllerName(AdminPanelController::class);
        $fieldList->setMethodName('indexAction');

        $fieldOptions = $this->fieldManager->getFieldOptions();
        $fieldOptions->setPatterns([
            'total_fields' => [
                '%total_fields%' => $this->countNumberOfAllFields()
            ]
        ]);

        $this->fieldManager->setCommand($this->createCollectionCmd);
        $this->fieldManager->run();
    }

    /**
     * @return int
     */
    private function countNumberOfAllFields() : int
    {
        $numRows = $this->getDoctrine()->getRepository(FieldConfig::class)
            ->countNumberOfAllFields();

        return $numRows;
    }
}
