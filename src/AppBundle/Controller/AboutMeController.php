<?php

namespace AppBundle\Controller;

use AppBundle\Menu\MainMenu;
use AppBundle\Service\Field\Command\CreateControllerCollectionCmd;
use AppBundle\Service\Field\FieldManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class AboutMeController
 */
class AboutMeController extends Controller
{
    private $createCollectionCmd;

    /** @var Request */
    private $request;
    /** @var FieldManager */
    private $fieldManager;

    /**
     * AboutMeController constructor.
     * @param CreateControllerCollectionCmd $createCollectionCmd
     */
    public function __construct(CreateControllerCollectionCmd $createCollectionCmd)
    {
        $this->createCollectionCmd = $createCollectionCmd;
    }

    /**
     * @Route("/about_me", name="app_about_me")
     *
     * @param Request $request
     * @param FieldManager $fieldManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, FieldManager $fieldManager)
    {
        $this->request = $request;
        $this->fieldManager = $fieldManager;

        $this->createControllerFields();
        $menu = new MainMenu($request->get('_route'));

        return $this->render('default/controller/app_about_me.html.twig', array(
            'title' => 'Tomasz Bukowski - Portfolio - O mnie',
            'menu' => $menu->getMenu(),
            'fields' => $fieldManager->getArrayCollection('_controller')
         ));
    }

    private function createControllerFields() : void
    {
        $fieldList = $this->fieldManager->getListOptions();
        $fieldList->setControllerName(AboutMeController::class);
        $fieldList->setMethodName('indexAction');

        $this->fieldManager->setCommand($this->createCollectionCmd);
        $this->fieldManager->run();
    }
}
