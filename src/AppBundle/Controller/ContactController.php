<?php

namespace AppBundle\Controller;

use AppBundle\Form\ContactType;
use AppBundle\Menu\MainMenu;
use AppBundle\Service\Field\Command\CreateControllerCollectionCmd;
use AppBundle\Service\Field\FieldManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class ContactController
 */
class ContactController extends Controller
{
    private $createCollectionCmd;

    /** @var Request */
    private $request;
    /** @var FieldManager */
    private $fieldManager;

    /**
     * ContactController constructor.
     * @param CreateControllerCollectionCmd $createCollectionCmd
     */
    public function __construct(CreateControllerCollectionCmd $createCollectionCmd)
    {
        $this->createCollectionCmd = $createCollectionCmd;
    }

    /**
     * @Route("/contact", name="app_contact")
     *
     * @param Request $request
     * @param FieldManager $fieldManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, FieldManager $fieldManager)
    {
        $this->request = $request;
        $this->fieldManager = $fieldManager;

        $form = $this->createForm(ContactType::class, null, array(
            'action' => $this->generateUrl('app_contact')
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->sendEmail($form->getData());
        }

        $this->createControllerFields();
        $menu = new MainMenu($request->get('_route'));

        $formField = $fieldManager->getCollection('_controller')->getFormField('app_contact_form');
        if (!is_null($formField)) {
            $formField->setFormView($form->createView());
        }

        return $this->render('default/controller/app_contact.html.twig', array(
            'title' => 'Tomasz Bukowski - Portfolio - Kontakt',
            'menu' => $menu->getMenu(),
            'fields' => $fieldManager->getArrayCollection('_controller')
        ));
    }

    private function createControllerFields() : void
    {
        $fieldList = $this->fieldManager->getListOptions();
        $fieldList->setControllerName(ContactController::class);
        $fieldList->setMethodName('indexAction');

        $this->fieldManager->setCommand($this->createCollectionCmd);
        $this->fieldManager->run();
    }

    /**
     * @param array $data
     */
    private function sendEmail(array $data)
    {
        $message = new \Swift_Message();
        $message->addFrom($data['from'])
            ->addTo('kosa03@kosa03.usermd.net')
            ->setSubject($data['subject'])
            ->setBody($data['message'], 'text/plain')
        ;

        if ($this->get('mailer')->send($message)) {
            $this->addFlash('success', 'Twoja wiadomość została wysłana.');
        }
        else {
            $this->addFlash('danger', 'Wystąpił problem z wysłaniem wiadomości. Przepraszamy.');
        }
    }
}
