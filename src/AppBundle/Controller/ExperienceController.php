<?php

namespace AppBundle\Controller;

use AppBundle\Menu\MainMenu;
use AppBundle\Service\Field\Command\CreateControllerCollectionCmd;
use AppBundle\Service\Field\FieldManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class ExperienceController
 */
class ExperienceController extends Controller
{
    private $createCollectionCmd;

    /** @var Request */
    private $request;
    /** @var FieldManager */
    private $fieldManager;

    /**
     * ExperienceController constructor.
     * @param CreateControllerCollectionCmd $createCollectionCmd
     */
    public function __construct(CreateControllerCollectionCmd $createCollectionCmd)
    {
        $this->createCollectionCmd = $createCollectionCmd;
    }

    /**
     * @Route("/experience", name="app_experience")
     *
     * @param Request $request
     * @param FieldManager $fieldManager
     */
    public function indexAction(Request $request, FieldManager $fieldManager)
    {
        $this->request = $request;
        $this->fieldManager = $fieldManager;

        $this->createControllerFields();
        $menu = new MainMenu($request->get('_route'));

        return $this->render('default/controller/app_experience.html.twig', array(
            'title' => 'Tomasz Bukowski - Portfolio - Doświadczenie',
            'menu' => $menu->getMenu(),
            'fields' => $fieldManager->getArrayCollection('_controller')
        ));
    }

    private function createControllerFields() : void
    {
        $fieldList = $this->fieldManager->getListOptions();
        $fieldList->setControllerName(ExperienceController::class);
        $fieldList->setMethodName('indexAction');

        $this->fieldManager->setCommand($this->createCollectionCmd);
        $this->fieldManager->run();
    }
}
