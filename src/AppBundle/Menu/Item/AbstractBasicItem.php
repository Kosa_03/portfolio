<?php

namespace AppBundle\Menu\Item;


abstract class AbstractBasicItem
{
    private $parent;
    private $role;
    private $onlyForAnonymous;

    /**
     * @return string
     */
    public function getClassName() : string
    {
        return static::class;
    }

    /**
     * @param ItemInterface|null $parent
     */
    public function setParent(?ItemInterface $parent) : void
    {
        $this->parent = $parent;
    }

    public function getParent() : ?ItemInterface
    {
        return $this->parent;
    }

    /**
     * @param string $role
     */
    public function setRole(string $role) : void
    {
        $this->role = $role;
    }

    public function getRole() : string
    {
        return $this->role;
    }

    /**
     * Set to true if item should be shown only for anonymous users.
     *
     * @param bool $onlyForAnonymous
     */
    public function setOnlyForAnonymous(bool $onlyForAnonymous = false) : void
    {
        $this->onlyForAnonymous = $onlyForAnonymous;
    }

    public function getOnlyForAnonymous() : bool
    {
        return $this->onlyForAnonymous;
    }
}
