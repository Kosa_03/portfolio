<?php

namespace AppBundle\Menu\Item;


class DropdownItem extends AbstractItem implements ItemInterface
{
    private $items = [];

    /**
     * DropdownItem constructor.
     *
     * @param array $options
     * @param string $name
     */
    public function __construct(string $name, array $options)
    {
        if (array_key_exists('label', $options)) {
            $this->setLabel($options['label']);
        } else {
            throw new \InvalidArgumentException('Property \'label\' in DropdownItem \'' . $name . '\' is required.');
        }

        $this->setUri(null);

        if (array_key_exists('role', $options)) {
            $this->setRole($options['role']);
        } else {
            $this->setRole('IS_AUTHENTICATED_ANONYMOUSLY');
        }

        if (array_key_exists('only_for_anonymous', $options)) {
            $this->setOnlyForAnonymous($options['only_for_anonymous']);
        } else {
            $this->setOnlyForAnonymous(false);
        }

        if (array_key_exists('active', $options)) {
            $this->setActive($options['active']);
        } else {
            $this->setActive(false);
        }

        if (array_key_exists('items', $options)) {
            $this->addItems($options['items']);
        } else {
            throw new \InvalidArgumentException('Property \'items\' in DropdownItem \'' . $name . '\' is required.');
        }
    }

    /**
     * @param array $items
     */
    private function addItems(array $items) : void
    {
        foreach ($items as $itemName => $itemObject) {
            if (array_key_exists($itemName, $this->items)) {
                throw new \InvalidArgumentException('MenuItem \'' . $itemName . '\' already exist in DropdownItem.');
            }

            $itemObject->setParent($this);
            $this->items[$itemName] = $itemObject;
        }
    }

    public function getItems() : array
    {
        return $this->items;
    }
}
