<?php

namespace AppBundle\Menu\Item;


interface ItemInterface
{
    public function getClassName() : string;
    public function setLabel(array $label) : void;
    public function getLabel() : array;
    public function setUri(?string $uri) : void;
    public function getUri() : ?string;
    public function setParent(?ItemInterface $parent) : void;
    public function getParent() : ?ItemInterface;
    public function setRole(string $role) : void;
    public function getRole() : string;
    public function setOnlyForAnonymous(bool $onlyForAnonymous) : void;
    public function getOnlyForAnonymous() : bool;
    public function setActive(bool $active) : void;
    public function getActive() : bool;
}
