<?php

namespace AppBundle\Menu\Item;


class MenuItem extends AbstractItem implements ItemInterface
{
    /**
     * MenuItem constructor.
     *
     * @param array $options
     * @param string $name
     */
    public function __construct(string $name, array $options)
    {
        if (array_key_exists('label', $options)) {
            $this->setLabel($options['label']);
        } else {
            throw new \InvalidArgumentException('Property \'label\' in MenuItem \'' . $name . '\' is required.');
        }

        if (array_key_exists('uri', $options)) {
            $this->setUri($options['uri']);
        } else {
            throw new \InvalidArgumentException('Property \'uri\' in MenuItem \'' . $name . '\' is required.');
        }

        if (array_key_exists('role', $options)) {
            $this->setRole($options['role']);
        } else {
            $this->setRole('IS_AUTHENTICATED_ANONYMOUSLY');
        }

        if (array_key_exists('only_for_anonymous', $options)) {
            $this->setOnlyForAnonymous($options['only_for_anonymous']);
        } else {
            $this->setOnlyForAnonymous(false);
        }

        if (array_key_exists('active', $options)) {
            $this->setActive($options['active']);
        } else {
            $this->setActive(false);
        }
    }
}
