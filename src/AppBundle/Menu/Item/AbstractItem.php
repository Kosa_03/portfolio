<?php

namespace AppBundle\Menu\Item;


abstract class AbstractItem extends AbstractBasicItem
{
    private $label;
    private $uri;
    private $active;

    /**
     * @param array $label
     */
    public function setLabel(array $label) : void
    {
        $this->label = $label;
    }

    public function getLabel() : array
    {
        return $this->label;
    }

    /**
     * @param string|null $uri
     */
    public function setUri(?string $uri) : void
    {
        $this->uri = $uri;
    }

    public function getUri() : ?string
    {
        return $this->uri;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active) : void
    {
        $this->active = $active;
    }

    public function getActive() : bool
    {
        return $this->active;
    }
}
