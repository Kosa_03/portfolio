<?php

namespace AppBundle\Menu\Item;


interface BasicItemInterface
{
    public function getClassName() : string;
    public function setParent(?ItemInterface $parent) : void;
    public function getParent() : ?ItemInterface;
    public function setRole(string $role) : void;
    public function getRole() : string;
    public function setOnlyForAnonymous(bool $onlyForAnonymous) : void;
    public function getOnlyForAnonymous() : bool;
}
