<?php

namespace AppBundle\Menu\Item;


class DropdownDivider extends AbstractBasicItem implements BasicItemInterface
{
    /**
     * DropdownDivider constructor.
     *
     * @param array $options
     */
    public function __construct(array $options)
    {
        if (array_key_exists('role', $options)) {
            $this->setRole($options['role']);
        } else {
            $this->setRole('IS_AUTHENTICATED_ANONYMOUSLY');
        }

        if (array_key_exists('only_for_anonymous', $options)) {
            $this->setOnlyForAnonymous($options['only_for_anonymous']);
        } else {
            $this->setOnlyForAnonymous(false);
        }
    }
}
