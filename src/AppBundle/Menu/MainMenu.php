<?php

namespace AppBundle\Menu;

use AppBundle\Menu\Item\DropdownDivider;
use AppBundle\Menu\Item\DropdownItem;
use AppBundle\Menu\Item\MenuItem;


class MainMenu implements MenuInterface
{
    private $menu;
    private $submenu = [];

    /**
     * MainMenu constructor.
     *
     * @param string $currentUriName
     */
    public function __construct(string $currentUriName)
    {
        $this->submenu['fields'] = new MenuBuilder();
        $this->submenu['fields']
            ->addItem('add_field', MenuItem::class, array(
                'label' => array(
                    'en' => 'New Field',
                    'pl' => 'Nowe pole'
                ),
                'uri' => 'add_field',
                'role' => 'ROLE_ADMIN'
            ))
            ->addDivider('divider', DropdownDivider::class, array(
                'role' => 'ROLE_ADMIN'
            ))
            ->addItem('fields_list', MenuItem::class, array(
                'label' => array(
                    'en' => 'Fields list',
                    'pl' => 'Lista pól'
                ),
                'uri' => 'list_fields',
                'role' => 'ROLE_ADMIN'
            ))
        ;

        $this->submenu['account'] = new MenuBuilder();
        $this->submenu['account']
            ->addItem('profile', MenuItem::class, array(
                'label' => array(
                    'en' => 'My account',
                    'pl' => 'Moje konto'
                ),
                'uri' => 'app_my_account',
                'role' => 'IS_AUTHENTICATED_FULLY'
            ))
            ->addDivider('divider_1', DropdownDivider::class, array(
                'role' => 'IS_AUTHENTICATED_FULLY'
            ))
            ->addItem('admin_panel', MenuItem::class, array(
                'label' => array(
                    'en' => 'Admin panel',
                    'pl' => 'Panel administracyjny'
                ),
                'uri' => 'admin_panel',
                'role' => 'ROLE_ADMIN'
            ))
            // TODO: To hide this element
//            ->addItem('fields_admin', DropdownItem::class, array(
//                'label' => array(
//                    'en' => 'Fields',
//                    'pl' => 'Fields'
//                ),
//                'role' => 'ROLE_ADMIN',
//                'items' => $this->submenu['fields']->getItems()
//            ))
            ->addDivider('divider_2', DropdownDivider::class, array(
                'role' => 'ROLE_ADMIN'
            ))
            ->addItem('logout', MenuItem::class, array(
                'label' => array(
                    'en' => 'Sign Out',
                    'pl' => 'Wyloguj'
                ),
                'uri' => 'app_logout',
                'role' => 'IS_AUTHENTICATED_FULLY'
            ))
        ;

        $this->submenu['blog'] = new MenuBuilder();
        $this->submenu['blog']
            ->addItem('add_blog_article', MenuItem::class, array(
                'label' => array(
                    'en' => 'New article',
                    'pl' => 'Nowy artykuł'
                ),
                'uri' => 'admin_blog_article_add',
                'role' => 'ROLE_ADMIN'
            ))
            ->addDivider('divider', DropdownDivider::class, array(
                'role' => 'ROLE_ADMIN'
            ))
            ->addItem('blog_article_list', MenuItem::class, array(
                'label' => array(
                    'en' => 'Article list',
                    'pl' => 'Lista artykułów'
                ),
                'uri' => 'admin_blog_article_list',
                'role' => 'ROLE_ADMIN'
            ))
            ->addItem('blog_tag_list', MenuItem::class, array(
                'label' => array(
                    'en' => 'Article tags list',
                    'pl' => 'Lista tagów bloga'
                ),
                'uri' => 'admin_blog_tag_list',
                'role' => 'ROLE_ADMIN'
            ))
        ;

        $this->menu = new MenuBuilder();
        $this->menu
            ->addItem('app_home', MenuItem::class, array(
                'label' => array(
                    'en' => 'Home',
                    'pl' => 'Home'
                ),
                'uri' => 'app_home'
            ))
            ->addItem('app_blog_article_list', MenuItem::class, array(
                'label' => array(
                    'en' => 'Blog',
                    'pl' => 'Blog'
                ),
                'uri' => 'app_blog_article_list'
            ))
            // TODO: To hide this element
//            ->addItem('blog_admin', DropdownItem::class, array(
//                'label' => array(
//                    'en' => 'Blog admin',
//                    'pl' => 'Blog admin'
//                ),
//                'role' => 'ROLE_ADMIN',
//                'items' => $this->submenu['blog']->getItems()
//            ))
            ->addItem('app_about_me', MenuItem::class, array(
                'label' => array(
                    'en' => 'About me',
                    'pl' => 'O mnie'
                ),
                'uri' => 'app_about_me'
            ))
            ->addItem('app_experience', MenuItem::class, [
                'label' => [
                    'en' => 'Experience',
                    'pl' => 'Doświadczenie'
                ],
                'uri' => 'app_experience'
            ])
            ->addItem('app_contact', MenuItem::class, array(
                'label' => array(
                    'en' => 'Contact',
                    'pl' => 'Kontakt'
                ),
                'uri' => 'app_contact'
            ))
            ->addItem('my_account', DropdownItem::class, array(
                'label' => array(
                    'en' => 'Account',
                    'pl' => 'Moje Konto'
                ),
                'role' => 'IS_AUTHENTICATED_FULLY',
                'items' => $this->submenu['account']->getItems()
            ))
//            ->addItem('app_login', MenuItem::class, array(
//                'label' => array(
//                    'en' => 'Sign In',
//                    'pl' => 'Zaloguj'
//                ),
//                'uri' => 'app_login',
//                'only_for_anonymous' => true
//            ))
            ->setActiveItems($currentUriName)
        ;
    }

    /**
     * @return MenuBuilder
     */
    public function getMenu() : MenuBuilder
    {
        return $this->menu;
    }
}
