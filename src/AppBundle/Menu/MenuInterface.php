<?php

namespace AppBundle\Menu;


interface MenuInterface
{
    public function getMenu() : MenuBuilder;
}
