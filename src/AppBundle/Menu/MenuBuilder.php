<?php

namespace AppBundle\Menu;

use AppBundle\Menu\Item\BasicItemInterface;
use AppBundle\Menu\Item\ItemInterface;
use AppBundle\Menu\Item\MenuItem;


class MenuBuilder
{
    private $items;
    private $mobileItem;

    public function __construct()
    {
        $this->items = [];
    }

    /**
     * @param string $itemName
     * @param string $itemType
     * @param array $itemOptions
     * @return MenuBuilder
     */
    public function addItem(string $itemName, string $itemType, array $itemOptions) : MenuBuilder
    {
        if (!in_array(ItemInterface::class, class_implements($itemType))) {
            throw new \BadFunctionCallException('Class \'' . $itemType . '\' is not an instance of \'' . ItemInterface::class . '\'');
        }

        if (!array_key_exists($itemName, $this->items)) {
            $itemObject = new $itemType($itemName, $itemOptions);
            $itemObject->setParent(null);

            $this->items[$itemName] = $itemObject;
        } else {
            throw new \InvalidArgumentException('MenuItem \'' . $itemName . '\' already exist in MenuBuilder.');
        }

        return $this;
    }

    /**
     * @param string $itemName
     * @param string $itemType
     * @param array $itemOptions
     * @return MenuBuilder
     */
    public function addDivider(string $itemName, string $itemType, array $itemOptions) : MenuBuilder
    {
        if (!in_array(BasicItemInterface::class, class_implements($itemType))) {
            throw new \BadFunctionCallException('Class \'' . $itemType . '\' is not an instance of \'' . BasicItemInterface::class . '\'');
        }

        if (!array_key_exists($itemName, $this->items)) {
            $itemObject = new $itemType($itemOptions);
            $itemObject->setParent(null);

            $this->items[$itemName] = $itemObject;
        } else {
            throw new \InvalidArgumentException('MenuItem \'' . $itemName . '\' already exist in MenuBuilder.');
        }

        return $this;
    }

    /**
     * @param string $currentUriName
     * @param array|null $items
     * @return MenuBuilder
     */
    public function setActiveItems(string $currentUriName, array $items = null) : MenuBuilder
    {
        if ($items == null) {
            $items = $this->getItems();
        }

        foreach ($items as $itemType) {
            if (method_exists($itemType, 'getItems')) {
                $this->setActiveItems($currentUriName, $itemType->getItems());
            }

            if (in_array(ItemInterface::class, class_implements($itemType))) {
                if ($currentUriName == $itemType->getUri()) {
                    $itemType->setActive(true);
                    $this->setActiveParents($itemType);
                }
            }
        }

        return $this;
    }

    /**
     * @param ItemInterface $itemType
     */
    private function setActiveParents(ItemInterface $itemType) : void
    {
        $parentType = $itemType->getParent();

        if ($parentType != null) {
            $parentType->setActive(true);

            $this->setActiveParents($parentType);
        }
    }

    public function getItems() : array
    {
        return $this->items;
    }

    /**
     * @param array $options
     * @return MenuBuilder
     */
    public function setMobileItem(array $options) : MenuBuilder
    {
        $this->mobileItem = new MenuItem('_mobile_item', $options);

        return $this;
    }

    public function getMobileItem() : MenuItem
    {
        return $this->mobileItem;
    }
}
