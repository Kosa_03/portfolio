$(document).ready( function() {
    var customFileLabel = '.custom-file-label';
    var customFileInput = '.custom-file-input';

    $(customFileInput).on("change", function(e) {
        var fileName = e.target.files[0].name;

        var parent = $( this ).parent();
        parent.find(customFileLabel).text(fileName);
    });
});
