$( document ).ready( function() {

    $.changeMainMenuPositioningToDekstop = function() {
        var oMainMenu = $('.app-navbar-main-menu');

        oMainMenu.removeClass('mobile-top');
        oMainMenu.addClass('fixed-top');
    };

    $.changeMainMenuPositioningToMobile = function() {
        var oMainMenu = $('.app-navbar-main-menu');

        oMainMenu.removeClass('fixed-top');
        oMainMenu.addClass('mobile-top');
    };

    $.bIsMenuActive = function () {
        var oRootMenuItems = $('#appMainMenu > ul').children('li.dropdown.menu-show');

        if (oRootMenuItems.length > 0) {
            return true;
        }

        return false;
    };

    $.iCountBreakPoint = function () {
        var oNavBarMenu = $('.app-navbar-main-menu');
        var iScreenHeight = $( window ).height();
        var iMainMenuHeight = oNavBarMenu.innerHeight();

        return (iScreenHeight - iMainMenuHeight);
    };

    $.changeMenuBackground = function (sColorName, bWithChildrens) {
        var oRootMenu = $('.app-navbar-main-menu');

        oRootMenu.removeClass('bg-light-black');
        oRootMenu.removeClass('bg-medium-black');
        oRootMenu.removeClass('bg-dark-black');

        oRootMenu.addClass(sColorName);

        if (bWithChildrens) {
            oRootMenu.find('.dropdown-menu').removeClass('bg-light-black');
            oRootMenu.find('.dropdown-menu').removeClass('bg-medium-black');
            oRootMenu.find('.dropdown-menu').removeClass('bg-dark-black');

            oRootMenu.find('.dropdown-menu').addClass(sColorName);
        }
    };
});
