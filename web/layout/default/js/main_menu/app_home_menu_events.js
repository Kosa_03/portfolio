$( document ).ready( function() {

    if ($.bIsDesktopScreen()) {
        var iScrollTop = $( window ).scrollTop();
        var iBreakPoint = $.iCountBreakPoint();

        if (iScrollTop > iBreakPoint) {
            $.changeMenuBackground('bg-dark-black', true);
        }

        if (iScrollTop <= iBreakPoint) {
            $.changeMenuBackground('bg-light-black', true);
        }
    }

    $('.app-navbar-main-menu .dropdown-toggle').on('click', function(event) {
        var iScrollTop = $( window ).scrollTop();
        var iBreakPoint = $.iCountBreakPoint();

        if ($.bIsDesktopScreen()) {
            if (iScrollTop <= iBreakPoint) {
                if ($.bIsMenuActive()) {
                    $.changeMenuBackground('bg-medium-black', true);

                } else {
                    $.changeMenuBackground('bg-light-black', true);
                }
            }
        }

        event.preventDefault();
    });

    $( document ).on('click', function(event) {
        var iScrollTop = $( window ).scrollTop();
        var iBreakPoint = $.iCountBreakPoint();
        var oParents = $(event.target).parents('.app-navbar-main-menu');

        if (oParents.length === 0) {
            if ($.bIsDesktopScreen() && (iScrollTop <= iBreakPoint)) {
                $.changeMenuBackground('bg-light-black', true);
            }
        }
    });

    $( window ).on('scroll', function() {
        var iScrollTop = $( window ).scrollTop();
        var iBreakPoint = $.iCountBreakPoint();

        if ($.bIsDesktopScreen()) {
            if (iScrollTop > iBreakPoint) {
                $.changeMenuBackground('bg-dark-black', true);
            }

            if (iScrollTop <= iBreakPoint) {
                if ($.bIsMenuActive()) {
                    $.changeMenuBackground('bg-medium-black', true);

                } else {
                    $.changeMenuBackground('bg-light-black', true);
                }
            }
        }
    });
});
