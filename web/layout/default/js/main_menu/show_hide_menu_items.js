$( document ).ready( function () {

    $.bIsDesktopScreen = function () {
        var iWindowWidth = $( window ).width();

        if (iWindowWidth > 575) {
            return true;
        }

        return false;
    };

    $.bIsMobileScreen = function () {
        var iWindowWidth = $( window ).width();

        if (iWindowWidth <= 575) {
            return true;
        }

        return false;
    };


    $.changeToggleIcon = function (oMenuHandler, sToggleClass, bWithChildren) {
        var oMenuTrigger = oMenuHandler.children('a');
        var oMenuContainer = oMenuHandler.children('ul');

        oMenuTrigger.removeClass('dropup-toggle');
        oMenuTrigger.removeClass('dropleft-toggle');

        if ($.bIsDesktopScreen()) {
            if (!oMenuHandler.hasClass('nav-item')) {
                oMenuTrigger.removeClass('dropdown-toggle');
                oMenuTrigger.addClass(sToggleClass);
            }
        }

        if ($.bIsMobileScreen()) {
            oMenuTrigger.addClass(sToggleClass);
        }

        if (bWithChildren) {
            oMenuContainer.find('.dropup-toggle').addClass(sToggleClass);
            oMenuContainer.find('.dropleft-toggle').addClass(sToggleClass);
            oMenuContainer.find('.dropdown-toggle').addClass(sToggleClass);

            if (sToggleClass === 'dropup-toggle') {
                oMenuContainer.find('.dropleft-toggle').removeClass('dropleft-toggle');
                oMenuContainer.find('.dropdown-toggle').removeClass('dropdown-toggle');
            }

            if (sToggleClass === 'dropleft-toggle') {
                oMenuContainer.find('.dropup-toggle').removeClass('dropup-toggle');
                oMenuContainer.find('.dropdown-toggle').removeClass('dropdown-toggle');
            }

            if (sToggleClass === 'dropdown-toggle') {
                oMenuContainer.find('.dropup-toggle').removeClass('dropup-toggle');
                oMenuContainer.find('.dropleft-toggle').removeClass('dropleft-toggle');
            }
        }
    };


    $.addShowingClass = function (oMenuHandler) {
        oMenuHandler.addClass('showing');
        oMenuHandler.parents('li').addClass('showing');
    };

    $.removeShowingClass = function (oMenuHandler) {
        oMenuHandler.removeClass('showing');
        oMenuHandler.parents('li').removeClass('showing');
    };


    $.addMenuShowClass = function (oMenuHandler, oMenuContainer) {
        oMenuHandler.addClass('menu-show');
        oMenuContainer.addClass('menu-show');
    };

    $.removeMenuShowClass = function (oMenuHandler, oMenuContainer) {
        oMenuHandler.removeClass('menu-show');
        oMenuContainer.removeClass('menu-show');
        oMenuContainer.find('.menu-show').removeClass('menu-show');
    };


    $.hideSubmenuOnDesktop = function (oMenuHandler, oMenuContainer) {
        $.changeToggleIcon(oMenuHandler, 'dropdown-toggle', true);
        oMenuContainer.find('.dropdown-submenu').slideUp();
    };


    $.showMenuOnDesktop = function (oMenuHandler, oMenuTrigger, oMenuContainer) {
        $.changeToggleIcon(oMenuHandler, 'dropleft-toggle', false);
        oMenuContainer.slideDown();
    };

    $.hideMenuOnDesktop = function (oMenuHandler, oMenuTrigger, oMenuContainer) {
        $.changeToggleIcon(oMenuHandler, 'dropdown-toggle', false);
        oMenuContainer.slideUp();
    };


    $.disactiveMenusOnDesktop = function () {
        var oRootMenuItems = $('#appMainMenu > ul').children('li.dropdown:not(.showing)');

        oRootMenuItems.each( function () {
            var oRootMenuItem = $(this);
            var oRootMenuTrigger = $(this).children('a');
            var oRootMenuContainer = oRootMenuItem.children('ul');

            $.hideSubmenuOnDesktop(oRootMenuItem, oRootMenuContainer);
            $.hideMenuOnDesktop(oRootMenuItem, oRootMenuTrigger, oRootMenuContainer);
            $.removeMenuShowClass(oRootMenuItem, oRootMenuContainer);
        });
    };


    $.showMenuOnMobile = function (oMenuHandler, oMenuContainer, oMenuParentContainer) {
        var oParentItems = oMenuParentContainer.children('li:not(.showing)');

        $.changeToggleIcon(oMenuHandler, 'dropup-toggle', false);

        oMenuContainer.slideDown();
        oParentItems.slideUp();
    };

    $.hideMenuOnMobile = function (oMenuHandler, oMenuContainer, oMenuParentContainer) {
        var oParentItems = oMenuParentContainer.children('li:not(.showing)');

        $.changeToggleIcon(oMenuHandler, 'dropdown-toggle', false);

        oMenuContainer.slideUp();
        oParentItems.slideDown();
    };
});
