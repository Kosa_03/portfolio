$( document ).ready( function() {

    if ($.bIsMobileScreen()) {
        $.changeMainMenuPositioningToMobile();
    }

    $('.app-navbar-main-menu .dropdown-toggle').on('click', function(event) {
        var oMenuHandler = $(this).parent('li');
        var oMenuTrigger = oMenuHandler.children('a');
        var oMenuContainer = oMenuHandler.children('ul');
        var oMenuParentContainer = oMenuHandler.parent('ul');

        if ($.bIsMobileScreen()) {
            if (oMenuContainer.is(':visible')) {
                $.addShowingClass(oMenuHandler);
                $.hideMenuOnMobile(oMenuHandler, oMenuContainer, oMenuParentContainer);
                $.removeShowingClass(oMenuHandler);
                $.removeMenuShowClass(oMenuHandler, oMenuContainer);

            } else {
                $.addShowingClass(oMenuHandler);
                $.addMenuShowClass(oMenuHandler, oMenuContainer);
                $.showMenuOnMobile(oMenuHandler, oMenuContainer, oMenuParentContainer);
                $.removeShowingClass(oMenuHandler);
            }
        }

        if ($.bIsDesktopScreen()) {
            if (oMenuContainer.is(':visible')) {
                $.hideSubmenuOnDesktop(oMenuHandler, oMenuContainer);
                $.hideMenuOnDesktop(oMenuHandler, oMenuTrigger, oMenuContainer);
                $.removeMenuShowClass(oMenuHandler, oMenuContainer);

            } else {
                $.addShowingClass(oMenuHandler);
                $.addMenuShowClass(oMenuHandler, oMenuContainer);
                $.disactiveMenusOnDesktop();
                $.showMenuOnDesktop(oMenuHandler, oMenuTrigger, oMenuContainer);
                $.removeShowingClass(oMenuHandler);
            }
        }

        event.preventDefault();
    });

    $( document ).on('click', function(event) {
        var oParents = $(event.target).parents('.app-navbar-main-menu');

        if (oParents.length === 0) {
            if ($.bIsMobileScreen()) {
                var oNavBarCollapse = $('.app-navbar-main-menu').children('.navbar-collapse');
                var oToggleButton = $('.app-navbar-main-menu').children('button');

                if (oToggleButton.attr('aria-expanded') === 'true') {
                    oNavBarCollapse.collapse('hide');
                }
            }

            if ($.bIsDesktopScreen()) {
                $.disactiveMenusOnDesktop();
            }
        }
    });
});

$( window ).on('resize', function() {
    if ($.bIsMobileScreen()) {
        $.changeMainMenuPositioningToMobile();
    }

    if ($.bIsDesktopScreen()) {
        $.changeMainMenuPositioningToDekstop();
    }
});
