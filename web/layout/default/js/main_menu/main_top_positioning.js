$( document ).ready( function() {

    var oNavBarMenu = $('.app-navbar-main-menu');
    var iMainMenuHeight = oNavBarMenu.innerHeight();

    $('main').css('margin-top', iMainMenuHeight);
});

$( window ).on('resize', function() {
    var oNavBarMenu = $('.app-navbar-main-menu');
    var iMainMenuHeight = oNavBarMenu.innerHeight();

    $('main').css('margin-top', iMainMenuHeight);
});
